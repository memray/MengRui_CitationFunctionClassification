# -*- encoding: utf-8 -*-
__author__ = 'Memray'

import os
import grid_macrof
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


class FeatureSetName:
    AbuJbara = 'AbuJbara'
    Charles = 'Charles'
    Dong = 'Dong'
    Our = 'Our'

# the root path of the project directory
ProjectBasePath = os.path.dirname(os.getcwdu()) + os.sep
# the argument template of grid.py
argvTemplate = 'grid_macrof.py -log2c 0,14,0.5 -log2g -18,-0,0.5 -v 10 ' \
               + '-svmtrain ' + ProjectBasePath + 'docs\\svm-train-macrof.exe ' \
               + '-gnuplot ' \
               + ProjectBasePath \
               + '\\svm\\gnuplot\\bin\\pgnuplot.exe {0}'

'''
    Run grid.py on specific .libsvm file
'''


def run_single_file(file):
    print str(argvTemplate).format(file)
    grid_macrof.run(argvTemplate.format(file).split())


"""
    Run grid on all the .libsvm files in specific directory
    Args:
        dirpath : The directory contains the files to be grid
"""


def run_directory(dir_path):
    for file in os.listdir(dir_path):
        if not file.endswith('libsvm'):
            continue
        if file.endswith('_all_vector.libsvm'):
            continue

        print 'run ' + os.path.join(dir_path, file)
        print argvTemplate.format(os.path.join(dir_path, file))
        grid_macrof.run(argvTemplate.format(os.path.join(dir_path, file)).split())


if __name__ == '__main__':

    # Run single file
    file_path = ProjectBasePath + 'docs\\BaseLine2\\BaseLine2_all_vector.libsvm'
    # print file_path
    run_single_file(file_path)

    # Run directory
    # dir_path = ProjectBasePath + 'docs\\BaseLine2\\'
    # run_directory(dir_path)

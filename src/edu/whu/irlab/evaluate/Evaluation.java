package edu.whu.irlab.evaluate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.io.FileUtils;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;

public class Evaluation {
	class Category{
		String classLabel = "";
		String classNumber = "";
		int totalNumber = 0;
		int rightNumber = 0;
		int wrongNumber = 0;
		public String toString(){
			return "类别"+classNumber+" "+classLabel+":\t"+ (float)rightNumber/(float)totalNumber * 100+"%\n"+
		"\t\t共 "+totalNumber+" 例，正确 "+rightNumber+" 例,错误 "+wrongNumber+" 例";
		}
		
	}
	
	public void evaluate(String predictFile){
		HashSet<String> filter = new HashSet<String>();
/*		filter.add("Basis");
		filter.add("Idea");
		filter.add("Compare");
		filter.add("GRelated");
		filter.add("SRelated");*/
		
		ArrayList<DataPoint> dataPoints = FeatureCacheUtils.readDataPoints(filter);
		Hashtable<String, Category> categoryMap = new Hashtable<String, Category>();
		for(DataPoint dp : dataPoints)
			if(categoryMap.get(dp.getClassLabelNumber())==null){
				Category category = new Category();
				category.classLabel = dp.getLabel2();
				category.classNumber = dp.getClassLabelNumber();
				categoryMap.put(dp.getClassLabelNumber(), category);
			}
			
		
		
		List<String> predictList = null;
		try {
			predictList = FileUtils.readLines(new File(predictFile), "utf8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//System.out.println(dataPoints.size());
		//System.out.println(predictList.size());
		DataPoint dp = new DataPoint();
		String classNumber = "";
		for(int i = 0 ; i < dataPoints.size(); i++){
			dp = dataPoints.get(i);
			classNumber = dp.getClassLabelNumber();
			categoryMap.get(classNumber).totalNumber++;
			if(predictList.get(i).equals(classNumber))
				categoryMap.get(classNumber).rightNumber++;
			else
				categoryMap.get(classNumber).wrongNumber++;
		}
		
		double accuracySum = 0;
		for(Category category:categoryMap.values()){
			System.out.println(category.toString());
			accuracySum += (float)category.rightNumber/(float)category.totalNumber;
		}
		System.out.println("Macro-F\t:\t"+accuracySum/(float)categoryMap.size());
	}
	
	public static void main(String[] args) {
		//new Evaluation().evaluate("output/our_feature.predict");
		new Evaluation().evaluate("output/dfki_feature.predict");
	}
}

package edu.whu.irlab.bean;

import edu.stanford.nlp.trees.TreeGraphNode;

public class AbstractEntity {
	TreeGraphNode NPNode;
	String NPString;
	TreeGraphNode VerbNode;
	String VerbString;
	
	public TreeGraphNode getNPNode() {
		return NPNode;
	}
	public void setNPNode(TreeGraphNode nPNode) {
		NPNode = nPNode;
	}
	public TreeGraphNode getVerbNode() {
		return VerbNode;
	}
	public void setVerbNode(TreeGraphNode verbNode) {
		VerbNode = verbNode;
	}
	public String getNPString() {
		return NPString;
	}
	public void setNPString(String nPString) {
		NPString = nPString;
	}
	public String getVerbString() {
		return VerbString;
	}
	public void setVerbString(String verbString) {
		VerbString = verbString;
	}
	
	
}

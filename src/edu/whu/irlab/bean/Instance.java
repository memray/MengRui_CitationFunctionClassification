package edu.whu.irlab.bean;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by Memray on 2015/3/15.
 */
public class Instance {
    //记录该instance所属的类别号
    public Integer m_ClassNumber;
    //记录属性的列表
    public ArrayList<Attribute> m_Attributes;
    //记录输出特征向量的具体特征值，由于特征向量十分稀疏，因此我们使用TreeMap来记录，key即特征向量的index
    public TreeMap<Integer, Double> m_Values = new TreeMap<>();

    public Instance(ArrayList<Attribute> attributeList, int m_ClassNumber) {
        this.m_Attributes = attributeList;
        this.m_ClassNumber = m_ClassNumber;
    }

    public Instance(int m_ClassNumber) {
        this.m_ClassNumber = m_ClassNumber;
    }

    public ArrayList<Attribute> getAttributes() {
        return m_Attributes;
    }

    public void setAttributes(ArrayList<Attribute> m_Attributes) {
        this.m_Attributes = m_Attributes;
    }

    public TreeMap<Integer, Double> getValues() {
        return m_Values;
    }

    public void setValues(TreeMap<Integer, Double> m_Values) {
        this.m_Values = m_Values;
    }

    public Integer getClassNumber() {
        return m_ClassNumber;
    }

    public void setClassNumber(Integer m_ClassNumber) {
        this.m_ClassNumber = m_ClassNumber;
    }
}

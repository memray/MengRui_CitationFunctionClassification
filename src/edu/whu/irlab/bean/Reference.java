package edu.whu.irlab.bean;

import edu.whu.irlab.dfki.feature.ReferenceFeature.ReferenceFeatureTemplate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lxb on 2015/4/20.
 * reference类，存放着reference级别的基本信息
 */
public class Reference {
    //确认的标签
    private String classLabelNumber;
    //labels 它所有citation的标签
    private List<String> labels;
    //文章的aclid
    private String aclid;
    //引文的年份
    private String year;
    //引文的作者
    private String author;
    //引文的citation 拼接后的 String
    private String sentenceContext;
    //引文的所有citation信息
    private List<Citation> citations;
    //引文对应的引文的特征模板
    private ReferenceFeatureTemplate referenceFeatureTemplate;

    //不重复的dataponints
    private List<DataPoint> norepeatDatapoints = new ArrayList<DataPoint>();

    public String getAclid() {
        return aclid;
    }

    public void setAclid(String aclid) {
        this.aclid = aclid;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSentenceContext() {
        return sentenceContext;
    }

    public void setSentenceContext(String sentenceContext) {
        this.sentenceContext = sentenceContext;
    }

    public List<Citation> getCitations() {
        return citations;
    }

    public void setCitations(List<Citation> citations) {
        this.citations = citations;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String getClassLabelNumber() {
        return classLabelNumber;
    }

    public void setClassLabelNumber(String classLabelNumber) {
        this.classLabelNumber = classLabelNumber;
    }

    public ReferenceFeatureTemplate getReferenceFeatureTemplate() {
        return referenceFeatureTemplate;
    }

    public void setReferenceFeatureTemplate(ReferenceFeatureTemplate referenceFeatureTemplate) {
        this.referenceFeatureTemplate = referenceFeatureTemplate;
    }

    public List<DataPoint> getNorepeatDatapoints() {
        return norepeatDatapoints;
    }

    public void setNorepeatDatapoints(List<DataPoint> norepeatDatapoints) {
        this.norepeatDatapoints = norepeatDatapoints;
    }
}

package edu.whu.irlab.bean;

/**
 * Article entity.
 */
public class AclArticle {
    // Fields
    private Long id;
    private String aclid;
    private String title;
    private String author;
    private String pdfurl;
    private String authorAffiliation;
    private String abstract_;
    private String keyword;
    private String journal;
    private String location;
    private String year;
    private String publisher;
    private String url;
    private String issue;
    private String volume;
    private String page;
    private String clc;
    private String mtc;
    private String yearOnline;
    private String fund;
    private Integer citedFrequency;
    private Integer downloadFrequency;
    private Short type;
    private String doi;
    private String issn;
    private String subject;
    private String venue;

    // Constructors

    /**
     * default constructor
     */
    public AclArticle() {
    }

    // Property accessors

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAclid() {
        return aclid;
    }

    public void setAclid(String aclID) {
        this.aclid = aclID;
    }

    public String getPdfurl() {
        return pdfurl;
    }

    public void setPdfurl(String pdfurl) {
        this.pdfurl = pdfurl;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorAffiliation() {
        return this.authorAffiliation;
    }

    public void setAuthorAffiliation(String authorAffiliation) {
        this.authorAffiliation = authorAffiliation;
    }

    public String getAbstract_() {
        return this.abstract_;
    }

    public void setAbstract_(String abstract_) {
        this.abstract_ = abstract_;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getJournal() {
        return this.journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getYear() {
        return this.year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIssue() {
        return this.issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getVolume() {
        return this.volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getPage() {
        return this.page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getClc() {
        return this.clc;
    }

    public void setClc(String clc) {
        this.clc = clc;
    }

    public String getMtc() {
        return this.mtc;
    }

    public void setMtc(String mtc) {
        this.mtc = mtc;
    }

    public String getYearOnline() {
        return this.yearOnline;
    }

    public void setYearOnline(String yearOnline) {
        this.yearOnline = yearOnline;
    }

    public String getFund() {
        return this.fund;
    }

    public void setFund(String fund) {
        this.fund = fund;
    }

    public Integer getCitedFrequency() {
        return this.citedFrequency;
    }

    public void setCitedFrequency(Integer citedFrequency) {
        this.citedFrequency = citedFrequency;
    }

    public Integer getDownloadFrequency() {
        return this.downloadFrequency;
    }

    public void setDownloadFrequency(Integer downloadFrequency) {
        this.downloadFrequency = downloadFrequency;
    }

    public Short getType() {
        return this.type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getIssn() {
        return this.issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

}


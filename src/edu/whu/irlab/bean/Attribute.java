package edu.whu.irlab.bean;

/**
 * Created by Memray on 2015/3/13.
 */

public class Attribute {
    /** Constant set */
    public enum ValueType{
        INTEGER, BOOLEAN, STRING, LIST, MAP, FLOAT, DOUBLE
    }

    /**
     * 保存该特征的名称
     */
    private String m_AttributeName;
    /**
     * 对于字符串类型的，在这里保存具体的字符串内容
     */
    private String m_ItemName;
    private ValueType m_Type;
    private Integer m_Index;

    /**
     * 记录该特征的特征选择的得分
     */
    private Float m_Score;
    public Attribute(String m_AttributeName, String m_ItemName, ValueType m_Type, Integer m_Index) {
        this.m_AttributeName = m_AttributeName;
        this.m_ItemName = m_ItemName;
        this.m_Type = m_Type;
        this.m_Index = m_Index;
    }

    public String getAttributeName() {
        return m_AttributeName;
    }

    public void setAttributeName(String m_Name) {
        this.m_AttributeName = m_Name;
    }

    public String getItemName() {
        return m_ItemName;
    }

    public void setItemName(String m_ItemName) {
        this.m_ItemName = m_ItemName;
    }

    public ValueType getType() {
        return m_Type;
    }

    public void setType(ValueType m_Type) {
        this.m_Type = m_Type;
    }

    public Integer getIndex() {
        return m_Index;
    }

    public void setIndex(Integer m_Index) {
        this.m_Index = m_Index;
    }

    public Float getScore() {
        return m_Score;
    }

    public void setScore(Float m_Score) {
        this.m_Score = m_Score;
    }

    public String toString(){
        if(m_ItemName.equals(""))
            return m_AttributeName+"\t"+m_Type+"\t"+m_Index;
        else
            return m_AttributeName+":"+m_ItemName+"\t"+m_Type+"\t"+m_Index;

    }
}
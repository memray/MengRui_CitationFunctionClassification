package edu.whu.irlab.bean;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.TreeGraph;
import edu.stanford.nlp.trees.TypedDependency;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.CitationFeatureTemplate;

public class DataPoint {
	//句子所在的文章的aclid
    String aclid;
    //句子的内容，引文标记已被( )替换
	String sentence;
    //保存一个没有经过任何处理DFKI句子原版
	String originSentence;
    //句子的postag
	String grammar;

    //句子在全文中的index位置
    int sentenceIndex;
    //整个文章中句子的个数
    int sentenceNumber;

	//引文中的作者标记
	ArrayList<String> authors = new ArrayList<String>();

	//从数据库中获取的文章对象信息
	AclArticle article;

    //对应数据库中的具体位置,属于的章节号，周围包括自己共有的引用数量
    AclSentence aclSentence;
    String formerNeighborSentence = "";
    String nextNeighborSentence = "";

    //Section信息，section name是章节的名字，number是具体的章节号
    private String sectionName = "";
    private String sectionNumberString = "";

	/**
	 * 该数据所对应特征对象
	 */
	CitationFeatureTemplate feature = new CitationFeatureTemplate();
    //Dependency Relations 语法依存关系特征,保存stanford parser的处理结果
    public List<TypedDependency> typedDependencies = null;
    //存储postag标注
    public List<TaggedWord> taggedWords = null;
    //句法树，提取最近词汇时用
    public static TreeGraph tree = null;

    /**
     * 引文在句子中的替换标记
     */
//	public static String reference_tag = "<citation>" ;
	/**
	 * 三个类别
	 * 	BackGround：背景和相关工作
	 *  Fundamental：idea,method,toolkit,data等
	 *  Compare:比较
	 */
	String label1;
	/**
	 * 五个类别
	 * GRelated : General Related 背景和相关工作
	 * SRelated : Specific Related 与当前工作特别相关
	 * Idea : 启发了当前工作的想法
	 * Basis : data、method、toolkit等基础元素在当前工作中使用
	 * Compare : 比较
	 */
	String label2;
	/**
	 * 情感极性的三个类别
	 * Positive, Negative, Neutral
	 */
	String polarity;
	String datastring;
	String classLabelNumber;
	String predictClassLabelNumber ="";
	/**
     * 加上标签后的文本，用于html显示
     */
	String annotatedSentence;
	ArrayList<SubjectEntity> subjectAgents;
	ArrayList<CitationAgent> referenceAgents;


	public DataPoint(String data){
		String[] strings = data.split("\t");
		//System.out.println(data);
		this.aclid = strings[0];
		this.sentence = strings[1];
		this.originSentence = strings[1];
		this.grammar = strings[2];
		this.label1 = strings[3];
		this.label2 = strings[4];
		this.polarity = strings[5];
		this.datastring = data;
		
		if (label2.equals("Idea")) {
			this.classLabelNumber = "1" ;
		} else if (label2.equals("Basis")) {
			this.classLabelNumber = "2" ;
		} else if (label2.equals("Compare")) {
			this.classLabelNumber = "3" ;
		} else if (label2.equals("GRelated")) {
			this.classLabelNumber = "4" ;
		} else if (label2.equals("SRelated")) {
			this.classLabelNumber = "4" ;
		} else if (label2.equals("MRelated")) {//Future works
			this.classLabelNumber = "4" ;
		}

	}
	
	
	public DataPoint() {
	}


	public String toString(){
		return "["+this.aclid+"]["+this.label2+"]"+this.sentence;
	}
	
	public String toHTML() {
		StringBuilder builder = new StringBuilder();
		builder.append("<div class=\"data-point\">");
		builder.append("<p class=\"basic-info\">["+this.aclid+"]["+this.label2+"]");
		if(this.predictClassLabelNumber !=null && !this.predictClassLabelNumber.equals("")){
			builder.append("[错分为 : "+ RunFeatureExtract.InstanceClassName.getClassName(Integer.parseInt(this.predictClassLabelNumber)+1)+"]");
		}
		builder.append("<章节 : "+this.getSectionName()+"></p>");
		builder.append("<span class='dfki-sentence'>DFKI句：").append(this.annotatedSentence.replace("<citation>", "< >")).append("</span><br/><br/>");
        if(this.getFeature().getPreviousSentenceRelated())
            builder.append("<span style='font-weight:bolder' class='previous-sentence'>前一句：" + this.getFormerNeighborSentence() + "</span><br/>");
        else
            builder.append("<span class='previous-sentence'>前一句：" + this.getFormerNeighborSentence() + "</span><br/>");
//		builder.append("<span class='current-sentence'>当前句：").append(this.annotatedSentence.replace("<citation>", "<span class=\"reference-tag\"> citation </span>")).append("</span><br/>");
		builder.append("<span class='current-sentence'>当前句：").append(this.getAclSentence().content).append("</span><br/>");

        if(this.getFeature().getFollowingSentenceRelated())
            builder.append("<span style='font-weight:bolder' class='success-sentence'>后一句："+this.getNextNeighborSentence()+"</span><br/>");
        else
            builder.append("<span class='success-sentence'>后一句："+this.getNextNeighborSentence()+"</span><br/>");
		builder.append("<table class=\"subject-info\">");
		
		Class featureClass = this.feature.getClass();
		Field[] fields = featureClass.getDeclaredFields();
		for(Field field : fields){
			builder.append("<tr>");
			field.setAccessible(true);
			try {
				Object value = field.get(this.getFeature());
				builder.append("<td class=\"td-feature\">"+field.getName()+" : "+value.toString()+"</td>");
			} catch (Exception e) {
				e.printStackTrace();
			}// 得到此属性的值
			builder.append("</tr>"); 
		}
/*		if(this.subjectAgents!=null)
		for(SubjectAgent subjectAgent : this.subjectAgents){
			builder.append("<tr>");
			builder.append("<td class=\"subject-np\">"+subjectAgent.getNPString()+"</td>");
			builder.append("<td class=\"subject-verb\">"+subjectAgent.getVerbString()+"</td>");
			builder.append("</tr>");
		}
		builder.append("</table>");
		
		builder.append("<table class=\"reference-info\">");
		if(this.referenceAgents!=null)
		for(CitationAgent referenceAgent : this.referenceAgents){
			builder.append("<tr>");
			builder.append("<td class=\"reference-np\">"+referenceAgent.getNPString()+"</td>");
			builder.append("<td class=\"reference-verb\">"+referenceAgent.getVerbString()+"</td>");
			builder.append("</tr>");
		}*/
		builder.append("</table>");
		
		builder.append("</div>");
		builder.append("<br/>");
		builder.append("<br/>");
		return builder.toString();
	}
	
	public String getAclid() {
		return aclid;
	}
	public void setAclid(String aclid) {
		this.aclid = aclid;
	}
	public String getSentence() {
		return sentence;
	}
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}
	public String getGrammar() {
		return grammar;
	}
	public void setGrammar(String grammar) {
		this.grammar = grammar;
	}
	public String getLabel1() {
		return label1;
	}
	public void setLabel1(String label1) {
		this.label1 = label1;
	}
	public String getLabel2() {
		return label2;
	}
	public void setLabel2(String label2) {
		this.label2 = label2;
	}
	public String getPolarity() {
		return polarity;
	}
	public void setPolarity(String polarity) {
		this.polarity = polarity;
	}

	public String getDatastring() {
		return datastring;
	}

	public void setDatastring(String datastring) {
		this.datastring = datastring;
	}

	public ArrayList<SubjectEntity> getSubjectAgents() {
		return subjectAgents;
	}

	public void setSubjectAgents(ArrayList<SubjectEntity> subjectAgents) {
		this.subjectAgents = subjectAgents;
	}

	public ArrayList<CitationAgent> getReferenceAgents() {
		return referenceAgents;
	}

	public void setReferenceAgents(ArrayList<CitationAgent> referenceAgents) {
		this.referenceAgents = referenceAgents;
	}

	public String getAnnotatedSentence() {
		return annotatedSentence;
	}

	public void setAnnotatedSentence(String annotatedSentence) {
		this.annotatedSentence = annotatedSentence;
	}

	public String getClassLabelNumber() {
		return classLabelNumber;
	}

	public void setClassLabelNumber(String classLabelNumber) {
		this.classLabelNumber = classLabelNumber;
	}

	public String getPredictClassLabelNumber() {
		return predictClassLabelNumber;
	}

	public void setPredictClassLabelNumber(String predictClassLabelNumber) {
		this.predictClassLabelNumber = predictClassLabelNumber;
	}

	public CitationFeatureTemplate getFeature() {
		return feature;
	}

	public void setFeature(CitationFeatureTemplate feature) {
		this.feature = feature;
	}

	public AclArticle getArticle() {
		return article;
	}

	public void setArticle(AclArticle article) {
		this.article = article;
	}

    public AclSentence getAclSentence() {
        return aclSentence;
    }

    public void setAclSentence(AclSentence aclSentence) {
        this.aclSentence = aclSentence;
    }

    public String getFormerNeighborSentence() {
        return formerNeighborSentence;
    }

    public void setFormerNeighborSentence(String formerNeighborSentence) {
        this.formerNeighborSentence = formerNeighborSentence;
    }

    public String getNextNeighborSentence() {
        return nextNeighborSentence;
    }

    public void setNextNeighborSentence(String nextNeighborSentence) {
        this.nextNeighborSentence = nextNeighborSentence;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    public int getSentenceIndex() {
        return sentenceIndex;
    }

    public void setSentenceIndex(int sentenceIndex) {
        this.sentenceIndex = sentenceIndex;
    }

    public int getSentenceNumber() {
        return sentenceNumber;
    }

    public void setSentenceNumber(int sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }

    public List<TypedDependency> getTypedDependencies() {
        return typedDependencies;
    }

    public void setTypedDependencies(List<TypedDependency> typedDependencies) {
        this.typedDependencies = typedDependencies;
    }
    public List<TaggedWord> getTaggedWords() {
        return taggedWords;
    }

    public void setTaggedWords(List<TaggedWord> taggedWords) {
        this.taggedWords = taggedWords;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionNumberString() {
        return sectionNumberString;
    }

    public void setSectionNumberString(String sectionNumberString) {
        this.sectionNumberString = sectionNumberString;
    }

    public static TreeGraph getTree() {
        return tree;
    }

    public static void setTree(TreeGraph tree) {
        DataPoint.tree = tree;
    }

    public String getOriginSentence() {
        return originSentence;
    }

    public void setOriginSentence(String originSentence) {
        this.originSentence = originSentence;
    }
}

package edu.whu.irlab.bean;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Memary on 2015/3/31.
 */
public class Citation {
    private String author;
    private String year;
    //lxb添加 每个citation 对应的 dp
    private DataPoint dataPoint;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public DataPoint getDataPoint() {
        return dataPoint;
    }

    public void setDataPoint(DataPoint dataPoint) {
        this.dataPoint = dataPoint;
    }

    /**
     * 从句子中抽取引文标记信息
     * @return
     */
    public static ArrayList<Citation> extractCitations(String sentence){
        ArrayList<Citation> citations = new ArrayList<>();
        sentence = sentence.replace("et al.", "");
        sentence = sentence.replace("et al", "");
        Pattern pattern = Pattern.compile("((?:(?:[A-Z][A-Za-z'\\-­]+(?:|\\s+&\\s+|\\s+and\\s+)?)\\s*[,;]?)+)" +
                "\\s*" +
                "\\(?(\\d{4}[AaBb]?)");
        Matcher matcher = pattern.matcher(sentence);
        String temp = "";
        while(matcher.find()){
            Citation citation = new Citation();
            citation.setAuthor(strip(matcher.group(1).trim(),"\\W"));
            citation.setYear(strip(matcher.group(2).trim(), "\\W"));
            //System.out.println("\t\t\t\t\t\t\t\t"+matcher.group(0) + "\t-->\t[" + citation.getAuthor() + "," + citation.getYear() + "]");
            citations.add(citation);
        }
        return citations;
    }

    public static String strip(String text, String regex) {
        int len = text.length();
        int st = 0;

        while ((st < len) && (text.substring(st, st+1).matches(regex))) {
            st++;
        }
        while ((st < len) && (text.substring(len-1, len).matches(regex))) {
            len--;
        }
        return ((st > 0) || (len < text.length())) ? text.substring(st, len) : text;
    }

    @Override
    public String toString(){
        return "["+this.year+"]"+this.author;
    }

    public boolean equals(Citation another){
        if (this.getAuthor().equals(another.getAuthor()) && this.getYear().equals(another.getYear()))
            return  true;
        return  false;
    }
}

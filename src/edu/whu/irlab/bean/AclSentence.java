package edu.whu.irlab.bean;

public class AclSentence {
	Long id;
	String aclid;
	String content;
	int is_caption;
	String caption_number;
	int serial_id;
	String function;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAclid() {
		return aclid;
	}
	public void setAclid(String aclid) {
		this.aclid = aclid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getIs_caption() {
		return is_caption;
	}
	public void setIs_caption(int is_caption) {
		this.is_caption = is_caption;
	}
	public String getCaption_number() {
		return caption_number;
	}
	public void setCaption_number(String caption_number) {
		this.caption_number = caption_number;
	}
	public int getSerial_id() {
		return serial_id;
	}
	public void setSerial_id(int serial_id) {
		this.serial_id = serial_id;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	
}

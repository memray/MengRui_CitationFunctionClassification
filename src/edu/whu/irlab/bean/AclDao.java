package edu.whu.irlab.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class AclDao {
	static Connection conn;
	static Statement statement;

	/**
	 * 方法用于从数据库中获取 该文章的元信息
	 * @param aclid 指定文章的aclid
	 * @return AclArticle对象
	 */
	public static AclArticle getArticle(String aclid){
		AclArticle article = new AclArticle();
		try{
			conn=getConnection();
			statement=(Statement)conn.createStatement();
			String sql = "SELECT * FROM acl_article WHERE aclid = '"+aclid+"'";
			ResultSet rs = statement.executeQuery(sql);

			if(rs.next()){
				article.setId(rs.getLong("id"));
				article.setAclid(rs.getString("aclid"));
				article.setTitle(rs.getString("title"));
				article.setAuthor(rs.getString("author"));
				article.setAbstract_(rs.getString("abstract"));
				article.setYear(rs.getString("year"));
				article.setVenue(rs.getString("venue"));
			}

			rs.close();
			conn.close();

		}catch(Exception e){
			e.printStackTrace();
		}
		return article;
	}

	/**
	 * 方法用于从数据库中获取 该文章下的 所有句子信息
	 * @param aclid 指定文章的aclid
	 * @return 符合要求的acl_sentence集合
	 */
	public static List<AclSentence> getSentences(String aclid){
		List<AclSentence> aclSentences = new ArrayList<AclSentence>();
		try{
			conn=getConnection();
			statement=(Statement)conn.createStatement();
			String sql = "select * from acl_sentence where aclid = '"+aclid+"'";
			ResultSet rs = statement.executeQuery(sql);
			
			while(rs.next()){

				AclSentence as = new AclSentence();
				as.setId(rs.getLong(1));
				as.setAclid(rs.getString(2));
				as.setContent(rs.getString(3));
				as.setIs_caption(rs.getInt(4));
				as.setCaption_number(rs.getString(5));
				as.setSerial_id(rs.getInt(6));
				
				aclSentences.add(as);
			}
			
			rs.close();
			conn.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return aclSentences ;
	}
	
	
	public static Connection getConnection(){
		Connection con=null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/new_citation?user=root&password=admin&useUnicode=true&characterEncoding=utf-8");
		}catch(Exception e){
			System.out.println("连接失败"+e.getMessage());
		}
		return con;
	}
	
	public static void main(String[] args){
//		new AclDao().getSentences("E99-1009");
		System.out.println(new AclDao().getArticle("E99-1009").getAuthor());
	}
}

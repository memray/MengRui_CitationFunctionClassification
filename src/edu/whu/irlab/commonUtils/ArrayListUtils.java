package edu.whu.irlab.commonUtils;

import java.util.ArrayList;

/**
 * Created by lxb on 2015/4/27.
 */
public class ArrayListUtils {

    //把一个列表A 添加到 列表B中，重复项不添加
    public static void addListAtoB(ArrayList<String> a,ArrayList<String> b){
        for(String s : a)
            if(!b.contains(s))
                b.add(s);
    }
}

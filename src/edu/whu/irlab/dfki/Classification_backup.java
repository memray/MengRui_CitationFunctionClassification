package edu.whu.irlab.dfki;

import java.io.*;
import java.util.*;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.bean.Instance;
import edu.whu.irlab.dfki.data.ConverterUtils;
import edu.whu.irlab.dfki.feature.FeatureConverter;
import edu.whu.irlab.dfki.feature.FeatureProcessor;
import edu.whu.irlab.dfki.feature.FeatureScaler;
import edu.whu.irlab.dfki.featureSelection.FeatureSelection;
import weka.core.Instances;


/**
 * 分类实验的主程序
 * @author Memray
 */
public class Classification_backup {
    public enum FeatureSetName{
        AbuJbara, Dong, Charles, Our
    }
    public enum InstanceClassName{
        Idea(1), Basis(2), Comparison(3), Background(4);
        private int id;
        InstanceClassName(int id){
            this.id = id;
        }
        public static InstanceClassName getClassName(int id){
            for(InstanceClassName className : InstanceClassName.values())
                if (className.id == id)
                    return className;
            return null;
        }

    }

    public static String TargetFeatureName;


    //工程的绝对路径
    public static String ProjectBasePath = System.getProperty("user.dir") + File.separator;
    public static String OutputBasePath = ProjectBasePath + "output" + File.separator;
    public static String DataCachePath = "";//存放数据Cache的位置(提取了postag、句法等信息)
    public static String FeatureCachePath = "";//存放特征临时文件的位置，包含DataCache中的数据以及额外抽取的特征数据

    HashSet<String> labels = new HashSet<>();
    public static ArrayList<DataPoint> dataPoints = new ArrayList<>();

    public static Boolean Lemmatization = true;
    /**
     * 初始化操作，包括读取元数据，路径变量设置等
     */
    public void init() {
        /**
         * Transfer to feature vectors
         */
        Classification_backup.DataCachePath = ProjectBasePath + "output" + File.separator + "DFKIDataCache.dat";
        Classification_backup.FeatureCachePath = ProjectBasePath + "output" + File.separator + "DFKIFeatureCache.dat";

    }

    /**
     * Transfer to feature vectors
     */
    public void feature_generate(Boolean lemmatization) {
        /**
         * 1. 抽取特征，基本生成一次缓存后可以快速调用
         */
        FeatureProcessor.createFeatureFile(dataPoints, this.FeatureCachePath,
                this.TargetFeatureName, lemmatization);

        /**
         * 2. 根据feature data生成特征向量
         */
        ArrayList<Instance> instanceList = FeatureConverter.getOurInstanceList(dataPoints);

        /**
         * 3. 转化为Weka的instances
         */
        Instances instances = ConverterUtils.convertInstances(instanceList, instanceList.get(0).getAttributes());
        System.out.println("共有" + instances.numAttributes() + "个特征");
        /**
         * 4. 对生成的特征文件进行scale
         */
        instances = FeatureScaler.scale(instances, "-S 1.0 -T 0.0".split("\\s"));

        /**
         * 5. 导出每一个特征集的特征列表(通过过滤器对不同特征集进行过滤)，执行对应的特征选择，将特征向量输出为libsvm格式
         */
/*        for (Classification.FeatureSetName featureSetName : Classification.FeatureSetName.values()) {

            *//*3.1 通过过滤器获取到该特征名下的子Instances*//*

            Instances subInstances = InstancesFilter.filterInstancesByFeatureSetName(instances, featureSetName);
            System.err.println(featureSetName + " 有 " + subInstances.numAttributes() + "个特征");


            *//*3.2 开始导出特征向量，包括特征列表和libsvm向量文件*//*

            System.out.println("导出" + featureSetName + "特征方案");
            FeatureConverter.exportAttributeList(subInstances, featureSetName, featureSetName + "_all_attribute.txt");
            FeatureConverter.exportLibsvmFeatureVector(subInstances, featureSetName + "", featureSetName + "_all_vector.libsvm");
            System.out.println("特征向量文件输出完毕");

            *//* 3.3 执行特征选择，返回一个按照得分从大到小排序的特征列表*//*

            System.out.println("正在执行InfoGain特征选择");
            FeatureSelection.runFeatureSelectionOnGivenInstances(subInstances, featureSetName + "");
        }*/
        /**
         * 6. 通过特征选择找出两两类别之间的特征差异
         */
        FeatureSelection.runSelectionBetweenClasses(instances);
    }


    public static void main(String[] args) {
        Classification_backup classifier = new Classification_backup();
        /**
         * 是否考虑进行词干提取Lemmatization
         */
        Lemmatization = true;
        classifier.init();//读取源数据
        classifier.feature_generate(Lemmatization);//生成特征信息


    }

}

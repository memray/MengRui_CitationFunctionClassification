package edu.whu.irlab.dfki.data;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.data.etc.ContentTagger;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import edu.whu.irlab.dfki.feature.FeatureExtractFunctions;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * 由于一些预处理操作十分耗时，如postag，句法分析，数据库读取等
 * 这里将这些操作提取出来提前运行，并保存到硬盘上方便下次直接调取使用。
 * 对sentence内容进行基本的初始化处理(替换引文标记等,不替换主语标记，需要时再替换，否则影响很多特征抽取)
 * initDataPoint和citationTagReplacement和readPhysicalDataFromDB三个操作
 * Created by Memray on 2015/3/17.
 */
public class DataCacheExtractor {
    public static void main(String[] args) throws IOException {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();

        int count = 0;
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();

        File outputFile = new File(runFeatureExtract.DataCachePath);
        outputFile.delete();
        StringBuilder sb = new StringBuilder();
        //初始化及对引文标记进行替换
        for(DataPoint dp : runFeatureExtract.dataPoints) {
            System.out.println("对句子初始化中，已初始化"+ ++count +"条数据");
            FeatureExtractFunctions.initDataPoint(dp);

            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));

            //AnnotatedSentence主要是为了输出html可视化
            dp.setAnnotatedSentence(dp.getSentence());
            dp.setAnnotatedSentence(ContentTagger.addAuthorTag(dp.getAnnotatedSentence()));
            dp.setAnnotatedSentence(ContentTagger.addFeatureTag(dp.getAnnotatedSentence()));
            dp.setAnnotatedSentence(ContentTagger.addCitationTag(dp.getAnnotatedSentence(), "<citation>"));

//            FeatureExtractFunctions.authorTagReplacement(dp);
            //从数据库中读取原文信息
            FeatureExtractFunctions.readPhysicalDataFromDB(dp);
            //处理句法信息(句法信息无法被保存为json，还是需要在处理特征时即时生成)

            sb.append(FeatureCacheUtils.toFeatureString(dp)).append("\n");
            FileUtils.write(outputFile, sb.toString() , true);
            sb.setLength(0);
        }

    }
}

package edu.whu.irlab.dfki.data;

import edu.whu.irlab.dfki.RunFeatureExtract;
import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Memray on 2015/3/24.
 */
public class InstancesFilter {
    /**
     * 用HashSet保存各个特征集中所包含的特征
     */
    //BaseLine1特征，1个， N-grams
    static HashSet<String> NgramFeatureSet = new HashSet<>(Arrays.asList("nGram"));
    //Athar特征，也是BaseLine2，2个，N-grams+Dependency
    static HashSet<String> AtharFeatureSet = new HashSet<>(Arrays.asList("nGram", "dependencyRelations"));
    //AbuJbara特征，11个
    static HashSet<String> AbuJbaraFeatureSet = new HashSet<>(Arrays.asList("citationDensity", "popularity", "citationLinkedKeyAbuWords",
            "selfCitationBoolean", "firstPersonPrononeBoolean", "thirdPersonPrononeBoolean", "negationCueBoolean", "speculationBoolean",
            "closestSubjectivityAbuCue", "contraryExpressionsBoolean", "sectionNumber", "dependencyRelations"));
    //Dong特征，24个boolean和weight特征，3个物理特征，7个句法结构特征
    static HashSet<String> DongFeatureSet = new HashSet<>(Arrays.asList("subjectBoolean", "quantityBoolean", "frequencyBoolean", "tenseBoolean", "exampleBoolean", "suggestBoolean", "hedgeBoolean", "ideaBoolean", "basisBoolean", "compareBoolean", "resultBoolean", "neighborSubjectBoolean",
            "subjectWeight", "quantityWeight", "frequencyWeight", "tenseWeight", "exampleWeight", "suggestWeight", "hedgeWeight", "ideaWeight", "basisWeight", "compareWeight", "resultWeight", "neighborSubjectWeight",
            "popularity", "citationDensity", "avgDens", "syntacticFeature_1", "syntacticFeature_2", "syntacticFeature_3", "syntacticFeature_4", "syntacticFeature_5", "syntacticFeature_6", "syntacticFeature_7"));

    //Teufel特征，12个，有一个modal的，和自提特征重复了
    static HashSet<String> TeufelFeatureSet = new HashSet<>(Arrays.asList("hasConceptualCue", "hasOperational", "hasEvolutionary", "hasJuxtapositional", "hasOrganic", "hasPerfunctory", "hasConfirmative", "hasNegational", "tense", "hasVoice", "modalWords", "paperLocation"));
    //Charles自提特征，18个各类特征以及4个reference特征
    static HashSet<String> CharlesOwnFeatureSet = new HashSet<>(Arrays.asList("constituentBoolean", "otherContrastBoolean", "selfCompBoolean", "otherCompBoolean", "selfGoodBoolean", "hasButBoolean", "comparativeBoolean", "modalBoolean", "firstPersonPrononeBoolean", "thirdPersonPrononeBoolean", "rootWord", "mainVerb", "positiveWords", "negativeWords", "hasCfBoolean", "hasResourceBoolean", "hasToolBoolean", "sentenceLocation",
            "referenceAge", "authorNumber", "publicationKeyWords", "selfCitationBoolean"));
    //Our的自提特征，改进Dong词表特征14个，数字特征2个，2个句法特征，4个物理特征
    static HashSet<String> OurFeatureSet = new HashSet<>(Arrays.asList("ourSubjectBoolean", "ourSubjectWeight", "ourIdeaBoolean", "ourIdeaWeight", "ourBasisBoolean", "ourBasisWeight", "toolBoolean", "toolWeight", "ourCompareBoolean", "ourCompareWeight", "ourQuantityBoolean", "ourQuantityWeight", "ourFrequencyBoolean", "ourFrequencyWeight", "ourTenseBoolean", "ourTenseWeight", "ourExampleBoolean", "ourExampleWeight", "ourSuggestBoolean", "ourSuggestWeight", "ourHedgeBoolean", "ourHedgeWeight", "ourResultBoolean", "ourResultWeight", "objectBoolean", "objectWeight", "futureBoolean", "futureWeight", "numberBoolean", "numberCount", "percentageBoolean", "percentageCount",
            "subjectLinkedKeyWords", "citationLinkedKeyWords", "sentenceLength", "shortSentenceNumber", "ShortSentenceAvgLength"
            , "subjectLinkedToCitationBoolean", "subjectCitationLinkedWords"
            , "previousSentenceRelated", "previousSentenceCitationNumber", "followingSentenceRelated", "followingSentenceCitationNumber"
            ,"subjectLinkedDependencies", "citationLinkedDependencies"
        ));
    //下面是Context的特征
    static HashSet<String> OurContextFeatureSet = new HashSet<>(Arrays.asList(
            "contextSubjectLinkedKeyWords", "contextnGram"
            , "ourContextSubjectBoolean", "ourContextSubjectWeight", "ourContextIdeaBoolean", "ourContextIdeaWeight" ,"ourContextBasisBoolean", "ourContextBasisWeight", "ourContextCompareBoolean", "ourContextCompareWeight", "ourContextResultBoolean", "ourContextResultWeight", "ourContextQuantityBoolean", "ourContextQuantityWeight", "ourContextFrequencyBoolean", "ourContextFrequencyWeight", "ourContextTenseBoolean", "ourContextTenseWeight", "ourContextExampleBoolean", "ourContextExampleWeight", "ourContextSuggestBoolean", "ourContextSuggestWeight", "contextOurHedgeBoolean", "contextOurHedgeWeight", "contextObjectBoolean", "contextObjectWeight", "contextToolBoolean", "contextToolWeight", "contextFutureBoolean", "contextFutureWeight"
            , "contextNumberCount", "contextNumberBoolean", "contextPercentageCount", "contextPercentageBoolean", ""
            , "ourContextSubjectWords", "ourContextIdeaWords", "ourContextBasisWords", "ourContextCompareWords", "ourContextResultWords", "ourContextQuantityWords", "ourContextFrequencyWords", "ourContextTenseWords", "ourContextExampleWords", "ourContextSuggestWords", "ourContextHedgeWords", "contextObjectWords", "contextToolWords", "contextFutureWords", ""
            ));


    /**
     * 对给定的Instances进行过滤，返回包含制定特征组的Instances
     *
     * @param instances
     * @param featureSetName
     * @return
     */
    public static Instances filterInstancesByFeatureSetName(Instances instances, RunFeatureExtract.FeatureSetName featureSetName) {
        HashSet<String> featureSet = null;
        /**
         * 1. 设定该特征集下需要导出的特征
         */
        if (featureSetName == RunFeatureExtract.FeatureSetName.BaseLine1) {
            featureSet = NgramFeatureSet;
        } else if (featureSetName == RunFeatureExtract.FeatureSetName.BaseLine2) {
            featureSet = AtharFeatureSet;
        } /*else if (featureSetName == RunFeatureExtract.FeatureSetName.AbuJbara) {
            featureSet = AbuJbaraFeatureSet;
        } else if (featureSetName == RunFeatureExtract.FeatureSetName.Dong) {
            featureSet = DongFeatureSet;
        } else if (featureSetName == RunFeatureExtract.FeatureSetName.Charles) {
            featureSet = CharlesOwnFeatureSet;
            featureSet.addAll(DongFeatureSet);
            featureSet.addAll(AtharFeatureSet);
            featureSet.addAll(TeufelFeatureSet);
        } else if (featureSetName == RunFeatureExtract.FeatureSetName.Our) {
            featureSet = OurFeatureSet;
            featureSet.addAll(DongFeatureSet);
            *//*featureSet.addAll(Arrays.asList("neighborSubjectWeight", "popularity", "citationDensity", "avgDens",
                    "syntacticFeature_1", "syntacticFeature_2", "syntacticFeature_3", "syntacticFeature_4",
                    "syntacticFeature_5", "syntacticFeature_6", "syntacticFeature_7"));*//*
            featureSet.addAll(AtharFeatureSet);
            featureSet.addAll(TeufelFeatureSet);
            featureSet.addAll(CharlesOwnFeatureSet);
            featureSet.addAll(AbuJbaraFeatureSet);
//            featureSet.remove("citationLinkedKeyWords");
//            featureSet.remove("citationLinkedKeyAbuWords");
//            featureSet.remove("AuthorNumber");
//            featureSet.remove("firstPersonPrononeBoolean");
//            featureSet.remove("thirdPersonPrononeBoolean");
//            featureSet.remove("referenceAge");
        }else if (featureSetName == RunFeatureExtract.FeatureSetName.OurContext) {
            featureSet = OurFeatureSet;
            featureSet.addAll(DongFeatureSet);
            featureSet.addAll(AtharFeatureSet);
            featureSet.addAll(TeufelFeatureSet);
            featureSet.addAll(CharlesOwnFeatureSet);
            featureSet.addAll(AbuJbaraFeatureSet);
            featureSet.addAll(OurContextFeatureSet);
        }*/
        /**
         * 2. 获取对应属性的index，不将class所在的类标也保存其中
         */
        ArrayList<weka.core.Attribute> retainedAttributes = getAttributesByFeatureSetName(instances, featureSet);
        /**
         * 3. 使用filter对Instances进行过滤
         */
        return filterInstancesByAttributes(instances, retainedAttributes);
    }

    /**
     * 根据给定的属性列表对instances进行过滤
     * 注意提前将class所在的属性添加到保留列表项中
     * @param instances
     * @param retainedAttributes
     */
    public static Instances filterInstancesByAttributes(Instances instances, ArrayList<weka.core.Attribute> retainedAttributes) {
        /*StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("-V ").append("-R ");
        retainedAttributes.stream().forEach(s -> stringBuffer.append(s.index()).append(","));
        //注意加上class所在的属性
        stringBuffer.append(instances.classIndex());
*/
        // 去重
        HashSet<Integer> indexSet = new HashSet<>();
        retainedAttributes.forEach(s->indexSet.add(s.index()));
        indexSet.add(instances.classIndex());

        Integer[] attributeIndex = indexSet.toArray(new Integer[0]);
        Arrays.sort(attributeIndex);

        int[] newAttributeIndex = new int[attributeIndex.length];
        for(int i = 0; i< attributeIndex.length; i++) {
            newAttributeIndex[i] = attributeIndex[i];
            //System.out.println(attributeIndex[i]+"\t"+instances.getAttributes().get(attributeIndex[i]).name());
        }
        /*Collections.sort(retainedAttributes, (a, b) -> a.index() - b.index());
        for(int i = 0; i < retainedAttributes.size(); i++)
            attributes[i] = retainedAttributes.get(i).index();*/

        Instances newInstances = null;
        try {
            // 创建一个remove，将基本信息传递给他
            Remove remove = new Remove();
            // 保留的属性的index
            remove.setAttributeIndicesArray(newAttributeIndex);
            // 设置为invert selection
            remove.setInvertSelection(true);
            // 将instances作为input传入
            remove.setInputFormat(instances);
            newInstances = Filter.useFilter(instances, remove);
        } catch (Exception e) {
            e.printStackTrace();
        }


/*
        // 由于Remove这孙子会把class这个属性干掉。。所以看看能否改用removeByName
        RemoveByName removeByName = new RemoveByName();
        try {
            Instances newInstances = Filter.useFilter(instances, removeByName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Remove这孙子会把class这个属性干掉，而且会重排序，导致class会找不到，放弃
        Remove remove = new Remove();
        try {
            remove.setInputFormat(instances);
            remove.setOptions(stringBuffer.toString().split("\\s"));
            Instances newInstances = Filter.useFilter(instances, remove);
            return newInstances;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;*/
        return newInstances;
    }

    /**
     * 根据给定的特征Set，返回在该Set中的属性的index，不包含class所在的属性
     * @param instances
     * @param featureSet
     * @return
     */
    private static ArrayList<weka.core.Attribute> getAttributesByFeatureSetName(Instances instances, HashSet<String> featureSet) {
        ArrayList<weka.core.Attribute> attributeList = new ArrayList<>();
        String attributeName;
        for (weka.core.Attribute attribute : instances.getAttributes()){
            if(!attribute.name().contains("_"))
                attributeName = attribute.name();
            else
                attributeName = attribute.name().substring(0, attribute.name().indexOf("_"));
            if (featureSet.contains(attributeName))
                attributeList.add(attribute);
        }
        return attributeList;
    }


    /**
     * 对指定特征集输出其特征向量，包括libsvm文件和一个index-name对照列表
     */
    public static void exportFeatureVector(RunFeatureExtract.FeatureSetName featureSetName) {
        /*
        HashSet<String> featureSet = new HashSet<>();

        System.out.println("开始导出特征向量");
        System.out.println("导出" + featureSetName + "特征方案");
        *//**
         * 1. 设定该特征集下需要导出的特征
         *//*
        if (featureSetName == Classification.FeatureSetName.AbuJbara) {
            featureSet = AbuJbaraFeatureSet;
        } else if (featureSetName == Classification.FeatureSetName.Dong) {
            featureSet = DongFeatureSet;
        } else if (featureSetName == Classification.FeatureSetName.Charles) {
            featureSet = CharlesOwnFeatureSet;
            featureSet.addAll(DongFeatureSet);
            featureSet.addAll(AtharFeatureSet);
            featureSet.addAll(TeufelFeatureSet);
        } else if (featureSetName == Classification.FeatureSetName.Our) {
            featureSet = OurFeatureSet;
            featureSet.addAll(DongFeatureSet);
            featureSet.addAll(AtharFeatureSet);
            featureSet.addAll(TeufelFeatureSet);
            featureSet.addAll(CharlesOwnFeatureSet);
            featureSet.addAll(AbuJbaraFeatureSet);
        }

        int outputIndex = 0;
        StringBuffer vectorBuffer = new StringBuffer();
        FeatureListPath = Classification.OutputBasePath + featureSetName + "_feature_list.txt";
        *//**
         * 2. 遍历AttributeList，输出AttributeName-index的对照表
         *//*
        HashMap<String, Integer> nameIndexMap = new HashMap<>();
        outputIndex = 0;
        for (Attribute attribute : attributeList)
            if (featureSet.contains(attribute.getAttributeName())) {
                if (attribute.getType() == Attribute.ValueType.INTEGER || attribute.getType() == Attribute.ValueType.BOOLEAN || attribute.getType() == Attribute.ValueType.DOUBLE || attribute.getType() == Attribute.ValueType.FLOAT) {
                    ++outputIndex;
                    vectorBuffer.append(outputIndex).append("\t").append(attribute.getAttributeName()).append("\n");
                    nameIndexMap.put(attribute.toString(), outputIndex);
                } else if (attribute.getType() == Attribute.ValueType.STRING || attribute.getType() == Attribute.ValueType.LIST) {
                    ++outputIndex;
                    vectorBuffer.append(outputIndex).append("\t").append(attribute.getAttributeName()).append("_").append(attribute.getItemName()).append("\n");
                    nameIndexMap.put(attribute.toString(), outputIndex);
                }
            }
        try {
            FileUtils.writeStringToFile(new File(FeatureListPath), vectorBuffer.toString(), "utf8", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        vectorBuffer = null;

        *//**
         * 3. 遍历每一个实例并输出
         *//*
        StringBuffer lineBuffer = new StringBuffer();
        VectorDataPath = Classification.OutputBasePath + featureSetName + "_vector.libsvm";
        File vectorDataFile = new File(VectorDataPath);
        vectorDataFile.delete();
        for (Instance instance : instanceList) {
            //首先输出类目号
            lineBuffer.append(instance.getClassNumber()).append(" ");
            for (Attribute attribute : attributeList)
                //输出在特征集中的对应维度的数值，由于保存的是稀疏矩阵，因此只输出保存的项，其余的忽略
                if (featureSet.contains(attribute.getAttributeName())) {
                    if (instance.getValues().containsKey(attribute.getIndex()))
                        lineBuffer.append(nameIndexMap.get(attribute.toString())).append(":").append(instance.getValues().get(attribute.getIndex())).append(" ");
                        *//*else {
                            System.out.println(attribute.toString());
                            lineBuffer.append(++outputIndex).append(":null_").append(attribute.toString()).append(" ");
                        }*//*
                }

            try {
                FileUtils.writeStringToFile(vectorDataFile, lineBuffer.append("\n").toString(), "utf8", true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            lineBuffer.setLength(0);
        }

        *//**
         * 4. 对生成的特征文件进行scale
         *//*
        ScaledDataPath = Classification.OutputBasePath + featureSetName + "_vector_scale.libsvm";
        feature_scale(VectorDataPath, ScaledDataPath);
        */
    }

    /**
     * 这里看会否需要对同名的attribute进行过滤，比如we这个词会在多个特征组中重复出现
     */
    public static ArrayList<Attribute> getFilteredAttributes(Instances instances) {
        HashSet<String> filter = new HashSet<>();
        ArrayList<weka.core.Attribute> attributeList = new ArrayList<>();
        String attributeName;
        for (weka.core.Attribute attribute : instances.getAttributes()){
            //获取到属性值
            if(!attribute.name().contains("_"))
                attributeName = attribute.name();
            else
                attributeName = attribute.name().substring(attribute.name().indexOf("_")+1, attribute.name().length());
//            System.out.println(attributeName);
            if (filter.contains(attributeName))
                continue;
            attributeList.add(attribute);
            filter.add(attributeName);
        }
        return attributeList;
    }
}

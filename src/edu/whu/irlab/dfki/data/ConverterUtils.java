package edu.whu.irlab.dfki.data;

import edu.whu.irlab.bean.Attribute;
import edu.whu.irlab.bean.Instance;
import edu.whu.irlab.bean.Reference;
import edu.whu.irlab.dfki.feature.ReferenceFeature.ReferenceFeatureUtils;
import org.apache.commons.io.FileUtils;
import weka.core.Instances;
import weka.core.SparseInstance;
import weka.core.converters.LibSVMLoader;
import weka.core.matrix.DoubleVector;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;
import weka.filters.unsupervised.attribute.NumericToNominal;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Memray on 3/23/2015.
 */
public class ConverterUtils {
    static Instances m_Structure;
    static Vector<double[]> m_Buffer;

    /**
     * 将我们的Instance对象列表转化为Weka的Instances对象
     *
     * @param instanceList
     * @param attributes
     * @return
     */
    public static Instances convertInstances(ArrayList<Instance> instanceList, ArrayList<Attribute> attributes) {
        ArrayList<weka.core.Attribute> atts = getAttributes(attributes);
        // 构建head信息
        m_Structure = getStructure("SVM_File", atts);
        m_Buffer = new Vector<>(instanceList.size());
        // 将数据转化到临时数组缓存中
        for (Instance instance : instanceList)
            m_Buffer.add(getDataBuffer(instance));
        // 对class所在的Attribute进行离散化处理
        NumericToNominal nm = new NumericToNominal();
        String[] options = new String[2];
        options[0] = "-R";
        options[1] = "last";

        Instances instances = getDataSet();
        //nominal
        try {
            nm.setOptions(options);
            nm.setInputFormat(instances);
            instances = Filter.useFilter(instances, nm);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 完成转化
        return instances;
    }

    private static Instances getDataSet() {
        Instances result;
        double[] sparse;
        double[] data;
        int i;

        result = new Instances(m_Structure, 0);

        // create instances from buffered arrays
        for (i = 0; i < m_Buffer.size(); i++) {
            //从缓存中读取某一行特征向量的值
            sparse = (double[]) m_Buffer.get(i);

            // 如果该向量的长度不等于属性的长度，则需要将稀疏表达的数组转化为完整的
            if (sparse.length != m_Structure.numAttributes()) {
                data = new double[m_Structure.numAttributes()];
                // attributes
                System.arraycopy(sparse, 0, data, 0, sparse.length - 1);
                // class
                data[data.length - 1] = sparse[sparse.length - 1];
            } else {
                data = sparse;
            }

            result.add(new SparseInstance(1, data));
        }

        return result;


    }

    /**
     * 将数据转化到临时数组缓存中
     *
     * @param instance
     */
    private static double[] getDataBuffer(Instance instance) {
        double[] result;
        int index;
        int max;
        double value;

        // determine max index
        max = m_Structure.numAttributes();
        // read values into array
        result = new double[max + 1];

        // 1. class 放在最后一列
        result[result.length - 1] = instance.getClassNumber() + 0.0;

        // 2. attributes
        for (Map.Entry<Integer, Double> entry : instance.m_Values.entrySet()) {
            index = entry.getKey();
            value = entry.getValue();
            result[index - 1] = value;
        }

        return result;

    }

    /**
     * 生成Instances的Structure
     */
    private static Instances getStructure(String svm_file, ArrayList<weka.core.Attribute> atts) {
        Instances m_structure = new Instances("SVM_File", atts, 0);
        //设置class所在的index
        m_structure.setClassIndex(m_structure.numAttributes() - 1);
        m_structure = new Instances(m_structure, 0);

        return m_structure;
    }

    /**
     * 转化attributes
     * 属性的名称定义如下：
     * 1. 如果包含getItemName()(是String类型的)，其命名为AttributeName()+"_"+getItemName()，如nGram_follow
     * 2. 否则特征名保存为AttributeName()
     */
    private static ArrayList<weka.core.Attribute> getAttributes(ArrayList<Attribute> attributes) {
        int numAtt = attributes.size();
        ArrayList<weka.core.Attribute> atts;
        atts = new ArrayList<weka.core.Attribute>(numAtt);
        String attributeName;
        for (int i = 0; i < numAtt - 1; i++) {
            attributeName = attributes.get(i).getItemName().isEmpty() ? attributes.get(i).getAttributeName() : attributes.get(i).getAttributeName() + "_" + attributes.get(i).getItemName();
            atts.add(new weka.core.Attribute(attributeName));
        }
        atts.add(new weka.core.Attribute("class"));
        return atts;
    }

}

package edu.whu.irlab.dfki.data.etc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import org.apache.commons.io.FileUtils;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.whu.irlab.bean.DataPoint;

/**
 * 处理DFKI数据的主程序，主要流程包括读取数据并进行句法分析，抽取出核心动词并输出html以方便分析。
 * 生成具体分类特征文件在FeatureExtraction中
 * @author Memray
 *
 */
public class Data2HTMLProcessor {
	static LexicalizedParser lexicalizedParser;
	private static GrammaticalStructureFactory grammaticalStructureFactory = null;
	static{
		lexicalizedParser = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		TreebankLanguagePack tlp = lexicalizedParser.getOp().langpack();
		grammaticalStructureFactory = tlp.grammaticalStructureFactory();
	}
	
	/**
	 * 如果predictResultPath为空则正常处理，如果不为空则只输出错分的结果
     * @param dataPoints
     * @param predictResultPath 预测的分类结果路径，如果为null则输出所有特征的信息
     * @param reportOutputPath
     */
	public void generateHTMLReport(ArrayList<DataPoint> dataPoints, String predictResultPath, String reportOutputPath){
		Boolean hasPredictResult = false;
        /*
         * 如果predictResultPath指定位置存在，则进行正负匹配
         */
		List<String> predictResultList = null;
        if(predictResultPath == null)
            predictResultPath = "";
		File predictFile = new File(predictResultPath);
		if(predictResultPath!=null && !predictResultPath.trim().equals("") && predictFile.exists()){
			hasPredictResult = true;
			try {
				predictResultList = FileUtils.readLines(new File(predictResultPath), "utf8");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
/*		labels.add("Basis");
		labels.add("Idea");

		labels.add("GRelated");
		labels.add("SRelated");
		labels.add("Compare");*/
		ArrayList<DataPoint> outputDataPoints = new ArrayList<DataPoint>();
		
		int count = -1;
		int badCount = 0;
		//HashSet<String> verbAgents = new HashSet<String>();
		
		for(DataPoint dataPoint : dataPoints){
			count++;
			if(hasPredictResult) {
                int predictValue = Integer.parseInt(predictResultList.get(count)) + 1;
                //分类正确的直接跳过
                if ((predictValue + "").equals(dataPoint.getClassLabelNumber()))
                    continue;
                else
                    badCount++;
			    dataPoint.setPredictClassLabelNumber(predictResultList.get(count));
            }


			//进行句法分析,使用TreeGraphNode以方便寻找父节点
			//TreeGraph tree = new TreeGraph(lexicalizedParse(data.getSentence().replaceAll("et al\\.", "et al")));
			//data.setSubjectAgents(GrammarUtils.getSubjectNPs(tree));
			//data.setReferenceAgents(GrammarUtils.getCitationNPs(tree,"<citation>"));
/*			for( SubjectAgent sa : data.getSubjectAgents()){
				verbAgents.add(new PorterStemmer().stemSentenceOrWords(sa.getVerbString()));
			}
			for( ReferenceAgent ra : data.getReferenceAgents()){
				verbAgents.add(new PorterStemmer().stemSentenceOrWords(ra.getVerbString()));
			}*/
            if(dataPoint.getFeature().getPreviousSentenceRelated()||dataPoint.getFeature().getFollowingSentenceRelated())
    			outputDataPoints.add(dataPoint);
		}
		if(hasPredictResult){
			System.out.println("共输出  "+badCount+" 个错例");
			FeatureCacheUtils.exportHTML(reportOutputPath, outputDataPoints);
		}else
			FeatureCacheUtils.exportHTML(reportOutputPath, outputDataPoints);
		//DataUtils.exportVerbs("corpus/verb1.txt", verbAgents);

	}
	
	public Tree lexicalizedParse(String sentence){
		//sentence = "Like the classic IBM models ( ) , our model will introduce a latent alignment vector a = {a 1 ,... ,a J } that specifies the position of an aligned target word for each source word";
		System.out.println(sentence);
		Tree tree = lexicalizedParser.parse(sentence);
		//输出上下文无关短语结构语法的树形表示
		//tree.pennPrint();
		return tree;
		
		//输出类型化的依存表示结果
/*		GrammaticalStructure gs = grammaticalStructureFactory.newGrammaticalStructure(parse);
		List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();
		System.out.println(tdl);
*/
		//输出句子里的词汇
/*		System.out.println("The words of the sentence:");
		for (Label lab : parse.yield()) {
			if (lab instanceof CoreLabel) {
				System.out.println(((CoreLabel) lab).toString("{map}"));
			} else {
				System.out.println(lab);
			}
		}
		*/
		//输出词性标注结果
		//System.out.println(parse.taggedYield());
	}
	
	public static void main(String[] args) {
		HashSet<String> labels = new HashSet<String>();
		ArrayList<DataPoint> dataPoints = FeatureCacheUtils.readDataPoints(labels);

		new Data2HTMLProcessor().generateHTMLReport(dataPoints,"output/our_feature.predict", "output\\visualization\\badcase.html");
	}
}

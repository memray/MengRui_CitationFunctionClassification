/**
 * @author memray
 * @mail memray@qq.com
 * 2014年7月5日下午7:11:08
 */
package edu.whu.irlab.dfki.data.etc;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 对正文内容进行处理，主要用于可视化分析
 * 包括给citation加标记，给paper第一人称加标记
 * 2014年7月5日下午7:11:08
 */
public class ContentTagger {
	
	//改成大小写分写，因为可能可能错误识别为美国的US
	private static String authorRegex = "";
	private static String featureRegex = "";
	static{
		//初始化原文主语识别部分
		String[] author_regexs = {"we","our","us","ours","this paper","this work","this study"};
		List<String> regexs = Arrays.asList(author_regexs);
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < regexs.size(); i++ ){
			if(i > 0)
				builder.append("|");
			builder.append("\\b").append(regexs.get(i)).append("\\b|\\b").append(regexs.get(i).substring(0,1).toUpperCase()).append(regexs.get(i).substring(1,regexs.get(i).length())).append("\\b");
		}
		authorRegex = builder.toString();
		
		//初始化特征识别部分
		String[] feature_regexs = {"same","similar","similarly","like","follow","follows","following","in the spirit of","as"};
		regexs = Arrays.asList(feature_regexs);
		builder = new StringBuilder();
		for(int i = 0; i < regexs.size(); i++ ){
			if(i > 0)
				builder.append("|");
			builder.append("\\b").append(regexs.get(i)).append("\\b|\\b").append(regexs.get(i).substring(0,1).toUpperCase()).append(regexs.get(i).substring(1,regexs.get(i).length())).append("\\b");
		}
		featureRegex = builder.toString();
		
		
	}
	/**
	 * 对正文文本进行预处理
	 * 1. 处理html中的转义字符为正常格式
	 * 2. 给引文标记
	 * @return
	 */
	public static String preprocess(String text){
		text = text.replace("&lt;", "<");
		text = text.replace("&gt;", ">");
		text = text.replace("&amp;", "&");
		text = text.replace("&quot;", "\"");
		text = text.replace("&nbsp;", " ");
		
/*		Pattern pattern = Pattern.compile("<a href=\"#BIB\\d+?\"|<a href=\"#s\\d+?\"", Pattern.DOTALL);
		Matcher matcher = pattern.matcher(text);
		if(matcher.find()){
			text = matcher.replaceAll(matcher.group()+" class=\"citation-tag\"");
			//System.out.println(text);
		}*/
		
		return text;
	}
	
	/**
	 * 找到正文中的代表作者的信息，并添加标记
	 * @return
	 */
	public static String addAuthorTag(String text){
		Pattern pattern = Pattern.compile(authorRegex);
		Matcher matcher = pattern.matcher(text);
		StringBuffer buffer = new StringBuffer();
		while(matcher.find()){
			//matcher.appendReplacement(buffer,"<subject-tag>");
			matcher.appendReplacement(buffer,"<subject>");
			//System.out.println(text);
		}
		matcher.appendTail(buffer);
		//text = text.replaceAll(author, "<a class=\"author-subject-tag\">$0</a>");
		return buffer.toString();
	}
	
	/**
	 * 将DFKI中的引文标记"()"，替换为"<citation>"
     * 需要的话，这里就算了,将section x.x也替换为一个citation标记
	 * @param citationTag 
	 * @return
	 */
	public static String addCitationTag(String text, String citationTag){
//		Pattern pattern = Pattern.compile("\\(\\s*?\\)|\\b[Ss]ection\\s*?\\d\\.?\\d?");
		Pattern pattern = Pattern.compile("\\(\\s*?\\)");
		Matcher matcher = pattern.matcher(text);
		StringBuffer buffer = new StringBuffer();
		while(matcher.find()){
			matcher.appendReplacement(buffer,citationTag);

			//matcher.appendReplacement(buffer,"<a class=\"author-subject-tag\">"+matcher.group()+"</a>");
			//System.out.println(text);
		}
		matcher.appendTail(buffer);
		//text = text.replaceAll(author, "<a class=\"author-subject-tag\">$0</a>");
		return buffer.toString();
	}
	
	/**
	 * 找到正文中的代表特征的词汇，并添加标记
	 * @return
	 */
	public static String addFeatureTag(String text){
		Pattern pattern = Pattern.compile(featureRegex);
		Matcher matcher = pattern.matcher(text);
		StringBuffer buffer = new StringBuffer();
		while(matcher.find()){
			matcher.appendReplacement(buffer,"<a class=\"feature-tag\">"+matcher.group()+"</a>");
			//System.out.println(text);
		}
		matcher.appendTail(buffer);
		//text = text.replaceAll(author, "<a class=\"author-subject-tag\">$0</a>");
		return buffer.toString();
	}
	
}

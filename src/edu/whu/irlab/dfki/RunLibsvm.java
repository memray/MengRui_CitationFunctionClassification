package edu.whu.irlab.dfki;

import edu.whu.irlab.classify.svm_train;
import edu.whu.irlab.dfki.data.etc.Data2HTMLProcessor;
import edu.whu.irlab.dfki.feature.FeatureProcessor;

import java.io.*;

/**
 * Created by Memray on 3/28/2015.
 */
public class RunLibsvm {

    //libsvm程序（train，grid等）的路径
    public String libsvmBasePath = RunFeatureExtract.ProjectBasePath + "svm" + File.separator + "libsvm" + File.separator + "windows" + File.separator;
    //libsvm的svm-train-macrof.exe的路径
    public static final String libsvmTrainExePath =  RunFeatureExtract.ProjectBasePath + "docs" + File.separator + "svm-train-macrof.exe";
    //工程的绝对路径
    public static String ProjectBasePath = System.getProperty("user.dir") + File.separator;
    public static String OutputBasePath = ProjectBasePath + "output" + File.separator;


    /**
     * Run LibSVM to predict
     */
    private void svm_train(String dataPath, String resultPath, double c, double g) {
        System.out.println("使用最优参数进行预测");

        System.out.println("使用svm_train对数据进行分类，并将交叉检验的结果输出到"+resultPath+"\n使用参数为 c=" + c + ", g=" + g);
        String ourTrainArgs;
        try {
            ourTrainArgs = " -s 0 -v 10 -t 2 -e 0.00001 -c " + c + " -g " + g + " " + dataPath;
            System.out.println(ourTrainArgs);
//            new svm_train().train(ourTrainArgs.split(" "));

            String command = libsvmTrainExePath + ourTrainArgs;

            Process process;
            System.out.println(command);
            process = Runtime.getRuntime().exec(command, null, new File(RunFeatureExtract.ProjectBasePath + "output"+File.separator));

            String line = "";
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    InputStream is = process.getErrorStream();
                }
            }).start();

            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate result report
     */
    private void generate_report(String predictDataPath, String reportOutputPath) {
        FeatureProcessor.createFeatureFile(RunFeatureExtract.dataPoints, RunFeatureExtract.FeatureCachePath,
                RunFeatureExtract.TargetFeatureName, true);
        new Data2HTMLProcessor().generateHTMLReport(RunFeatureExtract.dataPoints, predictDataPath, reportOutputPath);
/*        String dfkiScaleArgs = "-l 0 -u 1 dfki_feature.txt";
        svm_scale.scale(dfkiScaleArgs.split(" "), "dfki_feature.scale");
        String dfkiTrainArgs =
                "-s 0 -v 10 -t 2 -c 0.5 -g 0.5 dfki_feature.scale dfki_feature.model";
        svm_train.train(dfkiTrainArgs.split(" "), "output/dfki_feature_1.predict");*/
    }

    /**
     * Run grid.py for best parameters
     * @return
     */
    private double[] get_best_params(String dataPath) {
        // 定义返回数据，存储最优的c和g
        double[] parameters = new double[2];
        System.out.println("开始运行 grid.py");
        //command
        double log2cFloor = 2;
        double log2cCeiling = 22;
        double log2cStep = 0.5;

        double log2gFloor = -26;
        double log2gCeiling = -6;
        double log2gStep = 0.5;

        double iterationTimes = ((log2cCeiling - log2cFloor) / log2cStep + 1) * ((log2gCeiling - log2gFloor + 1) / log2gStep + 1);

        String command = null;
        /**
         * 区分windows和linux，区分普通方法(grid-macroF.py)和ensemble(grid-ensemble.py)方法
         */
        if(System.getProperties().getProperty("os.name").toLowerCase().indexOf("linux")!=-1) {
            //Linux, gnuplot自带
                command = "python " + libsvmBasePath + "grid-macroF.py -log2c " + log2cFloor + "," + log2cCeiling + "," + log2cStep + " -log2g " + log2gFloor + "," + log2gCeiling + "," + log2gStep
                        + " -v 10 -svmtrain  " + libsvmBasePath + "svm-train " + dataPath;
        }else{
            //Windows
                command = "cmd /c python " + libsvmBasePath + "grid-macroF.py -log2c " + log2cFloor + "," + log2cCeiling + "," + log2cStep + " -log2g " + log2gFloor + "," + log2gCeiling + "," + log2gStep
                        + " -v 10 -svmtrain  " + libsvmBasePath + "svm-train-macrof.exe -gnuplot " + ProjectBasePath + "svm"+ File.separator +"gnuplot"+ File.separator +"bin"+ File.separator +"pgnuplot.exe " + dataPath;
        }
        Process gridProcess;
        try {
            //gridProcess = Runtime.getRuntime().exec(command, null , new File("D:\\work\\PlayWorkspace\\MengRui_CitationFunctionClassiclassifyation\\svm\\libsvm\\windows\\"));
            System.out.println(command);
            gridProcess = Runtime.getRuntime().exec(command, null, new File(RunFeatureExtract.ProjectBasePath + "output"+File.separator));

            String line = "";
            double c = 0.0;
            double g = 0.0;
            double rate = 0.0;
            int count = 0;
            BufferedReader br = new BufferedReader(new InputStreamReader(gridProcess.getInputStream()));

            while ((line = br.readLine()) != null) {
                System.out.println(++count + " / " + (int) iterationTimes + " : " + line);
                if (line.indexOf("[local]") == -1)
                    continue;
                c = Double.parseDouble(line.substring(line.indexOf("c=") + 2, line.indexOf(", g=")));
                //System.out.println(c);
                g = Double.parseDouble(line.substring(line.indexOf("g=") + 2, line.indexOf(", rate=")));
                //System.out.println(g);
                rate = Double.parseDouble(line.substring(line.indexOf("rate=") + 5, line.indexOf(")")));
                //System.out.println(rate);
            }
            br.close();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    InputStream is = gridProcess.getErrorStream();
                }
            }).start();

/*            byte[] inBuffer = new byte[100];
            for (int i = 0; i > -1; i = gridProcess.getInputStream().read(inBuffer)) {
                // We have a new segment of input, so process it as a String..
                System.out.print(new String(inBuffer, 0, i));
            }*/

            gridProcess.waitFor();
            parameters[0] = c;
            parameters[1] = g;
            System.out.println("最佳参数为 c=" + c + ", g=" + g + ", rate=" + rate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  parameters;
    }

    public static void main(String[] args) {
        /**
         * 给定要分类的数据的路径
         */
        String fileName = "Our_top_600.libsvm";
        //输入文件的路径
        String dirPath = ProjectBasePath+"docs\\20150509_过滤ngram后实验屏蔽过拟合的影响\\20150508_Our_FilteredNgram_SubCitLinked_NoContext(79.052)\\";
        String dataPath = dirPath+fileName;
        //交叉检验的输出结果的路径
        String libsvmCVResultPath = dirPath+fileName+".result";
        //生成错例报告的路径
        String reportOutputPath = ProjectBasePath+"output\\visualization\\"+fileName+"_badcase.html";
        /**
         * 设定参数
         */
        double c = Math.pow(2,4.4);
        double g = Math.pow(2,-5);

        RunLibsvm run = new RunLibsvm();
        //run.get_best_params(dataPath);
        run.svm_train(dataPath, libsvmCVResultPath, c, g);

        run.generate_report(libsvmCVResultPath, reportOutputPath);

    }
}

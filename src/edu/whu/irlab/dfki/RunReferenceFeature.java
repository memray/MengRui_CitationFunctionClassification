package edu.whu.irlab.dfki;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import edu.whu.irlab.bean.Citation;
import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.bean.Reference;
import edu.whu.irlab.dfki.feature.FeatureProcessor;
import edu.whu.irlab.dfki.feature.ReferenceFeature.ReferenceFeatureUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lxb on 2015/4/20.
 * 抽取reference的feature
 */
public class RunReferenceFeature {

    //工程的绝对路径
    public static String ProjectBasePath = System.getProperty("user.dir") + File.separator;
    public static String OutputBasePath = ProjectBasePath + "output" + File.separator;
    // 原数据Data以及从数据库中取出的一些信息以及基本的初始化处理被缓存化到这里(提取了数据库信息，替换了标记等)
    public static String DataCachePath = ProjectBasePath + "output" + File.separator + "DFKIDataCache.dat";
    // 存放特征缓存文件的位置，包含DataCache中的数据以及额外抽取的特征数据
    public static String FeatureCachePath = ProjectBasePath + "output" + File.separator + "DFKIFeatureCache_Context.dat";

    // 存放生成的reference 缓存的目录
    public static String ReferenceCachePath = ProjectBasePath + "output" + File.separator + "Reference_Context.dat";
    public static ArrayList<DataPoint> dataPoints = new ArrayList<>();
    //指明是否需要进行词干提取，基本只对FeatureCache生成部分有影响。输出特征向量的一般需要额外处理
    public static Boolean Lemmatization = false;

    //读取文件中已经缓存了的reference信息
    public static List<Reference> readReferenceFile() {
        return null;
    }

    //生成reference信息,有的直接读取
    public void generateReferenceFile(ArrayList<Reference> referenceList) {
        File ReferenceFile = new File(ReferenceCachePath);
        if (ReferenceFile.exists()) {
            //如果引文信息文件存在的话，就直接赋值
            try {
                List<String> lines = FileUtils.readLines(ReferenceFile);
                for (String line : lines )
                    referenceList.add(JSON.parseObject(line,Reference.class));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            //如果引文信息不存在的话，生成这个文件
            /**
             * 1. 抽取特征，基本生成一次缓存后可以快速调用
             */
            FeatureProcessor.createFeatureFile(dataPoints, this.FeatureCachePath, "", Lemmatization);
            //维护一个hashset 用来存放一篇文章里的一个引文
            // 对于这么多citation来说，在同一文章里，作者和年份相同的时候，为相同的引文
            HashMap<String, Reference> referenceKey = new HashMap<String, Reference>();
            int citationNumber = 0;
            for (DataPoint dp : dataPoints) {
                //拼接 year_author_aclid
                String aclid = dp.getArticle().getAclid();
                //reference 的 label 根据的是citation的 label中 最重要的label来排布
                for (Citation citation : dp.getFeature().getCitationList()) {
                    //对每个citation 进行拼接
                    String referencek = citation.getYear() + "_" + citation.getAuthor() + "_" + aclid;
                    citation.setDataPoint(dp);//后面可能会用到之前处理过的特征，先把它放到citation里面
                    if (referenceKey.containsKey(referencek)) {
                        //如果这个生成的key已经存在，就把sentence赋进去，再把citation添加一下
                        Reference rf = referenceKey.get(referencek);
                        rf.setSentenceContext(rf.getSentenceContext() + " " + dp.getAclSentence().getContent());
                        rf.getCitations().add(citation);

                        //给reference的 label再添加新的
                        rf.getLabels().add(dp.getClassLabelNumber());

                        //判断一下，classlabel越小 就越重要 就要赋值进去
                        if (Integer.valueOf(dp.getClassLabelNumber()) < Integer.valueOf(rf.getClassLabelNumber()))
                            rf.setClassLabelNumber(dp.getClassLabelNumber());

                    } else {
                        //如果这个生成的key不存在，就把各项值先赋值进去
                        Reference rf = new Reference();
                        rf.setAclid(aclid);
                        rf.setAuthor(citation.getAuthor());
                        rf.setYear(citation.getYear());
                        rf.setSentenceContext(dp.getAclSentence().getContent());
                        List<Citation> citations = new ArrayList<Citation>();
                        citations.add(citation);
                        rf.setCitations(citations);
                        referenceKey.put(referencek, rf);

                        //给reference的 label再添加新的
                        ArrayList<String> labelList = new ArrayList<String>();
                        labelList.add(dp.getClassLabelNumber());
                        rf.setLabels(labelList);
                        //给datapoint赋值
                        rf.setClassLabelNumber(dp.getClassLabelNumber());

                    }
                    citationNumber++;
                }
            }

            //存储为json结构，逐行的写入到file中
            StringBuilder sb = new StringBuilder();
            for (String rKey : referenceKey.keySet()) {
                referenceList.add(referenceKey.get(rKey));//写入文件的时候，顺便赋值
                sb.append(JSONObject.toJSONString(referenceKey.get(rKey)));
                sb.append("\n");
            }
            try {
                FileUtils.write(ReferenceFile, sb);
                System.out.println("已经生成了 引文 特征文件");
                System.out.println(citationNumber);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

    }

    public static void main(String[] args) {
        ArrayList<Reference> referenceList = new ArrayList<Reference>();
        new RunReferenceFeature().generateReferenceFile(referenceList);
        //给每个reference 生成 不重复的 datapoints
        ReferenceFeatureUtils.getNotSameReferenceDatapoints(referenceList);

        //生成全局变量（附带抽取全局特征）
        ReferenceFeatureUtils.createNgramWords(referenceList);
        //生成全局大词表(附带抽取各单词特征)
        ReferenceFeatureUtils.getReferenceWordsFeature(referenceList);

        //生成weka特征
        ReferenceFeatureUtils.createWekaReference(referenceList);
    }
}

package edu.whu.irlab.dfki.feature;

import edu.whu.irlab.bean.Citation;
import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.bean.Attribute;
import edu.whu.irlab.bean.Instance;
import edu.whu.irlab.classify.svm_scale;
import edu.whu.irlab.dfki.RunFeatureExtract;
import org.apache.commons.io.FileUtils;
import weka.core.Instances;
import weka.core.converters.LibSVMSaver;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * 只负责将转化的特征中的所有数值转化为特征向量中的表示
 * Created by Memray on 2015/3/13.
 */
public class FeatureConverter {
    public static String VectorDataPath = "";
    public static String ScaledDataPath = "";
    public static String ModelDataPath = "";//model的输出路径
    public static String PredictDataPath = "";
    public static String FeatureListPath = "";//特征列表的位置

    //特征属性的数量
    public static int attributeNumber = 1;
    //特征属性的列表
    public static ArrayList<Attribute> attributeList = new ArrayList<>();
    //保存具体数据的列表
    public static ArrayList<Instance> instanceList = new ArrayList<>();

    /**
     * 负责去重分配属性Index的Hashmap
     */
    static HashMap<String, Integer> globalStringMap = new HashMap<>();

    /**
     * Arthar特征部分
     */
    //全局n-gram 词表
    static public HashMap<String, Integer> GlobalNgram = new HashMap<>();
    // 全局依存关系表，保存所有词汇间的依存关系，如 nsubj(outperform, algorithm)
    /**
     * Abu-Jbara特征部分
     */
    static public HashSet<String> GlobalDependencies = new HashSet<String>();
    //(只有Abu方案需要)Abu中需要遍历全部数据得到的全局词表，需要的是最近的距离引文最近的verb/Adjective/Adverb
    static public ArrayList<String> GlobalAbuWords = new ArrayList<String>();
    /**
     * 我们的特征抽取辅助部分
     * TODO 在这一层出现了对所有特征的toLowerCase
     */
    // 全局词表，key为词的形式，value为其id
    static public Hashtable<String, Integer> GlobalWordTable = new Hashtable<String, Integer>();

    public static ArrayList<Instance> getOurInstanceList(ArrayList<DataPoint> dataPoints) {
        for (DataPoint dataPoint : dataPoints) {
            instanceList.add(new Instance(attributeList, Integer.parseInt(dataPoint.getClassLabelNumber())));
        }
        //是否需要将context的特征进行合并
//        addContextFeaturesIntoGlobalFeatures(dataPoints);

        // 对ngram进行过滤
//        filterNgrams(dataPoints);

        //将特征转化为特征向量形式
        generateExportData(dataPoints);

        return instanceList;
    }

    /**
     * 将context中的特征加到全局特征中
     * @param dataPoints
     */
    private static void addContextFeaturesIntoGlobalFeatures(ArrayList<DataPoint> dataPoints) {

         for(DataPoint dp : dataPoints){
             CitationFeatureTemplate ft = dp.getFeature();

             /**
              * 第二部分，ngrams
              */
             dp.getFeature().getnGram().addAll(dp.getFeature().getContextnGram());

             /**
              * 第三部分，与主语和citation相连的动词、形容词、副词(介词、小品词 考虑去除),加前缀以区分
              */
             ft.getSubjectLinkedKeyWords().addAll(ft.getContextSubjectLinkedKeyWords());

            /**
             * 第一部分，改进布尔特征及数值特征
             */
             // 我们的特征主语cue(we,our,us,this..)
             ft.ourSubjectWords.addAll(ft.ourContextSubjectWords);
             if (ft.ourSubjectWords.size() != 0) {
                 ft.setOurSubjectBoolean(true);
                 ft.setOurSubjectWeight(ft.ourSubjectWords.size());
             } else {
                 ft.setOurSubjectBoolean(false);
             }
             // OurIdea特征(following,similar to, motivate,inspired..)
             ft.ourIdeaWords.addAll(ft.ourContextIdeaWords);
             if (ft.ourIdeaWords.size() != 0) {
                 ft.setOurIdeaBoolean(true);
                 ft.setOurIdeaWeight(ft.ourIdeaWords.size());
             } else {
                 ft.setOurIdeaBoolean(false);
             }
             // OurBasis特征(method,approach,model...)
             ft.ourBasisWords.addAll(ft.ourContextBasisWords);
             if (ft.ourBasisWords.size() != 0) {
                 ft.setOurBasisBoolean(true);
                 ft.setOurBasisWeight(ft.ourBasisWords.size());
             } else {
                 ft.setOurBasisBoolean(false);
             }
             // OurCompare特征
             ft.ourCompareWords.addAll(ft.ourContextCompareWords);
             if (ft.ourCompareWords.size() != 0) {
                 ft.setOurCompareBoolean(true);
                 ft.setOurCompareWeight(ft.ourCompareWords.size());
             } else {
                 ft.setOurCompareBoolean(false);
             }
             // OurResult特征
             ft.ourResultWords.addAll(ft.ourContextResultWords);
             if (ft.ourResultWords.size() != 0) {
                 ft.setOurResultBoolean(true);
                 ft.setOurResultWeight(ft.ourResultWords.size());
             } else {
                 ft.setOurResultBoolean(false);
             }

             // OurQuantity特征
             ft.ourQuantityWords.addAll(ft.ourContextQuantityWords);
             if (ft.ourQuantityWords.size() != 0) {
                 ft.setOurQuantityBoolean(true);
                 ft.setOurQuantityWeight(ft.ourQuantityWords.size());
             } else {
                 ft.setOurQuantityBoolean(false);
             }
             // OurFrequency特征
             ft.ourFrequencyWords.addAll(ft.ourContextFrequencyWords);
             if (ft.ourFrequencyWords.size() != 0) {
                 ft.setOurFrequencyBoolean(true);
                 ft.setOurFrequencyWeight(ft.ourFrequencyWords.size());
             } else {
                 ft.setOurFrequencyBoolean(false);
             }
             // OurTense特征
             ft.ourTenseWords.addAll(ft.ourContextTenseWords);
             if (ft.ourTenseWords.size() != 0) {
                 ft.setOurTenseBoolean(true);
                 ft.setOurTenseWeight(ft.ourTenseWords.size());
             } else {
                 ft.setOurTenseBoolean(false);
             }
             // OurExample特征
             ft.ourExampleWords.addAll( ft.ourContextExampleWords);
             if (ft.ourExampleWords.size() != 0) {
                 ft.setOurExampleBoolean(true);
                 ft.setOurExampleWeight(ft.ourExampleWords.size());
             } else {
                 ft.setOurExampleBoolean(false);
             }
             // OurSuggest特征
             ft.ourSuggestWords.addAll(ft.ourContextSuggestWords);
             if (ft.ourSuggestWords.size() != 0) {
                 ft.setOurSuggestBoolean(true);
                 ft.setOurSuggestWeight(ft.ourSuggestWords.size());
             } else {
                 ft.setOurSuggestBoolean(false);
             }
             // OurHedge特征
             ft.ourHedgeWords.addAll(ft.ourContextHedgeWords);
             if (ft.ourHedgeWords.size() != 0) {
                 ft.setOurHedgeBoolean(true);
                 ft.setOurHedgeWeight(ft.ourHedgeWords.size());
             } else {
                 ft.setOurHedgeBoolean(false);
             }

             // 对象cue
             ft.objectWords.addAll(ft.contextObjectWords);
             if (ft.objectWords.size() != 0) {
                 ft.setObjectBoolean(true);
                 ft.setObjectWeight(ft.objectWords.size());
             } else {
                 ft.setObjectBoolean(false);
             }
             // 工具、数据特征cue
             ft.toolWords.addAll(ft.contextToolWords);
             if (ft.toolWords.size() != 0) {
                 ft.setToolBoolean(true);
                 ft.setToolWeight(ft.toolWords.size());
             } else {
                 ft.setToolBoolean(false);
             }
             // 针对未来工作的future cue
             ft.futureWords.addAll(ft.contextFutureWords);
             if (ft.futureWords.size() != 0) {
                 ft.setFutureBoolean(true);
                 ft.setFutureWeight(ft.futureWords.size());
             } else {
                 ft.setFutureBoolean(false);
             }

             // 是否有大额的数字（多于一位）以及对应的次数
             ft.setNumberCount(ft.getNumberCount() + ft.getContextNumberCount());
             if (ft.getNumberCount() != 0) {
                 ft.setNumberBoolean(true);
             }

             // 是否有 百分比以及对应的次数
             ft.setPercentageCount(ft.getPercentageCount() + ft.getContextPercentageCount());
             if (ft.getPercentageCount() != 0) {
                 ft.setPercentageBoolean(true);
             }


         }
    }

    /**
     * 由于nGram中有大量的无用条目，因此需要在这里进行过滤
     * @param dataPoints
     */
    private static void filterNgrams(ArrayList<DataPoint> dataPoints) {
        /**
         * 1. 基本操作
         *   1.1 toLowerCase()
         *   1.2 去除掉ngram中的citation后缀
         *   1.3 前后的非字符信息
         *   1.4 stem
         */
        String temp = "";
        for(DataPoint dataPoint : dataPoints)
            for(int i = 0 ; i < dataPoint.getFeature().nGram.size(); i++) {
//                System.out.println(dataPoint.getFeature().nGram.get(i));
                temp = dataPoint.getFeature().nGram.get(i);
//                temp = temp.trim().toLowerCase();
                temp = FeatureExtractFunctions.citationTagRemoval(temp);
                temp = Citation.strip(temp, "\\W");
                temp = FeatureExtractFunctions.stemmer.stemWord(temp);
//                System.out.println(temp);
                dataPoint.getFeature().nGram.set(i, temp);
            }
        /**
         * 2. 高级过滤操作
         */
        premiumNgramFilter(dataPoints);
    }

    private static void premiumNgramFilter(ArrayList<DataPoint> dataPoints) {
        generateGlobalNgramMap(dataPoints, false);
        // 2. 读入停用词
        ArrayList<String> filter = new ArrayList<>();
        try {
            List<String> lines = FileUtils.readLines(new File(RunFeatureExtract.ProjectBasePath + "corpus" + File.separator + "stopword.txt"), "utf8");
            for(String line : lines) {
                line = line.trim();
                filter.add(line.trim());
                line = line.substring(0,1).toUpperCase()+line.substring(1);
                filter.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 3. 先过滤掉长度为1以及非字母的词
        for(Map.Entry<String, Integer> entry : GlobalNgram.entrySet()){
            if (entry.getKey().length()==1)
                filter.add(entry.getKey());
            Boolean containLetter = false;
            for(int i = 0; i < entry.getKey().length(); i++)
                if(Character.isAlphabetic(entry.getKey().charAt(i))){
                    containLetter = true;
                    break;
                }
            if(!containLetter)
                filter.add(entry.getKey());
        }

        filter.forEach(GlobalNgram::remove);

        // 4. 过滤掉词频前10%低的词以及词频为1的词
        ArrayList<Map.Entry<String, Integer>> ngramList = new ArrayList<>(GlobalNgram.entrySet());
        // 按照升序排序
        /*Collections.sort(ngramList, (a,b)->b.getValue().compareTo(a.getValue()));
        for(int i = 0 ; i < ngramList.size(); i++)
            if(i > ngramList.size()*0.9 || ngramList.get(i).getValue()==1)
                GlobalNgram.remove(ngramList.get(i));
        */
        // 5. 将过滤后的词保存到feature中
        for(DataPoint dataPoint : dataPoints) {
            filter = new ArrayList<>();
            for (int i = 0; i < dataPoint.getFeature().nGram.size(); i++) {
                if (!GlobalNgram.containsKey(dataPoint.getFeature().nGram.get(i)))
                    filter.add(dataPoint.getFeature().nGram.get(i));
            }
            dataPoint.getFeature().nGram.removeAll(filter);
        }
    }

    /**
     * Scale the feature_file
     */
    public static void feature_scale(String vectorDataPath, String ScaledDataPath) {
        String ourScaleArgs = "-l 0 -u 1 " + VectorDataPath;
        svm_scale.scale(ourScaleArgs.split(" "), ScaledDataPath);
    }

    /**
     * 生成AttributeList，并针对每一个Attribute为每一个数据instance转化数据
     *
     * @param dataPoints
     */
    private static void generateExportData(ArrayList<DataPoint> dataPoints) {

        /**
         * 1. reimplented features from Athar (2011)
         */
        //dependency pairs
        transformDataToVector(dataPoints, "dependencyRelations");
        //n-grams
        transformDataToVector(dataPoints, "nGram");

        /**
         * 2. reimplented features from Dong & Schaefer (2011)
         */
        /*
         * Textual Features 布尔特征及频率特征
         */
        transformDataToVector(dataPoints, "subjectBoolean");
        transformDataToVector(dataPoints, "quantityBoolean");
        transformDataToVector(dataPoints, "frequencyBoolean");
        transformDataToVector(dataPoints, "tenseBoolean");
        transformDataToVector(dataPoints, "exampleBoolean");
        transformDataToVector(dataPoints, "suggestBoolean");
        transformDataToVector(dataPoints, "hedgeBoolean");
        transformDataToVector(dataPoints, "ideaBoolean");
        transformDataToVector(dataPoints, "basisBoolean");
        transformDataToVector(dataPoints, "compareBoolean");
        transformDataToVector(dataPoints, "resultBoolean");
        transformDataToVector(dataPoints, "neighborSubjectBoolean");

        transformDataToVector(dataPoints, "subjectWeight");
        transformDataToVector(dataPoints, "quantityWeight");
        transformDataToVector(dataPoints, "frequencyWeight");
        transformDataToVector(dataPoints, "tenseWeight");
        transformDataToVector(dataPoints, "exampleWeight");
        transformDataToVector(dataPoints, "suggestWeight");
        transformDataToVector(dataPoints, "hedgeWeight");
        transformDataToVector(dataPoints, "ideaWeight");
        transformDataToVector(dataPoints, "basisWeight");
        transformDataToVector(dataPoints, "compareWeight");
        transformDataToVector(dataPoints, "resultWeight");
        transformDataToVector(dataPoints, "neighborSubjectWeight");
        /*
         * Physical Features
         */
        //Location(Section)
        //Popularity(number in the sentence)
        transformDataToVector(dataPoints, "popularity");
        //Density(sentence and neighbor sentences)
        transformDataToVector(dataPoints, "citationDensity");
        //AvgDens(average of Density)
        transformDataToVector(dataPoints, "avgDens");
        /*
         * Syntactic Features
         */
        transformDataToVector(dataPoints, "syntacticFeature_1");
        transformDataToVector(dataPoints, "syntacticFeature_2");
        transformDataToVector(dataPoints, "syntacticFeature_3");
        transformDataToVector(dataPoints, "syntacticFeature_4");
        transformDataToVector(dataPoints, "syntacticFeature_5");
        transformDataToVector(dataPoints, "syntacticFeature_6");
        transformDataToVector(dataPoints, "syntacticFeature_7");

        /**
         * 3. Abu-Jbara的特征
         */
        //1 在Dong特征中
        //2 在Dong特征中
        //3
        transformDataToVector(dataPoints, "citationLinkedKeyAbuWords");
        //4
        transformDataToVector(dataPoints, "selfCitationBoolean");
        //5
        transformDataToVector(dataPoints, "firstPersonPrononeBoolean");
        transformDataToVector(dataPoints, "thirdPersonPrononeBoolean");
        //6
        transformDataToVector(dataPoints, "negationCueBoolean");
        //7
        transformDataToVector(dataPoints, "speculationBoolean");
        //8
        transformDataToVector(dataPoints, "closestSubjectivityAbuCue");
        //9
        transformDataToVector(dataPoints, "contraryExpressionsBoolean");
        //10
        transformDataToVector(dataPoints, "sectionNumber");
        //11 dependencyRelations放在Charles那里


        /**
         * 4. Charles Jochim的特征部分
         */
        /*
         * reimplented features from Charles Jochim
         */
        // is-constituent
        transformDataToVector(dataPoints, "constituentBoolean");
        // other-contrast
        transformDataToVector(dataPoints, "otherContrastBoolean");
        // self-comp
        transformDataToVector(dataPoints, "selfCompBoolean");
        // other-comp
        transformDataToVector(dataPoints, "otherCompBoolean");
        // self-good
        transformDataToVector(dataPoints, "selfGoodBoolean");
        // but
        transformDataToVector(dataPoints, "hasButBoolean");
        // has-comp./sup.
        transformDataToVector(dataPoints, "comparativeBoolean");
        // modal
        transformDataToVector(dataPoints, "modalBoolean");
        // has-1stPRP
        // has-3rdPRP
        // root
        transformDataToVector(dataPoints, "rootWord");
        // main verb
        transformDataToVector(dataPoints, "mainVerb");
        // positive-words
        transformDataToVector(dataPoints, "positiveWords");
        // negative-words
        transformDataToVector(dataPoints, "negativeWords");
        // has cf.
        transformDataToVector(dataPoints, "hasCfBoolean");
        // has_resource
        transformDataToVector(dataPoints, "hasResourceBoolean");
        // has_tool
        transformDataToVector(dataPoints, "hasToolBoolean");
        // sentence loc.: 1st qtr, middle half, last qtr
        transformDataToVector(dataPoints, "sentenceLocation");

        /*
         * Reference features:
         */
        // reference age
        transformDataToVector(dataPoints, "referenceAge");
        // number of authors
        transformDataToVector(dataPoints, "authorNumber");
        // publication keywords (taken from name of publication [journal, proceedings]
        //    or from title [book, other]
        transformDataToVector(dataPoints, "publicationKeyWords");
        // self cite (also used by Teufel)
        // publication type

        ////////////////////////////////////////////////#
        // reimplented features from Teufel et al. (2006)
        ////////////////////////////////////////////////#
        // Boolean features on automatically extracted cue phrases
        // attempt to automatially duplicate manual process from Teufel et al., 2006
        //  i.e., one feature per class label
        // conceptual
        transformDataToVector(dataPoints, "hasConceptualCue");
        // operational
        transformDataToVector(dataPoints, "hasOperational");
        // evolutionary
        transformDataToVector(dataPoints, "hasEvolutionary");
        // juxtapositional
        transformDataToVector(dataPoints, "hasJuxtapositional");
        // organic
        transformDataToVector(dataPoints, "hasOrganic");
        // perfunctory
        transformDataToVector(dataPoints, "hasPerfunctory");
        // confirmative
        transformDataToVector(dataPoints, "hasConfirmative");
        // negational
        transformDataToVector(dataPoints, "hasNegational");
        // tense
        transformDataToVector(dataPoints, "tense");
        // voice
        transformDataToVector(dataPoints, "hasVoice");
        // modality, binary modal/no modal
        //  modal verb or None
        transformDataToVector(dataPoints, "modalWords");
        // paper loc.   binnedValues=0.25,0.5,0.75
        transformDataToVector(dataPoints, "paperLocation");

        /**
         * 5. 我们提出的改进特征部分
         */
        /*
         * 第一部分，改进布尔特征及数值特征
         */
        transformDataToVector(dataPoints, "ourSubjectBoolean");
        transformDataToVector(dataPoints, "ourSubjectWeight");
        transformDataToVector(dataPoints, "ourSubjectWords");

        transformDataToVector(dataPoints, "ourIdeaBoolean");
        transformDataToVector(dataPoints, "ourIdeaWeight");
        transformDataToVector(dataPoints, "ourIdeaWords");

        transformDataToVector(dataPoints, "ourBasisBoolean");
        transformDataToVector(dataPoints, "ourBasisWeight");
        transformDataToVector(dataPoints, "ourBasisWords");

        transformDataToVector(dataPoints, "toolBoolean");
        transformDataToVector(dataPoints, "toolWeight");
        transformDataToVector(dataPoints, "toolWords");

        transformDataToVector(dataPoints, "ourCompareBoolean");
        transformDataToVector(dataPoints, "ourCompareWeight");
        transformDataToVector(dataPoints, "ourCompareWords");

        transformDataToVector(dataPoints, "ourQuantityBoolean");
        transformDataToVector(dataPoints, "ourQuantityWeight");
        transformDataToVector(dataPoints, "ourQuantityWords");

        transformDataToVector(dataPoints, "ourFrequencyBoolean");
        transformDataToVector(dataPoints, "ourFrequencyWeight");
        transformDataToVector(dataPoints, "ourFrequencyWords");

        transformDataToVector(dataPoints, "ourTenseBoolean");
        transformDataToVector(dataPoints, "ourTenseWeight");
        transformDataToVector(dataPoints, "ourTenseWords");

        transformDataToVector(dataPoints, "ourExampleBoolean");
        transformDataToVector(dataPoints, "ourExampleWeight");
        transformDataToVector(dataPoints, "ourExampleWords");

        transformDataToVector(dataPoints, "ourSuggestBoolean");
        transformDataToVector(dataPoints, "ourSuggestWeight");
        transformDataToVector(dataPoints, "ourSuggestWords");

        transformDataToVector(dataPoints, "ourHedgeBoolean");
        transformDataToVector(dataPoints, "ourHedgeWeight");
        transformDataToVector(dataPoints, "ourHedgeWords");

        transformDataToVector(dataPoints, "ourResultBoolean");
        transformDataToVector(dataPoints, "ourResultWeight");
        transformDataToVector(dataPoints, "ourResultWords");

        transformDataToVector(dataPoints, "objectBoolean");
        transformDataToVector(dataPoints, "objectWeight");
        transformDataToVector(dataPoints, "objectWords");

        transformDataToVector(dataPoints, "futureBoolean");
        transformDataToVector(dataPoints, "futureWeight");
        transformDataToVector(dataPoints, "futureWords");
        //数字特征：是否存在大数字以及数字百分比出现几次的特征
        transformDataToVector(dataPoints, "numberBoolean");
        transformDataToVector(dataPoints, "numberCount");

        transformDataToVector(dataPoints, "percentageBoolean");
        transformDataToVector(dataPoints, "percentageCount");
        /*
         * 第二部分，句子中各种词汇的特征，放到最后
         */
        //与subject和citation相连的关键词
        /*
         * 第三部分，改进的物理特征
         */
        transformDataToVector(dataPoints, "sentenceLength");
        transformDataToVector(dataPoints, "shortSentenceNumber");

        transformDataToVector(dataPoints, "ShortSentenceAvgLength");

        /*
         * 我们改进特征的第二部分，句子中各种词汇的特征，放到最后
         */
        //与subject和citation相连的关键词和dependencies
        transformDataToVector(dataPoints, "subjectLinkedToCitationBoolean");
        transformDataToVector(dataPoints, "subjectCitationLinkedWords");

        transformDataToVector(dataPoints, "subjectLinkedKeyWords");
        transformDataToVector(dataPoints, "citationLinkedKeyWords");

        transformDataToVector(dataPoints, "subjectLinkedDependencies");
        transformDataToVector(dataPoints, "citationLinkedDependencies");
        /*
         * 前后句中的Citation以及个数
         */
        transformDataToVector(dataPoints, "citationNumber");
        transformDataToVector(dataPoints, "previousSentenceCitationNumber");
        transformDataToVector(dataPoints, "followingSentenceCitationNumber");
        transformDataToVector(dataPoints, "previousSentenceRelated");
        transformDataToVector(dataPoints, "followingSentenceRelated");
        transformDataToVector(dataPoints, "contextnGram");
        transformDataToVector(dataPoints, "contextSubjectLinkedKeyWords");
        /*
         * 上下文特征
         */
        transformDataToVector(dataPoints, "ourContextSubjectWords");
        transformDataToVector(dataPoints, "ourContextSubjectBoolean");
        transformDataToVector(dataPoints, "ourContextSubjectWeight");
        transformDataToVector(dataPoints, "ourContextIdeaWords");
        transformDataToVector(dataPoints, "ourContextIdeaBoolean");
        transformDataToVector(dataPoints, "ourContextIdeaWeight");
        transformDataToVector(dataPoints, "ourContextBasisWords");
        transformDataToVector(dataPoints, "ourContextBasisBoolean");
        transformDataToVector(dataPoints, "ourContextBasisWeight");
        transformDataToVector(dataPoints, "ourContextCompareWords");
        transformDataToVector(dataPoints, "ourContextCompareBoolean");
        transformDataToVector(dataPoints, "ourContextCompareWeight");
        transformDataToVector(dataPoints, "ourContextResultWords");
        transformDataToVector(dataPoints, "ourContextResultBoolean");
        transformDataToVector(dataPoints, "ourContextResultWeight");
        transformDataToVector(dataPoints, "ourContextQuantityWords");
        transformDataToVector(dataPoints, "ourContextQuantityBoolean");
        transformDataToVector(dataPoints, "ourContextQuantityWeight");
        transformDataToVector(dataPoints, "ourContextFrequencyWords");
        transformDataToVector(dataPoints, "ourContextFrequencyBoolean");
        transformDataToVector(dataPoints, "ourContextFrequencyWeight");
        transformDataToVector(dataPoints, "ourContextTenseWords");
        transformDataToVector(dataPoints, "ourContextTenseBoolean");
        transformDataToVector(dataPoints, "ourContextTenseWeight");
        transformDataToVector(dataPoints, "ourContextExampleWords");
        transformDataToVector(dataPoints, "ourContextExampleBoolean");
        transformDataToVector(dataPoints, "ourContextExampleWeight");
        transformDataToVector(dataPoints, "ourContextSuggestWords");
        transformDataToVector(dataPoints, "ourContextSuggestBoolean");
        transformDataToVector(dataPoints, "ourContextSuggestWeight");
        transformDataToVector(dataPoints, "ourContextHedgeWords");
        transformDataToVector(dataPoints, "contextOurHedgeBoolean");
        transformDataToVector(dataPoints, "contextOurHedgeWeight");
        transformDataToVector(dataPoints, "contextObjectWords");
        transformDataToVector(dataPoints, "contextObjectBoolean");
        transformDataToVector(dataPoints, "contextObjectWeight");
        transformDataToVector(dataPoints, "contextToolWords");
        transformDataToVector(dataPoints, "contextToolBoolean");
        transformDataToVector(dataPoints, "contextToolWeight");
        transformDataToVector(dataPoints, "contextFutureWords");
        transformDataToVector(dataPoints, "contextFutureBoolean");
        transformDataToVector(dataPoints, "contextFutureWeight");
        transformDataToVector(dataPoints, "contextNumberCount");
        transformDataToVector(dataPoints, "contextNumberBoolean");
        transformDataToVector(dataPoints, "contextPercentageCount");
        transformDataToVector(dataPoints, "contextPercentageBoolean");

        transformDataToVector(dataPoints, "contextSubjectLinkedKeyWords");
        transformDataToVector(dataPoints, "contextnGram");

    }

    /**
     * 输入给定field的名称，自动转换并添加到特征向量(instance)中对应数值
     *
     * @param dataPoints
     * @param attributeName
     */
    private static void transformDataToVector(ArrayList<DataPoint> dataPoints, String attributeName) {
        /**
         *  是否要在转化后的属性名中保留attributeName，保留的话许多相同值的特征会被区分开，如we这个词会在多个特征组中同时出现
         *  如果保留则效果为：attributeName + "_" + value1
         */
        boolean retainAttributeName = RunFeatureExtract.retainAttributeName;
        Attribute.ValueType valueType = null;
        CitationFeatureTemplate ft = dataPoints.get(0).getFeature();
        Object object = getValueByInvokeGetterMethod(ft, attributeName);
        if (object instanceof Boolean)
            valueType = Attribute.ValueType.BOOLEAN;
        else if (object instanceof Integer)
            valueType = Attribute.ValueType.INTEGER;
        else if (object instanceof Float)
            valueType = Attribute.ValueType.FLOAT;
        else if (object instanceof Double)
            valueType = Attribute.ValueType.DOUBLE;
        else if (object instanceof String)
            valueType = Attribute.ValueType.STRING;
        else if (object instanceof List)
            valueType = Attribute.ValueType.LIST;
        else
            valueType = Attribute.ValueType.STRING;

        /**
         * 针对不同的数据类型，进行不同的处理
         * BOOLEAN和INTEGER直接创建对应的Attribute并在instance中添加转化后的数值
         * STRING型的和LIST型的都需要在全局HashSet中添加并分配在向量空间中的index
         */
        String key = "", value = "";
        Integer index;
        if (valueType == Attribute.ValueType.BOOLEAN) {
            Attribute attribute = new Attribute(attributeName, "", valueType, attributeNumber);
            attributeList.add(attribute);

            for (int i = 0; i < dataPoints.size(); i++)
                instanceList.get(i).getValues().put(attributeNumber, booleanToDouble((Boolean) getValueByInvokeGetterMethod(dataPoints.get(i).getFeature(), attributeName)));

            attributeNumber++;
        } else if (valueType == Attribute.ValueType.INTEGER) {
            Attribute attribute = new Attribute(attributeName, "", valueType, attributeNumber);
            attributeList.add(attribute);

            for (int i = 0; i < dataPoints.size(); i++)
                instanceList.get(i).getValues().put(attributeNumber, Double.parseDouble(Integer.toString((Integer) getValueByInvokeGetterMethod(dataPoints.get(i).getFeature(), attributeName))));

            attributeNumber++;
        } else if (valueType == Attribute.ValueType.FLOAT) {
            Attribute attribute = new Attribute(attributeName, "", valueType, attributeNumber);
            attributeList.add(attribute);

            for (int i = 0; i < dataPoints.size(); i++)
                instanceList.get(i).getValues().put(attributeNumber, Double.parseDouble(Float.toString((Float) getValueByInvokeGetterMethod(dataPoints.get(i).getFeature(), attributeName))));

            attributeNumber++;
        } else if (valueType == Attribute.ValueType.DOUBLE) {
            Attribute attribute = new Attribute(attributeName, "", valueType, attributeNumber);
            attributeList.add(attribute);

            for (int i = 0; i < dataPoints.size(); i++)
                instanceList.get(i).getValues().put(attributeNumber, (Double) getValueByInvokeGetterMethod(dataPoints.get(i).getFeature(), attributeName));

            attributeNumber++;
        } else if (valueType == Attribute.ValueType.STRING) {

            for (int i = 0; i < dataPoints.size(); i++) {
                // String的具体内容
                value = (String) getValueByInvokeGetterMethod(dataPoints.get(i).getFeature(), attributeName);
                value = Citation.strip(value, "\\W");
                value = FeatureExtractFunctions.citationTagRemoval(value);
                // 对属性名进行去重
                if(retainAttributeName)
                    key = attributeName + "_" + value;
                else
                    key = value;
                if (value != null && !value.isEmpty()) {

                    if (globalStringMap.containsKey(key))
                        index = globalStringMap.get(key);
                    else {
                        globalStringMap.put(key, attributeNumber);
                        index = attributeNumber;
                        Attribute attribute = new Attribute(attributeName, value, valueType, index);
                        attributeList.add(attribute);
                        attributeNumber++;
                    }
                    //在TreeMap<Integer, Double> values中对应的index位置set值=1
                    instanceList.get(i).getValues().put(index, 1.0);
                }
            }

        } else if (valueType == Attribute.ValueType.LIST) {
            //对于LIST类型，基本就是遍历转化成多个STRING型
            for (int i = 0; i < dataPoints.size(); i++) {
                ArrayList<String> values = (ArrayList<String>) getValueByInvokeGetterMethod(dataPoints.get(i).getFeature(), attributeName);
                for (String value1 : values) {
                    value1 = Citation.strip(value1, "\\W");
                    value1 = FeatureExtractFunctions.citationTagRemoval(value1);
                    if(retainAttributeName)
                        key = attributeName + "_" + value1;
                    else
                        key = value1;

                    if (globalStringMap.containsKey(key))
                        index = globalStringMap.get(key);
                    else {
                        globalStringMap.put(key, attributeNumber);
                        index = attributeNumber;
                        Attribute attribute = new Attribute(attributeName, value1, valueType, index);
                        attributeList.add(attribute);
                        attributeNumber++;
                    }
                    instanceList.get(i).getValues().put(index, 1.0);
                }
            }
        }
    }


    /**
     * 输出为Charles Jochim的Feature Vector
     *
     * @param dataPoints Data records
     */
    private static void exportJochimFeatureVectors(ArrayList<DataPoint> dataPoints, String path, Boolean stem) {
        //因为要引用n-gram 所以要把生成的 n-gram 全局词表引入进来
        FeatureConverter.generateGlobalNgramMap(dataPoints, stem);
        //因为要引用Athar的方法，所以要生成引入Dependency词表
        FeatureConverter.generateGlobalDependencyMap(dataPoints);
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(path), false), "utf8"));
            for (DataPoint dp : dataPoints) {
                writer.write(dp.getFeature().toJochimFeatureVector() + "\n");
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 输出为Ulrich的Feature Vector
     *
     * @param dataPoints Data records
     * @param path       output path
     * @throws Exception IO
     */
    private static void exportDFKIFeatureVectors(ArrayList<DataPoint> dataPoints, String path) throws Exception {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(path), false), "utf8"));
        for (DataPoint dp : dataPoints) {
            writer.write(dp.getFeature().toDFKIFeatureVector());
        }
        writer.close();
    }

    /**
     * 输出为我们的特征vector
     *
     * @param dataPoints Data records
     * @param path       output path
     * @param stemmed    whether need stemming
     * @throws Exception
     */
    private static void exportOurFeatureVectors(ArrayList<DataPoint> dataPoints, String path, boolean stemmed) throws Exception {
        FeatureConverter.generateGlobalWordMap(dataPoints, stemmed);
        //因为要引用n-gram 所以要把生成的 n-gram 全局词表引入进来
        FeatureConverter.generateGlobalNgramMap(dataPoints, stemmed);
        //因为要引用Abu的方法，所以要把Abu生成的全局词表给引入进来
        FeatureConverter.generateGlobalDependencyMap(dataPoints);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(path), false), "utf8"));
        for (DataPoint dp : dataPoints) {
            writer.write(dp.getFeature().toOurFeatureVector() + "\n");
        }
        writer.close();
    }

    /**
     * 输出为Abu的特征vector
     *
     * @param dataPoints Data records
     * @param path       output path
     * @throws Exception
     */
    private static void exportAbuFeatureVectors(ArrayList<DataPoint> dataPoints, String path) throws Exception {
        //给全局Dependency词表赋值
        FeatureConverter.generateGlobalDependencyMap(dataPoints);
        //给全局“最近名词副词形容词”词表赋值
        FeatureConverter.generateGlobalAbuWords(dataPoints);

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(path), false), "utf8"));
        for (DataPoint dp : dataPoints) {
            writer.write(dp.getFeature().toAbuJbaraFeatureVector() + "\n");
        }
        writer.close();
    }

    /**
     * 根据所有特征，生成全局词表 将词表及对应词频一并输出
     * 同时 如果指定为需要stem，则将存储在词表中的内容也替换为stemmed后的形式，不用再二次stem
     * (由于后来又添加了许多新的词表，尤其是有HashSet形式的。。所以也设置在extract过程决定是否进行stem)
     */
    public static void generateGlobalWordMap(ArrayList<DataPoint> dataPoints,
                                             Boolean stem) {
        try {
            int count = 0;
            String temp = "";
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                Class featureTemplate = dp.getFeature().getClass();
                Field[] fs = featureTemplate.getDeclaredFields();
                for (int j = 0; j < fs.length; j++) {
                    Field f = fs[j];
                    f.setAccessible(true); // 设置些属性是可以访问的
                    if (f.getName().toLowerCase().endsWith("words")) {// 说明是需要添加词表的属性
                        ArrayList<String> words = (ArrayList<String>) f.get(dp
                                .getFeature());// 得到此属性的值

                        for (int k = 0; k < words.size(); k++) {
                            temp = words.get(k);
                            if (stem) {
                                temp = FeatureExtractFunctions.stemmer.stemWord(temp);
                                words.set(k, temp);
                            }
                            if (!GlobalWordTable.containsKey(temp))
                                GlobalWordTable.put(temp, count++);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据所有特征，生成n-gram 全局词表
     * 同时统计其词频
     */
    public static void generateGlobalNgramMap(ArrayList<DataPoint> dataPoints, Boolean stem) {
        try {
            String temp = "";
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                Class featureTemplate = dp.getFeature().getClass();
                Field[] fs = featureTemplate.getDeclaredFields();
                for (int j = 0; j < fs.length; j++) {
                    Field f = fs[j];
                    f.setAccessible(true); // 设置些属性是可以访问的
                    //以ngram结尾的field是所需特征
                    if (f.getName().toLowerCase().endsWith("ngram")) {
                        Object value = f.get(dp.getFeature());
                        ArrayList<String> valueList = (ArrayList<String>) value;
                        for (String word : valueList) {
                            if (stem)
                                temp = FeatureExtractFunctions.stemmer.stemWord(word);
                            else
                                temp = word;
                            if(GlobalNgram.containsKey(temp))
                                GlobalNgram.put(temp, GlobalNgram.get(temp)+1);
                            else
                                GlobalNgram.put(temp, 1);
                        }
                    }
                }
            }
//            System.out.println(GlobalNgram.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 根据HashMap<String, Integer> dependencyRelations
     * 生成Dependencies的全局词表HashSet<String> GlobalDependencies,将动词词表及对应词频一并输出
     * 是否进行lemmatize由之前生成dependencyRelationCount()的过程决定
     *
     * @param dataPoints
     */
    public static void generateGlobalDependencyMap(
            ArrayList<DataPoint> dataPoints) {
        try {
            int count = 0;
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                List<String> dependencies = dp.getFeature().dependencyRelations;
                for (String word : dependencies) {
//                    System.out.println(word);
                    GlobalDependencies.add(word);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建GlobalAbuWordTable词表，主要只有最近的 名词、形容词、副词
     * 需要lemmatized
     *
     * @param dataPoints
     */
    public static void generateGlobalAbuWords(
            ArrayList<DataPoint> dataPoints) {
        try {
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                ArrayList<String> abuWords = dp.getFeature().citationLinkedKeyAbuWords;
                //进行stem
                for (String word : abuWords)
                    GlobalAbuWords.add(FeatureExtractFunctions.stemmer.stemWord(word.toLowerCase()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 触发指定方法名的方法，返回对应的对象
     */
    public static Object getValueByInvokeGetterMethod(Object owner, String fieldname) {
        Class ownerClass = owner.getClass();

        Method method = null;
        Object object = null;
        try {
            method = ownerClass.getMethod(getGetter(fieldname));
            object = method.invoke(owner);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return object;
    }

    /**
     * 获取到对应field名的getter方法
     *
     * @param fieldName
     * @return
     */
    public static String getGetter(String fieldName) {

        if (fieldName == null || fieldName.length() == 0) {
            return null;
        }

        /* If the second char is upper, make 'get' + field name as getter name. For example, eBlog -> geteBlog */
        if (fieldName.length() > 2) {
            String second = fieldName.substring(1, 2);
            if (second.equals(second.toUpperCase())) {
                return new StringBuffer("get").append(fieldName).toString();
            }
        }

        /* Common situation */
        fieldName = new StringBuffer("get").append(fieldName.substring(0, 1).toUpperCase())
                .append(fieldName.substring(1)).toString();

        return fieldName;
    }

    /**
     * boolean值转int
     *
     * @param b
     * @return
     */
    public static Double booleanToDouble(boolean b) {
        if (b) {
            return 1.0;
        } else {
            return 0.0;
        }
    }

    /**
     * 输出index - AttributeName的对照表
     */
    public static void exportAttributeList(Instances instances, String outputPath) {
        int outputIndex = 0;
        StringBuffer lineBuffer = new StringBuffer();

        ArrayList<weka.core.Attribute> attributes = instances.getAttributes();
        outputIndex = 0;
        for (weka.core.Attribute attribute : attributes){
            lineBuffer.append(++outputIndex).append("\t").append(attribute.name()).append("\n");
        }

        try {
            FileUtils.writeStringToFile(new File(outputPath), lineBuffer.toString(), "utf8", false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * 对指定特征集输出其特征向量，包括libsvm文件和一个index-name对照列表
     * 会在output文件夹下建立一个featureSetName文件夹，并将输出libsvm保存到该文件夹下，以fileName为文件名
     * @param instances
     * @param outputPath
     */
    public static void exportLibsvmFeatureVector(Instances instances, String outputPath) {
        int outputIndex = 0;
        StringBuffer lineBuffer = new StringBuffer();
//        FeatureListPath = RunFeatureExtract.OutputBasePath + featureSetName + "_feature_list.txt";

        /**
         * 遍历每一个实例并输出
         */
        StringBuffer stringBuffer = new StringBuffer();
        LibSVMSaver saver = new LibSVMSaver();
        saver.setInstances(instances);
        try {
            saver.setDestination(new File(outputPath));
//            LibSVMSaver.runFileSaver(saver, args.split("\\s"));
            for (int i = 0; i < instances.numInstances(); i++)
                stringBuffer.append(saver.instanceToLibsvm(instances.instance(i))).append("\n");

            FileUtils.writeStringToFile(new File(outputPath), stringBuffer.toString(), "utf8", false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static class GlobalIndexFactory {
        /**
         * 根据输出返回对应的index
         * 在map中存在则直接返回，否则在map中添加
         * @param string
         * @return
         *//*
 /*        static Integer getGlobalStringIndex(String string){
            if(globalStringMap.containsKey(string))
                return globalStringMap.get(string);
            else{
                globalStringMap.put(string, attributeNumber);
                attributeNumber++;
                return attributeNumber-1;
            }
        }*/

    }
}

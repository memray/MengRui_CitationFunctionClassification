package edu.whu.irlab.dfki.feature;

import java.io.*;
import java.util.ArrayList;

import edu.whu.irlab.bean.DataPoint;

public class FeatureProcessor {
    /**
     * Extract feature and output to Feature_File
     *
     * @param dataPoints        Data records
     * @param featureCachePath   the data with extracted features
     *                          if the file exists, then read this file and create feature vectors
     *                          or re-extract features and output features to this file
     * @param featureTemplateName   Specify the target feature pattern
     *                          "our" means output to our_feature
     *                          "dfki" means to Ulrich's feature
     *                          "abu" means to Amjad Abu-Jbara's feature
     *                          "jochim" refers to Charles Jochim & Hinrich Schütze's feature
     */
    public static void createFeatureFile(ArrayList<DataPoint> dataPoints, String featureCachePath, String featureTemplateName, Boolean lemmatization) {
        System.err.println("开始处理特征");
        try {
            //从原数据文章中读取数据，并对sentence内容进行基本的初始化处理
             FeatureCacheUtils.readDataCache(dataPoints);

            /*
			 * 抽取所有数据中的特征信息，并保存到文件中作为缓存，有已生成的缓存则直接读取
			 */
            File featureDataFile = new File(featureCachePath);
            if (featureDataFile.exists()) {
                System.out.println("特征数据文件已存在  " + featureCachePath + " ,开始读取");
                //read and return vectors
                int readItemsNumber = FeatureCacheUtils.readFeatureCacheData(dataPoints, featureCachePath);
                if(readItemsNumber < dataPoints.size()){
                    System.out.println("部分数据无特征数据，继续生成  " + featureCachePath);
                    FeatureCacheUtils.extractFeatures(dataPoints, featureCachePath, readItemsNumber, lemmatization);
                }

            } else {
                System.out.println("特征数据文件不存在，重新生成特征文件  " + featureCachePath);
                //regenerate the feature data file and return vectors
                FeatureCacheUtils.extractFeatures(dataPoints, featureCachePath, 0, lemmatization);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}

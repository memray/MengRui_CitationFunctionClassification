package edu.whu.irlab.dfki.feature;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.feature.utils.PorterStemmer;
import edu.whu.irlab.dfki.feature.utils.WordlistCacheUtils;

/**
 * 此特征抽取器为通用抽取器，将所有特征抽取 并保存到FeatureTemplate中 2014-09-24
 * liuxingbang
 * 2014-10-13
 */
public class FeatureExtractUtils {

    //计算F-Measure
    public static double fScoreCompute(double p, double r, double b) {
        //p 为 精确率  ， r为召回率 , b为参数
        return (b * b + 1) * p * r / (b * b * p + r);
    }

    /**
     * 根据两个集合的Jaccard系数计算两个集合的相似度
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public static double JaccardCoefficient(HashSet<String> article_titleSet,
                                            HashSet<String> parscit_titleSet) {
        HashSet<String> intersection = (HashSet<String>) article_titleSet.clone();
        intersection.retainAll(parscit_titleSet);
        HashSet<String> unionSet = (HashSet<String>) article_titleSet.clone();
        for (String s : parscit_titleSet)
            unionSet.add(s);
        return (double) intersection.size() / (double) unionSet.size();
    }


    static List<String> introduction = Arrays.asList("introduct", "motiv", "abstract");
    static List<String> relativeWork = Arrays.asList("relat", "background",
            "previ", "induct");
    static List<String> method = Arrays.asList("method", "methodolog", "model",
            "research", "approach", "theori", "materi", "concept",
            "theoret", "procedur", "strateg");
    static List<String> experiment = Arrays.asList("data", "design", "experiment",
            "experi", "corpu", "featur", "corpor");
    static List<String> evaluation = Arrays.asList("exampl", "evalu", "measur",
            "analysi", "discuss", "result", "compar");
    static List<String> conclusion = Arrays.asList("conclus", "futur", "remark",
            "conclud", "summari", "comment");
    static PorterStemmer stemmer = new PorterStemmer();

    /**
     * 输入一个章节字符串 判断这个章节属于哪个类别
     *
     * @param sectionName
     * @return category
     */
    public static int getSectionCategory(String sectionName) {
        if (sectionName == null || sectionName.trim().equals(""))
            return 0;
        //System.out.println(sectionName);
        sectionName = stemmer.stemSentenceOrWords(sectionName.toLowerCase());
        for (String s : introduction)
            if (sectionName.contains(s))
                return 1;
        for (String s : relativeWork)
            if (sectionName.contains(s))
                return 2;
        for (String s : method)
            if (sectionName.contains(s))
                return 3;
        for (String s : experiment)
            if (sectionName.contains(s))
                return 4;
        for (String s : evaluation)
            if (sectionName.contains(s))
                return 5;
        for (String s : conclusion)
            if (sectionName.contains(s))
                return 6;

        return 3;
    }

    /**
     * 输入一个句子，提取句子中括号中的年份(粗略提取)
     */
    public static Integer getAclSentenceYear(String sentence) {
        String regex = "\\(.*?(\\d{4,10}).*?\\)";//获取一个字符串中 括号中的数字
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(sentence);
        Integer year = 0;
        while (m.find()){
            Integer currentyear = Integer.valueOf(m.group(1));
            if(currentyear>=1900&&currentyear<=2015){
                year = currentyear;
            }
        }
        return year;
    }

    /**
     * @param authors
     * @return 包含的作者数
     */

    public static int getNumberOfAuthors(String authors) {
        authors = authors.trim();
        if (authors == null || authors.length() == 0) {
            return 0;
        } else if (authors.contains(";")) {
            String[] author = authors.split(";");

            return author.length;
        } else {
            return 1;
        }
    }
    //因为标记要在进行标记替换之前
    public static void getSentenceLoc(DataPoint dp){
        //句子要最开始的，并没有替换标记的时候
        String sentence = dp.getSentence();
        String[] sentencesplit = sentence.split("( )");//标记才三个字符，忽略不计
        if(sentence.length()!=0){
            dp.getFeature().setSentenceLocation(sentencesplit[0].length()*1.0f/sentence.length());
        }else{
            dp.getFeature().setSentenceLocation(0.0f);
        }
    }

    /*
     * 输入一个sentence，找到这个sentence里面所有的subject，并返回距离引文处最近的Subjectivity Cue
     */
    public static String getClosestSubjectivityCue(String sentence) {
        String cloestSubjectivityCue = "";
        int k = 0;
        int minDistance = sentence.split(" ").length;
        // citation的位置
        int citationLocation = sentence.indexOf("<citation>");
        // 用hashmap 记录subjuectivity cue 以及位置，从而获得距离引文标记最近的subjectivity cue
        HashMap<String, Integer> subjectivityCues = new HashMap<String, Integer>();
        for (String word : sentence.split(" ")) {
            // 句子中的词是否存在于词表之中
            if (WordlistCacheUtils.subjectivityCueMap.containsKey(word))
                if (Math.abs(k - citationLocation) < minDistance) {
                    cloestSubjectivityCue = word;
                    minDistance = Math.abs(k - citationLocation);
                }
            k++;
        }
        return cloestSubjectivityCue;
    }

    public static void main(String[] args) {
    }
}

package edu.whu.irlab.dfki.feature.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.stanford.nlp.ling.TaggedWord;
import edu.whu.irlab.bean.DataPoint;
import org.apache.commons.io.FileUtils;

/**
 * 根据极性词表判断词极性
 */
public class OpinionFinder {
    //词典路径
    private static String path = "corpus/OpinionFinder.txt";
    //0表示消极(negative) 1表示积极(positive)
    private int sentiment = 0;

    //表示程度的强弱  0表示weaksubj 1表示strongsubj
    private int level = 0;

    private String word = "";

    //保存该词的词性
    private String pos = "";

    //正面情感词
    private static HashMap<String, OpinionFinder> positiveWords = null;
    //负面情感词
    private static HashMap<String, OpinionFinder> negativeWords = null;
    /**
     * 返回一个极性hashmap<word,OpinionFinder>
     */
    public HashMap<String, OpinionFinder> getOpinionFinders() {
        File f = new File(path);
        HashMap<String, OpinionFinder> opinionFinders = new HashMap<String, OpinionFinder>();
        try {
            List<String> lines = FileUtils.readLines(f, "utf-8");
            for (String line : lines) {
                OpinionFinder of = new OpinionFinder();
                String[] words = line.split(" ");
                words[0] = words[0].replace("type=", "").trim();
                //System.out.println(words[0]);
                if (words[0].equals("strongsubj"))
                    of.setLevel(1);
                else
                    of.setLevel(0);
                //System.out.println(of.getLevel());
                words[5] = words[5].replace("priorpolarity=", "").trim();
                if (words[5].equals("positive")) of.setSentiment(1);
                else of.setSentiment(0);

                words[2] = words[2].replace("word1=", "").trim();
                of.setWord(words[2]);


                words[3] = words[3].replace("pos1=", "").trim();
                of.setPos(words[3]);

                opinionFinders.put(words[2], of);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return opinionFinders;
    }

    /**
     * 返回正向极性的词表 hashmap<word,OpinionFinder>
     */
    public static HashMap<String, OpinionFinder> getPositiveWords() {
        if (positiveWords!=null)
            return positiveWords;
        File f = new File(path);
        HashMap<String, OpinionFinder> opinionFinders = new HashMap<String, OpinionFinder>();
        try {
            List<String> lines = FileUtils.readLines(f, "utf-8");
            for (String line : lines) {
                OpinionFinder of = new OpinionFinder();
                String[] words = line.split(" ");
                words[0] = words[0].replace("type=", "").trim();
                //System.out.println(words[0]);
                if (words[0].equals("strongsubj"))
                    of.setLevel(1);
                else
                    of.setLevel(0);
                //System.out.println(of.getLevel());
                words[5] = words[5].replace("priorpolarity=", "").trim();
                if (words[5].equals("positive"))
                    of.setSentiment(1);
                else
                    of.setSentiment(0);

                words[2] = words[2].replace("word1=", "").trim();
                of.setWord(words[2]);

                words[3] = words[3].replace("pos1=", "").trim();
                of.setPos(words[3]);

                //只保存有积极情感的词汇
                if (of.getSentiment()==1)
                    opinionFinders.put(words[2], of);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        positiveWords = opinionFinders;
        return opinionFinders;
    }

    /**
     * 同上，返回负面极性的词表 hashmap<word,OpinionFinder>
     */
    public static HashMap<String, OpinionFinder> getNegativeWords() {
        if (negativeWords!=null)
            return negativeWords;
        File f = new File(path);
        HashMap<String, OpinionFinder> sentimentWords = new HashMap<String, OpinionFinder>();
        try {
            List<String> lines = FileUtils.readLines(f, "utf-8");
            for (String line : lines) {
                OpinionFinder of = new OpinionFinder();
                String[] words = line.split(" ");
                words[0] = words[0].replace("type=", "").trim();
                //System.out.println(words[0]);
                if (words[0].equals("strongsubj"))
                    of.setLevel(1);
                else
                    of.setLevel(0);
                //System.out.println(of.getLevel());
                words[5] = words[5].replace("priorpolarity=", "").trim();
                if (words[5].equals("positive"))
                    of.setSentiment(1);
                else
                    of.setSentiment(0);

                words[2] = words[2].replace("word1=", "").trim();
                of.setWord(words[2]);

                words[3] = words[3].replace("pos1=", "").trim();
                of.setPos(words[3]);

                //只保存有消极情感的词汇
                if (of.getSentiment()==0)
                    sentimentWords.put(words[2], of);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        negativeWords = sentimentWords;
        return sentimentWords;

    }
    /**
     * 判断情感词表中是否包含该词，注意不同词性的匹配
     * 此处词表应预先过滤为只包含正向词或者只包含负面词
     * @param sentimentWords
     * @param taggedWord Stanford Parser 中的TaggedWord
     * @return
     */
    public static boolean containsWords(HashMap<String, OpinionFinder> sentimentWords, TaggedWord taggedWord) {
        if(!sentimentWords.containsKey(taggedWord.word().toLowerCase()))
            return false;
        OpinionFinder sentimentWord = sentimentWords.get(taggedWord.word());
        if(sentimentWord==null)
            return false;
        if(sentimentWord.getPos().equals("anypos"))
            return true;
        if(sentimentWord.getPos().equals("adj") && taggedWord.tag().startsWith("JJ"))
            return true;
        if(sentimentWord.getPos().equals("adverb") && taggedWord.tag().startsWith("RB"))
            return true;
        if(sentimentWord.getPos().equals("verb") && taggedWord.tag().startsWith("VB"))
            return true;
        if(sentimentWord.getPos().equals("noun") && taggedWord.tag().startsWith("NN"))
            return true;
        return false;
    }

    public static void main(String[] args) {
        OpinionFinder of = new OpinionFinder();
        HashMap<String, OpinionFinder> ofs = of.getOpinionFinders();
        for (String word : ofs.keySet()) {
            OpinionFinder opinionFinder = ofs.get(word);
        }
    }

    public int getSentiment() {
        return sentiment;
    }

    public void setSentiment(int sentiment) {
        this.sentiment = sentiment;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }



    public static void setNegativeWords(HashMap<String, OpinionFinder> negativeWords) {
        OpinionFinder.negativeWords = negativeWords;
    }

    public static ArrayList<String> extractPositiveWords(DataPoint dp) {
        HashMap<String, OpinionFinder> positiveWords = OpinionFinder.getPositiveWords();
        ArrayList<String> returnArrayList = new ArrayList<>();
        dp.getTaggedWords().stream().filter(s->OpinionFinder.containsWords(positiveWords, s)).forEach(s -> returnArrayList.add(s.word()));
        return returnArrayList;
    }

    public static ArrayList<String> extractNegativeWords(DataPoint dp) {
        HashMap<String, OpinionFinder> positiveWords = OpinionFinder.getNegativeWords();
        ArrayList<String> returnArrayList = new ArrayList<>();
        dp.getTaggedWords().stream().filter(s->OpinionFinder.containsWords(positiveWords, s)).forEach(s -> returnArrayList.add(s.word()));
        return returnArrayList;
    }
}

package edu.whu.irlab.dfki.feature.utils;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.*;
import edu.whu.irlab.bean.CitationAgent;
import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.bean.SubjectEntity;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureExtractFunctions;

/**
 * 使用句法分析从句子中提取与主语和引文标记最近的动词
 * 注意tree是在FeatureExtractor中赋值的
 * TODO 这个类肯定要再仔细检查一下，针对Charles的特征也要修改函数
 */
public class GrammarUtils {
    public static MaxentTagger tagger;

    public static LexicalizedParser lexicalizedParser;

    static {
        lexicalizedParser = LexicalizedParser
                .loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
        tagger = new MaxentTagger("lib/stanford-postagger/models/wsj-0-18-bidirectional-distsim.tagger".replace("/", File.separator));
    }

    public static TreeGraph tree;
    //表示否定含义的word和phrase
    static List<String> negationWordsCues = Arrays.asList("no", "not", "never", "neither", "nor", "none", "nobody", "nowhere", "nothing", "cannot", "can not", "without", "no one", "no way");
    static List<String> negationSuffixCues = Arrays.asList("n’t");


    /**
     * 通过Stanford Parser找出句子的关键动词，关键动词包括与subject-tag和citation-tag相连的动词
     * 动词经过lowercase处理，不做stem处理，是否stem由调用层决定
     *
     * @return
     */
    public static ArrayList<String> getKeyVerbs(TreeGraph treeG) {
        ArrayList<String> verbs = new ArrayList<String>();
        // 将本文的引文替换为一个统一标记<citation>

        ArrayList<SubjectEntity> subjectAgents = GrammarUtils
                .getSubjectNPs(treeG);
        ArrayList<CitationAgent> citationAgents = GrammarUtils
                .getCitationNPs(treeG, "<citation>");
        String temp = "";
        for (SubjectEntity sa : subjectAgents) {
            if (sa.getVerbString() == null
                    || sa.getVerbString().trim().equals(""))
                continue;

            temp = sa.getVerbString().toLowerCase();
            temp = temp
                    .replaceAll(
                            "\\bam\\b|\\bis\\b|\\bare\\b|\\bwas\\b|\\bwere\\b|\\bbeen\\b",
                            "be");
            verbs.add(temp);
        }
        for (CitationAgent ra : citationAgents) {
            if (ra.getVerbString() == null
                    || ra.getVerbString().trim().equals(""))
                continue;

            temp = ra.getVerbString().toLowerCase();
            temp = temp
                    .replaceAll(
                            "\\bam\\b|\\bis\\b|\\bare\\b|\\bwas\\b|\\bwere\\b|\\bbeen\\b",
                            "be");
            verbs.add(temp);
        }
        return verbs;
    }

    /**
     * 通过Stanford Parser找出句子中与指定实体相连的关键词。
     * 与指定实体（主语代词、引文代词、章节代词）相连的词汇包括（动词、形容词、副词、介词等）
     * 通过在word加前缀以与一般的词汇区分
     * 动词经过lowercase处理，不做stem处理，是否stem由调用层决定
     *
     * 由于这个函数使用的是parser的phrasal tree的结果，准确率较低，故抛弃
     * 改写后函数为：getLinkedKeyWordsOfCertainEntity()
     *
     * @param entityType 指明中心实体的类型（是subject还是citation）
     * @param wordTypes  指定要查找的词性,指明词性的前缀（动词 VB，形容词 JJ，副词 RB，介词 IN TO及小品词 RP）
     * @param wordNumber 指明需要返回几个指定词性的最近词汇
     * @return
     */
    @Deprecated
    public static ArrayList<String> getLinkedKeyWordsWithEntity(TreeGraph tree, EntityType entityType, String[] wordTypes, int wordNumber, Boolean stem) {
        GrammarUtils.tree = tree;
        ArrayList<String> keywords = new ArrayList<String>();

        ArrayList<TreeGraphNode> entityNPList = new ArrayList<TreeGraphNode>();

        //获取所有指定实体所在的NP，即其所在的TreeGraphNode
        entityNPList.addAll(GrammarUtils.findEntitiesNpNodes(entityType));

        //去重用,key为该词node的hashcode
        HashSet<Integer> indexSet = new HashSet<Integer>();
        //保存节点以及对应的距离
        HashMap<TreeGraphNode, Integer> keywordNodes = new HashMap<TreeGraphNode, Integer>();
        /*
         * 循环每一个NpNode以及每一个词性类型，在树中寻找
		 */
        for (TreeGraphNode node : entityNPList) {
            for (String wordType : wordTypes) {
                keywordNodes = GrammarUtils.findNearWords(node, wordType, wordNumber);
                for (Entry<TreeGraphNode, Integer> keywordNode : keywordNodes.entrySet()) {
                    if (keywordNode == null || keywordNode.getKey().label().toString("value").equals(""))
                        continue;
                    // 去重
                    if (indexSet.contains(keywordNode.hashCode()))
                        continue;
                    else {
                        indexSet.add(keywordNode.hashCode());
                        //按照 "实体类型_词汇"的形式返回
                        if (stem)
                            keywords.add(entityType + "_" + FeatureExtractFunctions.stemmer.stemWord(keywordNode.getKey().label().toString("value").toLowerCase()));
                        else
                            keywords.add(entityType + "_" + keywordNode.getKey().label().toString("value").toLowerCase());
                        //按照 "距离_实体类型_词汇"的形式返回
                        //keywords.add(keywordNode.getValue()+"_"+entityType+"_"+keywordNode.getKey().label().toString("value"));
                    }
                }
            }
        }
        //System.out.println(keywords);
        return keywords;
    }

    /**
     * 通过parser的Typed Dependency结果来寻找与给定的实体类型（作者主语，引文）相连接的dependency（动词、名词、形容词等）
     * @param dp 要处理的数据
     * @param entityType 要查找的类型
     * @param wordTypes 指定要返回的词性
     * @param wordNumber 指定要返回最近几个关键词
     * @param lemmatization 返回结果是否进行词干提取
     * @return 返回关键词列表，通过EntityType_word的形式返回
     */
    public static ArrayList<String> getLinkedDependenciesOfCertainEntity(DataPoint dp, EntityType entityType, String[] wordTypes, int wordNumber, boolean lemmatization) {
        ArrayList<String> returnList = new ArrayList<>();

        //保存author subject或者citation所在的位置
        ArrayList<TaggedWord> entityList = new ArrayList<>();
        entityList.addAll(GrammarUtils.findEntitiesWords(dp.getTaggedWords(), entityType));

        //去重用,保存候选词taggedWord
        HashSet<Integer> indexSet = new HashSet<>();
        /*
         * 循环每一个NpNode以及每一个词性类型，在树中寻找
		 */
        for (TaggedWord entity : entityList)
            for(String types:wordTypes){
                        returnList.addAll(findLinkedDependenciesAroundWords(dp, entity, types, wordNumber, entityType, lemmatization));
            }
        //System.out.println(keywords);
//        indexSet.stream().forEach(s -> returnList.add(dp.getTaggedWords().get(s).word().toLowerCase()));


        return returnList;
    }


    /**
     * 找到与指定词相连的最近的相关依存关系
     * @param dp
     * @param startPoint 开始搜索的起点
     * @param posType 要返回的词性
     * @param neededWordNumber 要返回几个词
     * @param entityType
     * @return 返回符合条件的Dependencies的List<String>
     */
    private static ArrayList<String> findLinkedDependenciesAroundWords(DataPoint dp, TaggedWord startPoint, String posType, int neededWordNumber, EntityType entityType, boolean lemmatization) {
        posType = posType.trim().toLowerCase();
        ArrayList<TaggedWord> wordQueue = new ArrayList<>();
        wordQueue.add(startPoint);
        ArrayList<String> returnList = new ArrayList<>();
        HashSet<Integer> filter = new HashSet<>();
        TaggedWord head;
        int headIndex;
        TaggedWord candidate;
        int candidateIndex;
        String reln, gov, dep, dependency, candidateWord = "";

        while (wordQueue.size() > 0) {
            head = wordQueue.get(0);
            headIndex = dp.getTaggedWords().indexOf(head);
            wordQueue.remove(0);
            filter.add(headIndex);
//            System.out.println(dp.getTaggedWords().get(head));
            for (TypedDependency td : dp.getTypedDependencies()) {
//                System.out.println(td);
                // 找到当前词的gov并加入到队列中，即向上寻找
                if (td.dep().index() == headIndex || td.gov().index() == headIndex){
                    if(td.dep().index() == headIndex) {
                        candidateIndex = td.gov().index();
                        candidate = dp.taggedWords.get(candidateIndex);
                    }else{
                        candidateIndex = td.dep().index();
                        candidate = dp.taggedWords.get(candidateIndex);
                    }
                    if(filter.contains(candidateIndex))
                        continue;
                    //找到符合条件的节点
                    if (candidate.tag().toLowerCase().startsWith(posType)) {
                        neededWordNumber--;
                        reln = td.reln().toString();
                        if(td.dep().index() == headIndex) {
                            candidateIndex = td.gov().index();
                            candidate = dp.taggedWords.get(candidateIndex);
                            candidateWord = candidate.word();
                            if(isModifiedByNegation(dp.getTaggedWords(), candidateIndex))
                                candidateWord = "not_" + candidate.word();
                            if (lemmatization)
                                candidateWord = FeatureExtractFunctions.stemmer.stemWord(candidateWord);

                            dependency = reln + "(" + candidateWord+","+entityType+")";
                        }else{
                            candidateIndex = td.dep().index();
                            candidate = dp.taggedWords.get(candidateIndex);
                            candidateWord = candidate.word();

                            if(isModifiedByNegation(dp.getTaggedWords(), candidateIndex))
                                candidateWord = "not_" + candidate.word();
                            if (lemmatization)
                                candidateWord = FeatureExtractFunctions.stemmer.stemWord(candidateWord);

                            dependency = reln + "(" + entityType +","+candidateWord+")";
                        }
                        returnList.add(dependency);
                        if(neededWordNumber<=0)
                            return returnList;
                    }
                    wordQueue.add(candidate);
                    filter.add(candidateIndex);
                }

            }
        }
        return returnList;
    }



    /**
     * 从句子中确定实体所在的节点
     *
     * @param entityType 指定获取的实体类型，subject或者citation
     *                   TODO 对于subject的实体类型处理可能要更复杂,this等增加消歧
     */
    @Deprecated
    public static ArrayList<TreeGraphNode> findEntitiesNpNodes(EntityType entityType) {
        //确定对应实体的标记，用regex来进行匹配
        HashSet<String> tagFilter = new HashSet<String>();
//        String[] author_regexs = {"we", "our", "us", "this", "here", "ours", "these"};
        if (entityType==EntityType.Author) {
                tagFilter.add("<subject>");
        } else {
            tagFilter.add("<citation>");
            tagFilter.add("_citation");
        }

        // 存储所有叶子节点
        ArrayList<TreeGraphNode> leafNodes = new ArrayList<TreeGraphNode>();
        ArrayList<Integer> leafHashCode = new ArrayList<Integer>();
        // 找到所有叶子节点
        findAllLeaves(leafNodes, tree.root());
        for (TreeGraphNode leafNode : leafNodes) {
            leafHashCode.add(leafNode.hashCode());
        }

        // 存储包含主语的节点
        ArrayList<TreeGraphNode> entityNodes = new ArrayList<TreeGraphNode>();

        // 找到包含主语的节点
        findNodesByRecursion(entityNodes, tree.root(), tagFilter);
        // 找到对应主语节点的NP
        ArrayList<TreeGraphNode> entityNPs = new ArrayList<>();
        for (TreeGraphNode subjectNode : entityNodes) {
            entityNPs.add(findNPsFromNode(subjectNode.parent()));
        }

        return entityNPs;
    }

    /**
     * 拓展自findNearestVerb（）
     * 给定一个Node，寻找与其最近的一个指定词性的词
     * 用BFS 先拓展兄弟节点，父节点及兄弟节点距离为一，往下距离递增
     * 注意范围，如果兄弟节点是标点则不再往那个方向寻找，如果出现从句SBAR则往下寻找
     *
     * @param node       表示寻找的起点
     * @param wordType   表示寻找的节点类型
     * @param wordNumber 表示寻找几个词（最近的多个词）
     */
    public static HashMap<TreeGraphNode, Integer> findNearWords(TreeGraphNode node, String wordType, int wordNumber) {
        HashMap<TreeGraphNode, Integer> resultNodes = new HashMap<TreeGraphNode, Integer>();
        int findNumber = 0;

        ArrayList<TreeGraphNode> queue = new ArrayList<TreeGraphNode>();
        HashSet<Integer> visitedSet = new HashSet<Integer>();
        visitedSet.add(tree.hashCode());
        ArrayList<TreeGraphNode> newNodes = new ArrayList<TreeGraphNode>();
        newNodes = getCandidateNodes(node, true);
        for (TreeGraphNode newNode : newNodes)
            if (!visitedSet.contains(newNode.hashCode())) {
                queue.add(newNode);
                visitedSet.add(newNode.hashCode());
            }

        int headIndex = 0;
        TreeGraphNode currentNode;
        //BFS结束条件，队列为空 或者是 找到的目标词汇数目达到了需求的数目
        while (headIndex < queue.size() && findNumber < wordNumber) {
            currentNode = queue.get(headIndex);
            // 找到目标词汇，添加到结果
            if (currentNode.isLeaf()
                    && currentNode.parent().label().value().startsWith(wordType)) {
                resultNodes.put(currentNode, ++findNumber);
            }
            //根据当前节点拓展新节点，拓展新节点
            newNodes = getCandidateNodes(currentNode, false);
            for (TreeGraphNode newNode : newNodes)
                if (!visitedSet.contains(newNode.hashCode())) {
                    queue.add(newNode);
                    visitedSet.add(newNode.hashCode());
                }
            headIndex++;
        }
        return resultNodes;

    }


    /**
     * 返回一个句子中主语代词所在的NP,以及其相连的词
     *
     * @param tree
     * @return 句子中的所有Subject所在NP以及相连动词
     */
    public static ArrayList<SubjectEntity> getSubjectNPs(TreeGraph tree) {

        // 存储所有叶子节点
        ArrayList<TreeGraphNode> leafNodes = new ArrayList<TreeGraphNode>();
        ArrayList<Integer> leafHashCode = new ArrayList<Integer>();
        // 找到所有叶子节点
        findAllLeaves(leafNodes, tree.root());
        for (TreeGraphNode leafNode : leafNodes) {
            leafHashCode.add(leafNode.hashCode());
        }

        // 存储包含主语的节点
        ArrayList<TreeGraphNode> subjectNodes = new ArrayList<TreeGraphNode>();

        HashSet<String> tagFilter = new HashSet<String>();
        String[] author_regexs = {"we", "our", "us", "this", "here", "ours",
                "these"};
        for (String author : author_regexs)
            tagFilter.add(author);

        /**
         * 系动词连接形容词的时候，形容词重要，单独拉出来考虑 系动词表
         */
        String[] linkedVerbs = {"is", "are", "was", "were"};
        HashSet<String> linkedVerbFilter = new HashSet<String>();
        for (String linkedVerb : linkedVerbs)
            linkedVerbFilter.add(linkedVerb);

        // 找到包含主语的节点
        findNodesByRecursion(subjectNodes, tree.root(), tagFilter);
        // 找到对应主语节点的NP
        ArrayList<TreeGraphNode> subjectNPs = new ArrayList<TreeGraphNode>();
        for (TreeGraphNode subjectNode : subjectNodes) {
            subjectNPs.add(findNPsFromNode(subjectNode.parent()));
        }

        // 输出NP里的词汇及其对应动词，保存到SubjectAgent中并返回
        ArrayList<SubjectEntity> subjectAgents = new ArrayList<SubjectEntity>();
        int i = 0;
        StringBuilder builder = new StringBuilder();
        HashSet<Integer> indexSet = new HashSet<Integer>();
        for (TreeGraphNode subjectNP : subjectNPs) {
            i++;
            builder.setLength(0);
            // 去重
            if (indexSet.contains(subjectNP.hashCode()))
                continue;
            else
                indexSet.add(subjectNP.hashCode());

            TreeGraphNode verb = findNearestVerb(subjectNP, linkedVerbFilter);

            for (Label lab : subjectNP.yield())
                builder.append(((CoreLabel) lab).toString("value") + " ");

            SubjectEntity subjectAgent = new SubjectEntity();
            subjectAgent.setNPNode(subjectNP);
            subjectAgent.setNPString(builder.toString());
            // System.out.println("主语NP["+subjectNP.hashCode()+"] : "+builder.toString());
            if (verb != null) {
                if (linkedVerbFilter.contains(verb.label().toString("value"))) {
                    int k = 0;
                    for (k = leafHashCode.indexOf(verb.hashCode()) + 1; k < leafHashCode
                            .size(); k++) {
                        if (leafNodes.get(k).parent().label().value().trim()
                                .startsWith("IN")
                                || leafNodes.get(k).parent().label().value()
                                .trim().startsWith("TO")
                                || leafNodes.get(k).parent().label().value()
                                .trim().startsWith("VBG")) {
                            break;
                        }
                    }
                    if (k == leafHashCode.size()) {
                        subjectAgent.setVerbNode(verb);
                        subjectAgent.setVerbString(verb.label().toString(
                                "value"));
                    } else {
                        if (leafNodes.get(k).parent().label().value().trim()
                                .startsWith("VBG")) {
                            subjectAgent.setVerbNode(leafNodes.get(k));
                            subjectAgent.setVerbString(verb.label().toString(
                                    "value")
                                    + " "
                                    + leafNodes.get(k).label()
                                    .toString("value"));

                        } else {
                            subjectAgent.setVerbNode(leafNodes.get(k - 1));
                            subjectAgent.setVerbString(verb.label().toString(
                                    "value")
                                    + " "
                                    + leafNodes.get(k - 1).label()
                                    .toString("value")
                                    + " "
                                    + leafNodes.get(k).label()
                                    .toString("value"));
                        }

                    }

                } else {
                    subjectAgent.setVerbNode(verb);
                    subjectAgent.setVerbString(verb.label().toString("value"));
                }
                // System.out.println("\t"+verb.label().toString("value"));
            }
            subjectAgents.add(subjectAgent);
        }

        return subjectAgents;
    }

    /**
     * 返回一个句子中引文代词(包括引文标记及章节标记)所在的NP,以及其相连的词
     *
     * @param tree
     * @return 句子中的所有Citation所在NP以及相连动词
     */
    public static ArrayList<CitationAgent> getCitationNPs(TreeGraph tree,
                                                          String refereceTag) {
        ArrayList<TreeGraphNode> referenceNodes = new ArrayList<TreeGraphNode>();
        HashSet<String> tagFilter = new HashSet<String>();
        tagFilter.add(refereceTag);
        /**
         * 系动词连接形容词的时候，形容词重要，单独拉出来考虑 系动词表
         */
        String[] linkedVerbs = {"is", "are", "was", "were"};
        HashSet<String> linkedVerbFilter = new HashSet<String>();
        for (String linkedVerb : linkedVerbs)
            linkedVerbFilter.add(linkedVerb);

        findNodesByRecursion(referenceNodes, tree.root(), tagFilter);
        // 存储所有叶子节点
        ArrayList<TreeGraphNode> leafNodes = new ArrayList<TreeGraphNode>();
        ArrayList<Integer> leafHashCode = new ArrayList<Integer>();
        // 找到所有叶子节点
        findAllLeaves(leafNodes, tree.root());
        for (TreeGraphNode leafNode : leafNodes) {
            leafHashCode.add(leafNode.hashCode());
        }

        ArrayList<TreeGraphNode> referenceNPs = new ArrayList<TreeGraphNode>();
        for (TreeGraphNode referenceNode : referenceNodes) {
            referenceNPs.add(findNPsFromNode(referenceNode.parent()));
        }

        // 输出NP里的词汇
        int i = 0;
        StringBuilder builder = new StringBuilder();
        HashSet<Integer> indexSet = new HashSet<Integer>();

        ArrayList<CitationAgent> referenceAgents = new ArrayList<CitationAgent>();

        for (TreeGraphNode referenceNP : referenceNPs) {
            i++;
            builder.setLength(0);

            // 去重
            if (indexSet.contains(referenceNP.hashCode()))
                continue;
            else
                indexSet.add(referenceNP.hashCode());

            TreeGraphNode verb = findNearestVerb(referenceNP, linkedVerbFilter);

            for (Label lab : referenceNP.yield())
                builder.append(((CoreLabel) lab).toString("value") + " ");

            CitationAgent referenceAgent = new CitationAgent();
            referenceAgent.setNPNode(referenceNP);
            referenceAgent.setNPString(builder.toString());
            // System.out.println("被引NP["+referenceNP.hashCode()+"] : "+builder.toString());
            if (verb != null) {
                if (linkedVerbFilter.contains(verb.label().toString("value"))) {
                    int k = 0;
                    for (k = leafHashCode.indexOf(verb.hashCode()) + 1; k < leafHashCode
                            .size(); k++) {
                        if (leafNodes.get(k).parent().label().value().trim()
                                .startsWith("IN")
                                || leafNodes.get(k).parent().label().value()
                                .trim().startsWith("TO")) {
                            break;
                        }
                    }
                    referenceAgent.setVerbNode(leafNodes.get(k - 1));
                    referenceAgent.setVerbString(leafNodes.get(k - 1).label()
                            .toString("value"));
                } else {
                    referenceAgent.setVerbNode(verb);
                    referenceAgent
                            .setVerbString(verb.label().toString("value"));
                }
                // System.out.println("\t"+verb.label().toString("value"));
            }
            // System.out.println(referenceAgent.getVerbString());
            referenceAgents.add(referenceAgent);

        }
        return referenceAgents;
    }

    /**
     * 递归寻找内容满足条件的节点
     *
     * @param nps
     * @param tree
     * @param filter
     */
    private static void findNodesByRecursion(ArrayList<TreeGraphNode> nps,
                                             TreeGraphNode tree, HashSet<String> filter) {
        TreeGraphNode[] kids = tree.children();
        for (TreeGraphNode kid : kids) {
            if (kid.isLeaf()) {
                // System.out.println(kid.nodeString());
                for(String target : filter)
                    if (kid.nodeString().trim().toLowerCase().contains(target)) {
                        nps.add(kid);
                        return;
                    }
            } else
                findNodesByRecursion(nps, kid, filter);
        }
    }

    /**
     * 遍历树，将所有叶子节点加到leaves中
     *
     * @param leaves
     * @param tree
     */
    public static void findAllLeaves(ArrayList<TreeGraphNode> leaves,
                                     TreeGraphNode tree) {
        TreeGraphNode[] kids = tree.children();
        for (TreeGraphNode kid : kids) {
            if (kid.isLeaf()) {
                leaves.add(kid);
                // System.out.println(kid.label().toString("value"));
                return;
            } else
                findAllLeaves(leaves, kid);
        }
    }

    /**
     * 自底向上寻找满足条件的NP
     *
     * @param tree
     */
    private static TreeGraphNode findNPsFromNode(TreeGraphNode tree) {
        boolean containsNP = false;
        if (tree.label().value().startsWith("NP"))
            containsNP = true;

        while (!containsNP && tree.parent() != null
                && tree.parent().label().value().startsWith("NP")) {
            tree = tree.parent();
            if (tree.label().value().startsWith("NP"))
                containsNP = true;
        }
        return tree;
    }

    /**
     * 给定一个NP，寻找与其最近的一个动词 用BFS 先拓展兄弟节点，父节点及兄弟节点距离为一，往下距离递增
     * 注意范围，如果兄弟节点是标点则不再往那个方向寻找，如果出现从句SBAR则往下寻找
     *
     * @param treeNode
     * @param filter
     */
    private static TreeGraphNode findNearestVerb(TreeGraphNode treeNode,
                                                 HashSet<String> filter) {
        ArrayList<TreeGraphNode> queue = new ArrayList<TreeGraphNode>();
        HashSet<Integer> visitedSet = new HashSet<Integer>();
        visitedSet.add(treeNode.hashCode());

        ArrayList<TreeGraphNode> newNodes = new ArrayList<TreeGraphNode>();
        newNodes = getCandidateNodes(treeNode, true);
        for (TreeGraphNode newNode : newNodes)
            if (!visitedSet.contains(newNode.hashCode())) {
                queue.add(newNode);
                visitedSet.add(newNode.hashCode());
            }

        int headIndex = 0;
        TreeGraphNode currentNode;
        while (headIndex < queue.size()) {
            currentNode = queue.get(headIndex);
            // System.out.println(currentNode.label().toString("{map}"));
            // 找到最近动词，返回
            if (currentNode.isLeaf()
                    && currentNode.parent().label().value().startsWith("V")) {
                /*
				 * 通过filter判断当前节点是否是系动词，如果是则寻找关联的形容词。。
				 */
                if (filter.contains(currentNode.label().toString("value"))) {
                    // 判断是否是 be动词 + 形容词的组合
                    TreeGraphNode[] brothers = currentNode.parent().children();
                    int index = 0;
                    while (brothers[index].hashCode() != currentNode.hashCode())
                        index++;
                    // System.out.println(currentNode.parent().parent().label());
                }
                return currentNode;
            }
            // 拓展新节点
            newNodes = getCandidateNodes(currentNode, false);
            for (TreeGraphNode newNode : newNodes)
                if (!visitedSet.contains(newNode.hashCode())) {
                    queue.add(newNode);
                    visitedSet.add(newNode.hashCode());
                }
            headIndex++;
        }
        return null;
    }

    /**
     * 用于findNearestVerb()中,返回与某个Node相连的Node
     *
     * @param tree
     * @param isFirst
     * @return
     */
    private static ArrayList<TreeGraphNode> getCandidateNodes(
            TreeGraphNode tree, boolean isFirst) {
        ArrayList<TreeGraphNode> candidateNodes = new ArrayList<TreeGraphNode>();
        TreeGraphNode[] children;
        // 添加兄弟节点
        if (tree.parent() != null && tree.parent().children() != null) {
            children = tree.parent().children();
            int index = 0;
            while (children[index].hashCode() != tree.hashCode())
                index++;
            for (int i = index - 1; i >= 0; i--) {
                String label = children[i].label().value().trim();
                if (label.startsWith("SBAR") || label.equals(",")
                        || label.equals(";") || label.equals("."))
                    break;
                candidateNodes.add(children[i]);
            }
            for (int i = index + 1; i < children.length; i++) {
                String label = children[i].label().value().trim();
                if (label.startsWith("SBAR") || label.equals(",")
                        || label.equals(";") || label.equals("."))
                    break;
                candidateNodes.add(children[i]);
            }
        }
        // 添加父节点
        if (tree.parent() != null)
            candidateNodes.add(tree.parent());
        // 添加子孙节点,第一次拓展时不需要
        if (!isFirst) {
            children = tree.children();
            if (children != null)
                for (TreeGraphNode child : children)
                    candidateNodes.add(child);
        }
        return candidateNodes;
    }


    /**
     * 找出句子中的比较级词汇
     * 不做stem处理，是否stem由调用层决定
     *
     * @return
     */
    public static ArrayList<String> getComparatives() {
        ArrayList<String> comparatives = new ArrayList<String>();
        if (haveSpecificWord(tree, "more"))
            comparatives.add("more");
        comparatives.addAll(getSpecificPOSWords("RBR"));
        comparatives.addAll(getSpecificPOSWords("JJR"));
        return comparatives;
    }

    /**
     * 通过Stanford Parser找出句子指定成分的词
     * lowercace，不做stem处理，是否stem由调用层决定
     *
     * @return 获取的词语列表
     */
    public static ArrayList<String> getSpecificPOSWords(String postag) {
        ArrayList<String> NlpWords = new ArrayList<String>();

        // 获取所有叶子节点
        ArrayList<TreeGraphNode> leafNodes = new ArrayList<TreeGraphNode>();
        findAllLeaves(leafNodes, tree.root());
        for (TreeGraphNode leafNode : leafNodes)
            if (leafNode.parent().label().value().trim().startsWith(postag)) {
                NlpWords.add(leafNode.label().toString("value").toLowerCase());
            }

        return NlpWords;
    }


    /**
     * 找出句子中是否包含某个特定的词
     * lowercace，不做stem处理，是否stem由调用层决定
     *
     * @param treeG （分析出来的树）, str 指定的句子成分
     * @return 获取的词语列表
     */
    public static boolean haveSpecificWord(TreeGraph treeG,
                                           String word) {

        // 获取所有叶子节点
        ArrayList<TreeGraphNode> leafNodes = new ArrayList<TreeGraphNode>();
        findAllLeaves(leafNodes, treeG.root());
        for (TreeGraphNode leafNode : leafNodes)
            if (leafNode.parent().label().value().trim().startsWith(word)) {
                return true;
            }
        return false;
    }

    /**
     * N-gram 特征提取
     *
     * @param sentence 句子， n ， 是否做stem
     * @return 获得的词语列表
     */
    public static ArrayList<String> getNgramWords(String sentence, int n, boolean stem) {
        ArrayList<String> nGramWords = new ArrayList<>();
        sentence = sentence.replaceAll("[,.]", " ").replaceAll(" +", " ");

        List<String> words = Arrays.asList(sentence.split("\\s"));

        int maxLength = words.size();
        int i = 0;

        for (String word : words) {

            if (!stem) {
                nGramWords.add(word);
                for (int k = 1; k < n; k++) {
                    if (i + k < maxLength)
                        word = word + "_" + words.get(i + k);
                    nGramWords.add(word);
                }
            } else {
                nGramWords.add(FeatureExtractFunctions.stemmer.stemWord(word));
                for (int k = 1; k < n; k++) {
                    if (i + k < maxLength)
                        word = word + "_" + FeatureExtractFunctions.stemmer.stemWord(words.get(i + k));
                    nGramWords.add(word);
                }
            }
            i++;
        }

        return nGramWords;
    }


    /**
     * 使用Stanford Postagger进行postag
     */
    public static List<TaggedWord> getTaggedWord(String sentence) {
        //生成postag标注
        List<HasWord> sent = Sentence.toWordList(sentence.trim().split("\\s+"));
        return tagger.tagSentence(sent);
    }


    /**
     * 找出句子中的依存关系。 输入句子，输出句子依存关系
     * typedDependenciesCCprocessed会压缩很多连词表达，如instead of
     * 改成gs.allTypedDependencies(); 即basic+extra的
     */
    public static List<TypedDependency> getTypedDependencies(DataPoint dp) {

        List<HasWord> rawWords = Sentence.toWordList(dp.getSentence().trim().split("\\s+"));
        Tree parse = lexicalizedParser.apply(rawWords);

        // 进行句法分析生成句法树,使用TreeGraphNode以方便寻找父节点
        dp.tree = new TreeGraph(parse);

        //生成依存树
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
        //dp.getFeature().setTypedDependencies(gs.typedDependenciesCCprocessed());
        dp.setTypedDependencies(gs.typedDependencies(true));

        //在执行句法分析之后重新生成postag并覆盖原有的（因为进行过一次citation标记替换，所以句子结构有变）
        //parse = lexicalizedParser.parse(dp.getSentence());
        ArrayList<TaggedWord> taggedWords = parse.taggedYield();
        taggedWords.add(0, new TaggedWord("root", "root"));
        dp.setTaggedWords(taggedWords);

        return dp.getTypedDependencies();
    }

    public static List<TypedDependency> getTypedDependenciesFromSentence(String sentence) {

        List<HasWord> rawWords = Sentence.toWordList(sentence.trim().split("\\s+"));
        Tree parse = lexicalizedParser.apply(rawWords);

        //生成依存树
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
        //dp.getFeature().setTypedDependencies(gs.typedDependenciesCCprocessed());
        List<TypedDependency> dependencies = gs.typedDependencies(true);

        //在执行句法分析之后重新生成postag并覆盖原有的（因为进行过一次citation标记替换，所以句子结构有变）
        //parse = lexicalizedParser.parse(dp.getSentence());

        return dependencies;
    }
    /**
     * 找出句子中的依存关系。 输入句子，输出句子依存关系
     * typedDependenciesCCprocessed会压缩很多连词表达，如instead of
     */
    public static List<TypedDependency> getStrictTypedDependencies(DataPoint dp) {

        List<HasWord> rawWords = Sentence.toWordList(dp.getSentence().trim().split("\\s+"));
        Tree parse = lexicalizedParser.apply(rawWords);

        // 进行句法分析生成句法树,使用TreeGraphNode以方便寻找父节点
        dp.tree = new TreeGraph(parse);

        //生成依存树
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
//        dp.setTypedDependencies(gs.typedDependencies(true));
        dp.setTypedDependencies(gs.typedDependenciesCCprocessed(false));

        //在执行句法分析之后重新生成postag并覆盖原有的（因为进行过一次citation标记替换，所以句子结构有变）
        //parse = lexicalizedParser.parse(dp.getSentence());
        ArrayList<TaggedWord> taggedWords = parse.taggedYield();
        taggedWords.add(0, new TaggedWord("root", "root"));
        dp.setTaggedWords(taggedWords);

        return dp.getTypedDependencies();
    }

    /**
     * 输出句子依存关系统计(关系,出现次数)
     * 注意要剔除有些词的citation标记
     * @param dp 指定要处理的数据
     * @param  lemmatization 指明是否需要进行词根提取
     */
    public static ArrayList<String> getDependencyRelations(DataPoint dp, Boolean lemmatization) {
        HashSet<String> statistics = new HashSet<>();

        List<TypedDependency> tdl = dp.getTypedDependencies();

        // 记录下出现的所有关系名和分别出现的次数
        String typedDependency = "";
        for (TypedDependency td : tdl) {
/*			String tdReln = td.reln().toString();
			String tdGov = td.gov().nodeString();
			String tdDep = td.dep().nodeString();
			System.out.println(tdReln+"("+ FeatureExtractor.stemmer.stemWord(tdGov)+","+ FeatureExtractor.stemmer.stemWord(tdDep)+")");
			*/
            if (lemmatization)
                typedDependency = td.reln().toString() + "(" + FeatureExtractFunctions.stemmer.stemWord(FeatureExtractFunctions.citationTagRemoval(td.gov().nodeString())) + "," + FeatureExtractFunctions.stemmer.stemWord(FeatureExtractFunctions.citationTagRemoval(td.dep().nodeString())) + ")";
            else
                typedDependency = td.reln().toString() + "(" + FeatureExtractFunctions.citationTagRemoval(td.gov().nodeString()) + "," + FeatureExtractFunctions.citationTagRemoval(td.dep().nodeString()) + ")";

            statistics.add(typedDependency);

        }
        return new ArrayList<>(statistics);
    }


    /**
     * Charles特征
     * 根据句法关系找到ROOT词汇
     * @param dp
     * @return
     */
    public static String findRootWord(DataPoint dp, Boolean lemmatization) {
        for (TypedDependency td : dp.getTypedDependencies()) {
            if (td.reln().toString().toLowerCase().equals("root"))
                if (lemmatization)
                    return FeatureExtractFunctions.stemmer.stemWord(FeatureExtractFunctions.citationTagRemoval(td.dep().value()));
                else
                    return FeatureExtractFunctions.citationTagRemoval(td.dep().value());
        }
        return "";
    }

    public static Integer findRootWordIndex(DataPoint dp) {
        for (TypedDependency td : dp.getTypedDependencies()) {
            if (td.reln().toString().toLowerCase().equals("root"))
                return td.dep().index();
        }
        return 0;
    }

    /**
     * Charles特征
     * 根据句法关系找到main verb
     * 方法是：
     * 1. 先定位到root关系的dep
     * 2. 如果是个动词，则返回，如果不是，则遍历其所有dep，返回是动词的一个
     *
     * @param dp
     * @return
     */
    public static String findMainVerb(DataPoint dp, Boolean lemmatization) {
        Tree parse = lexicalizedParser.parse(dp.getSentence());

        ArrayList<TaggedWord> taggedWords = parse.taggedYield();

        for (TypedDependency td : dp.getTypedDependencies()) {
            if (td.reln().toString().toLowerCase().equals("root")) {
                //System.out.println(td.toString());
//                System.out.println(taggedWords.get(td.dep().index()-1));

                int index = 0;
                if (taggedWords.get(td.dep().index() - 1).tag().startsWith("VB"))
                    index = td.dep().index();
                else
                    for (TypedDependency td1 : dp.getTypedDependencies())
                        if (td1.gov().index() == td.dep().index())
                            if (taggedWords.get(td1.dep().index() - 1).tag().startsWith("VB"))
                                index = td1.dep().index();

                //如果直接相连的几个词不是动词（一般是parser识别质量不高），返回最靠前的一个VB
                if (index == 0) {
                    for (index = 0; index < taggedWords.size(); index++)
                        if (taggedWords.get(index).tag().startsWith("VB")) {
                            index++;
                            break;
                        }
                }

                if (lemmatization)
                    return FeatureExtractFunctions.stemmer.stemWord(FeatureExtractFunctions.citationTagRemoval(taggedWords.get(index - 1).word()));
                else
                    return FeatureExtractFunctions.citationTagRemoval(taggedWords.get(index - 1).word());

            }
        }
        return "";
    }

    /**
     * Charles特征
     * 根据句法关系找到main verb
     * 方法是：
     * 1. 先定位到root关系的dep
     * 2. 如果是个动词，则返回，如果不是，则遍历其所有dep，返回是动词的一个
     *
     * @param dp
     * @return
     */
    public static TaggedWord findMainVerb(DataPoint dp) {
        Tree parse = lexicalizedParser.parse(dp.getSentence());

        ArrayList<TaggedWord> taggedWords = parse.taggedYield();

        for (TypedDependency td : dp.getTypedDependencies()) {
            if (td.reln().toString().toLowerCase().equals("root")) {
                //System.out.println(td.toString());
//                System.out.println(taggedWords.get(td.dep().index()-1));

                int index = 0;
                if (taggedWords.get(td.dep().index() - 1).tag().startsWith("VB"))
                    index = td.dep().index();
                else
                    for (TypedDependency td1 : dp.getTypedDependencies())
                        if (td1.gov().index() == td.dep().index())
                            if (taggedWords.get(td1.dep().index() - 1).tag().startsWith("VB")) {
                                index = td1.dep().index();
                                break;
                            }

                //如果直接相连的几个词不是动词（一般是parser识别质量不高），返回最靠前的一个VB
                if (index == 0) {
                    for (index = 0; index < taggedWords.size(); index++)
                        if (taggedWords.get(index).tag().startsWith("VB")) {
                            index++;
                            break;
                        }
                }

                return taggedWords.get(index - 1);
            }
        }
        return null;
    }


    static List<String> contrastiveConjunctions = Arrays.asList("although", "in spite of", "unlike", "even though", "though", "even if", "despite", "all the same", "notwithstanding", "at the same time", "whereas", "in contrast", "while", "on the contrary", "however", "even so", "instead", "compared to", "nevertheless", "contrary to", "yet", "still");

    /**
     * Charles特征
     * 看是否citation出现在一个比较连词contrastive conjunction引导的从句中
     *
     * @param dp
     * @return
     */
    public static boolean hasOtherContrast(DataPoint dp) {
        String regex;
        for (String contrastiveConjunction : contrastiveConjunctions) {
            ArrayList<TreeGraphNode> nodesQueue = new ArrayList<>();
            regex = "\\b" + contrastiveConjunction + "\\b";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(dp.getSentence().toLowerCase());
            //找到句子中是否包含比较连词
            if (matcher.find()) {
                //System.out.println("发现连词" + contrastiveConjunction);
                /*
                 * 如果找到了则找到该比较连词的位置，并将其gov加入搜索队列
                 */
                // 如果连词只是一个词，那比较简单，直接把其gov加入
                if (contrastiveConjunction.indexOf(" ") == -1) {
                    for (TypedDependency td : dp.getTypedDependencies())
                        if (td.dep().value().toLowerCase().equals(contrastiveConjunction)) {
                            nodesQueue.add(td.gov());
                        }
                } else {
                    //如果是一个phrase则麻烦一点，将他们的gov全部加入好了
                    List<HasWord> words = Sentence.toWordList(dp.getSentence().trim().split("\\s+"));
                    //第0个是root
                    int phraseLength = contrastiveConjunction.split(" ").length;
                    StringBuffer tempPhrase = new StringBuffer();
                    for (int i = 0; i < words.size() - phraseLength; i++) {
                        for (int j = 0; j < phraseLength; j++)
                            tempPhrase.append(" " + words.get(i + j));
                        if (tempPhrase.toString().toLowerCase().trim().equals(contrastiveConjunction)) {
                            for (TypedDependency td : dp.getTypedDependencies())
                                if ((td.dep().index() > i) && (td.dep().index() <= i + phraseLength)) {
                                    nodesQueue.add(td.gov());
                                }
                        }
                        tempPhrase.setLength(0);
                    }

                    for (TypedDependency td : dp.getTypedDependencies())
                        if (td.dep().value().toLowerCase().equals(contrastiveConjunction)) {
                            nodesQueue.add(td.gov());
                        }
                }
/*                if (nodesQueue.isEmpty())
                    System.out.println("not find gov");*/
                //从比较连词的gov为起点，找其所有dep中是否包含citation
                TreeGraphNode headNode;
                while (!nodesQueue.isEmpty()) {
                    headNode = nodesQueue.get(0);
                    nodesQueue.remove(0);
                    if (headNode.value().contains("<citation>") || headNode.value().contains("_citation"))
                        return true;

                    for (TypedDependency td1 : dp.getTypedDependencies())
                        if (td1.gov().index() == headNode.index())
                            nodesQueue.add(td1.dep());
                }
            }
        }
        return false;
    }

    /**
     * Charles特征
     * 看是否一个比较级词汇的主语是本文的主语代词（we，us等）引导的从句中
     *
     * @param dp
     * @return
     */
    public static boolean hasSelfComp(DataPoint dp) {
        /*if(dp.getSentence().indexOf("this paper")!=-1)
            System.out.println();*/
        ArrayList<Integer> wordQueue = new ArrayList<>();
        dp.getTaggedWords().stream().filter(s -> s.tag().equals("RBR") || s.tag().equals("JJR")).forEach(s -> wordQueue.add(dp.getTaggedWords().indexOf(s)));
        /*if(wordQueue.size()>0)
            wordQueue.stream().forEach(s->System.out.println(dp.getTaggedWords().get(s)));*/
        for (int index : wordQueue) {
            ArrayList<TreeGraphNode> subjectWords = findSubjectWord(dp, index);
            //System.out.println(subjectWords);
            for (TreeGraphNode subjectWord : subjectWords)
                if (subjectWord != null && checkIfContainsAuthor(dp, subjectWord)) {
                    return true;
                }
        }
        return false;
    }

    /**
     * Charles特征
     * 看是否一个比较级词汇的主语连接的是引文
     * TODO more specific clause (the bottom clause) is built using inverse entailment_citation , generally consisting of the representation of all the knowledge about that example
     * 这种补足语有时候包含重要的信息
     * TODO Since the number of parameters in NLM is still large , several smoothing methods are used_citation to produce more accurate probabilities
     * citation位置诡异
     * TODO Therefore we make use of an online learning algorithm proposed by <citation> , which has a much smaller computational cost
     * citation在对宾语的从句中
     *
     * @param dp
     * @return
     */
    public static boolean hasOtherComp(DataPoint dp) {
        ArrayList<Integer> wordQueue = new ArrayList<>();
        dp.getTaggedWords().stream().filter(s -> s.tag().equals("RBR") || s.tag().equals("JJR")).forEach(s -> wordQueue.add(dp.getTaggedWords().indexOf(s)));
        /*if(wordQueue.size()>0)
            wordQueue.stream().forEach(s->System.out.println(dp.getTaggedWords().get(s)));
*/
        for (int index : wordQueue) {
            ArrayList<TreeGraphNode> subjectWords = findSubjectWord(dp, index);
            for (TreeGraphNode subjectWord : subjectWords)
                if (subjectWord != null && checkIfContainsCitation(dp, subjectWord)) {
                    return true;
                }
        }
        return false;
    }

    /**
     * Charles特征
     * 看是否一个正向词汇的主语是否包含作者代词
     *
     * @param dp
     * @return
     */
    public static boolean hasSelfGood(DataPoint dp) {
        /*if(dp.getSentence().indexOf("this paper")!=-1)
            System.out.println();*/
        ArrayList<Integer> wordQueue = new ArrayList<>();
        HashMap<String, OpinionFinder> positiveWords = OpinionFinder.getPositiveWords();
        dp.getTaggedWords().stream().filter(s -> OpinionFinder.containsWords(positiveWords, s)).forEach(s -> wordQueue.add(dp.getTaggedWords().indexOf(s)));
        if (wordQueue.size() > 0)
            wordQueue.stream().forEach(s -> System.out.println(dp.getTaggedWords().get(s) + "-" + s));
        for (int index : wordQueue) {
            ArrayList<TreeGraphNode> subjectWords = findSubjectWord(dp, index);
            System.out.println(subjectWords);
//            System.out.println();
            for (TreeGraphNode subjectWord : subjectWords)
                if (subjectWord != null && checkIfContainsAuthor(dp, subjectWord)) {
                    return true;
                }
        }
        return false;
    }

    /**
     * 判断该词周围(该词以及其dep词汇)是否包含体现本文作者角色的特征词汇（we,us,this paper..）
     *
     * @param dp
     * @param subjectWord
     * @return
     */
    private static boolean checkIfContainsAuthor(DataPoint dp, TreeGraphNode subjectWord) {
        ArrayList<Integer> wordQueue = new ArrayList<>();
        wordQueue.add(subjectWord.index());
        List<TaggedWord> taggedWords = dp.getTaggedWords();
        HashSet<Integer> filter = new HashSet<>();

        while (wordQueue.size() > 0) {
            int head = wordQueue.get(0);
            wordQueue.remove(0);
            filter.add(head);
            if (taggedWords.get(head).word().toLowerCase().equals("us") || taggedWords.get(head).word().toLowerCase().equals("we")||taggedWords.get(head).word().toLowerCase().equals("ours"))
                return true;
            for (TypedDependency td : dp.getTypedDependencies())
                if (td.gov().index() == head) {
                    //System.out.println(td);
                    if (td.dep().value().toLowerCase().equals("our"))
                        return true;
                    if (td.gov().value().toLowerCase().equals("paper") || td.dep().value().toLowerCase().equals("this"))
                        return true;
                    if (td.gov().value().toLowerCase().equals("work") || td.dep().value().toLowerCase().equals("this"))
                        return true;
                    if (td.gov().value().toLowerCase().equals("study") || td.dep().value().toLowerCase().equals("this"))
                        return true;
                    if(!filter.contains(td.dep().index()))
                        wordQueue.add(td.dep().index());
                }
        }
        return false;
    }

    private static boolean checkIfContainsCitation(DataPoint dp, TreeGraphNode subjectWord) {
        ArrayList<Integer> wordQueue = new ArrayList<>();
        wordQueue.add(subjectWord.index());
        List<TaggedWord> taggedWords = dp.getTaggedWords();
        HashSet<Integer> filter = new HashSet<>();

        while (wordQueue.size() > 0) {
            int head = wordQueue.get(0);
            wordQueue.remove(0);
            filter.add(head);

            if (taggedWords.get(head).word().toLowerCase().contains("_citation") || taggedWords.get(head).word().toLowerCase().contains("<citation>")) {
                //System.out.println(taggedWords.get(head).word());
                return true;
            }
            for (TypedDependency td : dp.getTypedDependencies())
                if (td.gov().index() == head && !filter.contains(td.dep().index()))
                    wordQueue.add(td.dep().index());

        }
        return false;
    }

    /**
     * 递归寻找其主语，由于依存关系中存在环，因此需要一个HashMap进行去重过滤
     *
     * @param dp
     * @param index
     * @return
     */
    private static ArrayList<TreeGraphNode> findSubjectWord(DataPoint dp, int index) {
        ArrayList<Integer> wordQueue = new ArrayList<>();
        ArrayList<TreeGraphNode> returnList = new ArrayList<>();
        HashSet<Integer> filter = new HashSet<>();
        int head;
        wordQueue.add(index);
        while (wordQueue.size() > 0) {
            head = wordQueue.get(0);
            wordQueue.remove(0);
            filter.add(head);
//            System.out.println(dp.getTaggedWords().get(head));
            for (TypedDependency td : dp.getTypedDependencies()) {
//                System.out.println(td);
                // 找到当前词的gov并加入到队列中，即向上寻找
                if (td.dep().index() == head && !filter.contains(td.gov().index()))
                    wordQueue.add(td.gov().index());
                if (td.gov().index() == head) {
                    //找到主语关系并返回主语
                    if (td.reln().toString().contains("subj") && !dp.getTaggedWords().get(td.dep().index()).tag().startsWith("W"))
                        returnList.add(td.dep());
                }
            }
        }
        return returnList;
    }

    /**
     * 查看一个句子的 typeofdepencies 是否包括一个指定的dependency关系
     */
    public static boolean findDependency(DataPoint dp, String dependency) {
        GrammarUtils.getTypedDependencies(dp);
        Boolean b = false;
        for (TypedDependency typeofdependencies : dp.getTypedDependencies())
            if (typeofdependencies.toString().startsWith(dependency)) {
                b = true;
                break;
            }
        return b;
    }

    /**
     * 获取该句话的时态，抽取main verb，然后根据其形式来判断
     * tense 时态（过去时(0)，现在时(1)，未来时(2)，完成时(3)，进行时(4)）
     * 其他返回5
     *
     * @param dp
     * @return
     */
    public static int extractTense(DataPoint dp) {
        TaggedWord mainVerb = findMainVerb(dp);
        //System.out.println(mainVerb.toString());
        //VB  Verb, base form
        //VBP Verb, non-3rd person singular present
        //VBZ Verb, 3rd person singular present
        if (mainVerb.tag().equalsIgnoreCase("VB") || mainVerb.tag().equalsIgnoreCase("VBP") || mainVerb.tag().equalsIgnoreCase("VBZ"))
            return 1;
        //VBN past participle
        if (mainVerb.tag().equalsIgnoreCase("VBN")) {
            //看前面一个词是不是be，如果是be根据be的形式来判断时态
            if (dp.getTaggedWords().indexOf(mainVerb) > 0) {
                TaggedWord lastWord = dp.getTaggedWords().get(dp.getTaggedWords().indexOf(mainVerb) - 1);
                if (FeatureExtractFunctions.stemmer.stemWord(lastWord.word()).equals("be")) {
                    if (lastWord.word().equals("am") || lastWord.word().equals("is") || lastWord.word().equals("are") || lastWord.word().equals("be"))
                        return 1;
                    else if (lastWord.word().equals("was") || lastWord.word().equals("were"))
                        return 0;
                    else if (lastWord.word().equals("been"))
                        return 3;
                }
            } else {
                return 3;
            }
        }
        //VBD Verb, past tense
        if (mainVerb.tag().equalsIgnoreCase("VBD"))
            return 0;
        //VBG Verb, gerund or present participle
        if (mainVerb.tag().equalsIgnoreCase("VBG"))
            return 4;
        //VBN Verb, past participle
        if (mainVerb.tag().equalsIgnoreCase("VBD"))
            return 3;
        if (dp.getSentence().contains("will") || dp.getSentence().contains("going to"))
            return 2;
        return 5;
    }


    public enum EntityType{
        Citation, Author
    }

    /**
     * 通过parser的Typed Dependency结果来寻找与给定的实体类型（作者主语，引文）最近的关键词（动词、名词、形容词等）
     * @param entityType 要查找的类型
     * @param wordTypes 指定要返回的词性
     * @param wordNumber 指定要返回最近几个关键词
     * @param lemmatization 返回结果是否进行词干提取
     * @return 返回关键词列表，通过EntityType_word的形式返回
     */
    public static ArrayList<TaggedWord> getLinkedTaggedWordsOfCertainEntity(List<TaggedWord> taggedWords, List<TypedDependency> typedDependencies, EntityType entityType, String[] wordTypes, int wordNumber, boolean lemmatization) {
        ArrayList<TaggedWord> returnList = new ArrayList<>();

        //保存author subject或者citation所在的位置
        ArrayList<TaggedWord> entityList = new ArrayList<>();
        entityList.addAll(GrammarUtils.findEntitiesWords(taggedWords, entityType));
//        System.out.println(entityType + ":" + entityList);

        //去重用,保存候选词taggedWord
        HashSet<Integer> indexSet = new HashSet<>();
        /*
         * 循环每一个NpNode以及每一个词性类型，在树中寻找
		 */
        for (TaggedWord entity : entityList)
            for(String types:wordTypes){
                indexSet.addAll(findLinkedWordsAroundWords(taggedWords, typedDependencies, entity, types, wordNumber));
            }
        //System.out.println(keywords);
        indexSet.stream().forEach(index -> returnList.add(taggedWords.get(index)));

        return returnList;
    }

    public static ArrayList<String> getLinkedKeyWordsOfCertainEntity(List<TaggedWord> taggedWords, List<TypedDependency> typedDependencies, EntityType entityType, String[] wordTypes, int wordNumber, boolean lemmatization) {
        ArrayList<TaggedWord> linkedWords = getLinkedTaggedWordsOfCertainEntity(taggedWords, typedDependencies, entityType, wordTypes, wordNumber, lemmatization);
        ArrayList<String> returnList = new ArrayList<>();

        String tempWord;
        for(TaggedWord linkedWord : linkedWords){
            tempWord = linkedWord.word().trim().toLowerCase();
            if(GrammarUtils.isModifiedByNegation(taggedWords, taggedWords.indexOf(linkedWord)))
                tempWord = "not_"+tempWord;
            if (RunFeatureExtract.Lemmatization)
                returnList.add(FeatureExtractFunctions.stemmer.stemWord(tempWord));
            else
                returnList.add(tempWord);
        }

        return returnList;
    }

    /**
     * 判断index位置处的词前是否有否定表达
     * 设定窗口长度为3
     * @param index
     * @return
     */
    public static boolean isModifiedByNegation(List<TaggedWord> taggedWords, int index) {
//        System.out.println("Start:"+dp.getTaggedWords().get(index).word());
        int startIndex = index < 3 ? 0 : index - 3;
        for(int i = index-1 ; i >= startIndex; i--){
            String word = taggedWords.get(i).word();
            if(word.equals(","))
                break;
//            System.out.println(word);
            if(negationWordsCues.stream().anyMatch(s -> s.equals(word)))
                return true;
            if(negationSuffixCues.stream().anyMatch(s->word.endsWith(s)))
                return true;
        }
        return false;
    }

    /**
     * 判断句子中是否包含否定表达
     * @return
     */
    public static boolean containsNegation(String sentence) {
        List<String> words = Arrays.asList(sentence.split("\\s+"));
        for(String word : words){
            if(negationWordsCues.stream().anyMatch(s->s.equals(word)))
                return true;
            if(negationSuffixCues.stream().anyMatch(s->word.endsWith(s)))
                return true;
        }
        return false;
    }
    /**
     * 找到与指定词相连的最近的相关词
     *
     * @param taggedWords
     * @param typedDependencies
     *@param startPoint 开始搜索的起点
     * @param posType 要返回的词性
     * @param neededWordNumber 要返回几个词    @return
     */
    private static ArrayList<Integer> findLinkedWordsAroundWords(List<TaggedWord> taggedWords, List<TypedDependency> typedDependencies, TaggedWord startPoint, String posType, int neededWordNumber) {
        posType = posType.trim().toLowerCase();
        ArrayList<TaggedWord> wordQueue = new ArrayList<>();
        wordQueue.add(startPoint);
        ArrayList<Integer> returnList = new ArrayList<>();
        HashSet<Integer> filter = new HashSet<>();
        TaggedWord head;
        int headIndex;
        TaggedWord candidate;
        int candidateIndex;

        while (wordQueue.size() > 0) {
            head = wordQueue.get(0);
            headIndex = taggedWords.indexOf(head);
            wordQueue.remove(0);
            filter.add(headIndex);
//            System.out.println(dp.getTaggedWords().get(head));
            for (TypedDependency td : typedDependencies) {
//                System.out.println(td);
                // 找到当前词的gov并加入到队列中，即向上寻找
                if (td.dep().index() == headIndex || td.gov().index() == headIndex){
                    if(td.dep().index() == headIndex) {
                        candidateIndex = td.gov().index();
                        candidate = taggedWords.get(candidateIndex);
                    }else{
                        candidateIndex = td.dep().index();
                        candidate = taggedWords.get(candidateIndex);
                    }
                    if(filter.contains(candidateIndex))
                        continue;
                    if (candidate.tag().toLowerCase().startsWith(posType)) {
                        neededWordNumber--;
                        returnList.add(candidateIndex);
                        if(neededWordNumber<=0)
                            return returnList;
                    }
                    wordQueue.add(candidate);
                    filter.add(candidateIndex);
                }

            }
        }
        return returnList;
    }

    /**
     * 从句子中确定实体所在的节点
     *
     * @param entityType 指定获取的实体类型，subject或者citation
     */
    public static ArrayList<TaggedWord> findEntitiesWords(List<TaggedWord> taggedWords, EntityType entityType) {

        ArrayList<TaggedWord> returnList = new ArrayList<>();

        for (TaggedWord taggedWord : taggedWords){
            if (entityType==EntityType.Author) {
                if (taggedWord.word().trim().toLowerCase().equals("us") || taggedWord.word().trim().toLowerCase().equals("we")){
                    returnList.add(taggedWord);
                    continue;
                }
                if (taggedWord.word().trim().toLowerCase().equals("our")||taggedWord.word().trim().toLowerCase().equals("ours")){
                    returnList.add(taggedWord);
                    continue;
                }
                int index = taggedWords.indexOf(taggedWord);
                if( index < taggedWords.size()-1){
                    if (taggedWords.get(index).word().trim().toLowerCase().equals("this"))
                        if (taggedWords.get(index+1).word().trim().toLowerCase().equals("paper")||taggedWords.get(index+1).word().trim().toLowerCase().equals("work")||taggedWords.get(index+1).word().trim().toLowerCase().equals("study") )
                            returnList.add(taggedWord);
                }

            } else {
                if (taggedWord.word().trim().toLowerCase().contains("<citation>") || taggedWord.word().trim().toLowerCase().contains("_citation")){
                    returnList.add(taggedWord);
                    continue;
                }
            }

        }
        return returnList;
    }

    /**
     * 找到句子的主语
     * @return
     */
    public static String findSubjectPhrase(DataPoint dp) {
        ArrayList<TaggedWord> wordQueue = new ArrayList<>();
        /**
         * 将ROOT节点的词放进队列
         * 或者试试将MainVerb放进去
         */
//        TaggedWord rootWord = dp.getTaggedWords().get(findRootWordIndex(dp));
        TaggedWord rootWord = findMainVerb(dp);
        System.out.println("RootWord:\t"+dp.getTaggedWords().get(findRootWordIndex(dp)));
        System.out.println("MainVerb:\t"+rootWord);
        wordQueue.add(rootWord);
        TaggedWord head,candidate = null;
        TreeGraphNode candidateNode = null;
        int headIndex,candidateIndex = 0;
        HashSet<Integer> filter = new HashSet<>();
        boolean findSubject = false;

        HashSet<TreeGraphNode> subjectQueue = new HashSet<>();
        /**
         * 通过迭代找到句子的主语
         */
        while (wordQueue.size() > 0 && !findSubject) {
            head = wordQueue.get(0);
            headIndex = dp.getTaggedWords().indexOf(head);
            wordQueue.remove(0);
            filter.add(headIndex);
            //System.out.println(dp.getTaggedWords().get(headIndex));
            for (TypedDependency td : dp.getTypedDependencies()) {
//                System.out.println(td);
                if (td.dep().index() == headIndex || td.gov().index() == headIndex){
                    if(td.dep().index() == headIndex) {
                        candidateIndex = td.gov().index();
                        candidate = dp.taggedWords.get(candidateIndex);
                        candidateNode = td.gov();
                    }else{
                        candidateIndex = td.dep().index();
                        candidate = dp.taggedWords.get(candidateIndex);
                        candidateNode = td.dep();
                    }
                    if(filter.contains(candidateIndex))
                        continue;
                    if (td.reln().toString().contains("subj") && !dp.getTaggedWords().get(td.dep().index()).tag().startsWith("W")) {
                        subjectQueue.add(candidateNode);
                        System.out.println(candidate);
                    }
                    filter.add(candidateIndex);
                    wordQueue.add(candidate);
                }
            }
        }

        if(subjectQueue.size()>0) {
            System.out.println("发现"+subjectQueue.size()+"个主语");
            for(TreeGraphNode node : subjectQueue) {
                System.out.println(getPhrase(dp, node, filter));
                if (checkIfContainsAuthor(dp, node))
                    System.out.println("主语中包含作者");

                if (checkIfContainsCitation(dp, node))
                    System.out.println("主语中包含引文");
            }
        }else
            System.out.println("未找到主语");
        return "";
    }

    public static String getPhrase(DataPoint dp, TreeGraphNode node, HashSet<Integer> filter) {
        ArrayList<Integer> wordQueue = new ArrayList<>();
        wordQueue.add(node.index());
        List<TaggedWord> taggedWords = dp.getTaggedWords();

        while (wordQueue.size() > 0) {
            int head = wordQueue.get(0);
            wordQueue.remove(0);
            filter.add(head);

            for (TypedDependency td : dp.getTypedDependencies())
                if (td.gov().index() == head) {
                    if (!filter.contains(td.dep().index()))
                        wordQueue.add(td.dep().index());
                }
        }

        ArrayList<Integer> indeces = new ArrayList<>();
        filter.stream().forEach(indeces::add);
        indeces.sort((a,b)->a.compareTo(b));
        StringBuffer buffer = new StringBuffer();
        indeces.stream().filter(s -> s > 0 && s < taggedWords.size()).map(s->taggedWords.get(s)).forEach(s->buffer.append(s.word()+" "));

        return buffer.toString();
    }

}

	

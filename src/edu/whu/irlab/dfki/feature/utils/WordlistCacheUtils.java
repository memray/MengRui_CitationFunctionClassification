package edu.whu.irlab.dfki.feature.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * 类名称：WordlistCacheUtils
 * 类描述：用于缓存全局词表
 * 创建人：lxb
 * 创建时间：2014年10月28日 下午4:20:19
 */
public class WordlistCacheUtils {
    //各种词表所在路径
    static String cuePhrasesPath = "corpus" + File.separator + "cuePhrases" + File.separator;
    static String specelationCuePath = "corpus" + File.separator + "speculationCue.txt";
    //MAP<key:词表中的词,map:词表中的极性以及强弱属性>
    //用于提取abu的第8个特征：Closest Subjectivity Cue
    static public HashMap<String, OpinionFinder> subjectivityCueMap = new HashMap<String, OpinionFinder>();
    //用于提取abu的第7个特征
    static public ArrayList<String> abuSpeculationCueList = new ArrayList<String>();
    //用于提取abu的第6个特征
    static public HashSet<String> abuNegationCueSet = new HashSet<String>();
    //把cuePhrases文件夹里面的所有词表存储到一个treemap中<词表名字,词表里面的词>
    //treemap保持词表存放的顺序是固定的，方便后面的处理
    static public TreeMap<String, List<String>> cuePhrasesMap = new TreeMap<String, List<String>>();

    static {
        //把程序所需要的所有全局词表set到全局变量中
        subjectivityCueMap = new OpinionFinder().getOpinionFinders();
        abuSpeculationCueList = new WordlistCacheUtils().getSpeculationCueList();
        cuePhrasesMap = new WordlistCacheUtils().getCuePhrases();
    }

    /**
     * 获取 speculationCue 词表
     */
    public ArrayList<String> getSpeculationCueList() {
        ArrayList<String> specelationcuelSet = new ArrayList<String>();
        List<String> lines;
        try {
            lines = FileUtils.readLines(new File(specelationCuePath));
            for (String line : lines)
                specelationcuelSet.add(line.trim());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return specelationcuelSet;
    }

    /**
     * 生成 NegationCue 词表
     * 由于词表很小可以写到数组中，因此FeatureExtractor里没有用到该段程序
     */
    String negationCuePath = "corpus" + File.separator + "SEM-2012-SharedTask-CD-SCO-training-09032012.txt";

    public HashSet<String> getNegationCueList() {
        HashSet<String> negationCuesSet = new HashSet<String>();

        List<String> lines;
        String line = "";
        String[] columns;
        try {
            lines = FileUtils.readLines(new File(negationCuePath));
            int i = 0;
            while (true) {
                if (i >= lines.size())
                    break;
                if (lines.get(i).trim().isEmpty() || lines.get(i).indexOf("\t") == -1) {
                    i++;
                    continue;
                }
                line = lines.get(i);
                columns = line.split("\t");
                if (columns[7].trim().equals("***")) {
                    i++;
                    continue;
                } else
                    for (int j = 0; j + 7 < columns.length; j += 3)
                        if (!columns[j + 7].trim().equals("_")) {
                            negationCuesSet.add(columns[j + 7].trim().toLowerCase());
                            //System.out.println(i+" : "+columns[j+7].trim());
                        }

                i++;
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (String negationCue : negationCuesSet) {
                stringBuffer.append(negationCue).append("\n");
            }
            FileUtils.writeStringToFile(new File("corpus" + File.separator + "negationCue.txt"), stringBuffer.toString(), "utf8", false);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return negationCuesSet;
    }

    /**
     * @return 各个词表名字以及对应包含多少词的map
     */
    public TreeMap<String, List<String>> getCuePhrases() {
        TreeMap<String, List<String>> cuePhrasesMap = new TreeMap<String, List<String>>();
        for (File file : new File(cuePhrasesPath).listFiles()) {
            try {
                //获取词表内容
                String wordS = FileUtils.readFileToString(file);
                String[] wordString = wordS.split("\\s*,\\s*");
                cuePhrasesMap.put(file.getName().substring(0, file.getName().indexOf(".")), Arrays.asList(wordString));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return cuePhrasesMap;
    }

    public static void main(String[] args) {
        //new WordlistCacheUtils().getNegationCueList();
        TreeMap<String, List<String>> cp = new WordlistCacheUtils().getCuePhrases();
        List<String> list = cp.get("OP");
        for (String s : cp.keySet()) {
            System.out.println(s);
        }
    }
}

package edu.whu.irlab.dfki.feature;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.utils.GrammarUtils;

/**
 * 此特征抽取器为通用抽取器，将所有特征抽取 并保存到FeatureTemplate中 2014-09-24 liuxingbang 2014-10-5
 * Modified by Memray
 */
public class FeatureExtractor {
    /**
     * 抽取特征。 输入一个DataPoint类型数据，返回一个FeatureTemplate的List
     */
    public static CitationFeatureTemplate extract(DataPoint dp) {
        CitationFeatureTemplate ft = dp.getFeature();

        ft.setClassLable(dp.getLabel2());
        ft.setClassLableNumber(dp.getClassLabelNumber());

        try {
            GrammarUtils.tree = dp.tree;
            /**
             * Athar 特征部分
             */
            FeatureExtractFunctions.extractAtharFeatures(dp);

            /**
             * Teufel et al. (2006) 特征部分
             * 除了自引的都不实现了
             */
            FeatureExtractFunctions.extractTeufelFeatures(dp);

            /**
             * Abu-Jbara 特征
             */
            FeatureExtractFunctions.extractAbuJbaraFeatures(dp);

            /**
             * Ulrich特征部分
             */
            FeatureExtractFunctions.extractUlrichFeatures(dp);

            /**
             * reimplented features from Jochim & Schütze (2012) 42~59，ngram在6
             */
            FeatureExtractFunctions.extractCharlesFeatures(dp);

            /**
             * 我们添加的特征部分
             */
			FeatureExtractFunctions.extractOurFeatures(dp);
            FeatureExtractFunctions.extractExtraOurFeatures(dp);
            //释放掉以下资源
            dp.setTaggedWords(null);
            dp.setTypedDependencies(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ft;
    }


    /**
     * 抽取某一个记录中的Physical Features特征
     */
    public static void physicalFeatureExtract(DataPoint d) {
        //计算sentence的paper loc.
        float position = (float)d.getSentenceIndex()/(float)d.getSentenceNumber();
        if (position <= 0.25)
            d.getFeature().setPaperLocation(0.25f);
        else if (position>0.25 && position<=0.5)
            d.getFeature().setPaperLocation(0.5f);
        else if (position>0.5 && position<=0.75)
            d.getFeature().setPaperLocation(0.75f);
        else if (position>0.75 && position<=1)
            d.getFeature().setPaperLocation(1f);
        //获取前后句
        String neighborSentences = d.getFormerNeighborSentence()+d.getNextNeighborSentence();
        int neighborSentenceNumber = 0;
        if(!d.getFormerNeighborSentence().trim().isEmpty())
            neighborSentenceNumber++;
        if(!d.getNextNeighborSentence().trim().isEmpty())
            neighborSentenceNumber++;

        // 计算引用句中的引文个数
        int citationCountInCitingSentence = 0;
        String tag = "<citation";
        for (String token : d.getSentence().split(" "))
            if (token.startsWith(tag))
                citationCountInCitingSentence++;

        d.getFeature().setPopularity(citationCountInCitingSentence);

        // 计算相邻句中的引文个数

        //该正则只用来匹配前后句中的标记，引文句中通过<citation(注意没后半尖括号)标记来找
        String regex = "\\(.*?\\d{4}\\)";
        Pattern pattern = Pattern.compile(regex);
        int citationCountInNeighborSentences = 0;
        Matcher matcher = pattern.matcher(neighborSentences);
        while (matcher.find())
            citationCountInNeighborSentences++;
        d.getFeature().setCitationDensity(
                citationCountInCitingSentence
                        + citationCountInNeighborSentences);

        // 相邻句中的引文密度
        if (neighborSentenceNumber != 0)
            d.getFeature().setAvgDens(
                    (float)citationCountInNeighborSentences
                            / (float)neighborSentenceNumber);


        // Section Location,对应6个章节类型（Introduction, Related work, Method,
        // Experiment, Evaluation, and Conclusion）
        d.getFeature().setSectionNumber(
                FeatureExtractUtils.getSectionCategory(d.getSectionName()));

        // 计算句子的长度
        String[] words = d.getSentence().split(" ");
        if (words != null)
            d.getFeature().setSentenceLength(words.length);
        // 计算分句的个数
        words = d.getSentence().split(",");
        if (words != null)
            d.getFeature().setShortSentenceNumber(words.length);
        // 计算分句的平均长度
        if (d.getFeature().getShortSentenceNumber() != 0)
            d.getFeature().setShortSentenceAvgLength((float) d.getFeature().getSentenceLength() / (float) d.getFeature().getShortSentenceNumber());

/*            if (d.getFeature().getAclSentence() == null) {
                System.out.println("没有在数据库中找到对应句子");
            } else {
//                System.out.println("作者:\t" + d.getArticle().getAuthor());
                System.out.println("原句:\t" + d.getSentence());
                System.out.println("新句:\t" + d.getFeature().getAclSentence().getContent());
                System.out.println("章节:\t" + d.getFeature().getSectionNumber());
                System.out.println("句长:\t" + d.getFeature().getSentenceLength());
                System.out.println("句数:\t" + d.getFeature().getShortSentenceNumber());
                System.out.println("均长:\t" + d.getFeature().getShortSentenceAvgLength());
            }*/


    }

    /**
     * 输出为特征向量的列表，用于特征选择后查看各个向量的效果，即输出：特征id\t该特征内容
     * 基本同FeatureTemplate中的toOurFeatureVector()方法，只不过把输出特征改为
     *
     * @return
     */
    public static void outputFeatureVectorList() throws IOException {
        StringBuilder builder = new StringBuilder();

        int i = 1;

        /**
         * Ulrich 词表特征
         */
        builder.append((i++) + ":Ulrich_SubjectWeight\n");
        builder.append((i++) + ":Ulrich_hasSubject\n");
        builder.append((i++) + ":Ulrich_QuantityWeight\n");
        builder.append((i++) + ":Ulrich_hasQuantity\n");
        builder.append((i++) + ":Ulrich_FrequencyWeight\n");
        builder.append((i++) + ":Ulrich_hasFrequency\n");
        builder.append((i++) + ":Ulrich_TenseWeight\n");
        builder.append((i++) + ":Ulrich_hasTense\n");
        builder.append((i++) + ":Ulrich_ExampleWeight\n");
        builder.append((i++) + ":Ulrich_hasExample\n");
        builder.append((i++) + ":Ulrich_SuggestWeight\n");
        builder.append((i++) + ":Ulrich_hasSuggest\n");

        builder.append((i++) + ":Ulrich_HedgeWeight\n");
        builder.append((i++) + ":Ulrich_IdeaWeight\n");
        builder.append((i++) + ":Ulrich_hasIdea\n");
        builder.append((i++) + ":Ulrich_BasisWeight\n");
        builder.append((i++) + ":Ulrich_hasBasis\n");
        builder.append((i++) + ":Ulrich_CompareWeight\n");
        builder.append((i++) + ":Ulrich_hasCompare\n");
        builder.append((i++) + ":Ulrich_ResultWeight\n");
        builder.append((i++) + ":Ulrich_hasResult\n");

        /**
         * Ulrich 句法特征
         */
        builder.append((i++) + ":Ulrich_hasSyntacticFeature_1\n");
        builder.append((i++) + ":Ulrich_hasSyntacticFeature_2\n");
        builder.append((i++) + ":Ulrich_hasSyntacticFeature_3\n");
        builder.append((i++) + ":Ulrich_hasSyntacticFeature_4\n");
        builder.append((i++) + ":Ulrich_hasSyntacticFeature_5\n");
        builder.append((i++) + ":Ulrich_hasSyntacticFeature_6\n");
        builder.append((i++) + ":Ulrich_hasSyntacticFeature_7\n");

        /**
         * Ulrich 物理特征
         */
        builder.append((i++) + ":UlrichPhysical_sectionNumber\n");
        builder.append((i++) + ":UlrichPhysical_popularity\n");
        builder.append((i++) + ":UlrichPhysical_citationDensity\n");
        builder.append((i++) + ":UlrichPhysical_avgDens\n");
        builder.append((i++) + ":UlrichPhysical_sentenceLength\n");
        builder.append((i++) + ":UlrichPhysical_shortSentenceNumber\n");
        /**
         * 我们的 词表特征
         */
        //继承并改进Ulrich的词表
        builder.append((i++) + ":OurSubjectWeight\n");
        builder.append((i++) + ":hasOurSubjectBoolean\n");
        builder.append((i++) + ":getOurCompareWeight\n");
        builder.append((i++) + ":hasOurCompareBoolean\n");
        builder.append((i++) + ":getOurIdeaWeight\n");
        builder.append((i++) + ":hasOurIdeaBoolean\n");
        builder.append((i++) + ":getOurBasisWeight\n");
        builder.append((i++) + ":hasOurBasisBoolean\n");
        builder.append((i++) + ":getOurResultWeight\n");
        builder.append((i++) + ":hasOurResultBoolean\n");

        builder.append((i++) + ":getOurQuantityWeight\n");
        builder.append((i++) + ":hasOurQuantityBoolean\n");
        builder.append((i++) + ":getOurFrequencyWeight\n");
        builder.append((i++) + ":hasOurFrequencyBoolean\n");
        builder.append((i++) + ":getOurTenseWeight\n");
        builder.append((i++) + ":hasOurTenseBoolean\n");
        builder.append((i++) + ":getOurExampleWeight\n");
        builder.append((i++) + ":hasOurExampleBoolean\n");
        builder.append((i++) + ":getOurSuggestWeight\n");
        builder.append((i++) + ":hasOurSuggestBoolean\n");

        builder.append((i++) + ":getOurHedgeWeight\n");
        builder.append((i++) + ":hasOurHedgeBoolean\n");

        /**
         * 原创词表
         */
        builder.append((i++) + ":OurOrigin_getFutureWeight\n");
        builder.append((i++) + ":OurOrigin_hasFutureBoolean\n");
        builder.append((i++) + ":OurOrigin_getToolWeight\n");
        builder.append((i++) + ":OurOrigin_hasToolBoolean\n");

        builder.append((i++) + ":comparativeBoolean\n");

        /**
         * 输出词汇特征
         */
        if (RunFeatureExtract.TargetFeatureName.equals("our")) {
            /**
             * 我们的 词汇特征，不使用filter了
             */
            //用treemap，key为词汇的在全局词表中的序号ID，通过treemap默认按照升序排序
            TreeMap<Integer, String> wordTreeMap = new TreeMap<Integer, String>();
            for (Map.Entry<String, Integer> entry : FeatureConverter.GlobalWordTable.entrySet())
                wordTreeMap.put(entry.getValue(), entry.getKey());
            for (Map.Entry<Integer, String> entry : wordTreeMap.entrySet()) {
                builder.append((i + entry.getKey()) + ":" + entry.getValue() + "\n");
            }
        }
        /*
         * jochim的保存在GlobalNgram中
         */
        else if (RunFeatureExtract.TargetFeatureName.equals("jochim")) {
            for (String word : FeatureConverter.GlobalNgram.keySet())
               /* if (this.getnGram().contains(word))
                    builder.append((i++) + ":1");
                else
                    builder.append((i++) + ":0");*/
                builder.append((i++) + ":" + word + "\n");
        }

        //FileUtils.writeStringToFile(new File(Classification.FeatureListPath), builder.toString(), "utf8", false);
    }


}

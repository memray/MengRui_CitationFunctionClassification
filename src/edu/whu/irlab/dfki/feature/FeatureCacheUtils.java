package edu.whu.irlab.dfki.feature;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;

import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.utils.GrammarUtils;
import org.apache.commons.io.FileUtils;
import com.alibaba.fastjson.JSON;
import edu.whu.irlab.bean.DataPoint;

/**
 * 用于返回指定类别标签的所有数据
 * 直接读取原始的文件(DFKI.txt)，方便维护，忽略中间生成文件
 */
public class FeatureCacheUtils {
    /**
     * 读取从原数据（corpus/DFKI.txt）中的抽取的初步处理信息的缓存
     * @param dataPoints
     */
    public static ArrayList<DataPoint> readDataCache(ArrayList<DataPoint> dataPoints) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(RunFeatureExtract.DataCachePath), "utf8"));
            String line = "";
            DataPoint tempDataPoint = new DataPoint();
            int i = 0;
            while ((line = reader.readLine()) != null) {
                //System.out.println("正在读取第"+ (i+1) +"条数据");
                tempDataPoint = parseFeatureString(line);
                dataPoints.add(tempDataPoint);
                i++;
            }
            System.out.println("共读取了 "+i+" 条特征数据");
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataPoints;
    }



    /**
     * 读取原数据（corpus/DFKI.txt）中的所有类别的数据，并转化为对象返回
     */
    public static ArrayList<DataPoint> readDataPoints() {
        return readDataPoints(new HashSet<String>());
    }

    /**
     * 读取原数据（corpus/DFKI.txt）并转化为对象返回
     * @param labelNames 指明需要返回的功能类别，默认全部返回
     * @return
     */
	public static ArrayList<DataPoint> readDataPoints(HashSet<String> labelNames) {
		ArrayList<DataPoint> dataList = new ArrayList<DataPoint>();
		try {
			String path = "corpus/DFKI.txt";
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(path), "utf8"));
			String line = "";

			while ((line = reader.readLine()) != null) {
				// System.out.println(line);
				DataPoint data = new DataPoint(line);
				
				//如果没有注明类别，则全部加入
				if(labelNames.size()==0){
					dataList.add(data);
					continue;
				}
				
				if (labelNames.contains(data.getLabel2()))
					dataList.add(data);
			}
			reader.close();

            int count = 0;


		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataList;
	}
	
	/**
	 * 处理DFKI数据，提取所有特征并写入目标文件中
     * @param outPutPath 为输出路径
     * @param startIndex 增加断点续处理的逻辑，该index表明从第几个数据开始重新生成特征
     */
	public static void extractFeatures(ArrayList<DataPoint> dataPoints, String outPutPath, int startIndex, Boolean lemmatization){
		try {
            File outputFile = new File(outPutPath);
            for(int i = startIndex; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                System.out.println("生成第  "+ (i+1) +" 条");
                /*if(count < 53)
                    continue;*/
                //抽取句法信息
                GrammarUtils.getTypedDependencies(dp);
                //抽取物理特征
    			FeatureExtractor.physicalFeatureExtract(dp);
	    		//抽取其他特征
                FeatureExtractor.extract(dp);
                FileUtils.write(outputFile, toFeatureString(dp) + "\n", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 读取之前（generateFextractFeatures据，通过dataPoints返回
     * 返回总共从缓存中读取了多少个条目
	 */
	public static int readFeatureCacheData(ArrayList<DataPoint> dataPoints, String featureDataPath){
		try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(featureDataPath), "utf8"));
			String line = "";
            DataPoint tempDataPoint = new DataPoint();
			int i = 0;
			while ((line = reader.readLine()) != null) {
				//System.out.println("正在读取第"+ (i+1) +"条数据");
                tempDataPoint = parseFeatureString(line);
                if(tempDataPoint.getSentence().equals(dataPoints.get(i).getSentence()))
                    dataPoints.set(i, tempDataPoint);
                else
                    System.out.println("发现不匹配");
				i++;
			}
			System.out.println("共读取了 "+i+" 条特征数据");
			reader.close();
            return i;
		} catch (Exception e) {
			e.printStackTrace();
		}
        return 0;
	}
	
	/**
	 * 把提取的动词词表放到一个txt中，方便后期去取
	 */
/*	public static void exportVerbs(String path, HashSet<String> verbSet) {
		StringBuilder builder = new StringBuilder();
		for(String s : verbSet){
			builder.append(s);
			builder.append("\r\n");	
		}
		try {
			FileUtils.write(new File(path), builder.toString(), "utf8", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/
	/**
	 * 输出到一个html以方便查看
	 */
	public static void exportHTML(String path, ArrayList<DataPoint> dataPoints) {
		StringBuilder builder = new StringBuilder();
		builder.append("<html><head>");
		builder.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");

		builder.append("<link type=\"text/css\" rel=\"stylesheet\" href=\"style/style.css\" />");
		builder.append("</head>");
		builder.append("<body>");
		for (DataPoint data : dataPoints)
			builder.append(data.toHTML());
		builder.append("</body></html>");
		try {
			FileUtils.write(new File(path), builder.toString(), "utf8", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将不同的label2的数据输出到对应的文件
	 * 一次一个
	 * @param path
	 */
	public void exportSeperately(String path) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(path), "utf8"));
			String line = "";
			ArrayList<DataPoint> dataList = new ArrayList<DataPoint>();

			while ((line = reader.readLine()) != null) {
				line.split("\t");
				DataPoint data = new DataPoint(line);
				dataList.add(data);
			}

			String outputField = "Idea";
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(new File("corpus/" + outputField
							+ ".txt")), "utf8"));
			for (DataPoint dataPoint : dataList) {
				if (dataPoint.getLabel2().equals(outputField))
					writer.write(dataPoint.getDatastring() + "\r\n");
			}
			reader.close();
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 根据Feature Data文件中的数据创建对象
	 * @param featureString
	 */
	public static DataPoint parseFeatureString (String featureString){
		return JSON.parseObject(featureString, DataPoint.class);
	}
	
	/**
	 * 将对象转化为包含特征信息的字符串,以写入文件
	 * @return
	 */
	public static String toFeatureString(DataPoint dp){
		return JSON.toJSONString(dp);
/*		StringBuilder builder = new StringBuilder();
		//分类信息
		builder.append(this.classLableNumber);
		builder.append("\t"+this.classLable);
		//Ulrich词表特征
		builder.append("\t"+this.getSubjectWeight());
		builder.append("\t"+booleanToInt(this.hasSubjectBoolean()));
		
		builder.append("\t"+this.getQuantityWeight());
		builder.append("\t"+booleanToInt(this.hasQuantityBoolean()));
		
		builder.append("\t"+this.getFrequencyWeight());
		builder.append("\t"+booleanToInt(this.hasFrequencyBoolean()));
		
		builder.append("\t"+this.getTenseWeight());
		builder.append("\t"+booleanToInt(this.hasTenseBoolean()));
		
		builder.append("\t"+this.getExampleWeight());
		builder.append("\t"+booleanToInt(this.hasExampleBoolean()));
		
		builder.append("\t"+this.getSuggestWeight());
		builder.append("\t"+booleanToInt(this.hasSuggestBoolean()));
		
		builder.append("\t"+this.getHedgeWeight());
		builder.append("\t"+booleanToInt(this.hasHedgeBoolean()));
		
		builder.append("\t"+this.getIdeaWeight());
		builder.append("\t"+booleanToInt(this.hasIdeaBoolean()));
		
		builder.append("\t"+this.getBasisWeight());
		builder.append("\t"+booleanToInt(this.hasBasisBoolean()));
		
		builder.append("\t"+this.getCompareWeight());
		builder.append("\t"+booleanToInt(this.hasCompareBoolean()));
		
		builder.append("\t"+this.getResultWeight());
		builder.append("\t"+booleanToInt(this.hasResultBoolean()));
		//Ulrich物理特征
		builder.append("\t"+this.sectionNumber);
		builder.append("\t"+this.citationCount);
		builder.append("\t"+this.citationDensity);
		builder.append("\t"+this.avgDens);
		//Ulrich句法特征
		builder.append("\t"+booleanToInt(this.hasSyntacticFeature_1()));
		builder.append("\t"+booleanToInt(this.hasSyntacticFeature_2()));
		builder.append("\t"+booleanToInt(this.hasSyntacticFeature_3()));
		builder.append("\t"+booleanToInt(this.hasSyntacticFeature_4()));
		builder.append("\t"+booleanToInt(this.hasSyntacticFeature_5()));
		builder.append("\t"+booleanToInt(this.hasSyntacticFeature_6()));
		builder.append("\t"+booleanToInt(this.hasSyntacticFeature_7()));

		//我们的词表特征
		builder.append("\t"+this.getOurSubjectWeight());
		builder.append("\t"+booleanToInt(this.hasOurSubjectBoolean()));
		
		builder.append("\t"+this.getOurIdeaWeight());
		builder.append("\t"+booleanToInt(this.hasOurIdeaBoolean()));
		
		builder.append("\t"+this.getOurBasisWeight());
		builder.append("\t"+booleanToInt(this.hasOurBasisBoolean()));
		
		builder.append("\t"+this.getToolWeight());
		builder.append("\t"+booleanToInt(this.hasToolBoolean()));
		
		builder.append("\t"+this.getOurCompareWeight());
		builder.append("\t"+booleanToInt(this.hasOurCompareBoolean()));
		
		builder.append("\t"+this.getFutureWeight());
		builder.append("\t"+booleanToInt(this.hasFutureBoolean()));

		builder.append("\t"+this.getNumberCount());
		builder.append("\t"+booleanToInt(this.isNumberBoolean()));
		builder.append("\t"+this.getPercentageCount());
		builder.append("\t"+booleanToInt(this.isPercentageBoolean()));
		
		builder.append("\t"+booleanToInt(this.isComparativeBoolean()));
		
		//我们的据法提取词汇特征
		builder.append("\t");
		for(String verb : verbWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : subjectAndVerbs)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : adjectiveWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : adverbWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : determinerWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : existentialWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : modalWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : predeterminerWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		builder.append("\t");
		for(String verb : prononeWords)
			builder.append(verb.replace("\t", " ")).append(" ");
		
		return builder.toString();*/
	}
	
	
	public static void main(String[] args) {
		//new DataUtils().exportSeperately(path);
		
		String outputpath = "corpus/DFKIFeatureData.txt";
		FeatureCacheUtils.extractFeatures(FeatureCacheUtils.readDataPoints(new HashSet<String>()), outputpath, 0, true);
	}
}

package edu.whu.irlab.dfki.feature;

import java.util.ArrayList;
import java.util.List;

import edu.whu.irlab.bean.Citation;
import edu.whu.irlab.dfki.feature.utils.WordlistCacheUtils;


/**
 * 2014-9-24
 * liuxingbang
 * 特征类
 * 每个数据实例生成一个FeatureTemplate，FeatureExtractor将所有特征提取并保存为一个FeatureTemplate
 * 该类中只保存最后输出的特征，别的中间信息都保存到DataPoint中
 */
public class CitationFeatureTemplate extends FeatureTemplate {
    /**
     * 公共数据信息
     */
    //类别
    private String classLable = "";
    private String classLableNumber = "";

    /**
     * Teufel et al. (2006) 特征部分
     */
    private Boolean selfCitationBoolean = false;
    /**
     * Arthar特征部分
     */
    //n-gram特征
    ArrayList<String> nGram = new ArrayList<String>();
    /*
     * 对句法信息进行统计
	 * 必须以relations 结尾，方便featureextractor中抽取所有依存关系
	 */
    public List<String> dependencyRelations = new ArrayList<>();
    /**
     * Abu特征部分
     */
    //特征1. Reference count 在上下文中引文标记的个数
    //特征2. Is Separate 该引文是成组出现的还是单个出现的
    //特征3. Closest Verb/Adjective/Adverb 距离引文最近的动词、形容词、副词（需要lemmatized）
    public ArrayList<String> citationLinkedKeyAbuWords = new ArrayList<String>();
    //特征4. Self Citation 是否是自引
    //特征5. Contains 1st/3rd PP 是否包含人称代词
    public ArrayList<String> firstPersonPrononeWords = new ArrayList<String>();
    public ArrayList<String> thirdPersonPrononeWords = new ArrayList<String>();
    private boolean firstPersonPrononeBoolean = false;
    private boolean thirdPersonPrononeBoolean = false;
    //特征6. Negation Cue 否定表达，通过词表获取  出现与否
    private boolean negationCueBoolean = false;
    //特征7. Speculation Cue 推测表达，通过词表获取  出现与否
    public ArrayList<String> speculationCues = new ArrayList<String>();
    private boolean speculationBoolean = false;
    private int speculationWeight = 0;
    //特征8. Closest Subjectivity Cue 最近的主观性词汇，通过 OpinionFinder 找到
    public String closestSubjectivityAbuCue = "";
    //特征9. Contrary Expressions 转折连词，是否包含 from Biber (1988)
    public ArrayList<String> contraryExpressionsWords = new ArrayList<String>();
    private boolean contraryExpressionsBoolean = false;
    //特征10. Section 章节类型
    //特征11. Dependency Relation lemmatized形式的依存关系

    /**
     * DFKI特征部分
     */
    /*
     * 布尔特征及频率特征
	 */
    private boolean subjectBoolean = false;
    private boolean quantityBoolean = false;
    private boolean frequencyBoolean = false;
    private boolean tenseBoolean = false;
    private boolean exampleBoolean = false;
    private boolean suggestBoolean = false;
    private boolean hedgeBoolean = false;
    private boolean ideaBoolean = false;
    private boolean basisBoolean = false;
    private boolean compareBoolean = false;
    private boolean resultBoolean = false;
    private boolean neighborSubjectBoolean = false;

    private int subjectWeight = 0;
    private int quantityWeight = 0;
    private int frequencyWeight = 0;
    private int tenseWeight = 0;
    private int exampleWeight = 0;
    private int suggestWeight = 0;
    private int hedgeWeight = 0;
    private int ideaWeight = 0;
    private int basisWeight = 0;
    private int compareWeight = 0;
    private int resultWeight = 0;
    private int neighborSubjectWeight = 0;

    ArrayList<String> subjectWords = new ArrayList<String>();
    ArrayList<String> quantityWords = new ArrayList<String>();
    ArrayList<String> frequencyWords = new ArrayList<String>();
    ArrayList<String> tenseWords = new ArrayList<String>();
    ArrayList<String> exampleWords = new ArrayList<String>();
    ArrayList<String> suggestWords = new ArrayList<String>();
    ArrayList<String> hedgeWords = new ArrayList<String>();
    ArrayList<String> ideaWords = new ArrayList<String>();
    ArrayList<String> basisWords = new ArrayList<String>();
    ArrayList<String> compareWords = new ArrayList<String>();
    ArrayList<String> resultWords = new ArrayList<String>();
    ArrayList<String> neighborSubjectWords = new ArrayList<String>();
    /*
     * 物理信息特征，包括引文所在章节位置以及引文密度
     */
    //Section Location,对应6个章节类型（Introduction, Related work,	Method, Experiment, Evaluation, and Conclusion）
    private int sectionNumber = 0;

    //Popularity 句子中的引文数量,number of references cited in the	same sentence
    private int popularity = 0;
    //Density 本句及相邻句中不同引文的数量,number of different references in the	citation sentence and its neighbor sentences.
    private int citationDensity = 0;
    //AvgDens 相邻句子中引文的密度 AvgDens: average of Density among neighbor sentences surrounding the citation sentence.
    private double avgDens = 0.0;
    /*
     * 句法层面特征
     */
    private boolean syntacticFeature_1 = false;
    private boolean syntacticFeature_2 = false;
    private boolean syntacticFeature_3 = false;
    private boolean syntacticFeature_4 = false;
    private boolean syntacticFeature_5 = false;
    private boolean syntacticFeature_6 = false;
    private boolean syntacticFeature_7 = false;

    /**
     * Charles Jochim的特征部分
     */
    private boolean constituentBoolean = false;
    private boolean otherContrastBoolean = false;
    private boolean selfCompBoolean = false;
    private boolean otherCompBoolean = false;
    private boolean selfGoodBoolean = false;
    private boolean hasButBoolean = false;
    //关于比较级的特征
    private boolean comparativeBoolean = false;
    ArrayList<String> comparativeWords = new ArrayList<String>();
    //情态动词 can may should
    private boolean modalBoolean = false;
    ArrayList<String> modalWords = new ArrayList<String>();
    // has-1stPRP，见Abu-jbara的特征5
    // has-3rdPRP
    // root 词汇
    private String rootWord = "";
    // main verb
    private String mainVerb = "";
    // positive-words BOW，取自Wilson 05的OpinionFinder
    public List<String> positiveWords = new ArrayList<String>();
    // negative-words
    public List<String> negativeWords = new ArrayList<String>();

    // has cf.
    private boolean hasCfBoolean = false;
    // has_resource
    private boolean hasResourceBoolean = false;
    // has_tool
    private boolean hasToolBoolean = false;
    // sentence loc.: 1st qtr, middle half, last qtr 引文记号在句子中的位置，分为(0~0.25,0.25~0.75,0.75~1)（句子中有多个引文时没法用）
    private float sentenceLocation = (float) 0.0;
    // reference age
    private int referenceAge = 0;
    // number of authors
    private int authorNumber = 0;
    // publication keywords (taken from name of publication [journal, proceedings]
    //    or from title [book, other]
    public List<String> publicationKeyWords = new ArrayList<String>();
    // publication type 这个没用


    // has conceptual cue
    private boolean hasConceptualCue = false;
    // has operational
    private boolean hasOperational = false;
    // has evolutionary
    private boolean hasEvolutionary = false;
    // has juxtapositional
    private boolean hasJuxtapositional = false;
    // has organic
    private boolean hasOrganic = false;
    // has perfunctory
    private boolean hasPerfunctory = false;
    // has confirmative
    private boolean hasConfirmative = false;
    // has negational
    private boolean hasNegational = false;
    // tense 时态（过去时(0)，现在时(1)，未来时(2)，完成时(3)，进行时(4)）
    private int tense;
    // has voice 是否有被动语态
    private boolean hasVoice = false;
    // paper loc. 句子所在的文章的位置.
    private float paperLocation = (float)0.0;

    //CONC CONF EVOL JUX NEG OP ORG PERF的顺序,获取的是这样一个list 包含了每个种类词出现的次数
    private List<Integer> conceptualCueList = new ArrayList<Integer>();

    /**
     * 本文改进提出的特征
     */
	/*
	 * 第一部分，改进布尔特征及数值特征
	 */
    private boolean ourSubjectBoolean = false;
    private int ourSubjectWeight = 0;

    private boolean ourIdeaBoolean = false;
    private int ourIdeaWeight = 0;

    private boolean ourBasisBoolean = false;
    private int ourBasisWeight = 0;

    private boolean toolBoolean = false;
    private int toolWeight = 0;

    private boolean ourCompareBoolean = false;
    private int ourCompareWeight = 0;

    private boolean ourQuantityBoolean = false;
    private int ourQuantityWeight = 0;

    private boolean ourFrequencyBoolean = false;
    private int ourFrequencyWeight = 0;

    private boolean ourTenseBoolean = false;
    private int ourTenseWeight = 0;

    private boolean ourExampleBoolean = false;
    private int ourExampleWeight = 0;

    private boolean ourSuggestBoolean = false;
    private int ourSuggestWeight = 0;

    private boolean ourHedgeBoolean = false;
    private int ourHedgeWeight = 0;

    private boolean ourResultBoolean = false;
    private int ourResultWeight = 0;

    //一些引用对象的词表
    private boolean objectBoolean = false;
    private int objectWeight = 0;

    //针对未来工作的future cue
    private boolean futureBoolean = false;
    private int futureWeight = 0;

    //数字特征：是否存在大数字以及数字百分比出现几次的特征
    private boolean numberBoolean = false;
    private int numberCount = 0;
    private boolean percentageBoolean = false;
    private int percentageCount = 0;

    /*
     * 第二部分，句子中各种词汇的特征
     * 注意，这些词表的命名必须以Words为结尾
     */
    //对应于上述词表中的词汇特征
    ArrayList<String> ourSubjectWords = new ArrayList<String>();
    ArrayList<String> ourIdeaWords = new ArrayList<String>();
    ArrayList<String> ourBasisWords = new ArrayList<String>();
    ArrayList<String> ourCompareWords = new ArrayList<String>();
    ArrayList<String> ourResultWords = new ArrayList<String>();

    ArrayList<String> ourQuantityWords = new ArrayList<String>();
    ArrayList<String> ourFrequencyWords = new ArrayList<String>();
    ArrayList<String> ourTenseWords = new ArrayList<String>();
    ArrayList<String> ourExampleWords = new ArrayList<String>();
    ArrayList<String> ourSuggestWords = new ArrayList<String>();
    ArrayList<String> ourHedgeWords = new ArrayList<String>();

    ArrayList<String> objectWords = new ArrayList<String>();
    ArrayList<String> toolWords = new ArrayList<String>();
    ArrayList<String> futureWords = new ArrayList<String>();

    //句法分析提取的动词
    // 是否subject与citation相连(窗口的)
    private boolean subjectLinkedToCitationBoolean = false;
    // 与subject和citation均相连的关键词
    public List<String> subjectCitationLinkedWords = new ArrayList<>();
    //与subject和citation相连的关键词
    public List<String> subjectLinkedKeyWords = new ArrayList<>();
    public List<String> citationLinkedKeyWords = new ArrayList<>();
    public List<String> subjectLinkedDependencies = new ArrayList<>();
    public List<String> citationLinkedDependencies = new ArrayList<>();
/*
    //动词
    ArrayList<String> verbWords = new ArrayList<String>();
    //形容词
    ArrayList<String> adjectiveWords = new ArrayList<String>();
    //副词
    ArrayList<String> adverbWords = new ArrayList<String>();
    //限定词 all、an、the
    ArrayList<String> determinerWords = new ArrayList<String>();
    //存在词  there
    ArrayList<String> existentialWords = new ArrayList<String>();
    //前位限定词  all both half many quite such sure this
    ArrayList<String> predeterminerWords = new ArrayList<String>();
    //人称代词
    ArrayList<String> prononeWords = new ArrayList<String>();
*/

    /*
     * 第三部分，改进的物理特征
     */
    //句子的长度（词数）
    private int sentenceLength = 0;
    //句子中包含的短句个数（逗号分隔）
    private int shortSentenceNumber = 0;
    //短句的平均长度
    private double ShortSentenceAvgLength = 0.0;
    //前一句中的Citation以及个数
    private ArrayList<Citation> citationList = new ArrayList<>();
    private int citationNumber = 0;
    private ArrayList<Citation> previousSentenceCitationList = new ArrayList<>();
    private int previousSentenceCitationNumber = 0;
    private ArrayList<Citation> followingSentenceCitationList = new ArrayList<>();
    private int followingSentenceCitationNumber = 0;
    private boolean previousSentenceRelated = false;
    private boolean followingSentenceRelated = false;
    //上下文特征
    public ArrayList<String> ourContextSubjectWords = new ArrayList<>();
    private boolean ourContextSubjectBoolean = false;
    private int ourContextSubjectWeight = 0;
    public ArrayList<String> ourContextIdeaWords = new ArrayList<>();
    private boolean ourContextIdeaBoolean = false;
    private int ourContextIdeaWeight = 0;
    public ArrayList<String> ourContextBasisWords = new ArrayList<>();
    private boolean ourContextBasisBoolean;
    private int ourContextBasisWeight = 0;
    public ArrayList<String> ourContextCompareWords = new ArrayList<>();
    private boolean ourContextCompareBoolean = false;
    private int ourContextCompareWeight = 0;
    public ArrayList<String> ourContextResultWords = new ArrayList<>();
    private boolean ourContextResultBoolean = false;
    private int ourContextResultWeight = 0;
    public ArrayList<String> ourContextQuantityWords = new ArrayList<>();
    private boolean ourContextQuantityBoolean = false;
    private int ourContextQuantityWeight = 0;
    public ArrayList<String> ourContextFrequencyWords = new ArrayList<>();
    private boolean ourContextFrequencyBoolean = false;
    private int ourContextFrequencyWeight = 0;
    public ArrayList<String> ourContextTenseWords = new ArrayList<>();
    private boolean ourContextTenseBoolean = false;
    private int ourContextTenseWeight = 0;
    public ArrayList<String> ourContextExampleWords = new ArrayList<>();
    private boolean ourContextExampleBoolean = false;
    private int ourContextExampleWeight = 0;

    public ArrayList<String> ourContextSuggestWords = new ArrayList<>();
    private boolean ourContextSuggestBoolean = false;
    private int ourContextSuggestWeight = 0;
    public ArrayList<String> ourContextHedgeWords = new ArrayList<>();
    private boolean contextOurHedgeBoolean = false;
    private int contextOurHedgeWeight = 0;
    public ArrayList<String> contextObjectWords = new ArrayList<>();
    private boolean contextObjectBoolean = false;
    private int contextObjectWeight = 0;
    public ArrayList<String> contextToolWords = new ArrayList<>();
    private boolean contextToolBoolean = false;
    private int contextToolWeight = 0;
    public ArrayList<String> contextFutureWords = new ArrayList<>();
    private boolean contextFutureBoolean = false;
    private int contextFutureWeight = 0;
    private int contextNumberCount = 0;
    private boolean contextNumberBoolean = false;
    private int contextPercentageCount = 0;
    private boolean contextPercentageBoolean = false;
    private ArrayList<String> contextSubjectLinkedKeyWords = new ArrayList<>();
    private ArrayList<String> contextnGram = new ArrayList<>();

    /**
     * 输入一个值，找到对应的特征，并set为true
     *
     * @param k
     */
    public void setSyntacticFeature(int k) {
        if (k == 1) {
            this.syntacticFeature_1 = true;
        } else if (k == 2) {
            this.syntacticFeature_2 = true;
        } else if (k == 3) {
            this.syntacticFeature_3 = true;
        } else if (k == 4) {
            this.syntacticFeature_4 = true;
        } else if (k == 5) {
            this.syntacticFeature_5 = true;
        } else if (k == 6) {
            this.syntacticFeature_6 = true;
        } else {
            this.syntacticFeature_7 = true;
        }
    }

    /**
     * 输出为Ulrich的特征向量
     *
     * @return
     */
    public String toDFKIFeatureVector() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.classLableNumber);
        builder.append(" 1:" + this.getQuantityWeight());
        builder.append(" 2:" + booleanToInt(this.getQuantityBoolean()));
        builder.append(" 3:" + this.getFrequencyWeight());
        builder.append(" 4:" + booleanToInt(this.getFrequencyBoolean()));
        builder.append(" 5:" + this.getTenseWeight());
        builder.append(" 6:" + booleanToInt(this.getTenseBoolean()));
        builder.append(" 7:" + this.getExampleWeight());
        builder.append(" 8:" + booleanToInt(this.getExampleBoolean()));
        builder.append(" 9:" + this.getSuggestWeight());
        builder.append(" 10:" + booleanToInt(this.getSuggestBoolean()));
        builder.append(" 11:" + this.getHedgeWeight());
        builder.append(" 12:" + booleanToInt(this.getHedgeBoolean()));
        builder.append(" 13:" + this.getIdeaWeight());
        builder.append(" 14:" + booleanToInt(this.getIdeaBoolean()));
        builder.append(" 15:" + this.getBasisWeight());
        builder.append(" 16:" + booleanToInt(this.getBasisBoolean()));
        builder.append(" 17:" + this.getCompareWeight());
        builder.append(" 18:" + booleanToInt(this.getCompareBoolean()));
        builder.append(" 19:" + this.getResultWeight());
        builder.append(" 20:" + booleanToInt(this.getResultBoolean()));
        builder.append(" 21:" + this.getSubjectWeight());
        builder.append(" 22:" + booleanToInt(this.getSubjectBoolean()));


        builder.append(" 23:" + booleanToInt(this.getSyntacticFeature_1()));
        builder.append(" 24:" + booleanToInt(this.getSyntacticFeature_2()));
        builder.append(" 25:" + booleanToInt(this.getSyntacticFeature_3()));
        builder.append(" 26:" + booleanToInt(this.getSyntacticFeature_4()));
        builder.append(" 27:" + booleanToInt(this.getSyntacticFeature_5()));
        builder.append(" 28:" + booleanToInt(this.getSyntacticFeature_6()));
        builder.append(" 29:" + booleanToInt(this.getSyntacticFeature_7()));
        builder.append(" 30:" + this.sectionNumber);
        builder.append(" 31:" + this.popularity);
        builder.append(" 32:" + this.citationDensity);

        builder.append("\n");

        return builder.toString();
    }

    /**
     * 输出为Abu的特征向量
     *
     * @return
     */
    public String toAbuJbaraFeatureVector() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.classLableNumber);
        int i = 1;
        /*
         * 1. Citation Context中的引文数量
         */
        builder.append(" " + (i++) + ":" + this.citationDensity);
        /*
         * 2. Is Separate 句子是否是独立的
         */
        builder.append(" " + (i++) + ":" + (this.popularity > 0 ? 1 : 0));
        /*
         * 3. 最近的Verb/Adjective/Adverb（需要lemmatized）
         */
        for (String word : FeatureConverter.GlobalAbuWords)
            if (this.citationLinkedKeyAbuWords.contains(word))
                builder.append(" " + (i++) + ":" + 1);
            else
                builder.append(" " + (i++) + ":0");
        /*
         * 4. 是否是自引
         */
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSelfCitationBoolean()));
        /*
         * 5. 包含 1st/3rd PP 是否包含第一或者第三人称代词
         */
        builder.append(" " + (i++) + ":" + booleanToInt(this.getFirstPersonPrononeBoolean() || this.getThirdPersonPrononeBoolean()));
        /*
         * 6. 是否包含Negation词汇 SEM 2012 negation detection shared task
         */
        builder.append(" " + (i++) + ":" + booleanToInt(this.getNegationCueBoolean()));
        /*
         * 7. 是否上下文中包含speculation cue
         */
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSpeculationBoolean()));
//        builder.append(" " + (i++) + ":" + this.getSpeculationWeight());
        /*
         * 8. 最近的Subjectivity Cue，来自OpinionFinder
         */
        for (String word : WordlistCacheUtils.subjectivityCueMap.keySet())
            if (this.getClosestSubjectivityAbuCue().equals(word))
                builder.append(" " + (i++) + ":1");
            else
                builder.append(" " + (i++) + ":0");
        /*
         * 9. 是否包含Contrary Expression 来自Biber(1988)
         */
        builder.append(" " + (i++) + ":" + booleanToInt(this.getContraryExpressionsBoolean()));
        /*
         * 10. section number
         */
        builder.append(" " + (i++) + ":" + this.sectionNumber);
        /*
         * 11. Dependency Relations （需要lemmatized）
         */
        for (String word : FeatureConverter.GlobalDependencies)
            if (this.getDependencyRelations().contains(word))
                builder.append(" " + (i++) + ":1");
            else
                builder.append(" " + (i++) + ":0");

        return builder.toString();
    }


    /**
     * 由于我们也使用ngram特征，因此词表内容中的每个词可以不作为特征输出了，但是布尔型和权重的特征仍然保留
     * 输出为我们的特征向量
     *
     * @return
     */
    public String toOurFeatureVector() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.classLableNumber);

        int i = 1;

        /**
         * Ulrich 词表特征
         */
        builder.append(" " + (i++) + ":" + this.getSubjectWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSubjectBoolean()));
        builder.append(" " + (i++) + ":" + this.getQuantityWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getQuantityBoolean()));
        builder.append(" " + (i++) + ":" + this.getFrequencyWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getFrequencyBoolean()));
        builder.append(" " + (i++) + ":" + this.getTenseWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getTenseBoolean()));
        builder.append(" " + (i++) + ":" + this.getExampleWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getExampleBoolean()));
        builder.append(" " + (i++) + ":" + this.getSuggestWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSuggestBoolean()));

        builder.append(" " + (i++) + ":" + this.getHedgeWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getHedgeBoolean()));
        builder.append(" " + (i++) + ":" + this.getIdeaWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getIdeaBoolean()));
        builder.append(" " + (i++) + ":" + this.getBasisWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getBasisBoolean()));
        builder.append(" " + (i++) + ":" + this.getCompareWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getCompareBoolean()));
        builder.append(" " + (i++) + ":" + this.getResultWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getResultBoolean()));

        /**
         * Ulrich 句法特征
         */
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_1()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_2()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_3()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_4()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_5()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_6()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_7()));

        /**
         * Ulrich 物理特征
         */
        builder.append(" " + (i++) + ":" + this.sectionNumber);
        builder.append(" " + (i++) + ":" + this.popularity);
        builder.append(" " + (i++) + ":" + this.citationDensity);
        builder.append(" " + (i++) + ":" + this.avgDens);
        builder.append(" " + (i++) + ":" + this.sentenceLength);
        builder.append(" " + (i++) + ":" + this.shortSentenceNumber);
        /**
         * 我们的 词表特征
         */
        //继承并改进Ulrich的词表
        builder.append(" " + (i++) + ":" + this.getOurSubjectWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurSubjectBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurCompareWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurCompareBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurIdeaWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurIdeaBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurBasisWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurBasisBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurResultWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurResultBoolean()));

        builder.append(" " + (i++) + ":" + this.getOurQuantityWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurQuantityBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurFrequencyWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurFrequencyBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurTenseWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurTenseBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurExampleWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurExampleBoolean()));
        builder.append(" " + (i++) + ":" + this.getOurSuggestWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurSuggestBoolean()));

        builder.append(" " + (i++) + ":" + this.getOurHedgeWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getOurHedgeBoolean()));

        /**
         * 原创词表
         */
        builder.append(" " + (i++) + ":" + this.getFutureWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getFutureBoolean()));
        builder.append(" " + (i++) + ":" + this.getToolWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getToolBoolean()));

        builder.append(" " + (i++) + ":" + booleanToInt(this.comparativeBoolean));

        /**
         * 我们的 词汇特征
         * 由于我们也使用ngram特征，因此词表内容中的每个词汇可以不作为特征输出了，但是布尔型和权重的特征仍然保留
         */
        //词汇特征类型filter，如果在这个filter中则输出到vector中
        /*ArrayList<String> wordFilterList = new ArrayList<String>();
        wordFilterList.add("ourSubjectWords");


        wordFilterList.add("ourSubjectWords");
        wordFilterList.add("ourIdeaWords");
        wordFilterList.add("ourBasisWords");
        wordFilterList.add("ourCompareWords");
        wordFilterList.add("resultWords");

        wordFilterList.add("ourQuantityWords");
        wordFilterList.add("ourFrequencyWords");
        wordFilterList.add("ourTenseWords");
        wordFilterList.add("ourExampleWords");
        wordFilterList.add("ourSuggestWords");
        wordFilterList.add("ourHedgeWords");

        wordFilterList.add("comparativeWords");
        wordFilterList.add("objectWords");
        wordFilterList.add("futureWords");
        wordFilterList.add("toolWords");
        wordFilterList.add("objectWords");
        wordFilterList.add("comparativeWords");

        wordFilterList.add("verbWords");
        wordFilterList.add("subjectLinkedKeyWords");
        wordFilterList.add("citationLinkedKeyWords");
        wordFilterList.add("adjectiveWords");
        wordFilterList.add("adverbWords");
        wordFilterList.add("determinerWords");
        wordFilterList.add("existentialWords");
        wordFilterList.add("modalWords");
        wordFilterList.add("predeterminerWords");
        wordFilterList.add("prononeWords");

        //用treemap，key为词汇的在全局词表中的序号ID，通过treemap默认按照升序排序
        TreeMap<Integer, Integer> wordTreeMap = new TreeMap<Integer, Integer>();

        Class featureTemplate = this.getClass();

        for (String filter : wordFilterList) {
            try {
                //System.err.println(filter);
                Field field = featureTemplate.getDeclaredField(filter);
                field.setAccessible(true); // 设置属性是可以访问的
                ArrayList<String> words = (ArrayList<String>) field.get(this);// 得到此属性的值
                for (String word : words) {
                    wordTreeMap.put(FeatureExtractor.GlobalWordTable.get(word), 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //这是我们词表特征的输出
		for(Integer wordID : wordTreeMap.keySet())
			builder.append(" "+(i+wordID)+":1");
        */

        /**
         * Arthar的ngram和dependency特征
         */
        for (String word : FeatureConverter.GlobalNgram.keySet())
            if (this.getnGram().contains(word))
                builder.append(" " + (i++) + ":1");
            else
                builder.append(" " + (i++) + ":0");

        for (String word : FeatureConverter.GlobalDependencies)
            if (this.getDependencyRelations().contains(word)){
                builder.append(" " + (i++) + ":1");
            } else {
                builder.append(" " + (i++) + ":0");
            }

        return builder.toString();
    }

    /**
     * 输出Charles Jochim的特征向量
     * 按照其在发布数据中的特征，应该输出65个特征+ngram
     *
     * @return
     */
    public String toJochimFeatureVector() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.classLableNumber);

        int i = 1;
        /**
         * reimplented features from Dong & Schaefer (2011)
         */
        /*
         * 实现Ulrich词表特征，对应Stanford Classifer properties中的7~30
         */
        //has subject cue
        builder.append(" " + (i++) + ":" + this.getSubjectWeight());
        //subject 'weight'
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSubjectBoolean()));
        //has quantity cue
        builder.append(" " + (i++) + ":" + this.getQuantityWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getQuantityBoolean()));
        //has frequency cue
        builder.append(" " + (i++) + ":" + this.getFrequencyWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getFrequencyBoolean()));
        //has tense cue
        builder.append(" " + (i++) + ":" + this.getTenseWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getTenseBoolean()));
        //has example cue
        builder.append(" " + (i++) + ":" + this.getExampleWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getExampleBoolean()));
        //has suggest cue
        builder.append(" " + (i++) + ":" + this.getSuggestWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSuggestBoolean()));
        //has hedge cue
        builder.append(" " + (i++) + ":" + this.getHedgeWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getHedgeBoolean()));
        //has idea cue
        builder.append(" " + (i++) + ":" + this.getIdeaWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getIdeaBoolean()));
        //has basis cue
        builder.append(" " + (i++) + ":" + this.getBasisWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getBasisBoolean()));
        //has compare cue
        builder.append(" " + (i++) + ":" + this.getCompareWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getCompareBoolean()));
        //has result cue
        builder.append(" " + (i++) + ":" + this.getResultWeight());
        builder.append(" " + (i++) + ":" + booleanToInt(this.getResultBoolean()));

        //neighbor has subject
        builder.append(" " + (i++) + ":" + this.getNeighborSubjectWeight());
        //neighbor subject 'weight'
        builder.append(" " + (i++) + ":" + booleanToInt(this.getNeighborSubjectBoolean()));

        /*
         * Ulrich 物理特征，对应于31~33
         */
        builder.append(" " + (i++) + ":" + this.popularity);
        builder.append(" " + (i++) + ":" + this.avgDens);
        builder.append(" " + (i++) + ":" + this.citationDensity);

        /*
         * Ulrich 句法特征，对应于34~40
         */
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_1()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_2()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_3()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_4()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_5()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_6()));
        builder.append(" " + (i++) + ":" + booleanToInt(this.getSyntacticFeature_7()));

        /**
         * reimplented features from Jochim & Schütze (2012) 42~59，ngram在6
         */
/*
        // is-constituent
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has other-contrast
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has self-comp
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has other-comp
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // is self-good
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has but
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // 比较级/最高级的词汇 has-comp./sup.
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has modal
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has-1stPRP
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has-3rdPRP
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // root 词汇
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // main verb
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // positive-words
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // negative-words
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has cf.
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has_resource
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has_tool
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // sentence loc.: 1st qtr, middle half, last qtr 引文记号在句子中的位置，分为(0~0.25,0.25~0.75,0.75~1)（句子中有多个引文时没法用）
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        */
/*
         *Reference features: 73~77
         *//*

        // reference age
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // number of authors
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // publication keywords (taken from name of publication [journal, proceedings]
        //    or from title [book, other]
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // self cite (also used by Teufel)
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // publication type
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));

        */
/**
 * reimplented features from Teufel et al. (2006) 对应60~72
 *//*

        // Boolean features on automatically extracted cue phrases
        // attempt to automatially duplicate manual process from Teufel et al., 2006
        //  i.e., one feature per class label
        // has conceptual cue
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has operational
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has evolutionary
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has juxtapositional
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has organic
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has perfunctory
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has confirmative
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has negational <has_neg_cue>
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // tense 时态 <modal>
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // voice 语态（主动/被动）<active>
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // has modality, binary modal/no modal <modal>
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // modal verb or None would/can等
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
        // paper loc. 在文章中的位置(前中后)
        builder.append(" " + (i++) + ":" + booleanToInt(this.isConstituent()));
*/

        /**
         * reimplented features from Athar (2011)
         */
        /*
         * 引文上下文内容的ngram信息，对应于6
         */
        for (String word : FeatureConverter.GlobalNgram.keySet())
            if (this.getnGram().contains(word))
                builder.append(" " + (i++) + ":1");
            else
                builder.append(" " + (i++) + ":0");
        /*
         * 引文上下文内容的dependency pairs信息，对应于41
         */
        for (String word : FeatureConverter.GlobalDependencies)
            if (this.getDependencyRelations().contains(word)){
                builder.append(" " + (i++) + ":1");
            } else {
                builder.append(" " + (i++) + ":0");
            }

        return builder.toString();
    }


    /**
     * boolean值转int
     *
     * @param b
     * @return
     */
    public static int booleanToInt(boolean b) {
        if (b) {
            return 1;
        } else {
            return 0;
        }
    }


    public String getClassLable() {
        return classLable;
    }

    public void setClassLable(String classLable) {
        this.classLable = classLable;
    }

    public String getClassLableNumber() {
        return classLableNumber;
    }

    public void setClassLableNumber(String classLableNumber) {
        this.classLableNumber = classLableNumber;
    }

    public Boolean getSelfCitationBoolean() {
        return selfCitationBoolean;
    }

    public void setSelfCitationBoolean(Boolean selfCitationBoolean) {
        this.selfCitationBoolean = selfCitationBoolean;
    }

    public ArrayList<String> getnGram() {
        return nGram;
    }

    public void setnGram(ArrayList<String> nGram) {
        this.nGram = nGram;
    }

    public List<String> getDependencyRelations() {
        return dependencyRelations;
    }

    public void setDependencyRelations(List<String> dependencyRelations) {
        this.dependencyRelations = dependencyRelations;
    }

    public ArrayList<String> getCitationLinkedKeyAbuWords() {
        return citationLinkedKeyAbuWords;
    }

    public void setCitationLinkedKeyAbuWords(ArrayList<String> citationLinkedKeyAbuWords) {
        this.citationLinkedKeyAbuWords = citationLinkedKeyAbuWords;
    }

    public ArrayList<String> getFirstPersonPrononeWords() {
        return firstPersonPrononeWords;
    }

    public void setFirstPersonPrononeWords(ArrayList<String> firstPersonPrononeWords) {
        this.firstPersonPrononeWords = firstPersonPrononeWords;
    }

    public ArrayList<String> getThirdPersonPrononeWords() {
        return thirdPersonPrononeWords;
    }

    public void setThirdPersonPrononeWords(ArrayList<String> thirdPersonPrononeWords) {
        this.thirdPersonPrononeWords = thirdPersonPrononeWords;
    }

    public boolean getFirstPersonPrononeBoolean() {
        return firstPersonPrononeBoolean;
    }

    public void setFirstPersonPrononeBoolean(boolean firstPersonPrononeBoolean) {
        this.firstPersonPrononeBoolean = firstPersonPrononeBoolean;
    }

    public boolean getThirdPersonPrononeBoolean() {
        return thirdPersonPrononeBoolean;
    }

    public void setThirdPersonPrononeBoolean(boolean thirdPersonPrononeBoolean) {
        this.thirdPersonPrononeBoolean = thirdPersonPrononeBoolean;
    }

    public boolean getNegationCueBoolean() {
        return negationCueBoolean;
    }

    public void setNegationCueBoolean(boolean negationCueBoolean) {
        this.negationCueBoolean = negationCueBoolean;
    }

    public ArrayList<String> getSpeculationCues() {
        return speculationCues;
    }

    public void setSpeculationCues(ArrayList<String> speculationCues) {
        this.speculationCues = speculationCues;
    }

    public boolean getSpeculationBoolean() {
        return speculationBoolean;
    }

    public void setSpeculationBoolean(boolean speculationBoolean) {
        this.speculationBoolean = speculationBoolean;
    }

    public int getSpeculationWeight() {
        return speculationWeight;
    }

    public void setSpeculationWeight(int speculationWeight) {
        this.speculationWeight = speculationWeight;
    }

    public String getClosestSubjectivityAbuCue() {
        return closestSubjectivityAbuCue;
    }

    public void setClosestSubjectivityAbuCue(String closestSubjectivityAbuCue) {
        this.closestSubjectivityAbuCue = closestSubjectivityAbuCue;
    }

    public ArrayList<String> getContraryExpressionsWords() {
        return contraryExpressionsWords;
    }

    public void setContraryExpressionsWords(ArrayList<String> contraryExpressionsWords) {
        this.contraryExpressionsWords = contraryExpressionsWords;
    }

    public boolean getContraryExpressionsBoolean() {
        return contraryExpressionsBoolean;
    }

    public void setContraryExpressionsBoolean(boolean contraryExpressionsBoolean) {
        this.contraryExpressionsBoolean = contraryExpressionsBoolean;
    }

    public boolean getSubjectBoolean() {
        return subjectBoolean;
    }

    public void setSubjectBoolean(boolean subjectBoolean) {
        this.subjectBoolean = subjectBoolean;
    }

    public boolean getQuantityBoolean() {
        return quantityBoolean;
    }

    public void setQuantityBoolean(boolean quantityBoolean) {
        this.quantityBoolean = quantityBoolean;
    }

    public boolean getFrequencyBoolean() {
        return frequencyBoolean;
    }

    public void setFrequencyBoolean(boolean frequencyBoolean) {
        this.frequencyBoolean = frequencyBoolean;
    }

    public boolean getTenseBoolean() {
        return tenseBoolean;
    }

    public void setTenseBoolean(boolean tenseBoolean) {
        this.tenseBoolean = tenseBoolean;
    }

    public boolean getExampleBoolean() {
        return exampleBoolean;
    }

    public void setExampleBoolean(boolean exampleBoolean) {
        this.exampleBoolean = exampleBoolean;
    }

    public boolean getSuggestBoolean() {
        return suggestBoolean;
    }

    public void setSuggestBoolean(boolean suggestBoolean) {
        this.suggestBoolean = suggestBoolean;
    }

    public boolean getHedgeBoolean() {
        return hedgeBoolean;
    }

    public void setHedgeBoolean(boolean hedgeBoolean) {
        this.hedgeBoolean = hedgeBoolean;
    }

    public boolean getIdeaBoolean() {
        return ideaBoolean;
    }

    public void setIdeaBoolean(boolean ideaBoolean) {
        this.ideaBoolean = ideaBoolean;
    }

    public boolean getBasisBoolean() {
        return basisBoolean;
    }

    public void setBasisBoolean(boolean basisBoolean) {
        this.basisBoolean = basisBoolean;
    }

    public boolean getCompareBoolean() {
        return compareBoolean;
    }

    public void setCompareBoolean(boolean compareBoolean) {
        this.compareBoolean = compareBoolean;
    }

    public boolean getResultBoolean() {
        return resultBoolean;
    }

    public void setResultBoolean(boolean resultBoolean) {
        this.resultBoolean = resultBoolean;
    }

    public boolean getNeighborSubjectBoolean() {
        return neighborSubjectBoolean;
    }

    public void setNeighborSubjectBoolean(boolean neighborSubjectBoolean) {
        this.neighborSubjectBoolean = neighborSubjectBoolean;
    }

    public int getSubjectWeight() {
        return subjectWeight;
    }

    public void setSubjectWeight(int subjectWeight) {
        this.subjectWeight = subjectWeight;
    }

    public int getQuantityWeight() {
        return quantityWeight;
    }

    public void setQuantityWeight(int quantityWeight) {
        this.quantityWeight = quantityWeight;
    }

    public int getFrequencyWeight() {
        return frequencyWeight;
    }

    public void setFrequencyWeight(int frequencyWeight) {
        this.frequencyWeight = frequencyWeight;
    }

    public int getTenseWeight() {
        return tenseWeight;
    }

    public void setTenseWeight(int tenseWeight) {
        this.tenseWeight = tenseWeight;
    }

    public int getExampleWeight() {
        return exampleWeight;
    }

    public void setExampleWeight(int exampleWeight) {
        this.exampleWeight = exampleWeight;
    }

    public int getSuggestWeight() {
        return suggestWeight;
    }

    public void setSuggestWeight(int suggestWeight) {
        this.suggestWeight = suggestWeight;
    }

    public int getHedgeWeight() {
        return hedgeWeight;
    }

    public void setHedgeWeight(int hedgeWeight) {
        this.hedgeWeight = hedgeWeight;
    }

    public int getIdeaWeight() {
        return ideaWeight;
    }

    public void setIdeaWeight(int ideaWeight) {
        this.ideaWeight = ideaWeight;
    }

    public int getBasisWeight() {
        return basisWeight;
    }

    public void setBasisWeight(int basisWeight) {
        this.basisWeight = basisWeight;
    }

    public int getCompareWeight() {
        return compareWeight;
    }

    public void setCompareWeight(int compareWeight) {
        this.compareWeight = compareWeight;
    }

    public int getResultWeight() {
        return resultWeight;
    }

    public void setResultWeight(int resultWeight) {
        this.resultWeight = resultWeight;
    }

    public int getNeighborSubjectWeight() {
        return neighborSubjectWeight;
    }

    public void setNeighborSubjectWeight(int neighborSubjectWeight) {
        this.neighborSubjectWeight = neighborSubjectWeight;
    }

    public ArrayList<String> getSubjectWords() {
        return subjectWords;
    }

    public void setSubjectWords(ArrayList<String> subjectWords) {
        this.subjectWords = subjectWords;
    }

    public ArrayList<String> getQuantityWords() {
        return quantityWords;
    }

    public void setQuantityWords(ArrayList<String> quantityWords) {
        this.quantityWords = quantityWords;
    }

    public ArrayList<String> getFrequencyWords() {
        return frequencyWords;
    }

    public void setFrequencyWords(ArrayList<String> frequencyWords) {
        this.frequencyWords = frequencyWords;
    }

    public ArrayList<String> getTenseWords() {
        return tenseWords;
    }

    public void setTenseWords(ArrayList<String> tenseWords) {
        this.tenseWords = tenseWords;
    }

    public ArrayList<String> getExampleWords() {
        return exampleWords;
    }

    public void setExampleWords(ArrayList<String> exampleWords) {
        this.exampleWords = exampleWords;
    }

    public ArrayList<String> getSuggestWords() {
        return suggestWords;
    }

    public void setSuggestWords(ArrayList<String> suggestWords) {
        this.suggestWords = suggestWords;
    }

    public ArrayList<String> getHedgeWords() {
        return hedgeWords;
    }

    public void setHedgeWords(ArrayList<String> hedgeWords) {
        this.hedgeWords = hedgeWords;
    }

    public ArrayList<String> getIdeaWords() {
        return ideaWords;
    }

    public void setIdeaWords(ArrayList<String> ideaWords) {
        this.ideaWords = ideaWords;
    }

    public ArrayList<String> getBasisWords() {
        return basisWords;
    }

    public void setBasisWords(ArrayList<String> basisWords) {
        this.basisWords = basisWords;
    }

    public ArrayList<String> getCompareWords() {
        return compareWords;
    }

    public void setCompareWords(ArrayList<String> compareWords) {
        this.compareWords = compareWords;
    }

    public ArrayList<String> getResultWords() {
        return resultWords;
    }

    public void setResultWords(ArrayList<String> resultWords) {
        this.resultWords = resultWords;
    }

    public ArrayList<String> getNeighborSubjectWords() {
        return neighborSubjectWords;
    }

    public void setNeighborSubjectWords(ArrayList<String> neighborSubjectWords) {
        this.neighborSubjectWords = neighborSubjectWords;
    }

    public int getSectionNumber() {
        return sectionNumber;
    }

    public void setSectionNumber(int sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public int getCitationDensity() {
        return citationDensity;
    }

    public void setCitationDensity(int citationDensity) {
        this.citationDensity = citationDensity;
    }

    public double getAvgDens() {
        return avgDens;
    }

    public void setAvgDens(double avgDens) {
        this.avgDens = avgDens;
    }

    public boolean getSyntacticFeature_1() {
        return syntacticFeature_1;
    }

    public void setSyntacticFeature_1(boolean syntacticFeature_1) {
        this.syntacticFeature_1 = syntacticFeature_1;
    }

    public boolean getSyntacticFeature_2() {
        return syntacticFeature_2;
    }

    public void setSyntacticFeature_2(boolean syntacticFeature_2) {
        this.syntacticFeature_2 = syntacticFeature_2;
    }

    public boolean getSyntacticFeature_3() {
        return syntacticFeature_3;
    }

    public void setSyntacticFeature_3(boolean syntacticFeature_3) {
        this.syntacticFeature_3 = syntacticFeature_3;
    }

    public boolean getSyntacticFeature_4() {
        return syntacticFeature_4;
    }

    public void setSyntacticFeature_4(boolean syntacticFeature_4) {
        this.syntacticFeature_4 = syntacticFeature_4;
    }

    public boolean getSyntacticFeature_5() {
        return syntacticFeature_5;
    }

    public void setSyntacticFeature_5(boolean syntacticFeature_5) {
        this.syntacticFeature_5 = syntacticFeature_5;
    }

    public boolean getSyntacticFeature_6() {
        return syntacticFeature_6;
    }

    public void setSyntacticFeature_6(boolean syntacticFeature_6) {
        this.syntacticFeature_6 = syntacticFeature_6;
    }

    public boolean getSyntacticFeature_7() {
        return syntacticFeature_7;
    }

    public void setSyntacticFeature_7(boolean syntacticFeature_7) {
        this.syntacticFeature_7 = syntacticFeature_7;
    }

    public boolean getConstituentBoolean() {
        return constituentBoolean;
    }

    public void setConstituentBoolean(boolean constituentBoolean) {
        this.constituentBoolean = constituentBoolean;
    }

    public boolean getOtherContrastBoolean() {
        return otherContrastBoolean;
    }

    public void setOtherContrastBoolean(boolean otherContrastBoolean) {
        this.otherContrastBoolean = otherContrastBoolean;
    }

    public boolean getSelfCompBoolean() {
        return selfCompBoolean;
    }

    public void setSelfCompBoolean(boolean selfCompBoolean) {
        this.selfCompBoolean = selfCompBoolean;
    }

    public boolean getOtherCompBoolean() {
        return otherCompBoolean;
    }

    public void setOtherCompBoolean(boolean otherCompBoolean) {
        this.otherCompBoolean = otherCompBoolean;
    }

    public boolean getSelfGoodBoolean() {
        return selfGoodBoolean;
    }

    public void setSelfGoodBoolean(boolean selfGoodBoolean) {
        this.selfGoodBoolean = selfGoodBoolean;
    }

    public boolean getHasButBoolean() {
        return hasButBoolean;
    }

    public void setHasButBoolean(boolean hasButBoolean) {
        this.hasButBoolean = hasButBoolean;
    }

    public boolean getComparativeBoolean() {
        return comparativeBoolean;
    }

    public void setComparativeBoolean(boolean comparativeBoolean) {
        this.comparativeBoolean = comparativeBoolean;
    }

    public ArrayList<String> getComparativeWords() {
        return comparativeWords;
    }

    public void setComparativeWords(ArrayList<String> comparativeWords) {
        this.comparativeWords = comparativeWords;
    }

    public boolean getModalBoolean() {
        return modalBoolean;
    }

    public void setModalBoolean(boolean modalBoolean) {
        this.modalBoolean = modalBoolean;
    }

    public ArrayList<String> getModalWords() {
        return modalWords;
    }

    public void setModalWords(ArrayList<String> modalWords) {
        this.modalWords = modalWords;
    }

    public String getRootWord() {
        return rootWord;
    }

    public void setRootWord(String rootWord) {
        this.rootWord = rootWord;
    }

    public String getMainVerb() {
        return mainVerb;
    }

    public void setMainVerb(String mainVerb) {
        this.mainVerb = mainVerb;
    }

    public List<String> getPositiveWords() {
        return positiveWords;
    }

    public void setPositiveWords(List<String> positiveWords) {
        this.positiveWords = positiveWords;
    }

    public List<String> getNegativeWords() {
        return negativeWords;
    }

    public void setNegativeWords(List<String> negativeWords) {
        this.negativeWords = negativeWords;
    }

    public boolean getHasCfBoolean() {
        return hasCfBoolean;
    }

    public void setHasCfBoolean(boolean hasCfBoolean) {
        this.hasCfBoolean = hasCfBoolean;
    }

    public boolean getHasResourceBoolean() {
        return hasResourceBoolean;
    }

    public void setHasResourceBoolean(boolean hasResourceBoolean) {
        this.hasResourceBoolean = hasResourceBoolean;
    }

    public boolean getHasToolBoolean() {
        return hasToolBoolean;
    }

    public void setHasToolBoolean(boolean hasToolBoolean) {
        this.hasToolBoolean = hasToolBoolean;
    }

    public float getSentenceLocation() {
        return sentenceLocation;
    }

    public void setSentenceLocation(float sentenceLocation) {
        this.sentenceLocation = sentenceLocation;
    }

    public int getReferenceAge() {
        return referenceAge;
    }

    public void setReferenceAge(int referenceAge) {
        this.referenceAge = referenceAge;
    }

    public int getAuthorNumber() {
        return authorNumber;
    }

    public void setAuthorNumber(int authorNumber) {
        this.authorNumber = authorNumber;
    }

    public List<String> getPublicationKeyWords() {
        return publicationKeyWords;
    }

    public void setPublicationKeyWords(List<String> publicationKeyWords) {
        this.publicationKeyWords = publicationKeyWords;
    }

    public boolean getHasConceptualCue() {
        return hasConceptualCue;
    }

    public void setHasConceptualCue(boolean hasConceptualCue) {
        this.hasConceptualCue = hasConceptualCue;
    }

    public boolean getHasOperational() {
        return hasOperational;
    }

    public void setHasOperational(boolean hasOperational) {
        this.hasOperational = hasOperational;
    }

    public boolean getHasEvolutionary() {
        return hasEvolutionary;
    }

    public void setHasEvolutionary(boolean hasEvolutionary) {
        this.hasEvolutionary = hasEvolutionary;
    }

    public boolean getHasJuxtapositional() {
        return hasJuxtapositional;
    }

    public void setHasJuxtapositional(boolean hasJuxtapositional) {
        this.hasJuxtapositional = hasJuxtapositional;
    }

    public boolean getHasOrganic() {
        return hasOrganic;
    }

    public void setHasOrganic(boolean hasOrganic) {
        this.hasOrganic = hasOrganic;
    }

    public boolean getHasPerfunctory() {
        return hasPerfunctory;
    }

    public void setHasPerfunctory(boolean hasPerfunctory) {
        this.hasPerfunctory = hasPerfunctory;
    }

    public boolean getHasConfirmative() {
        return hasConfirmative;
    }

    public void setHasConfirmative(boolean hasConfirmative) {
        this.hasConfirmative = hasConfirmative;
    }

    public boolean getHasNegational() {
        return hasNegational;
    }

    public void setHasNegational(boolean hasNegational) {
        this.hasNegational = hasNegational;
    }

    public int getTense() {
        return tense;
    }

    public void setTense(int tense) {
        this.tense = tense;
    }

    public boolean getHasVoice() {
        return hasVoice;
    }

    public void setHasVoice(boolean hasVoice) {
        this.hasVoice = hasVoice;
    }

    public float getPaperLocation() {
        return paperLocation;
    }

    public void setPaperLocation(float paperLocation) {
        this.paperLocation = paperLocation;
    }

    public List<Integer> getConceptualCueList() {
        return conceptualCueList;
    }

    public void setConceptualCueList(List<Integer> conceptualCueList) {
        this.conceptualCueList = conceptualCueList;
    }

    public boolean getOurSubjectBoolean() {
        return ourSubjectBoolean;
    }

    public void setOurSubjectBoolean(boolean ourSubjectBoolean) {
        this.ourSubjectBoolean = ourSubjectBoolean;
    }

    public int getOurSubjectWeight() {
        return ourSubjectWeight;
    }

    public void setOurSubjectWeight(int ourSubjectWeight) {
        this.ourSubjectWeight = ourSubjectWeight;
    }

    public boolean getOurIdeaBoolean() {
        return ourIdeaBoolean;
    }

    public void setOurIdeaBoolean(boolean ourIdeaBoolean) {
        this.ourIdeaBoolean = ourIdeaBoolean;
    }

    public int getOurIdeaWeight() {
        return ourIdeaWeight;
    }

    public void setOurIdeaWeight(int ourIdeaWeight) {
        this.ourIdeaWeight = ourIdeaWeight;
    }

    public boolean getOurBasisBoolean() {
        return ourBasisBoolean;
    }

    public void setOurBasisBoolean(boolean ourBasisBoolean) {
        this.ourBasisBoolean = ourBasisBoolean;
    }

    public int getOurBasisWeight() {
        return ourBasisWeight;
    }

    public void setOurBasisWeight(int ourBasisWeight) {
        this.ourBasisWeight = ourBasisWeight;
    }

    public boolean getToolBoolean() {
        return toolBoolean;
    }

    public void setToolBoolean(boolean toolBoolean) {
        this.toolBoolean = toolBoolean;
    }

    public int getToolWeight() {
        return toolWeight;
    }

    public void setToolWeight(int toolWeight) {
        this.toolWeight = toolWeight;
    }

    public boolean getOurCompareBoolean() {
        return ourCompareBoolean;
    }

    public void setOurCompareBoolean(boolean ourCompareBoolean) {
        this.ourCompareBoolean = ourCompareBoolean;
    }

    public int getOurCompareWeight() {
        return ourCompareWeight;
    }

    public void setOurCompareWeight(int ourCompareWeight) {
        this.ourCompareWeight = ourCompareWeight;
    }

    public boolean getOurQuantityBoolean() {
        return ourQuantityBoolean;
    }

    public void setOurQuantityBoolean(boolean ourQuantityBoolean) {
        this.ourQuantityBoolean = ourQuantityBoolean;
    }

    public int getOurQuantityWeight() {
        return ourQuantityWeight;
    }

    public void setOurQuantityWeight(int ourQuantityWeight) {
        this.ourQuantityWeight = ourQuantityWeight;
    }

    public boolean getOurFrequencyBoolean() {
        return ourFrequencyBoolean;
    }

    public void setOurFrequencyBoolean(boolean ourFrequencyBoolean) {
        this.ourFrequencyBoolean = ourFrequencyBoolean;
    }

    public int getOurFrequencyWeight() {
        return ourFrequencyWeight;
    }

    public void setOurFrequencyWeight(int ourFrequencyWeight) {
        this.ourFrequencyWeight = ourFrequencyWeight;
    }

    public boolean getOurTenseBoolean() {
        return ourTenseBoolean;
    }

    public void setOurTenseBoolean(boolean ourTenseBoolean) {
        this.ourTenseBoolean = ourTenseBoolean;
    }

    public int getOurTenseWeight() {
        return ourTenseWeight;
    }

    public void setOurTenseWeight(int ourTenseWeight) {
        this.ourTenseWeight = ourTenseWeight;
    }

    public boolean getOurExampleBoolean() {
        return ourExampleBoolean;
    }

    public void setOurExampleBoolean(boolean ourExampleBoolean) {
        this.ourExampleBoolean = ourExampleBoolean;
    }

    public int getOurExampleWeight() {
        return ourExampleWeight;
    }

    public void setOurExampleWeight(int ourExampleWeight) {
        this.ourExampleWeight = ourExampleWeight;
    }

    public boolean getOurSuggestBoolean() {
        return ourSuggestBoolean;
    }

    public void setOurSuggestBoolean(boolean ourSuggestBoolean) {
        this.ourSuggestBoolean = ourSuggestBoolean;
    }

    public int getOurSuggestWeight() {
        return ourSuggestWeight;
    }

    public void setOurSuggestWeight(int ourSuggestWeight) {
        this.ourSuggestWeight = ourSuggestWeight;
    }

    public boolean getOurHedgeBoolean() {
        return ourHedgeBoolean;
    }

    public void setOurHedgeBoolean(boolean ourHedgeBoolean) {
        this.ourHedgeBoolean = ourHedgeBoolean;
    }

    public int getOurHedgeWeight() {
        return ourHedgeWeight;
    }

    public void setOurHedgeWeight(int ourHedgeWeight) {
        this.ourHedgeWeight = ourHedgeWeight;
    }

    public boolean getOurResultBoolean() {
        return ourResultBoolean;
    }

    public void setOurResultBoolean(boolean ourResultBoolean) {
        this.ourResultBoolean = ourResultBoolean;
    }

    public int getOurResultWeight() {
        return ourResultWeight;
    }

    public void setOurResultWeight(int ourResultWeight) {
        this.ourResultWeight = ourResultWeight;
    }

    public boolean getObjectBoolean() {
        return objectBoolean;
    }

    public void setObjectBoolean(boolean objectBoolean) {
        this.objectBoolean = objectBoolean;
    }

    public int getObjectWeight() {
        return objectWeight;
    }

    public void setObjectWeight(int objectWeight) {
        this.objectWeight = objectWeight;
    }

    public boolean getFutureBoolean() {
        return futureBoolean;
    }

    public void setFutureBoolean(boolean futureBoolean) {
        this.futureBoolean = futureBoolean;
    }

    public int getFutureWeight() {
        return futureWeight;
    }

    public void setFutureWeight(int futureWeight) {
        this.futureWeight = futureWeight;
    }

    public boolean getNumberBoolean() {
        return numberBoolean;
    }

    public void setNumberBoolean(boolean numberBoolean) {
        this.numberBoolean = numberBoolean;
    }

    public int getNumberCount() {
        return numberCount;
    }

    public void setNumberCount(int numberCount) {
        this.numberCount = numberCount;
    }

    public boolean getPercentageBoolean() {
        return percentageBoolean;
    }

    public void setPercentageBoolean(boolean percentageBoolean) {
        this.percentageBoolean = percentageBoolean;
    }

    public int getPercentageCount() {
        return percentageCount;
    }

    public void setPercentageCount(int percentageCount) {
        this.percentageCount = percentageCount;
    }

    public ArrayList<String> getOurSubjectWords() {
        return ourSubjectWords;
    }

    public void setOurSubjectWords(ArrayList<String> ourSubjectWords) {
        this.ourSubjectWords = ourSubjectWords;
    }

    public ArrayList<String> getOurIdeaWords() {
        return ourIdeaWords;
    }

    public void setOurIdeaWords(ArrayList<String> ourIdeaWords) {
        this.ourIdeaWords = ourIdeaWords;
    }

    public ArrayList<String> getOurBasisWords() {
        return ourBasisWords;
    }

    public void setOurBasisWords(ArrayList<String> ourBasisWords) {
        this.ourBasisWords = ourBasisWords;
    }

    public ArrayList<String> getOurCompareWords() {
        return ourCompareWords;
    }

    public void setOurCompareWords(ArrayList<String> ourCompareWords) {
        this.ourCompareWords = ourCompareWords;
    }

    public ArrayList<String> getOurResultWords() {
        return ourResultWords;
    }

    public void setOurResultWords(ArrayList<String> ourResultWords) {
        this.ourResultWords = ourResultWords;
    }

    public ArrayList<String> getOurQuantityWords() {
        return ourQuantityWords;
    }

    public void setOurQuantityWords(ArrayList<String> ourQuantityWords) {
        this.ourQuantityWords = ourQuantityWords;
    }

    public ArrayList<String> getOurFrequencyWords() {
        return ourFrequencyWords;
    }

    public void setOurFrequencyWords(ArrayList<String> ourFrequencyWords) {
        this.ourFrequencyWords = ourFrequencyWords;
    }

    public ArrayList<String> getOurTenseWords() {
        return ourTenseWords;
    }

    public void setOurTenseWords(ArrayList<String> ourTenseWords) {
        this.ourTenseWords = ourTenseWords;
    }

    public ArrayList<String> getOurExampleWords() {
        return ourExampleWords;
    }

    public void setOurExampleWords(ArrayList<String> ourExampleWords) {
        this.ourExampleWords = ourExampleWords;
    }

    public ArrayList<String> getOurSuggestWords() {
        return ourSuggestWords;
    }

    public void setOurSuggestWords(ArrayList<String> ourSuggestWords) {
        this.ourSuggestWords = ourSuggestWords;
    }

    public ArrayList<String> getOurHedgeWords() {
        return ourHedgeWords;
    }

    public void setOurHedgeWords(ArrayList<String> ourHedgeWords) {
        this.ourHedgeWords = ourHedgeWords;
    }

    public ArrayList<String> getObjectWords() {
        return objectWords;
    }

    public void setObjectWords(ArrayList<String> objectWords) {
        this.objectWords = objectWords;
    }

    public ArrayList<String> getToolWords() {
        return toolWords;
    }

    public void setToolWords(ArrayList<String> toolWords) {
        this.toolWords = toolWords;
    }

    public ArrayList<String> getFutureWords() {
        return futureWords;
    }

    public void setFutureWords(ArrayList<String> futureWords) {
        this.futureWords = futureWords;
    }

    public List<String> getSubjectLinkedKeyWords() {
        return subjectLinkedKeyWords;
    }

    public void setSubjectLinkedKeyWords(List<String> subjectLinkedKeyWords) {
        this.subjectLinkedKeyWords = subjectLinkedKeyWords;
    }

    public List<String> getCitationLinkedKeyWords() {
        return citationLinkedKeyWords;
    }

    public void setCitationLinkedKeyWords(List<String> citationLinkedKeyWords) {
        this.citationLinkedKeyWords = citationLinkedKeyWords;
    }

    public int getSentenceLength() {
        return sentenceLength;
    }

    public void setSentenceLength(int sentenceLength) {
        this.sentenceLength = sentenceLength;
    }

    public int getShortSentenceNumber() {
        return shortSentenceNumber;
    }

    public void setShortSentenceNumber(int shortSentenceNumber) {
        this.shortSentenceNumber = shortSentenceNumber;
    }

    public double getShortSentenceAvgLength() {
        return ShortSentenceAvgLength;
    }

    public void setShortSentenceAvgLength(double shortSentenceAvgLength) {
        ShortSentenceAvgLength = shortSentenceAvgLength;
    }

    public List<String> getSubjectLinkedDependencies() {
        return subjectLinkedDependencies;
    }

    public void setSubjectLinkedDependencies(List<String> subjectLinkedDependencies) {
        this.subjectLinkedDependencies = subjectLinkedDependencies;
    }

    public List<String> getCitationLinkedDependencies() {
        return citationLinkedDependencies;
    }

    public void setCitationLinkedDependencies(List<String> citationLinkedDependencies) {
        this.citationLinkedDependencies = citationLinkedDependencies;
    }

    public ArrayList<Citation> getPreviousSentenceCitationList() {
        return previousSentenceCitationList;
    }

    public void setPreviousSentenceCitationList(ArrayList<Citation> previousSentenceCitationList) {
        this.previousSentenceCitationList = previousSentenceCitationList;
    }

    public int getPreviousSentenceCitationNumber() {
        return previousSentenceCitationNumber;
    }

    public void setPreviousSentenceCitationNumber(int previousSentenceCitationNumber) {
        this.previousSentenceCitationNumber = previousSentenceCitationNumber;
    }

    public ArrayList<Citation> getFollowingSentenceCitationList() {
        return followingSentenceCitationList;
    }

    public void setFollowingSentenceCitationList(ArrayList<Citation> followingSentenceCitationList) {
        this.followingSentenceCitationList = followingSentenceCitationList;
    }

    public int getCitationNumber() {
        return citationNumber;
    }

    public void setCitationNumber(int citationNumber) {
        this.citationNumber = citationNumber;
    }

    public ArrayList<Citation> getCitationList() {
        return citationList;
    }

    public void setCitationList(ArrayList<Citation> citationList) {
        this.citationList = citationList;
    }

    public int getFollowingSentenceCitationNumber() {
        return followingSentenceCitationNumber;
    }

    public void setFollowingSentenceCitationNumber(int followingSentenceCitationNumber) {
        this.followingSentenceCitationNumber = followingSentenceCitationNumber;
    }

    public void setPreviousSentenceRelated(boolean previousSentenceRelated) {
        this.previousSentenceRelated = previousSentenceRelated;
    }

    public boolean getPreviousSentenceRelated() {
        return previousSentenceRelated;
    }

    public void setFollowingSentenceRelated(boolean followingSentenceRelated) {
        this.followingSentenceRelated = followingSentenceRelated;
    }

    public boolean getFollowingSentenceRelated() {
        return followingSentenceRelated;
    }

    public void setOurContextSubjectBoolean(boolean ourContextSubjectBoolean) {
        this.ourContextSubjectBoolean = ourContextSubjectBoolean;
    }

    public boolean getOurContextSubjectBoolean() {
        return ourContextSubjectBoolean;
    }

    public void setOurContextSubjectWeight(int ourContextSubjectWeight) {
        this.ourContextSubjectWeight = ourContextSubjectWeight;
    }

    public int getOurContextSubjectWeight() {
        return ourContextSubjectWeight;
    }

    public void setOurContextIdeaBoolean(boolean ourContextIdeaBoolean) {
        this.ourContextIdeaBoolean = ourContextIdeaBoolean;
    }

    public boolean getOurContextIdeaBoolean() {
        return ourContextIdeaBoolean;
    }

    public void setOurContextIdeaWeight(int ourContextIdeaWeight) {
        this.ourContextIdeaWeight = ourContextIdeaWeight;
    }

    public int getOurContextIdeaWeight() {
        return ourContextIdeaWeight;
    }

    public void setOurContextBasisBoolean(boolean ourContextBasisBoolean) {
        this.ourContextBasisBoolean = ourContextBasisBoolean;
    }

    public boolean getOurContextBasisBoolean() {
        return ourContextBasisBoolean;
    }

    public void setOurContextBasisWeight(int ourContextBasisWeight) {
        this.ourContextBasisWeight = ourContextBasisWeight;
    }

    public int getOurContextBasisWeight() {
        return ourContextBasisWeight;
    }

    public void setOurContextCompareBoolean(boolean ourContextCompareBoolean) {
        this.ourContextCompareBoolean = ourContextCompareBoolean;
    }

    public boolean getOurContextCompareBoolean() {
        return ourContextCompareBoolean;
    }

    public void setOurContextCompareWeight(int ourContextCompareWeight) {
        this.ourContextCompareWeight = ourContextCompareWeight;
    }

    public int getOurContextCompareWeight() {
        return ourContextCompareWeight;
    }

    public void setOurContextResultBoolean(boolean ourContextResultBoolean) {
        this.ourContextResultBoolean = ourContextResultBoolean;
    }

    public boolean getOurContextResultBoolean() {
        return ourContextResultBoolean;
    }

    public void setOurContextResultWeight(int ourContextResultWeight) {
        this.ourContextResultWeight = ourContextResultWeight;
    }

    public int getOurContextResultWeight() {
        return ourContextResultWeight;
    }

    public void setOurContextQuantityBoolean(boolean ourContextQuantityBoolean) {
        this.ourContextQuantityBoolean = ourContextQuantityBoolean;
    }

    public boolean getOurContextQuantityBoolean() {
        return ourContextQuantityBoolean;
    }

    public void setOurContextQuantityWeight(int ourContextQuantityWeight) {
        this.ourContextQuantityWeight = ourContextQuantityWeight;
    }

    public int getOurContextQuantityWeight() {
        return ourContextQuantityWeight;
    }

    public void setOurContextFrequencyBoolean(boolean ourContextFrequencyBoolean) {
        this.ourContextFrequencyBoolean = ourContextFrequencyBoolean;
    }

    public boolean getOurContextFrequencyBoolean() {
        return ourContextFrequencyBoolean;
    }

    public void setOurContextFrequencyWeight(int ourContextFrequencyWeight) {
        this.ourContextFrequencyWeight = ourContextFrequencyWeight;
    }

    public int getOurContextFrequencyWeight() {
        return ourContextFrequencyWeight;
    }

    public void setOurContextTenseBoolean(boolean ourContextTenseBoolean) {
        this.ourContextTenseBoolean = ourContextTenseBoolean;
    }

    public boolean getOurContextTenseBoolean() {
        return ourContextTenseBoolean;
    }

    public void setOurContextTenseWeight(int ourContextTenseWeight) {
        this.ourContextTenseWeight = ourContextTenseWeight;
    }

    public int getOurContextTenseWeight() {
        return ourContextTenseWeight;
    }

    public void setOurContextExampleBoolean(boolean ourContextExampleBoolean) {
        this.ourContextExampleBoolean = ourContextExampleBoolean;
    }

    public boolean getOurContextExampleBoolean() {
        return ourContextExampleBoolean;
    }

    public void setOurContextExampleWeight(int ourContextExampleWeight) {
        this.ourContextExampleWeight = ourContextExampleWeight;
    }

    public int getOurContextExampleWeight() {
        return ourContextExampleWeight;
    }

    public void setOurContextSuggestBoolean(boolean ourContextSuggestBoolean) {
        this.ourContextSuggestBoolean = ourContextSuggestBoolean;
    }

    public boolean getOurContextSuggestBoolean() {
        return ourContextSuggestBoolean;
    }

    public void setOurContextSuggestWeight(int ourContextSuggestWeight) {
        this.ourContextSuggestWeight = ourContextSuggestWeight;
    }

    public int getOurContextSuggestWeight() {
        return ourContextSuggestWeight;
    }

    public void setContextOurHedgeBoolean(boolean contextOurHedgeBoolean) {
        this.contextOurHedgeBoolean = contextOurHedgeBoolean;
    }

    public boolean getContextOurHedgeBoolean() {
        return contextOurHedgeBoolean;
    }

    public void setContextOurHedgeWeight(int contextOurHedgeWeight) {
        this.contextOurHedgeWeight = contextOurHedgeWeight;
    }

    public int getContextOurHedgeWeight() {
        return contextOurHedgeWeight;
    }

    public void setContextObjectBoolean(boolean contextObjectBoolean) {
        this.contextObjectBoolean = contextObjectBoolean;
    }

    public boolean getContextObjectBoolean() {
        return contextObjectBoolean;
    }

    public void setContextObjectWeight(int contextObjectWeight) {
        this.contextObjectWeight = contextObjectWeight;
    }

    public int getContextObjectWeight() {
        return contextObjectWeight;
    }

    public void setContextToolBoolean(boolean contextToolBoolean) {
        this.contextToolBoolean = contextToolBoolean;
    }

    public boolean getContextToolBoolean() {
        return contextToolBoolean;
    }

    public void setContextToolWeight(int contextToolWeight) {
        this.contextToolWeight = contextToolWeight;
    }

    public int getContextToolWeight() {
        return contextToolWeight;
    }

    public void setContextFutureBoolean(boolean contextFutureBoolean) {
        this.contextFutureBoolean = contextFutureBoolean;
    }

    public boolean getContextFutureBoolean() {
        return contextFutureBoolean;
    }

    public void setContextFutureWeight(int contextFutureWeight) {
        this.contextFutureWeight = contextFutureWeight;
    }

    public int getContextFutureWeight() {
        return contextFutureWeight;
    }

    public void setContextNumberCount(int contextNumberCount) {
        this.contextNumberCount = contextNumberCount;
    }

    public int getContextNumberCount() {
        return contextNumberCount;
    }

    public void setContextNumberBoolean(boolean contextNumberBoolean) {
        this.contextNumberBoolean = contextNumberBoolean;
    }

    public boolean getContextNumberBoolean() {
        return contextNumberBoolean;
    }

    public void setContextPercentageCount(int contextPercentageCount) {
        this.contextPercentageCount = contextPercentageCount;
    }

    public int getContextPercentageCount() {
        return contextPercentageCount;
    }

    public void setContextPercentageBoolean(boolean contextPercentageBoolean) {
        this.contextPercentageBoolean = contextPercentageBoolean;
    }

    public boolean getContextPercentageBoolean() {
        return contextPercentageBoolean;
    }

    public void setContextSubjectLinkedKeyWords(ArrayList<String> contextSubjectLinkedKeyWords) {
        this.contextSubjectLinkedKeyWords = contextSubjectLinkedKeyWords;
    }

    public ArrayList<String> getContextSubjectLinkedKeyWords() {
        return contextSubjectLinkedKeyWords;
    }

    public void setContextnGram(ArrayList<String> contextnGram) {
        this.contextnGram = contextnGram;
    }

    public ArrayList<String> getContextnGram() {
        return contextnGram;
    }

    public ArrayList<String> getContextFutureWords() {
        return contextFutureWords;
    }

    public void setContextFutureWords(ArrayList<String> contextFutureWords) {
        this.contextFutureWords = contextFutureWords;
    }

    public ArrayList<String> getOurContextSubjectWords() {
        return ourContextSubjectWords;
    }

    public void setOurContextSubjectWords(ArrayList<String> ourContextSubjectWords) {
        this.ourContextSubjectWords = ourContextSubjectWords;
    }

    public ArrayList<String> getOurContextIdeaWords() {
        return ourContextIdeaWords;
    }

    public void setOurContextIdeaWords(ArrayList<String> ourContextIdeaWords) {
        this.ourContextIdeaWords = ourContextIdeaWords;
    }

    public ArrayList<String> getOurContextBasisWords() {
        return ourContextBasisWords;
    }

    public void setOurContextBasisWords(ArrayList<String> ourContextBasisWords) {
        this.ourContextBasisWords = ourContextBasisWords;
    }

    public ArrayList<String> getOurContextCompareWords() {
        return ourContextCompareWords;
    }

    public void setOurContextCompareWords(ArrayList<String> ourContextCompareWords) {
        this.ourContextCompareWords = ourContextCompareWords;
    }

    public ArrayList<String> getOurContextResultWords() {
        return ourContextResultWords;
    }

    public void setOurContextResultWords(ArrayList<String> ourContextResultWords) {
        this.ourContextResultWords = ourContextResultWords;
    }

    public ArrayList<String> getOurContextQuantityWords() {
        return ourContextQuantityWords;
    }

    public void setOurContextQuantityWords(ArrayList<String> ourContextQuantityWords) {
        this.ourContextQuantityWords = ourContextQuantityWords;
    }

    public ArrayList<String> getOurContextFrequencyWords() {
        return ourContextFrequencyWords;
    }

    public void setOurContextFrequencyWords(ArrayList<String> ourContextFrequencyWords) {
        this.ourContextFrequencyWords = ourContextFrequencyWords;
    }

    public ArrayList<String> getOurContextTenseWords() {
        return ourContextTenseWords;
    }

    public void setOurContextTenseWords(ArrayList<String> ourContextTenseWords) {
        this.ourContextTenseWords = ourContextTenseWords;
    }

    public ArrayList<String> getOurContextExampleWords() {
        return ourContextExampleWords;
    }

    public void setOurContextExampleWords(ArrayList<String> ourContextExampleWords) {
        this.ourContextExampleWords = ourContextExampleWords;
    }

    public ArrayList<String> getOurContextSuggestWords() {
        return ourContextSuggestWords;
    }

    public void setOurContextSuggestWords(ArrayList<String> ourContextSuggestWords) {
        this.ourContextSuggestWords = ourContextSuggestWords;
    }

    public ArrayList<String> getOurContextHedgeWords() {
        return ourContextHedgeWords;
    }

    public void setOurContextHedgeWords(ArrayList<String> ourContextHedgeWords) {
        this.ourContextHedgeWords = ourContextHedgeWords;
    }

    public ArrayList<String> getContextObjectWords() {
        return contextObjectWords;
    }

    public void setContextObjectWords(ArrayList<String> contextObjectWords) {
        this.contextObjectWords = contextObjectWords;
    }

    public ArrayList<String> getContextToolWords() {
        return contextToolWords;
    }

    public void setContextToolWords(ArrayList<String> contextToolWords) {
        this.contextToolWords = contextToolWords;
    }

    public List<String> getSubjectCitationLinkedWords() {
        return subjectCitationLinkedWords;
    }

    public void setSubjectCitationLinkedWords(List<String> subjectCitationLinkedWords) {
        this.subjectCitationLinkedWords = subjectCitationLinkedWords;
    }

    public boolean getSubjectLinkedToCitationBoolean() {
        return subjectLinkedToCitationBoolean;
    }

    public void setSubjectLinkedToCitationBoolean(boolean subjectLinkedToCitationBoolean) {
        this.subjectLinkedToCitationBoolean = subjectLinkedToCitationBoolean;
    }
}

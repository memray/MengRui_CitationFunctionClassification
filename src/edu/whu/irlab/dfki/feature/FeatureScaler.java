package edu.whu.irlab.dfki.feature;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;


/**
 * Created by Memray on 2015/3/24.
 */
public class FeatureScaler {
    /**
     * 对该instanceList进行scale
     * @param instances
     */
    public static Instances scale(Instances instances, String[] args) {
        Normalize normalize = new Normalize();
        try {
            normalize.setOptions(args);
            normalize.setInputFormat(instances);
            return Filter.useFilter(instances, normalize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

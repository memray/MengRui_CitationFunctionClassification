package edu.whu.irlab.dfki.feature.ReferenceFeature;

import edu.whu.irlab.bean.Citation;
import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.bean.Reference;
import edu.whu.irlab.commonUtils.ArrayListUtils;
import org.apache.commons.io.FileUtils;
import weka.core.pmml.Array;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by lxb on 2015/4/22.
 * 对reference特征处理的一些工具
 */
public class ReferenceFeatureUtils {
    //全局的nGramWords词表
    static public ArrayList<String> nGramWords = new ArrayList<>();
    //全局大词表
    static public ArrayList<String> allWords = new ArrayList<>();

    //创建n-gram全局词表,并且把各自的n-gram放在对应的featuretemplate里
    public static void createNgramWords(ArrayList<Reference> references) {
        for (Reference reference : references) {
            for (Citation citation : reference.getCitations()) {
                for (String s : citation.getDataPoint().getFeature().getnGram()) {
                    //把各个citation包含的特征，赋给reference里去
                    //生成全局词表
                    if (!nGramWords.contains(s))
                        nGramWords.add(s);
                    //给各个reference赋值
                    if (reference.getReferenceFeatureTemplate() == null)
                        reference.setReferenceFeatureTemplate(new ReferenceFeatureTemplate());
                    if (reference.getReferenceFeatureTemplate().getReferenceNGramWords() == null || reference.getReferenceFeatureTemplate().getReferenceNGramWords().size() == 0) {
                        ArrayList<String> ngrams = new ArrayList<>();
                        ngrams.add(s);
                        reference.getReferenceFeatureTemplate().setReferenceNGramWords(ngrams);
                    } else {
                        ArrayList<String> ngrams = reference.getReferenceFeatureTemplate().getReferenceNGramWords();
                        if (!ngrams.contains(s)) {
                            ngrams.add(s);
                        }
                    }
                }
            }
        }
    }

    /**
     * 写一个reference的通用方法，把reference各个不重复 datapoint 的值合并到它的属性中去
     * 这是一个把 datapoint 中的一些特征映射到reference级别上来的方法
     * 通过反射的途径，把需要的属性set进reference里面去
     */
    public static void getGenearlReferenceAttr(ArrayList<Reference> references, String attrName) {
        for (Reference reference : references) {
            //先获取指定属性的值
            List<DataPoint> datapoints = reference.getNorepeatDatapoints();
            ArrayList<String> generalReferenceWords = new ArrayList<>();
            for (DataPoint dataPoint : datapoints) {
                try {
                    //获取getter方法
                    Method getter = dataPoint.getFeature().getClass().getMethod("get" + attrName);
                    ArrayList<String> dataPointwords = (ArrayList<String>) getter.invoke(dataPoint.getFeature());
                    ArrayListUtils.addListAtoB(dataPointwords, generalReferenceWords);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            //这个时候 调用 referencetemplate 的set方法,把上面获取到的词表放入到reference 对应的 list中去
            try {
                Method setter = reference.getReferenceFeatureTemplate().getClass().getMethod("set" + attrName, ArrayList.class);
                setter.invoke(reference.getReferenceFeatureTemplate(), generalReferenceWords);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }


        }
    }

    //liu 添加功能

    /**
     * 输入reference列表，生成需要训练的arff文件
     * type : 1 : baseline 2 :加上我们的特征
     */

    private static void generateArffAndXml(ArrayList<Reference> references, File file, int type) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> wordsList = new ArrayList<String>();
        if (type == 1) {
            wordsList = nGramWords; // n

        } else if (type == 2) {
            wordsList = allWords;
        }

        //先生成头信息
        sb.append("@relation " + file.getName() + "\n\n");

        //再生成属性列表
        int index = 1;
        for (String s : wordsList) {
            sb.append("@attribute attr_" + (index++) + " numeric\n");
        }
        sb.append("@attribute idea {0,1}\n");
        sb.append("@attribute basis {0,1}\n");
        sb.append("@attribute compare {0,1}\n");
        sb.append("@attribute relative {0,1}\n");

        sb.append("\n@data\n");
        //再生成数据列表
        for (Reference reference : references) {
            ArrayList<String> referenceNGram = reference.getReferenceFeatureTemplate().getReferenceNGramWords();
            for (String s : nGramWords) {
                //如果存在就赋值为1.0
                if (referenceNGram.contains(s))
                    sb.append("1.0,");
                else
                    sb.append("0.0,");
            }
            List<String> labels = reference.getLabels();
            for (int k = 1; k <= 3; k++) {
                if (labels.contains("" + k))
                    sb.append("1,");
                else
                    sb.append("0,");
            }
            if (labels.contains("4"))
                sb.append("1\n");
            else
                sb.append("0\n");
        }

        try {
            FileUtils.writeStringToFile(file, sb.toString(), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //生成weka需要的文件( baseline : n-gram)
    public static void createWekaReference(ArrayList<Reference> references) {
        //base的 arff
        String base_arffPath = "corpus/referenceCorpus/reference_baseline.arff";
        //加上我们的词表特征之后的 arff
        String our_arffPath = "corpus/referenceCorpus/reference_ours.arff";

        File base_ReferenceArffFile = new File(base_arffPath);
        if (base_ReferenceArffFile.exists()) {
            System.out.println("baseline 文件已经存在 ， 无需生成");
        } else {
            generateArffAndXml(references, base_ReferenceArffFile, 1);
        }

        File ours_ReferenceArffFile = new File(our_arffPath);
        if (ours_ReferenceArffFile.exists()) {
            System.out.println("our 文件已经存在 ， 无需生成");
        } else {
            generateArffAndXml(references, ours_ReferenceArffFile, 2);
        }
    }

    /*
        一个reference可能包含多个citation，这多个citation也可以会存在于同一句中，
        一个citation 肯定指向一个 datapoint，然而可能指向相同的datapoints，
        这里，我们生成不相同的datapoints列表，方便进一步讨论
     */
    public static void getNotSameReferenceDatapoints(ArrayList<Reference> references) {
        for (Reference r : references) {
            HashSet<String> aclidSet = new HashSet<>();
            for (Citation citation : r.getCitations()) {
                if (!aclidSet.contains(citation.getDataPoint().getAclid()))
                    r.getNorepeatDatapoints().add(citation.getDataPoint());
                aclidSet.add(citation.getDataPoint().getAclid());
            }
        }
    }

    /**
     * 把references 列表中的所有的words属性取出来，生成全局大词表
     */
    public static void generateAllWordsList(Reference reference) {
        //先获取referenceFeatureTemplate里的所有方法
        Method[] methods = reference.getReferenceFeatureTemplate().getClass().getMethods();
        for (Method method : methods) {
            //获得 所有的words 列表
            if (method.getName().matches("get\\w*?Words")) {
                try {
                    //运行这个get方法
                    ArrayList<String> objectList = (ArrayList) method.invoke(reference.getReferenceFeatureTemplate());
                    for (String ob : objectList) {
                        //拼接一下字符串 方法名_词
                        String pjWords = method.getName() + "_" + ob;
                        //不在全局词表中的话 添加到全局大词表中
                        if (!allWords.contains(pjWords))
                            allWords.add(pjWords);
                    }

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 把reference中的词表特征获取一下，并且生成下全局词表
     */
    public static void getReferenceWordsFeature(ArrayList<Reference> references) {
        ReferenceFeatureTemplate rft = new ReferenceFeatureTemplate();
        /**
         * 获取ReferenceFeatureTemplate类的属性列表，抛去ngram，因为之前取过了,这样获取了属性
         * 字符串，方便下一步处理
         */
        //这里获取所有的属性
        Field[] fields = rft.getClass().getDeclaredFields();
        for (int k = 0; k < fields.length; k++) {
            String fieldName = fields[k].getName();
            if(fieldName.endsWith("Words")&&!fieldName.equals("referenceNGramWords")){
                //先把第一个字母变成大写的,因为到最后要拼接成get set 方法
                String finalWord = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                //把reference的每个属性都获取生成一下
                System.out.println("================");
                System.out.println(finalWord);
                System.out.println("================");
                ReferenceFeatureUtils.getGenearlReferenceAttr(references, finalWord);
            }
        }
        //生成全局大词表
        for(Reference reference :references)
            generateAllWordsList(reference);

    }

    public static void main(String[] args) {
        String test = "getSubjectWords";

    }

}

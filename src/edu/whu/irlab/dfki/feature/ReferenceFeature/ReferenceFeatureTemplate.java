package edu.whu.irlab.dfki.feature.ReferenceFeature;

import edu.whu.irlab.dfki.feature.FeatureTemplate;

import java.util.ArrayList;

/**
 * Created by lxb on 2015/4/21.
 */
public class ReferenceFeatureTemplate extends FeatureTemplate {
    //n-gram
    private ArrayList<String> referenceNGramWords = new ArrayList<String>();

    /*
    liuxingbang 添加特征 用于比较
     */
    //reference在一篇文章里的citation个数
    private Integer citationNumber = 0;
    //reference 的 citation 处于的章节位置 : 一共 6个位置
    private int[] sectionLocation = new int[6];

    //dfki词表特征
    ArrayList<String> subjectWords = new ArrayList<String>();
    ArrayList<String> quantityWords = new ArrayList<String>();
    ArrayList<String> frequencyWords = new ArrayList<String>();
    ArrayList<String> tenseWords = new ArrayList<String>();
    ArrayList<String> exampleWords = new ArrayList<String>();
    ArrayList<String> suggestWords = new ArrayList<String>();
    ArrayList<String> hedgeWords = new ArrayList<String>();
    ArrayList<String> ideaWords = new ArrayList<String>();
    ArrayList<String> basisWords = new ArrayList<String>();
    ArrayList<String> compareWords = new ArrayList<String>();
    ArrayList<String> resultWords = new ArrayList<String>();
    ArrayList<String> neighborSubjectWords = new ArrayList<String>();

    //对应于上述词表中的词汇特征
    ArrayList<String> ourSubjectWords = new ArrayList<String>();
    ArrayList<String> ourIdeaWords = new ArrayList<String>();
    ArrayList<String> ourBasisWords = new ArrayList<String>();
    ArrayList<String> ourCompareWords = new ArrayList<String>();
    ArrayList<String> ourResultWords = new ArrayList<String>();

    ArrayList<String> ourQuantityWords = new ArrayList<String>();
    ArrayList<String> ourFrequencyWords = new ArrayList<String>();
    ArrayList<String> ourTenseWords = new ArrayList<String>();
    ArrayList<String> ourExampleWords = new ArrayList<String>();
    ArrayList<String> ourSuggestWords = new ArrayList<String>();
    ArrayList<String> ourHedgeWords = new ArrayList<String>();

    ArrayList<String> objectWords = new ArrayList<String>();
    ArrayList<String> toolWords = new ArrayList<String>();
    ArrayList<String> futureWords = new ArrayList<String>();

    public ArrayList<String> getReferenceNGramWords() {
        return referenceNGramWords;
    }

    public void setReferenceNGramWords(ArrayList<String> referenceNGramWords) {
        this.referenceNGramWords = referenceNGramWords;
    }

    public Integer getCitationNumber() {
        return citationNumber;
    }

    public void setCitationNumber(Integer citationNumber) {
        this.citationNumber = citationNumber;
    }

    public int[] getSectionLocation() {
        return sectionLocation;
    }

    public void setSectionLocation(int[] sectionLocation) {
        this.sectionLocation = sectionLocation;
    }

    public ArrayList<String> getSubjectWords() {
        return subjectWords;
    }

    public void setSubjectWords(ArrayList<String> subjectWords) {
        this.subjectWords = subjectWords;
    }

    public ArrayList<String> getQuantityWords() {
        return quantityWords;
    }

    public void setQuantityWords(ArrayList<String> quantityWords) {
        this.quantityWords = quantityWords;
    }

    public ArrayList<String> getFrequencyWords() {
        return frequencyWords;
    }

    public void setFrequencyWords(ArrayList<String> frequencyWords) {
        this.frequencyWords = frequencyWords;
    }

    public ArrayList<String> getTenseWords() {
        return tenseWords;
    }

    public void setTenseWords(ArrayList<String> tenseWords) {
        this.tenseWords = tenseWords;
    }

    public ArrayList<String> getExampleWords() {
        return exampleWords;
    }

    public void setExampleWords(ArrayList<String> exampleWords) {
        this.exampleWords = exampleWords;
    }

    public ArrayList<String> getSuggestWords() {
        return suggestWords;
    }

    public void setSuggestWords(ArrayList<String> suggestWords) {
        this.suggestWords = suggestWords;
    }

    public ArrayList<String> getHedgeWords() {
        return hedgeWords;
    }

    public void setHedgeWords(ArrayList<String> hedgeWords) {
        this.hedgeWords = hedgeWords;
    }

    public ArrayList<String> getIdeaWords() {
        return ideaWords;
    }

    public void setIdeaWords(ArrayList<String> ideaWords) {
        this.ideaWords = ideaWords;
    }

    public ArrayList<String> getBasisWords() {
        return basisWords;
    }

    public void setBasisWords(ArrayList<String> basisWords) {
        this.basisWords = basisWords;
    }

    public ArrayList<String> getCompareWords() {
        return compareWords;
    }

    public void setCompareWords(ArrayList<String> compareWords) {
        this.compareWords = compareWords;
    }

    public ArrayList<String> getResultWords() {
        return resultWords;
    }

    public void setResultWords(ArrayList<String> resultWords) {
        this.resultWords = resultWords;
    }

    public ArrayList<String> getNeighborSubjectWords() {
        return neighborSubjectWords;
    }

    public void setNeighborSubjectWords(ArrayList<String> neighborSubjectWords) {
        this.neighborSubjectWords = neighborSubjectWords;
    }

    public ArrayList<String> getOurSubjectWords() {
        return ourSubjectWords;
    }

    public void setOurSubjectWords(ArrayList<String> ourSubjectWords) {
        this.ourSubjectWords = ourSubjectWords;
    }

    public ArrayList<String> getOurIdeaWords() {
        return ourIdeaWords;
    }

    public void setOurIdeaWords(ArrayList<String> ourIdeaWords) {
        this.ourIdeaWords = ourIdeaWords;
    }

    public ArrayList<String> getOurBasisWords() {
        return ourBasisWords;
    }

    public void setOurBasisWords(ArrayList<String> ourBasisWords) {
        this.ourBasisWords = ourBasisWords;
    }

    public ArrayList<String> getOurCompareWords() {
        return ourCompareWords;
    }

    public void setOurCompareWords(ArrayList<String> ourCompareWords) {
        this.ourCompareWords = ourCompareWords;
    }

    public ArrayList<String> getOurResultWords() {
        return ourResultWords;
    }

    public void setOurResultWords(ArrayList<String> ourResultWords) {
        this.ourResultWords = ourResultWords;
    }

    public ArrayList<String> getOurQuantityWords() {
        return ourQuantityWords;
    }

    public void setOurQuantityWords(ArrayList<String> ourQuantityWords) {
        this.ourQuantityWords = ourQuantityWords;
    }

    public ArrayList<String> getOurFrequencyWords() {
        return ourFrequencyWords;
    }

    public void setOurFrequencyWords(ArrayList<String> ourFrequencyWords) {
        this.ourFrequencyWords = ourFrequencyWords;
    }

    public ArrayList<String> getOurTenseWords() {
        return ourTenseWords;
    }

    public void setOurTenseWords(ArrayList<String> ourTenseWords) {
        this.ourTenseWords = ourTenseWords;
    }

    public ArrayList<String> getOurExampleWords() {
        return ourExampleWords;
    }

    public void setOurExampleWords(ArrayList<String> ourExampleWords) {
        this.ourExampleWords = ourExampleWords;
    }

    public ArrayList<String> getOurSuggestWords() {
        return ourSuggestWords;
    }

    public void setOurSuggestWords(ArrayList<String> ourSuggestWords) {
        this.ourSuggestWords = ourSuggestWords;
    }

    public ArrayList<String> getOurHedgeWords() {
        return ourHedgeWords;
    }

    public void setOurHedgeWords(ArrayList<String> ourHedgeWords) {
        this.ourHedgeWords = ourHedgeWords;
    }

    public ArrayList<String> getObjectWords() {
        return objectWords;
    }

    public void setObjectWords(ArrayList<String> objectWords) {
        this.objectWords = objectWords;
    }

    public ArrayList<String> getToolWords() {
        return toolWords;
    }

    public void setToolWords(ArrayList<String> toolWords) {
        this.toolWords = toolWords;
    }

    public ArrayList<String> getFutureWords() {
        return futureWords;
    }

    public void setFutureWords(ArrayList<String> futureWords) {
        this.futureWords = futureWords;
    }
}

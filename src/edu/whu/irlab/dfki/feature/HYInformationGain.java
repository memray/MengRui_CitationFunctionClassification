package edu.whu.irlab.dfki.feature;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;

public class HYInformationGain {

	/**
	 * 对于一个数据集，计算类别的信息增益
	 * 
	 * @param dataset
	 * @return
	 */
	public double calEntropy(List<List<String>> dataset) {
		int length = dataset.size();
		// System.out.println("length:" + length);
		// System.out.println("width:" + width);
		Map<String, Integer> clasmap = new HashMap<String, Integer>();
		for (int i = 0; i < length; i++) {
			// 对于这个实验 SVM的label在第一列
			String label = dataset.get(i).get(0);
			if (clasmap.containsKey(label)) {
				clasmap.put(label, clasmap.get(label) + 1);
			} else {
				clasmap.put(label, 1);
			}
		}
		double shannonEnt = 0.0;
		for (String label : clasmap.keySet()) {
			double plabel = clasmap.get(label) / (double) length;
			// System.out.println(label + ":" + plabel);
			shannonEnt -= plabel * (Math.log(plabel) / Math.log(2));
		}

		return shannonEnt;
	}

	/**
	 * 对于数据集中的一个特征，根据其每一个值进行分割数据
	 * 
	 * @param dataset
	 * @param axis
	 * @param value
	 * @return
	 */
	public List<List<String>> splitDataSet(List<List<String>> dataset,
			int axis, String value) {
		// 对于某一维特征，如果等于value，保留，返回
		List<List<String>> list = new ArrayList<List<String>>();
		for (List<String> feature : dataset) {
			if (feature.get(axis).equals(value)) {
				List<String> fl = new ArrayList<String>();
				fl.addAll(feature);
				fl.remove(axis);
				list.add(fl);
			}
		}
		return list;
	}

	public Map<Integer, Double> chooseBestFeatureToSplit(
			List<List<String>> dataset) {
		// 后面的n-1维是特征，第一维是label
		int length = dataset.get(0).size();
		// 计算数据集的熵
		double baseEntropy = calEntropy(dataset);
		// 使用一个哈希表来存储每一个特征的信息增益，然后选取特征
		Map<Integer, Double> igmap = new HashMap<Integer, Double>();
		Set<String> featureset = new HashSet<String>();
		// 特征集合
		// 对于每一个特征，获得该特征的值的集合
		// 对于SVM 特征在1~(length-1)的index中
		for (int i = 1; i < length; i++) {
			featureset.clear();
			for (List<String> feature : dataset) {
				featureset.add(feature.get(i));
			}
			double newEntropy = 0.0;
			for (String value : featureset) {
				// 对于该特征的所有值，计算其信息熵
				List<List<String>> vfdata = splitDataSet(dataset, i, value);
				// 计算该子集的信息增益
				double prob = vfdata.size() / (double) dataset.size();
				newEntropy += prob * calEntropy(vfdata);
			}
			// 计算信息增益
			double ig = baseEntropy - newEntropy;
			igmap.put(i, ig);
		}
		// System.out.println("choose feature complete");
		return igmap;
	}

	public Set<Integer> sortmap(Map<Integer, Double> map, int n) {
		Set<Integer> set = new HashSet<Integer>();
		// 这里将map.entrySet()转换成list
		List<Map.Entry<Integer, Double>> list = new ArrayList<Map.Entry<Integer, Double>>(
				map.entrySet());
		// 然后通过比较器来实现排序
		Collections.sort(list, new Comparator<Map.Entry<Integer, Double>>() {
			// 升序排序
			public int compare(Entry<Integer, Double> o1,
					Entry<Integer, Double> o2) {
				return o1.getValue() - o2.getValue() > 0 ? -1 : 1;
			}

		});

		for (int i = 0; i < n; i++) {
			Map.Entry<Integer, Double> mapping = list.get(i);
			set.add(mapping.getKey());
		}
		// System.out.println("sort map complete");
		return set;
	}

	public void print(List<List<String>> features, int n) {
		Set<Integer> indexes = sortmap(chooseBestFeatureToSplit(features), n);
		// System.out.println("最終長度：" + indexes.size());
		for (List<String> feature : features) {
			List<String> newlist = new ArrayList<String>();
			// System.out.println(feature.get(0));
			for (int i : indexes) {
				newlist.add(feature.get(i));
			}

			/*System.out.println(feature.get(0) + " "
					+ Assembler.generateString(newlist, true));*/
		}
	}

	public List<List<String>> loadData(String path, int length)
			throws IOException {
		List<String> lines = FileUtils.readLines(new File(path));
		List<List<String>> list = new ArrayList<List<String>>();
		for (String line : lines) {
			List<String> features = new ArrayList<String>();

			for (int i = 0; i < length + 1; i++) {
				//TODO features.add(Feature.SPARSE_TAG);
			}
			String[] fs = line.split(" ");
			features.set(0, fs[0]);
			for (int i = 1; i < fs.length; i++) {
				String[] sps = fs[i].split(":");
				int index = Integer.parseInt(sps[0]);
				features.set(index, sps[1]);
			}
			list.add(features);
		}
		// System.out.println("loading data complete");
		return list;
	}

	public static void main(String[] args) throws NumberFormatException,
			IOException {
		if (args.length != 3) {
			System.out.println("第一个参数特征文件，第二个特征是特征的维度，第三个是保留的比例");
			return;
		}
		HYInformationGain ig = new HYInformationGain();
		ig.print(ig.loadData(args[0], Integer.parseInt(args[1])),
				(int) (Float.parseFloat(args[2]) * Integer.parseInt(args[1])));
	}
}

package edu.whu.irlab.dfki.feature;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.TypedDependency;
import edu.whu.irlab.bean.*;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.data.etc.ContentTagger;
import edu.whu.irlab.dfki.feature.utils.GrammarUtils;
import edu.whu.irlab.dfki.feature.utils.OpinionFinder;
import edu.whu.irlab.dfki.feature.utils.PorterStemmer;
import edu.whu.irlab.dfki.feature.utils.WordlistCacheUtils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 由于FeatureExtractor中内容过多，将抽取特征的子函数写在这里，方便测试
 * Created by Memray on 3/3/2015.
 */
public class FeatureExtractFunctions {


    /**
     * 进行一部分初始化处理，如去除et al.等特殊表达式
     *
     * @param dp
     */
    public static void initDataPoint(DataPoint dp) {
        dp.setSentence(dp.getSentence().replace("et al.", ""));
        dp.setSentence(dp.getSentence().replaceAll(",|;|:", " $0 "));

    }

    /**
     * 从数据库中读取该条数据的具体物理信息，包括原文中的句子内容，前后句内容，章节信息等
     * @param d
     */
    public static void readPhysicalDataFromDB(DataPoint d) {
        // 循环，对于每一条记录在数据库寻找对应的语句
        String[] aclId = d.getAclid().split("_");
        //从数据库中获取到文章的其他相关原信息并赋值（主要为了判断是否是自引）
        d.setArticle(AclDao.getArticle("P" + aclId[0]));
        List<AclSentence> sentenceList = AclDao.getSentences("P" + aclId[0]);
        //通过Jaccard系数找到最相近的句子
        double maxJaccard = 0.0;
        d.setSentenceNumber(sentenceList.size());
        for (AclSentence sentence : sentenceList) {
            String[] dfkiWords = d.getSentence().split(" ");
            String[] aclWords = sentence.getContent().split(" ");
            HashSet<String> dfkiSet = new HashSet<String>(
                    Arrays.asList(dfkiWords));
            HashSet<String> aclSet = new HashSet<String>(
                    Arrays.asList(aclWords));
            double jaccardCoefficient = FeatureExtractUtils.JaccardCoefficient(
                    dfkiSet, aclSet);
            if (jaccardCoefficient > maxJaccard) {
                maxJaccard = jaccardCoefficient;
                d.setAclSentence(sentence);
                //获取句号
                d.setSentenceIndex(sentenceList.indexOf(sentence));
            }
        }
        //找到对应的句子，并加上前一句和后一句
        int currentIndex = sentenceList.indexOf(d.getAclSentence());

        float paperloc = (currentIndex + 1) * (float) 1.0 / sentenceList.size();


        String neighborSentences = "";
        int neighborSentenceNumber = 0;
        if ((currentIndex - 1 != 0)
                && (sentenceList.get(currentIndex - 1).getIs_caption() == 0)) {
            neighborSentences += sentenceList.get(currentIndex - 1)
                    .getContent();
            d.setFormerNeighborSentence(sentenceList.get(currentIndex - 1)
                    .getContent());
            neighborSentenceNumber++;
        }
        if ((currentIndex + 1 != sentenceList.size())
                && (sentenceList.get(currentIndex + 1).getIs_caption() == 0)) {
            neighborSentences += sentenceList.get(currentIndex + 1)
                    .getContent();
            d.setNextNeighborSentence(sentenceList.get(currentIndex + 1)
                    .getContent());
            neighborSentenceNumber++;
        }
        // 找section位置
        for (int k = currentIndex; k >= 0; k--) {
            if (sentenceList.get(k).getIs_caption() == 1
                    && !sentenceList.get(k).getCaption_number().equals("")
                    && !sentenceList.get(k).getCaption_number()
                    .contains(".")) {
                d.setSectionName(sentenceList.get(k).getContent());
                d.setSectionName(
                        sentenceList.get(k).getContent());
                d.setSectionNumberString(
                        sentenceList.get(k).getCaption_number());
                break;
            }
        }
    }

    /**
     * 将本文的部分代表作者主语的词替换为一个统一标记，统一替换为<subject>
     */
    public static void authorTagReplacement(DataPoint dp) {

        dp.setSentence(ContentTagger.addAuthorTag(dp.getSentence()));

    }

    /**
     * 将本文的引文替换为一个统一标记，其中要根据引文标记的上下文postag来判断引文标记的替换情况
     * 如果该引文在句子中有作用则为<citation>，否则是将标记与前一个词合并，形式为word_citation
     * 注意在许多地方需要把word_citation还原为word，不然程序无法识别为同一个词或者stemmer后效果诡异
     */
    public static String citationTagReplacement(String sentence) {
        /*
         * 本来是想比对句子中的词和其词性，来判断添加哪种citation标记，但是两边对不上，只能通过Stanford Postagger来自己处理了
         */
        //把原引文标记"( )"标记替换为<citation>
        sentence = ContentTagger.addCitationTag(sentence, "<citation>");
        //把标点（,）的前后增加空格
        sentence = sentence.replace(",", " , ");
        //先进行postag抽取
        List<TaggedWord> taggedWords = GrammarUtils.getTaggedWord(sentence);
        //进行postag
        /*for (TaggedWord tw : taggedSent) {
            System.out.println(tw.toString());
        }*/
        //根据<citation>前面一个词的postag来判断这个引文标记在句子中是否有作用
        int i = 0;
        while (i < taggedWords.size()) {
            TaggedWord taggedWord = taggedWords.get(i);
            if (!taggedWord.word().contains("<citation>")) {
                i++;
                continue;
            }
            /*
             * 以下情况的引文标记在句子中有作用，保留<citation>标记
             */
            //在句子头上
            if (i == 0) {
                i++;
                continue;
            }
            //前面一个词是介词，词性为IN或者TO
            if (taggedWords.get(i - 1).tag().equals("IN") || taggedWords.get(i - 1).tag().equals("TO")) {
                i++;
                continue;
            }
            //前一个词是动词且是动词主动时态（是VB但是非VBN）
            if (taggedWords.get(i - 1).tag().startsWith("VB") && !taggedWords.get(i - 1).tag().equals("VBN")) {
                i++;
                continue;
            }
            //前一个词是连词如While、However（CC连词）
            if (taggedWords.get(i - 1).tag().equals("CC")) {
                i++;
                continue;
            }

            //前面是标点符号
            if (taggedWords.get(i - 1).tag().length() == 1 && !Character.isLetter(taggedWords.get(i - 1).tag().charAt(0))) {
                i++;
                continue;
            }
            //前面是e.g., cf.
            if (taggedWords.get(i - 1).word().trim().equals("e.g.") || taggedWords.get(i - 1).word().trim().equals("cf.")) {
                i++;
                continue;
            }
            /*
             * 否则将citation拼到前一个词上（citation_word），并删除该citation标记
             */
            taggedWords.get(i - 1).setWord(taggedWords.get(i - 1).word() + "_citation");
            taggedWords.remove(i);
        }

        StringBuffer stringBuffer = new StringBuffer();
        taggedWords.forEach(s -> stringBuffer.append(s.word() + " "));
//        System.out.println(dp.getSentence());
//        System.out.println(stringBuffer);
//        System.out.println();
        return stringBuffer.toString();
    }

    /**
     * 判断该word是否包含_citation后缀，有的话则删除掉
     *
     * @param word
     * @return
     */
    public static String citationTagRemoval(String word) {
        if (word.endsWith("_citation"))
            return word.substring(0, word.length() - 9);
        else if(word.endsWith("_cit"))
            return word.substring(0, word.length() - 4);
        else if(word.endsWith("_citat"))
            return word.substring(0, word.length() - 6);
        else
            return word;
    }

    /**
     * Arthar特征部分
     */
    //全局n-gram 词表
    static public HashSet<String> GlobalNgram = new HashSet<String>();

    /**
     * Athar 特征部分
     */
    public static void extractAtharFeatures(DataPoint dp) {
        String sentence = dp.getSentence().trim();
        /*
         * 特征1. n-gram, Jochim使用了1-gram
         */
        dp.getFeature().setnGram(GrammarUtils.getNgramWords(sentence, 1, RunFeatureExtract.Lemmatization));
        /*
         * 特征2. Dependency Relations 语法依存特征
         */
        // Stanford 语法关系分析, 将依存关系保存到TypedDependencies，将统计数量保存到DependencyCount中
        dp.getFeature().setDependencyRelations(GrammarUtils
                .getDependencyRelations(dp, true));
    }


    /**
     * Teufel et al. (2006) 特征部分
     */
    public static void extractTeufelFeatures(DataPoint dp) {
        /**
         * 1. 是否是自引。通过把d.article.Author中的姓在句子中查找,只判断第一作者即可
         * Author形式如下Deng, Yonggang; Gao, Yuqing
         * 注意这里要在数据库的原句中寻找，因为Dong的数据中已经对句子的引文内容进行了剔除
         */
        dp.getFeature().setSelfCitationBoolean(FeatureExtractFunctions.isSelfCiting(dp));

        String sentence = dp.getSentence().trim();
        CitationFeatureTemplate ft = dp.getFeature();
        //treemap顺序是固定的，这样方便通过循环的方式来生成一个列表
        /**
         * 2. Phrase 词表
         * CONC CONF EVOL JUX NEG OP ORG PERF的顺序
         */

        TreeMap<String, List<String>> cuePhrases = WordlistCacheUtils.cuePhrasesMap;
        List<Integer> cuePhrasesSizeList = new ArrayList<Integer>();
        for (String cuekey : cuePhrases.keySet()) {
            cuePhrasesSizeList.add(wordExtract(sentence, cuePhrases.get(cuekey)).size());
        }
        ft.setConceptualCueList(cuePhrasesSizeList);
        //大于零，说明存在
        // has conceptual cue
        if (cuePhrasesSizeList.get(0) > 0)
            ft.setHasConceptualCue(true);
        // has operational
        if (cuePhrasesSizeList.get(5) > 0)
            ft.setHasOperational(true);
        // has evolutionary
        if (cuePhrasesSizeList.get(2) > 0)
            ft.setHasEvolutionary(true);
        // has juxtapositional
        if (cuePhrasesSizeList.get(3) > 0)
            ft.setHasJuxtapositional(true);
        // has organic
        if (cuePhrasesSizeList.get(6) > 0)
            ft.setHasOrganic(true);
        // has perfunctory
        if (cuePhrasesSizeList.get(7) > 0)
            ft.setHasPerfunctory(true);
        // has confirmative
        if (cuePhrasesSizeList.get(1) > 0)
            ft.setHasConfirmative(true);
        // has negational <has_neg_cue>
        if (cuePhrasesSizeList.get(4) > 0)
            ft.setHasNegational(true);
        /**
         * 3. tense 时态
          */
        dp.getFeature().setTense(GrammarUtils.extractTense(dp));
        /**
         * 4. voice 语态（主动/被动）<active>  判断typeofdependency,是否存在 auxpass (被动语态)
         */

        dp.getFeature().setHasVoice(GrammarUtils.findDependency(dp, "auxpass"));

        /**
         * 5. has modality, binary modal/no modal <modal>
         *     modal verb or None would/can等
         *     情态动词 can may should
         */
        dp.getFeature().setModalWords(GrammarUtils.getSpecificPOSWords("MD"));

        //6. paper location 四分 在物理特征部分抽取 sentenceLocation


    }

    /**
     * Abu-Jbara特征部分
     */
    // 全局依存关系表，保存所有词汇间的依存关系，如 nsubj(outperform, algorithm)
    static public HashSet<String> GlobalDependencies = new HashSet<String>();
    //(只有Abu方案需要)Abu中需要遍历全部数据得到的全局词表，需要的是最近的距离引文最近的verb/Adjective/Adverb
    static public ArrayList<String> GlobalAbuWords = new ArrayList<String>();
    // 第5个特征：第一人称和第三人称代词
    static List<String> firstPersonPrononeFilter = Arrays.asList("I", "me", "mine", "we", "us", "ours", "myself", "ourselves");
    static List<String> thirdPersonPrononeFilter = Arrays.asList(
            "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "they", "them", "theirs");
    // 第6个特征：Negation词表，分为前缀词、后缀、整词和Phrase
    static public HashSet<String> negationWordFilter = new HashSet<String>() {{
        add("no");
        add("rather");
        add("absence");
        add("none");
        add("without");
        add("nor");
        add("prevent");
        add("not");
        add("nowhere");
        add("contrary");
        add("nobody");
        add("refused");
        add("nothing");
        add("less");
        add("never");
        add("fail");
        add("neither");
        add("except");
        add("reverse");
    }};
    static public HashSet<String> negationPrefixFilter = new HashSet<String>() {{
        add("dis");
        add("un");
        add("im");

    }};
    static public HashSet<String> negationSuffixFilter = new HashSet<String>() {{
        add("n't");
    }};
    static public HashSet<String> negationPhraseFilter = new HashSet<String>() {{
        add("other than");
        add("rather than");
    }};

    /*
     *  第9个特征： Contrary Expression的词表， 内容在Biber附录的239页
     *  这两个没有实现，ALL-P代词，T# tone unit boundary
     *  ALL-P/T# + that is/else/altogether + T#
     *  ALL-P/T# + rather + T#/,/ xxx (xxx is NOT:ADJ/ADV)
     */
    static List<String> contraryExpressionFilter = Arrays.asList("however", "nevertheless", "nonetheless", "notwithstanding", "otherwise", "rather",
            "in comparison", "in contrast", "by contrast", "by comparison", "on the contrary", "on the other hand");

    /**
     * Abu-Jbara 特征
     * 以下特征取自其他地方
     * 1 上下文中引文数(Dong&Schäfer),2 引文句中是否只有一个引文(Dong&Schäfer),
     * 4 是否是自引(Teufel),10 所在章节的类别(Dong&Schäfer),11 句法依存关系(Arthar 2012)
     */
    public static void extractAbuJbaraFeatures(DataPoint dp) {
        CitationFeatureTemplate ft = dp.getFeature();
        String sentence = dp.getSentence().toLowerCase();
        /*
         * 特征3. 找距离引文标记最近的 动词、形容词、副词
         */
        String[] abuPosTags = {"VB", "JJ", "RB"};
        ft.setCitationLinkedKeyAbuWords(GrammarUtils.getLinkedKeyWordsOfCertainEntity(dp.getTaggedWords(), dp.getTypedDependencies(), GrammarUtils.EntityType.Citation, abuPosTags, 1, RunFeatureExtract.Lemmatization));

       /* ft.setCitationLinkedKeyAbuWords(GrammarUtils.getLinkedKeyWordsWithEntity(dp.tree,
                GrammarUtils.EntityType.Citation, abuPosTags, 1, true));*/
        /*
         * 特征5. 是否包含 1st/3rd Person Pronoun
         */
        ft.firstPersonPrononeWords = wordExtract(sentence, firstPersonPrononeFilter);
        if (ft.firstPersonPrononeWords.size() != 0) {
            ft.setFirstPersonPrononeBoolean(true);
        }
        ft.thirdPersonPrononeWords = wordExtract(sentence, thirdPersonPrononeFilter);
        if (ft.thirdPersonPrononeWords.size() != 0) {
            ft.setThirdPersonPrononeBoolean(true);
        }
        /*
         * 特征6. Negation Cue 通过词表获取  出现与否
         * 这个真是不太靠谱，distance之类的词也会被匹配到
         * negationWordFilter, negationPrefixFilter, negationSuffixFilter, negationPhraseFilter
         */
        for (String word : dp.getSentence().split("\\s")) {
            word = word.trim().toLowerCase();
            if (negationWordFilter.contains(word))
                ft.setNegationCueBoolean(true);
            for (String prefix : negationPrefixFilter)
                if (word.startsWith(prefix))
                    ft.setNegationCueBoolean(true);
            for (String suffix : negationPrefixFilter)
                if (word.endsWith(suffix))
                    ft.setNegationCueBoolean(true);
        }
        for (String phrase : negationPhraseFilter) {
            if (dp.getSentence().toLowerCase().contains(phrase))
                ft.setNegationCueBoolean(true);
        }

        /*
         * 特征7. Speculation Cue 通过词表获取  出现与否
         */
        ft.speculationCues = wordExtract(sentence,
                WordlistCacheUtils.abuSpeculationCueList);
        if (ft.speculationCues.size() != 0) {
            ft.setSpeculationBoolean(true);
            ft.setSpeculationWeight(ft.speculationCues.size());
        } else {
            ft.setSpeculationBoolean(false);
        }

        /*
         * 特征8. Closest Subjectivity Cue 通过 OpinionFinder 找到
         */
        if (FeatureExtractUtils.getClosestSubjectivityCue(sentence) != "")
            ft.setClosestSubjectivityAbuCue(FeatureExtractUtils.getClosestSubjectivityCue(sentence));
        /*
         * 特征9. Contrary Expressions 是否包含 from Biber (1988)
         */
        ft.contraryExpressionsWords = wordExtract(sentence,
                contraryExpressionFilter);
        if (ft.contraryExpressionsWords.size() != 0) {
            ft.setContraryExpressionsBoolean(true);
        } else {
            ft.setContraryExpressionsBoolean(false);
        }
    }

    /**
     * Ulrich特征部分
     */
    // Ulrich特征词表
    static List<String> quantityFilter = Arrays.asList("many", "some", "most",
            "several", "number of", "numerous", "variety", "range of");
    static List<String> frequencyFilter = Arrays.asList("usually", "often",
            "common", "commonly", "typical", "typically", "traditional",
            "traditionally");
    static List<String> tenseFilter = Arrays.asList("early", "previous",
            "prior", "recent", "recently");
    static List<String> exampleFilter = Arrays.asList("such as", "example",
            "for instance", "e.g.");
    static List<String> suggestFilter = Arrays.asList("may", "might", "could",
            "can", "would", "will", "should");
    static List<String> hedgeFilter = Arrays.asList("suppose", "conjecture",
            "want", "possible");
    static List<String> ideaFilter = Arrays.asList("following", "similar to",
            "motivate", "inspired", "idea", "spirit");
    static List<String> basisFilter = Arrays.asList("provided by",
            "taken from", "extracted from", "based on", "use", "runFeatureSelectionOnGivenInstances", "apply",
            "extend", "measure", "evaluate", "modify", "extract");
    static List<String> compareFilter = Arrays.asList("compar", "differ",
            "deviate", "contrast", "exceed", "outperform", "opposed",
            "consistent with", "significant");
    static List<String> resultFilter = Arrays.asList("result", "accuracy",
            "precision", "performance", "baseline");
    static List<String> subjectFilter = Arrays.asList("we", "our", "us",
            "table", "figure", "paper", "algorithm", "here");

    // Ulrich特征句法匹配表
    static String syntactic_regexs_1 = ".*\\( \\) VV[DPZN].*";
    static String syntactic_regexs_2 = ".*(VHP|VHZ) VV.*";
    static String syntactic_regexs_3 = ".*VH(D|G|N|P|Z) (RB )*VBN.*";
    static String syntactic_regexs_4 = ".*MD (RB )*VB(RB )* VVN.*";
    static String syntactic_regexs_5 = "[^IW.]*VB(D|P|Z) (RB )*VV[ND].*";
    static String syntactic_regexs_6 = "(RB )*PP (RB )*V.*";
    static String syntactic_regexs_7 = ".*VVG (NP )*(CC )*\\( \\).*";

    /**
     * Ulrich特征部分
     */
    public static void extractUlrichFeatures(DataPoint dp) {
        CitationFeatureTemplate ft = dp.getFeature();
        String sentence = dp.getSentence().trim().toLowerCase();
        /*
         * 第一部分：Textual Features 词表特征
	     */
        // 主语cue(we,our,us,table..)
        ft.subjectWords = wordExtract(sentence, subjectFilter);
        if (ft.subjectWords.size() != 0) {
            ft.setSubjectBoolean(true);
            ft.setSubjectWeight(ft.subjectWords.size());
        } else {
            ft.setSubjectBoolean(false);
        }
        // Quantity特征(many,some,most..)
        ft.quantityWords = wordExtract(sentence, quantityFilter);
        if (ft.quantityWords.size() != 0) {
            ft.setQuantityBoolean(true);
            ft.setQuantityWeight(ft.quantityWords.size());
        } else {
            ft.setQuantityBoolean(false);
        }
        // Frequency特征(usually,often..)
        ft.frequencyWords = wordExtract(sentence, frequencyFilter);
        if (ft.frequencyWords.size() != 0) {
            ft.setFrequencyBoolean(true);
            ft.setFrequencyWeight(ft.frequencyWords.size());
        } else {
            ft.setFrequencyBoolean(false);
        }
        // Tense特征(recent,prior,previous..)
        ft.tenseWords = wordExtract(sentence, tenseFilter);
        if (ft.tenseWords.size() != 0) {
            ft.setTenseBoolean(true);
            ft.setTenseWeight(ft.tenseWords.size());
        } else {
            ft.setTenseBoolean(false);
        }
        // Example特征(such as ,e.g.)
        ft.exampleWords = wordExtract(sentence, exampleFilter);
        if (ft.exampleWords.size() != 0) {
            ft.setExampleBoolean(true);
            ft.setExampleWeight(ft.exampleWords.size());
        } else {
            ft.setExampleBoolean(false);
        }
        // Suggest特征(may,could,can,would..)
        ft.suggestWords = wordExtract(sentence, suggestFilter);
        if (ft.suggestWords.size() != 0) {
            ft.setSuggestBoolean(true);
            ft.setSuggestWeight(ft.suggestWords.size());
        } else {
            ft.setSuggestBoolean(false);
        }
        // Hedge特征(suppose,conjecture,want,possible..)
        ft.hedgeWords = wordExtract(sentence, hedgeFilter);
        if (ft.hedgeWords.size() != 0) {
            ft.setHedgeBoolean(true);
            ft.setHedgeWeight(ft.hedgeWords.size());
        } else {
            ft.setHedgeBoolean(false);
        }
        // Idea特征(following,similar to, motivate,inspired..)
        ft.ideaWords = wordExtract(sentence, ideaFilter);
        if (ft.ideaWords.size() != 0) {
            ft.setIdeaBoolean(true);
            ft.setIdeaWeight(ft.ideaWords.size());
        } else {
            ft.setIdeaBoolean(false);
        }
        // Basis特征(use,provided by,take from..)
        ft.basisWords = wordExtract(sentence, basisFilter);
        if (ft.basisWords.size() != 0) {
            ft.setBasisBoolean(true);
            ft.setBasisWeight(ft.basisWords.size());
        } else {
            ft.setBasisBoolean(false);
        }
        // Compare特征(compare,differ,contrast..)
        ft.compareWords = wordExtract(sentence, compareFilter);
        if (ft.compareWords.size() != 0) {
            ft.setCompareBoolean(true);
            ft.setCompareWeight(ft.compareWords.size());
        } else {
            ft.setCompareBoolean(false);
        }
        // Result Cue(result,accuracy..)
        ft.resultWords = wordExtract(sentence, resultFilter);
        if (ft.resultWords.size() != 0) {
            ft.setResultBoolean(true);
            ft.setResultWeight(ft.resultWords.size());
        } else {
            ft.setResultBoolean(false);
        }
        // Neighbor subjects
        ft.neighborSubjectWords = wordExtract(dp.getFormerNeighborSentence(), subjectFilter);
        ft.neighborSubjectWords.addAll(wordExtract(dp.getNextNeighborSentence(), subjectFilter));
        if (ft.neighborSubjectWords.size() != 0) {
            ft.setNeighborSubjectBoolean(true);
            ft.setNeighborSubjectWeight(ft.neighborSubjectWords.size());
        } else {
            ft.setNeighborSubjectBoolean(false);
        }
        /*
         * Ulrich特征第二部分：Physical Features
         * 物理特征（位置、频率）,在physicalFeatureExtract()已提取
         */

        /*
         * Ulrich特征第三部分：Syntactic Features 句法匹配
         */
        List<String> regexs = Arrays.asList(syntactic_regexs_1,
                syntactic_regexs_2, syntactic_regexs_3, syntactic_regexs_4,
                syntactic_regexs_5, syntactic_regexs_6, syntactic_regexs_7);
        for (int i = 0; i < regexs.size(); i++) {
            String regex = regexs.get(i);
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(dp.getGrammar());
            if (m.find()) {
                ft.setSyntacticFeature(i + 1);
            }
        }

    }

    /**
     * reimplented features from Jochim & Schütze (2012) 42~59，ngram在6
     */
    public static void extractCharlesFeatures(DataPoint dp) {

        // is-constituent 根据句子中包含是word_citation的形式而不是完整的<citation>，则为true
        dp.getFeature().setConstituentBoolean(isConstituent(dp));
        // has other-contrast
        dp.getFeature().setOtherContrastBoolean((GrammarUtils.hasOtherContrast(dp)));
        // has self-comp
        dp.getFeature().setSelfCompBoolean((GrammarUtils.hasSelfComp(dp)));
        // has other-comp
        dp.getFeature().setOtherCompBoolean((GrammarUtils.hasOtherComp(dp)));
        // is self-good
        dp.getFeature().setSelfGoodBoolean((GrammarUtils.hasSelfGood(dp)));
        // has but
        dp.getFeature().setHasButBoolean(containsSpecificWord(dp.getSentence(), "\\bbut\\b"));
        // has cf.
        dp.getFeature().setHasCfBoolean(containsSpecificWord(dp.getSentence(), "cf\\."));

        /*
         * 比较级/最高级的词汇 has-comp./sup.
         * 获取识别到的比较级词汇特征，直接保存到对应词表comparativeWords中
         */
        ArrayList<String> ComparativeWords = GrammarUtils.getComparatives();
        if (ComparativeWords == null || ComparativeWords.isEmpty()) {
            dp.getFeature().setComparativeBoolean(false);
        } else {
            dp.getFeature().setComparativeBoolean(true);
            for (String c : ComparativeWords) {
                dp.getFeature().getComparativeWords().add(c);
            }
        }
        // has modal
        if (dp.getFeature().modalWords.size() > 0)
            dp.getFeature().setModalBoolean(true);
        // has-1stPRP，见Abu-jbara的特征5
        // has-3rdPRP
        // root 词汇
        dp.getFeature().setRootWord(GrammarUtils.findRootWord(dp, RunFeatureExtract.Lemmatization));
        // main verb
        dp.getFeature().setMainVerb(GrammarUtils.findMainVerb(dp, RunFeatureExtract.Lemmatization));

        // positive-words
        dp.getFeature().setPositiveWords(OpinionFinder.extractPositiveWords(dp));
        // negative-words
        dp.getFeature().setNegativeWords(OpinionFinder.extractNegativeWords(dp));

        // has_resource TODO
        // has_tool TODO
        // sentence loc.: 1st qtr, middle half, last qtr 引文记号在句子中的位置，分为(0~0.25,0.25~0.75,0.75~1)（句子中有多个引文时没法用）
        FeatureExtractUtils.getSentenceLoc(dp);//这里似乎有点问题，因为这里 已经把引文标记替换了，所以要在引文标记替换前计算，引文在句子中位置
        /*
         *Reference features: 73~77
         */

        // reference age  文章的发行时间减去引文的时间
        Integer citationyear = FeatureExtractUtils.getAclSentenceYear(dp.getAclSentence().getContent());
        dp.getFeature().setReferenceAge(
                Integer.valueOf(dp.getArticle().getYear()) - FeatureExtractUtils.getAclSentenceYear(dp.getAclSentence().getContent())
        );
        if (citationyear == 0)
            dp.getFeature().setReferenceAge(0);//说明没匹配到 差异就为0
        else {
            dp.getFeature().setReferenceAge(
                    //施引方-被引方
                    Integer.valueOf(dp.getArticle().getYear()) - citationyear
            );
        }
        // number of authors
        dp.getFeature().setAuthorNumber(FeatureExtractUtils.getNumberOfAuthors(dp.getArticle().getAuthor()));

        // publication keywords (taken from name of publication [journal, proceedings] or from title [book, other] TODO
        // self cite (also used by Teufel)
        // publication type

    }

    /**
     * Teufel特征
     * 判断是否是自引。通过把d.article.Author中的姓在句子中查找,只判断第一作者即可
     * Author形式如下Deng, Yonggang; Gao, Yuqing
     * 注意这里要在数据库的原句中寻找，因为Dong的数据中已经对句子的引文内容进行了剔除
     */
    public static boolean isSelfCiting(DataPoint dp) {
        String[] authors = dp.getArticle().getAuthor().split(";");
        String author = authors[0];
        if (author.contains(","))
            author = author.split(",")[0].trim();
        author = "\\b" + author + "\\b";
        Pattern pattern = Pattern.compile(author);
        Matcher matcher = pattern.matcher(dp.getAclSentence().getContent());
        if (matcher.find()) {
//            System.out.println(dp.getAclSentence().getContent());
//            System.out.println(author);
            return true;
        }
        return false;
    }

    /**
     * Charles特征
     * 判断该citation在句子中是否扮演成分
     * 在预处理中有对citation标记进行替换，如果保留完整的<citation>则表明扮演成分，如果是word_citation则表明不担任句子成分
     * @param dp
     * @return
     */
    public static boolean isConstituent(DataPoint dp) {
        if (dp.getSentence().indexOf("_citation") != -1)
            return false;
        return true;
    }

    /**
     * 我们的特征抽取辅助部分
     */
    // 全局词表，key为词的形式，value为其id
    static public Hashtable<String, Integer> GlobalWordTable = new Hashtable<String, Integer>();

    public static PorterStemmer stemmer = new PorterStemmer();
    public static List<String> ourSubjectFilter = Arrays.asList("we", "our", "us", "ours", "this paper", "this work", "this study");
    static List<String> ourIdeaFilter = Arrays.asList("follow", "same",
            "similar to", "motivat", "inspir", "idea", "spirit");
    static List<String> ourBasisFilter = Arrays.asList("provided by",
            "taken from", "extracted from", "based on", "use", "runFeatureSelectionOnGivenInstances", "appl",
            "extend", "measur", "evaluat", "modify", "extract");
    static List<String> ourCompareFilter = Arrays.asList("compar", "differ",
            "contrast", "comparison", "equal", "exceed", "outperform", "oppos", "consistent with",
            "signific", "golden standard", "than", "unlike");
    static List<String> ourResultFilter = Arrays.asList("result", "accuracy",
            "precision", "performance", "baseline", "experiment");
    /*
     下面的几个our词表和Ulrich的几乎没区别
     */
    static List<String> ourQuantityFilter = Arrays.asList("many", "some",
            "most", "several", "number of", "numerous", "variety", "range of");
    static List<String> ourFrequencyFilter = Arrays.asList("usually", "often",
            "common", "commonly", "typical", "typically", "traditional",
            "traditionally");
    static List<String> ourTenseFilter = Arrays.asList("early", "previous",
            "prior", "recent", "recently");
    static List<String> ourExampleFilter = Arrays.asList("such as", "example",
            "for instance", "e.g.");
    static List<String> ourSuggestFilter = Arrays.asList("may", "might",
            "could", "can", "would", "will", "should");
    static List<String> ourHedgeFilter = Arrays.asList("suppose", "conjecture",
            "want", "possible");

    static List<String> objectFilter = Arrays.asList("method", "approach",
            "algorithm", "model", "concept", "equation", "scheme");
    static List<String> toolFilter = Arrays.asList("corpus", "corpora",
            "dataset", "tool", "data", "system", "toolkit", "dev set");
    static List<String> futureFilter = Arrays.asList("future", "envision",
            "next step", "possibly", "possible", "maybe", "perhaps",
            "potential");

    // 正则表达式匹配 数字，注意要排除百分号和小数
    static String number_regex = "\\s\\d{2,}\\s|\\d{2,}(\\.\\d+)?(?!%)$";
    // 正则表达式匹配 百分数和小数
    static String percentage_regex = "\\d+\\.?\\d+%|0\\.\\d+";

    /**
     * 我们添加的特征部分
     */
    public static void extractOurFeatures(DataPoint dp) {
        CitationFeatureTemplate ft = dp.getFeature();
        // 这里使用未替换标记的句子，否则很多主语无法找到
        String sentence = dp.getOriginSentence().trim().toLowerCase();
        /*
         * 第一部分，改进布尔特征及数值特征
         */
        // 我们的特征主语cue(we,our,us,this..)
        ft.ourSubjectWords = wordExtract(sentence, ourSubjectFilter);
        if (ft.ourSubjectWords.size() != 0) {
            ft.setOurSubjectBoolean(true);
            ft.setOurSubjectWeight(ft.ourSubjectWords.size());
        } else {
            ft.setOurSubjectBoolean(false);
        }
        // OurIdea特征(following,similar to, motivate,inspired..)
        ft.ourIdeaWords = wordExtract(sentence, ourIdeaFilter);
        if (ft.ourIdeaWords.size() != 0) {
            ft.setOurIdeaBoolean(true);
            ft.setOurIdeaWeight(ft.ourIdeaWords.size());
        } else {
            ft.setOurIdeaBoolean(false);
        }
        // OurBasis特征(method,approach,model...)
        ft.ourBasisWords = wordExtract(sentence, ourBasisFilter);
        if (ft.ourBasisWords.size() != 0) {
            ft.setOurBasisBoolean(true);
            ft.setOurBasisWeight(ft.ourBasisWords.size());
        } else {
            ft.setOurBasisBoolean(false);
        }
        // OurCompare特征
        ft.ourCompareWords = wordExtract(sentence, ourCompareFilter);
        if (ft.ourCompareWords.size() != 0) {
            ft.setOurCompareBoolean(true);
            ft.setOurCompareWeight(ft.ourCompareWords.size());
        } else {
            ft.setOurCompareBoolean(false);
        }
        // OurResult特征
        ft.ourResultWords = wordExtract(sentence, ourResultFilter);
        if (ft.ourResultWords.size() != 0) {
            ft.setOurResultBoolean(true);
            ft.setOurResultWeight(ft.ourResultWords.size());
        } else {
            ft.setOurResultBoolean(false);
        }

        // OurQuantity特征
        ft.ourQuantityWords = wordExtract(sentence, ourQuantityFilter);
        if (ft.ourQuantityWords.size() != 0) {
            ft.setOurQuantityBoolean(true);
            ft.setOurQuantityWeight(ft.ourQuantityWords.size());
        } else {
            ft.setOurQuantityBoolean(false);
        }
        // OurFrequency特征
        ft.ourFrequencyWords = wordExtract(sentence, ourFrequencyFilter);
        if (ft.ourFrequencyWords.size() != 0) {
            ft.setOurFrequencyBoolean(true);
            ft.setOurFrequencyWeight(ft.ourFrequencyWords.size());
        } else {
            ft.setOurFrequencyBoolean(false);
        }
        // OurTense特征
        ft.ourTenseWords = wordExtract(sentence, ourTenseFilter);
        if (ft.ourTenseWords.size() != 0) {
            ft.setOurTenseBoolean(true);
            ft.setOurTenseWeight(ft.ourTenseWords.size());
        } else {
            ft.setOurTenseBoolean(false);
        }
        // OurExample特征
        ft.ourExampleWords = wordExtract(sentence, ourExampleFilter);
        if (ft.ourExampleWords.size() != 0) {
            ft.setOurExampleBoolean(true);
            ft.setOurExampleWeight(ft.ourExampleWords.size());
        } else {
            ft.setOurExampleBoolean(false);
        }
        // OurSuggest特征
        ft.ourSuggestWords = wordExtract(sentence, ourSuggestFilter);
        if (ft.ourSuggestWords.size() != 0) {
            ft.setOurSuggestBoolean(true);
            ft.setOurSuggestWeight(ft.ourSuggestWords.size());
        } else {
            ft.setOurSuggestBoolean(false);
        }
        // OurHedge特征
        ft.ourHedgeWords = wordExtract(sentence, ourHedgeFilter);
        if (ft.ourHedgeWords.size() != 0) {
            ft.setOurHedgeBoolean(true);
            ft.setOurHedgeWeight(ft.ourHedgeWords.size());
        } else {
            ft.setOurHedgeBoolean(false);
        }

        // 对象cue
        ft.objectWords = wordExtract(sentence, objectFilter);
        if (ft.objectWords.size() != 0) {
            ft.setObjectBoolean(true);
            ft.setObjectWeight(ft.objectWords.size());
        } else {
            ft.setObjectBoolean(false);
        }
        // 工具、数据特征cue
        ft.toolWords = wordExtract(sentence, toolFilter);
        if (ft.toolWords.size() != 0) {
            ft.setToolBoolean(true);
            ft.setToolWeight(ft.toolWords.size());
        } else {
            ft.setToolBoolean(false);
        }
        // 针对未来工作的future cue
        ft.futureWords = wordExtract(sentence, futureFilter);
        if (ft.futureWords.size() != 0) {
            ft.setFutureBoolean(true);
            ft.setFutureWeight(ft.futureWords.size());
        } else {
            ft.setFutureBoolean(false);
        }

        // 是否有大额的数字（多于一位）以及对应的次数
        Pattern p = Pattern.compile(number_regex);
        Matcher m = p.matcher(sentence);
        int k = 0;
        while (m.find()) {
            k++;
        }
        ft.setNumberCount(k);
        if (k != 0) {
            ft.setNumberBoolean(true);
        }

        // 是否有 百分比以及对应的次数
        p = Pattern.compile(percentage_regex);
        m = p.matcher(sentence);
        k = 0;
        while (m.find()) {
            k++;
        }
        ft.setPercentageCount(k);
        if (k != 0) {
            ft.setPercentageBoolean(true);
        }

        /*
         * 第二部分，句子中各种词汇的特征
         */

        /**
         * 与主语和citation相连的动词、形容词、副词(介词、小品词 考虑去除),加前缀以区分
         * TODO 是不是应该缩小，介词和小品词应当与其动词及形容词合并
         */
//        String[] posTags = {"VB", "JJ", "RB", "IN", "RP", "NN"};
        String[] posTags = {"VB"};
        ArrayList<TaggedWord> subjectLinkedWords = GrammarUtils.getLinkedTaggedWordsOfCertainEntity(dp.getTaggedWords(), dp.getTypedDependencies(), GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization);
        ArrayList<TaggedWord> citationLinkedWords = GrammarUtils.getLinkedTaggedWordsOfCertainEntity(dp.getTaggedWords(), dp.getTypedDependencies(), GrammarUtils.EntityType.Citation, posTags, 2, RunFeatureExtract.Lemmatization);
        /**
         * 找到与subject和citation均相连的word
         */
        String tempWord;
        TaggedWord taggedWord;
        for(TaggedWord subjectLinkedWord : subjectLinkedWords)
            for(TaggedWord citationLinkedWord : citationLinkedWords)
                if(dp.getTaggedWords().indexOf(subjectLinkedWord)==dp.getTaggedWords().indexOf(citationLinkedWord)){
                    ft.setSubjectLinkedToCitationBoolean(true);
                    tempWord = subjectLinkedWord.word().toLowerCase();
                    taggedWord = subjectLinkedWord;
                    if(FeatureExtractFunctions.stemmer.stemWord(tempWord).equals("be")){
                        for (TypedDependency typedDependency : dp.getTypedDependencies())
                            if(typedDependency.reln().toString().equals("cop")&&typedDependency.dep().index()==dp.getTaggedWords().indexOf(subjectLinkedWord)){
                                tempWord = "be_"+typedDependency.gov().value().toLowerCase();
                                taggedWord = dp.getTaggedWords().get(typedDependency.gov().index());
                            }

                    }
                    if(GrammarUtils.isModifiedByNegation(dp.getTaggedWords(), dp.getTaggedWords().indexOf(taggedWord)))
                        tempWord = "not_"+tempWord;
                    if (RunFeatureExtract.Lemmatization)
                        ft.getSubjectCitationLinkedWords().add(FeatureExtractFunctions.stemmer.stemWord(tempWord));
                    else
                        ft.getSubjectCitationLinkedWords().add(tempWord);
                }
        posTags = new String[] {"VB", "JJ", "RB"};
        ft.setSubjectLinkedKeyWords(GrammarUtils.getLinkedKeyWordsOfCertainEntity(dp.getTaggedWords(), dp.getTypedDependencies(), GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization));
        ft.setCitationLinkedKeyWords(GrammarUtils.getLinkedKeyWordsOfCertainEntity(dp.getTaggedWords(), dp.getTypedDependencies(), GrammarUtils.EntityType.Citation, posTags, 2, RunFeatureExtract.Lemmatization));
        /*ft.setSubjectLinkedKeyWords(GrammarUtils.getLinkedKeyWordsWithEntity(dp.tree,
                GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization));
        ft.setCitationLinkedKeyWords(GrammarUtils.getLinkedKeyWordsWithEntity(dp.tree,
                GrammarUtils.EntityType.Citation, posTags, 2, RunFeatureExtract.Lemmatization));*/
        /*// 动词
        ft.setVerbWords(GrammarUtils.getSpecificPOSWords("VB"));
        // 形容词
        ft.setAdjectiveWords(GrammarUtils.getSpecificPOSWords("JJ"));
        // 副词
        ft.setAdverbWords(GrammarUtils.getSpecificPOSWords("RB"));
        // 限定词 all、an、the
        ft.setDeterminerWords(GrammarUtils.getSpecificPOSWords("DT"));
        // 存在词 there
        ft.setExistentialWords(GrammarUtils.getSpecificPOSWords("EX"));
        // 前位限定词 all both half many quite such sure this
        ft.setPredeterminerWords(GrammarUtils.getSpecificPOSWords("PDT"));
        // 人称代词
        ft.setPrononeWords(GrammarUtils.getSpecificPOSWords("PRP"));*/

    }

    /**
     * 抽取另外一些特征
     * @param dp
     */
    public static void extractExtraOurFeatures(DataPoint dp) {
        CitationFeatureTemplate ft = dp.getFeature();
        /**
         * 1. 抽取相连的dependency，注意处理否定表达
         */
        String[] posTags = {"VB", "JJ", "RB"};
        ft.getSubjectLinkedDependencies().addAll(GrammarUtils.getLinkedDependenciesOfCertainEntity(dp, GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization));
        ft.getCitationLinkedDependencies().addAll(GrammarUtils.getLinkedDependenciesOfCertainEntity(dp, GrammarUtils.EntityType.Citation, posTags, 2, RunFeatureExtract.Lemmatization));
        /**
         * 2. 抽取相邻句中的物理特征（引文数目）
         */
        ft.setCitationList(Citation.extractCitations(dp.getAclSentence().getContent()));
        ft.setCitationNumber(ft.getCitationList().size());
        ft.setPreviousSentenceCitationList(Citation.extractCitations(dp.getFormerNeighborSentence()));
        ft.setPreviousSentenceCitationNumber(ft.getPreviousSentenceCitationList().size());
        ft.setFollowingSentenceCitationList(Citation.extractCitations(dp.getNextNeighborSentence()));
        ft.setFollowingSentenceCitationNumber(ft.getFollowingSentenceCitationList().size());
        /**
         * 3. 看前后句是否相关
         */
        //看前一句是否相关
        String context = dp.getSentence().trim();
        context = context.replace("et al.", "");
        context = context.replaceAll(",|;|:", " $0 ");

        if(isCurrentContextRelated(context, dp.getFeature().getPreviousSentenceCitationList())){
            ft.setPreviousSentenceRelated(true);
//            System.out.println("前一句相关");

            addContextFeature(dp, dp.getFormerNeighborSentence());
        }

        //看后一句是否相关
        context = dp.getNextNeighborSentence().trim();
        context = context.replace("et al.", "");
        context = context.replaceAll(",|;|:", " $0 ");
        if(isCurrentContextRelated(context, dp.getFeature().getFollowingSentenceCitationList())){
            ft.setFollowingSentenceRelated(true);
//            System.out.println("后一句相关");
            addContextFeature(dp, dp.getNextNeighborSentence());
        }
    }

    /**
     * 将相邻句的部分特征加入dp
     * @param dp
     * @param contextSentence
     */
    private static void addContextFeature(DataPoint dp, String contextSentence) {
        CitationFeatureTemplate ft = dp.getFeature();
        // 这里使用未替换标记的句子，否则很多主语无法找到
        String sentence = contextSentence.trim();
        /*
         * 第一部分，改进布尔特征及数值特征
         */
        // 我们的特征主语cue(we,our,us,this..)
        ft.ourContextSubjectWords.addAll(wordExtract(sentence, ourSubjectFilter));
        if (ft.ourContextSubjectWords.size() != 0) {
            ft.setOurContextSubjectBoolean(true);
            ft.setOurContextSubjectWeight(ft.ourContextSubjectWords.size());
        } else {
            ft.setOurContextSubjectBoolean(false);
        }
        // OurIdea特征(following,similar to, motivate,inspired..)
        ft.ourContextIdeaWords.addAll(wordExtract(sentence, ourIdeaFilter));
        if (ft.ourContextIdeaWords.size() != 0) {
            ft.setOurContextIdeaBoolean(true);
            ft.setOurContextIdeaWeight(ft.ourContextIdeaWords.size());
        } else {
            ft.setOurContextIdeaBoolean(false);
        }
        // OurBasis特征(method,approach,model...)
        ft.ourContextBasisWords.addAll(wordExtract(sentence, ourBasisFilter));
        if (ft.ourContextBasisWords.size() != 0) {
            ft.setOurContextBasisBoolean(true);
            ft.setOurContextBasisWeight(ft.ourBasisWords.size());
        } else {
            ft.setOurContextBasisBoolean(false);
        }
        // OurCompare特征
        ft.ourContextCompareWords.addAll(wordExtract(sentence, ourCompareFilter));
        if (ft.ourContextCompareWords.size() != 0) {
            ft.setOurContextCompareBoolean(true);
            ft.setOurContextCompareWeight(ft.ourContextCompareWords.size());
        } else {
            ft.setOurContextCompareBoolean(false);
        }
        // OurResult特征
        ft.ourContextResultWords.addAll(wordExtract(sentence, ourResultFilter));
        if (ft.ourContextResultWords.size() != 0) {
            ft.setOurContextResultBoolean(true);
            ft.setOurContextResultWeight(ft.ourResultWords.size());
        } else {
            ft.setOurContextResultBoolean(false);
        }

        // OurQuantity特征
        ft.ourContextQuantityWords.addAll(wordExtract(sentence, ourQuantityFilter));
        if (ft.ourContextQuantityWords.size() != 0) {
            ft.setOurContextQuantityBoolean(true);
            ft.setOurContextQuantityWeight(ft.ourContextQuantityWords.size());
        } else {
            ft.setOurContextQuantityBoolean(false);
        }
        // OurFrequency特征
        ft.ourContextFrequencyWords.addAll(wordExtract(sentence, ourFrequencyFilter));
        if (ft.ourContextFrequencyWords.size() != 0) {
            ft.setOurContextFrequencyBoolean(true);
            ft.setOurContextFrequencyWeight(ft.ourContextFrequencyWords.size());
        } else {
            ft.setOurContextFrequencyBoolean(false);
        }
        // OurTense特征
        ft.ourContextTenseWords.addAll(wordExtract(sentence, ourTenseFilter));
        if (ft.ourContextTenseWords.size() != 0) {
            ft.setOurContextTenseBoolean(true);
            ft.setOurContextTenseWeight(ft.ourContextTenseWords.size());
        } else {
            ft.setOurContextTenseBoolean(false);
        }
        // OurExample特征
        ft.ourContextExampleWords.addAll(wordExtract(sentence, ourExampleFilter));
        if (ft.ourContextExampleWords.size() != 0) {
            ft.setOurContextExampleBoolean(true);
            ft.setOurContextExampleWeight(ft.ourContextExampleWords.size());
        } else {
            ft.setOurContextExampleBoolean(false);
        }
        // OurSuggest特征
        ft.ourContextSuggestWords.addAll(wordExtract(sentence, ourSuggestFilter));
        if (ft.ourContextSuggestWords.size() != 0) {
            ft.setOurContextSuggestBoolean(true);
            ft.setOurContextSuggestWeight(ft.ourContextSuggestWords.size());
        } else {
            ft.setOurContextSuggestBoolean(false);
        }
        // OurHedge特征
        ft.ourContextHedgeWords.addAll(wordExtract(sentence, ourHedgeFilter));
        if (ft.ourContextHedgeWords.size() != 0) {
            ft.setContextOurHedgeBoolean(true);
            ft.setContextOurHedgeWeight(ft.ourContextHedgeWords.size());
        } else {
            ft.setContextOurHedgeBoolean(false);
        }

        // 对象cue
        ft.contextObjectWords.addAll(wordExtract(sentence, objectFilter));
        if (ft.contextObjectWords.size() != 0) {
            ft.setContextObjectBoolean(true);
            ft.setContextObjectWeight(ft.contextObjectWords.size());
        } else {
            ft.setContextObjectBoolean(false);
        }
        // 工具、数据特征cue
        ft.contextToolWords.addAll(wordExtract(sentence, toolFilter));
        if (ft.contextToolWords.size() != 0) {
            ft.setContextToolBoolean(true);
            ft.setContextToolWeight(ft.contextToolWords.size());
        } else {
            ft.setContextToolBoolean(false);
        }
        // 针对未来工作的future cue
        ft.contextFutureWords.addAll(wordExtract(sentence, futureFilter));
        if (ft.contextFutureWords.size() != 0) {
            ft.setContextFutureBoolean(true);
            ft.setContextFutureWeight(ft.contextFutureWords.size());
        } else {
            ft.setContextFutureBoolean(false);
        }

        // 是否有大额的数字（多于一位）以及对应的次数
        Pattern p = Pattern.compile(number_regex);
        Matcher m = p.matcher(sentence);
        int k = 0;
        while (m.find()) {
            k++;
        }
        ft.setContextNumberCount(k);
        if (k != 0) {
            ft.setContextNumberBoolean(true);
        }

        // 是否有 百分比以及对应的次数
        p = Pattern.compile(percentage_regex);
        m = p.matcher(sentence);
        k = 0;
        while (m.find()) {
            k++;
        }
        ft.setContextPercentageCount(k);
        if (k != 0) {
            ft.setContextPercentageBoolean(true);
        }

        /**
         * 第二部分，ngrams
         */
        dp.getFeature().setContextnGram(GrammarUtils.getNgramWords(sentence, 1, RunFeatureExtract.Lemmatization));

        /**
         * 第三部分，与主语和citation相连的动词、形容词、副词(介词、小品词 考虑去除),加前缀以区分
         */
//        String[] posTags = {"VB", "JJ", "RB", "IN", "RP", "NN"};
        String[] posTags = {"VB", "JJ", "RB"};
        List<TaggedWord> taggedWords = GrammarUtils.getTaggedWord(sentence);
        taggedWords.add(0, new TaggedWord("root", "root"));
        List<TypedDependency> typedDependencies = GrammarUtils.getTypedDependenciesFromSentence(sentence);

        ft.setContextSubjectLinkedKeyWords(GrammarUtils.getLinkedKeyWordsOfCertainEntity(taggedWords, typedDependencies, GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization));
    }

    /**
     * 输入：一个字符串，一个词表 输出：在句子中出现的词表的内容(因而不需要考虑是否进行stem)
     * 词表有两种情况，有的是phrase，针对不同的类型使用不同的方法
     * 如果是phrase则直接contains(正则的话应该用\\b+word+\\b)
     * 如果是word则将句子打散后进行比较，不是句子contains的原因是，有些dictionaryWord可能会错误匹配，如"there".Contains("here")==true
     */
    public static ArrayList<String> wordExtract(String sentence,
                                                List<String> dictionary) {
        HashSet<String> returnSet = new HashSet<String>();
        String[] wordList = (sentence.toLowerCase().split("\\s"));

        for (String dictionaryWord : dictionary) {
            //没有空格说明是一般的word，使用startsWith()来判断
            if (dictionaryWord.indexOf(" ") == -1) {
                for (String word : wordList)
                    if (stemmer.stemWord(word.toLowerCase().trim()).equals(stemmer.stemWord(dictionaryWord))) {
                        //这里额外处理，为了区分us和use在stem后效果相同的问题
                        if(dictionaryWord.equals("us"))//如果字典词是us，则注意处理word是use还是us
                            if (!word.toLowerCase().trim().equals("us") && stemmer.stemWord(word.toLowerCase().trim()).equals("us"))//判断是否是use的变形
                                continue;
                        returnSet.add(dictionaryWord);
                    }
            } else {
                //否则是一个phrase
                if (sentence.contains(dictionaryWord))
                    returnSet.add(dictionaryWord);
            }
        }
        ArrayList<String> returnList = new ArrayList<String>();
        returnSet.forEach(returnList::add);
        return returnList;
    }

    /**
     * 判断句子中是否包含特定的词汇，注意句子要lowercase和trim
     *
     * @param sentence
     * @param wordRegex 要匹配的文字，要正则格式的
     * @return 布尔型，是否包含
     */
    public static Boolean containsSpecificWord(String sentence,
                                               String wordRegex) {
        Pattern pattern = Pattern.compile(wordRegex);
        Matcher matcher = pattern.matcher(sentence.trim().toLowerCase());
        return matcher.find();
    }

    /**
     * 根据所有特征，生成全局词表 将词表及对应词频一并输出
     * 同时 如果指定为需要stem，则将存储在词表中的内容也替换为stemmed后的形式，不用再二次stem
     * (由于后来又添加了许多新的词表，尤其是有HashSet形式的。。所以也设置在extract过程决定是否进行stem)
     */
    public static void generateGlobalWordMap(ArrayList<DataPoint> dataPoints,
                                             Boolean stem) {
        try {
            int count = 0;
            String temp = "";
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                Class featureTemplate = dp.getFeature().getClass();
                Field[] fs = featureTemplate.getDeclaredFields();
                for (int j = 0; j < fs.length; j++) {
                    Field f = fs[j];
                    f.setAccessible(true); // 设置些属性是可以访问的
                    if (f.getName().toLowerCase().endsWith("words")) {// 说明是需要添加词表的属性
                        ArrayList<String> words = (ArrayList<String>) f.get(dp
                                .getFeature());// 得到此属性的值

                        for (int k = 0; k < words.size(); k++) {
                            temp = words.get(k);
                            if (stem) {
                                temp = stemmer.stemWord(temp);
                                words.set(k, temp);
                            }
                            if (!GlobalWordTable.containsKey(temp))
                                GlobalWordTable.put(temp, count++);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据所有特征，生成n-gram 全局词表
     */
    public static void generateGlobalNgramMap(ArrayList<DataPoint> dataPoints, Boolean stem) {
        try {
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                Class featureTemplate = dp.getFeature().getClass();
                Field[] fs = featureTemplate.getDeclaredFields();
                for (int j = 0; j < fs.length; j++) {
                    Field f = fs[j];
                    f.setAccessible(true); // 设置些属性是可以访问的
                    //以ngram结尾的field是所需特征
                    if (f.getName().toLowerCase().endsWith("ngram")) {
                        Object value = f.get(dp.getFeature());
                        ArrayList<String> valueList = (ArrayList<String>) value;
                        for (String word : valueList)
                            if (stem)
                                GlobalNgram.add(stemmer.stemWord(word.toLowerCase()));
                            else
                                GlobalNgram.add(word.toLowerCase());
                    }
                }
            }
//            System.out.println(GlobalNgram.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 根据HashMap<String, Integer> dependencyRelationCount
     * 生成Dependencies的全局词表HashSet<String> GlobalDependencies,将动词词表及对应词频一并输出
     *
     * @param dataPoints
     */
    public static void generateGlobalDependencyMap(
            ArrayList<DataPoint> dataPoints) {
        try {
            int count = 0;
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                List<String> dependencies = dp.getFeature().dependencyRelations;
                for (String word : dependencies) {
//                    System.out.println(word);
                    GlobalDependencies.add(word);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建GlobalAbuWordTable词表，主要只有最近的 名词、形容词、副词
     * 需要lemmatized
     *
     * @param dataPoints
     */
    public static void generateGlobalAbuWords(
            ArrayList<DataPoint> dataPoints) {
        try {
            for (int i = 0; i < dataPoints.size(); i++) {
                DataPoint dp = dataPoints.get(i);
                ArrayList<String> abuWords = dp.getFeature().citationLinkedKeyAbuWords;
                //进行stem
                for (String word : abuWords)
                    GlobalAbuWords.add(stemmer.stemWord(word.toLowerCase()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static HashSet<String> workNonesMap = new HashSet<>(Arrays.asList("account", "algorithm", "analysis", "analyses", "approach", "approaches",
            "application", "applications", "architecture", "architectures", "characterization", "characterisation",
            "component", "components", "corpus", "corpora", "design", "designs", "evaluation", "evaluations",
            "example", "examples", "experiment", "experiments", "extension", "extensions", "evaluation", "formalism", "formalisms",
            "formalization", "for malizations", "formalization", "formalizations", "formulation", "formulations", "framework", "frameworks",
            "implementation", "implementations", "investigation", "investigations", "machinery", "machineries", "method", "methods", "methodology", "methodologies",
            "model", "models", "module", "modules", "paper", "papers", "process", "processes", "procedure", "procedures", "program", "programs",
            "prototype", "prototypes", "research", "researches", "result", "results", "strategy", "strategies", "system", "systems",
            "technique", "techniques", "theory", "theories", "tool", "tools", "treatment", "treatments", "work", "works"));
    /**
     * 看前一句是否相关
     */
    public static boolean isCurrentContextRelated(String contextSentence, ArrayList<Citation> contextCitationList){
        contextSentence = contextSentence.toLowerCase();
        contextSentence = contextSentence.replace("et al.", "");
        contextSentence = contextSentence.replaceAll(",|;|:", " $0 ");
        contextSentence = FeatureExtractFunctions.citationTagReplacement(contextSentence);

        /**
         * 如果前一句有引文则直接认定不相关
         */
        if (contextCitationList.size() > 0)
            return false;

        /**
         * 看前后句中是否有同一个引文
         * 严格来讲即便前一句与当前句有一样的引文也不该影响本句的function判断，故取消
         */
        /*
        boolean match = false;

        for(Citation citation : dataPoint.getFeature().getCitationList())
            for(Citation preCitation : dataPoint.getFeature().getPreviousSentenceCitationList())
                if(citation.equals(preCitation)) {
                    match = true;
                    System.out.println("有相同的引文");
                    return match;
                }*/

        /**
         * 如果打头的词是承接因果的连词，则认为是相关的
         * 承接连词如hence，so，therefore等
         */
        if(contextSentence.startsWith("also")||
                contextSentence.startsWith("although")||
                contextSentence.startsWith("besides")||
                contextSentence.startsWith("but")||
                contextSentence.startsWith("despite")||
                contextSentence.startsWith("even though")||
                contextSentence.startsWith("furthermore")||
                contextSentence.startsWith("however")||
                contextSentence.startsWith("in addition")||
                contextSentence.startsWith("in spite of")||
                contextSentence.startsWith("instead")||
                contextSentence.startsWith("instead of")||
                contextSentence.startsWith("moreover")||
                contextSentence.startsWith("nonetheless")||
                contextSentence.startsWith("on the contrary")||
                contextSentence.startsWith("on the other hand")||
                contextSentence.startsWith("regardless of")||
                contextSentence.startsWith("still")||
                contextSentence.startsWith("then")||
                contextSentence.startsWith("though")||
                contextSentence.startsWith("whereas")||
                contextSentence.startsWith("while")||
                contextSentence.startsWith("yet")||
                contextSentence.startsWith("and")||
                contextSentence.startsWith("accordingly")||
                contextSentence.startsWith("likewise")||
                contextSentence.startsWith("consequently")||
                contextSentence.startsWith("nevertheless")||
                contextSentence.startsWith("further")||
                contextSentence.startsWith("hence")||
                contextSentence.startsWith("therefore")||
                contextSentence.startsWith("thus")||
                contextSentence.startsWith("as a result")||
                contextSentence.startsWith("so")
                ){
            return true;
        }
        /**
         * 看句子中有没有第三人称代词和指示代词
         */
        List<TaggedWord> taggedWords = GrammarUtils.getTaggedWord(contextSentence);
        taggedWords.add(0, new TaggedWord("root", "root"));

        int index = 0;
        for(TaggedWord taggedWord : taggedWords) {
            /**
             * 看是否有匹配的第三人称代词及指示代词
             */
            if (taggedWord.word().trim().toLowerCase().matches("\\bit\\b|\\bthey\\b|\\bthem\\b|\\bthis\\b|\\bthese\\b|\\bthose\\b|\\btheir\\b|\\bit's\\b|\\btheirs\\b|\\bits\\b|\\bsuch\\b")) {
                index = taggedWords.indexOf(taggedWord);
                /**
                 * 如果this后面是跟着这几个词则不是我们想要的代指效果
                 */
                if (index + 1 < taggedWords.size())
                    if (taggedWord.word().equals("this")
                            && (taggedWords.get(index + 1).word().equals("paper")
                            || taggedWords.get(index + 1).word().equals("section")
                            || taggedWords.get(index + 1).word().equals("work")
                            || taggedWords.get(index + 1).word().equals("study")))
                        continue;
                /**
                 * it后面几个词内出现that，是强调句型，忽略这个it
                 */
                boolean isStress = false;
                if (taggedWord.word().equals("it"))
                    for(int i = 1; i < 5 && index+i < taggedWords.size(); i++)
                        if(taggedWords.get(index + i).word().equals("that")) {
                            isStress = true;
                            break;
                        }
                if(isStress)
                    continue;
//                System.out.println(taggedWord.word());
//                System.out.println("发现有第三人称代词或者指示代词");
                //dataPoint.setSentence(dataPoint.getSentence().replaceAll("\\bit\\b|\\bthey\\b|\\bthem\\b|\\bthis\\b|\\bthese\\b|\\bthose\\b|\\btheir\\b|\\bit's\\b|\\btheirs\\b|\\bits\\b","<tag>$0</tag>"));
                return true;
            }

            /**
             * 看是否有the+work noun的搭配
             */
            if(taggedWord.word().trim().toLowerCase().equals("the") && workNonesMap.contains(taggedWords.get(index + 1).word()))
                return true;
            /**
             * 如果是work noun的复数形式打头
             * 感觉不是太靠谱，最好还是有指示词的效果比较可靠
             */
            /*if(taggedWord.tag().equals("NNS") && workNonesMap.contains(taggedWord.word()))
                return true;*/
            /**
             * 如果指示词前面还有别的NN，则有可能该指示词是指向该名词而非上一句，故舍弃
             */
            if(taggedWord.tag().startsWith("NN"))
                break;
        }
        return false;
    }

    /**
     * 看前一句是否相关
     * 很简单的只检查上下文句是否有引文以及是否有本文主语代词表示
     */
    public static boolean isContextRelated(String sentence, ArrayList<Citation> preCitationList) {
        /**
         * 如果前一句有引文则直接认定不相关
         */
        if (preCitationList.size() > 0)
            return false;
        /**
         * 前一句中是否有subject的内容
         */
        ArrayList<String> subjects = wordExtract(sentence, ourSubjectFilter);
        if (subjects.size() > 0)
            return true;

        return false;

    }

    /**
     * 看前一句是否相关
     * @param dataPoint
     */
    public static boolean isLatterSentenceRelated(DataPoint dataPoint){
        for(Citation citation : dataPoint.getFeature().getCitationList())
            for(Citation nextCitation : dataPoint.getFeature().getFollowingSentenceCitationList())
                if(citation.equals(nextCitation)) {
                    return true;
                }

        if(containsSpecificWord(dataPoint.getSentence().toLowerCase(), "\\bit\\b|\\bthey\\b|\\bthem\\b|\\bthis\\b|\\bthese\\b|\\bthat\\b|\\bthose\\b"))
            return true;
        if(containsSpecificWord(dataPoint.getSentence().toLowerCase(), "\\btheir\\b|\\bit's\\b|\\btheirs\\b|\\bits\\b"))
            return true;
        return false;
    }

}

package edu.whu.irlab.dfki.featureSelection;

import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.data.InstancesFilter;
import edu.whu.irlab.dfki.feature.FeatureConverter;
import org.apache.commons.io.FileUtils;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.core.Attribute;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Memray on 2015/3/23.
 */
public class FeatureSelection {


    /**
     * 在给定数据上进行特征选择，并按照不同size进行输出
     *
     * @param subInstances   要进行特征选择的数据集（只包含featureSetName的数据，其他数据应提前过滤掉）
     * @param featureSetName 该数据集的名称
     */
    public static void runFeatureSelectionOnGivenInstances(Instances subInstances, String featureSetName) {
        /**
         * 1. 在给定数据上进行IG特征选择
         *    注意里面包含class所在的属性
         */
//        ArrayList<Map.Entry<Attribute, Double>> selectedFeatureScoreList = FeatureSelection.featureSelect(subInstances);

        /**
         * 2.初始化搜索算法（search method）及属性评测算法（attribute evaluator）
         */
        Ranker rank = new Ranker();
        InfoGainAttributeEval infoGainAttributeEval = new InfoGainAttributeEval();
//        System.out.println(RunFeatureExtract.OutputBasePath + featureSetName + File.separator + "top_feature.txt");
        /**
         * 3.根据评测算法评测各个属性
         */
        try {
            infoGainAttributeEval.buildEvaluator(subInstances);

            //System.out.println(rank.search(eval, trainIns));

            /**
             * 4.按照特定搜索算法对属性进行筛选
             * 在这里使用的Ranker算法仅仅是属性按照InfoGain的大小进行排序
             */
            int[] attrIndex = rank.search(infoGainAttributeEval, subInstances);

            /**
             * 5.打印结果信息
             * 在这里我们得到属性的排序结果同时将每个属性的InfoGain信息打印出来
             */
            StringBuffer attrInfoGainInfo = new StringBuffer();
            for (int i = 0; i < attrIndex.length; i++) {
                attrInfoGainInfo.append(subInstances.getAttributes().indexOf(subInstances.attribute(attrIndex[i]))+1);
                attrInfoGainInfo.append(",\"");
                attrInfoGainInfo.append((subInstances.attribute(attrIndex[i]).name()));
                attrInfoGainInfo.append("\",");
                attrInfoGainInfo.append(infoGainAttributeEval.evaluateAttribute(attrIndex[i]));
                attrInfoGainInfo.append("\n");
            }
//            System.out.println(attrIndexInfo.toString());
//            System.out.println(attrInfoGainInfo.toString());
            System.out.println("全部特征的排序列表输出到：\t" + RunFeatureExtract.OutputBasePath + featureSetName + File.separator + "top_feature.txt");
            FileUtils.writeStringToFile(new File(RunFeatureExtract.OutputBasePath + featureSetName + File.separator + "top_feature.txt"), attrInfoGainInfo.toString(), "utf8", false);

            /**
             * 6. 输出特征选择后不同size的特征集合的libsvm特征文件
             */
        /*
         * 此处可以使用reduceDimensionality()来对instances进行reduce，缺点是只能够执行一次，不同size需要执行多次feature selection
         */
//            Instances reducedInst = eval.reduceDimensionality(inst);
            for (Integer featureNumber : Arrays.asList(100, 200, 300, 400, 500, 600, 700, 800, 900, 1000)) {
                System.out.println("正在输出" + featureSetName + " " + featureNumber + "个选择特征的结果");
                /**
                 * 6.1 定义一个列表存储候选输出的Attribute
                 */
                ArrayList<Attribute> candidateTopFeatureList = new ArrayList<>();
                /**
                 * 6.2 将Top N的特征加入列表中
                 */
                // 如果需要输出的特征个数已经大于特征集的特征个数，则不需要继续进行输出了
                if (featureNumber > subInstances.numAttributes())
                    continue;
                for (int i = 0; i < featureNumber; i++) {
                    if(i > subInstances.numAttributes())
                        break;
                    candidateTopFeatureList.add(subInstances.attribute(attrIndex[i]));
                }
                /**
                 * 6.3 将原数据过滤为只具有Top N的特征，并输出到文件中
                 */
                String filePath = RunFeatureExtract.OutputBasePath + featureSetName + File.separator + featureSetName + "_top_" + featureNumber + ".libsvm";
                FeatureConverter.exportLibsvmFeatureVector(InstancesFilter.filterInstancesByAttributes(subInstances, candidateTopFeatureList), filePath);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


/*        *//**
         * 2. 输出特征选择的排序结果，结果中只包含IG的值大于0的属性
         *//*
        System.out.println("正在输出"+featureSetName+"特征选择的结果");
        StringBuilder stringBuilder = new StringBuilder();
        for(Map.Entry<Attribute, Double> entry : selectedFeatureScoreList){
           stringBuilder.append(entry.getKey().index()).append(",\""+entry.getKey().name()).append("\",").append(entry.getValue()).append("\n");
        }
        try {
            FileUtils.writeStringToFile(new File(RunFeatureExtract.ProjectBasePath+"output"+File.separator+featureSetName+File.separator+"top_features.txt"), stringBuilder.toString(), "utf8", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
*/

    }

    /**
     * 在给定数据instances上进行特征选择，按照得分降序排序，并将结果以ArrayList<Map.Entry<Attribute, Double>>的形式返回
     * 注意将class属性干掉，其作为一个得分为0.0的特征存在，可能影响我们后来处理
     * <p>
     * 注:之前太蠢了居然用TreeMap<Double, Attribute>来保存，虽然方便排序，但是得分相同的特征就会被忽略
     *
     * @param instances
     * @return
     */
        public static ArrayList<Map.Entry<Attribute, Double>> featureSelect (Instances instances){
            ArrayList<Map.Entry<Attribute, Double>> returnList = new ArrayList<>();
            HashMap<Attribute, Double> attributeTreeMap = new HashMap<>();
            try {
                //初始化IG的evaluator
                InfoGainAttributeEval infoGainAttributeEval = new InfoGainAttributeEval();
                infoGainAttributeEval.setOptions("-M 1 -B 0".split("\\s"));
                //执行IG
                infoGainAttributeEval.buildEvaluator(instances);
                //读取每个Attribute的score放到TreeMap中以方便排序输出
                for (Attribute attribute : instances.getAttributes()) {
                    //输出时注意过滤掉class属性
                    if (!attribute.name().equals("class"))
                        attributeTreeMap.put(attribute, infoGainAttributeEval.evaluateAttribute(instances.getAttributes().indexOf(attribute)));
                }
                for (Map.Entry<Attribute, Double> entry : attributeTreeMap.entrySet())
                    returnList.add(entry);
                Collections.sort(returnList, (a, b) -> b.getValue().compareTo(a.getValue()));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return returnList;

        }

        /**
         * 通过特征选择找出两两类别之间的特征差异，带上全部特征的，形成一个csv输出以方便对比
         *
         * @param instances
         */

    public static void runSelectionBetweenClasses(Instances instances) {
        // 构建一个HashMap<Attribute, ArrayList<Double>>保存对应的结果，ArrayList<Double>的不同index保存不同
        HashMap<Attribute, ArrayList<Double>> allSelectedFeature = new HashMap<>();
        /**
         * 进行整个数据集的特征选择
         */
        int roundIndex = 0;
        System.out.println(roundIndex + ". 此轮共有数据:" + instances.size());

        ArrayList<Map.Entry<Attribute, Double>> selectedFeatures = FeatureSelection.featureSelect(instances);
        for (Map.Entry<Attribute, Double> entry : selectedFeatures) {
            if (!allSelectedFeature.containsKey(entry.getValue())) {
                allSelectedFeature.put(entry.getKey(), new ArrayList<>());
                for (int i = 0; i < 7; i++)
                    allSelectedFeature.get(entry.getValue()).add(0.0);
            }
            allSelectedFeature.get(entry.getValue()).add(roundIndex, entry.getValue());
        }

        /**
         * 进行两两类别之间的特征选择
         */
        System.out.println("开始进行不同类别间的特征选择比较");
        for (int i = 0; i < instances.classAttribute().numValues() - 1; i++)
            for (int j = i + 1; j < instances.classAttribute().numValues(); j++) {
                Instances newInstances = new Instances(instances);
                int index = 0;
                while (index < newInstances.numInstances()) {
                    if (newInstances.get(index).classValue() + 1 != Double.parseDouble(instances.classAttribute().value(i))
                            && newInstances.get(index).classValue() + 1 != Double.parseDouble(instances.classAttribute().value(j))) {
                        newInstances.delete(index);
                    } else
                        index++;
                }
                int iCount = 0;
                int jCount = 0;
                for (int k = 0; k < newInstances.numInstances(); k++) {
                    if (newInstances.instance(k).classValue() + 1 == Double.parseDouble(instances.classAttribute().value(i)))
                        iCount++;
                    if (newInstances.instance(k).classValue() + 1 == Double.parseDouble(instances.classAttribute().value(j)))
                        jCount++;
                }

                /**
                 * 将这轮特征选择结果保存到allSelectedFeature中
                 */
                roundIndex++;
                System.out.println(roundIndex + ". 此轮共有数据:" + newInstances.size());
                System.out.println(instances.classAttribute().value(i) + ":" + iCount);
                System.out.println(instances.classAttribute().value(j) + ":" + jCount);

                selectedFeatures = FeatureSelection.featureSelect(newInstances);
                for (Map.Entry<Attribute, Double> entry : selectedFeatures) {
                    if (!allSelectedFeature.containsKey(entry.getValue())) {
                        allSelectedFeature.put(entry.getKey(), new ArrayList<>(7));
                        for (int k = 0; k < 7; k++)
                            allSelectedFeature.get(entry.getValue()).add(0.0);
                    }
                    allSelectedFeature.get(entry.getValue()).add(roundIndex, entry.getValue());
                }
                System.out.println();
/*
                StringBuffer stringBuffer = new StringBuffer();
                String fileName = Classification.InstanceClassName.getClassName(Integer.parseInt(instances.classAttribute().value(i)))+
                        "_"+Classification.InstanceClassName.getClassName(Integer.parseInt(instances.classAttribute().value(j))) + ".txt";
                selectedFeatures.entrySet().stream().forEach(s->stringBuffer.append(s.getValue().name()).append("\t").append(s.getValue().index()).append("\t").append(s.getKey()).append("\n"));

                System.out.println("特征选择结果输出到"+fileName);
                try {
                    FileUtils.writeStringToFile(new File(Classification.ProjectBasePath+"output"+File.separator+fileName), stringBuffer.toString(), "utf8", false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println();
*/
            }
        /**
         * 将所有结果输出到一个csv中
         */

/*
        String csvFilePath = "c:/test.csv";
        CsvWriter wr =new CsvWriter(csvFilePath,',', Charset.forName("SJIS"));
        String[] contents = {"aaaaa","bbbbb","cccccc","ddddddddd"};
        try {
            wr.writeRecord(contents);
        } catch (IOException e) {
            e.printStackTrace();
        }
        wr.close();
*/


        StringBuffer buffer = new StringBuffer();
        buffer.append("Feature Name,").append("All");
        for (int i = 1; i < instances.classAttribute().numValues(); i++)
            for (int j = i + 1; j <= instances.classAttribute().numValues(); j++)
                buffer.append(",").append(RunFeatureExtract.InstanceClassName.getClassName(i)).append(" Vs ").append(RunFeatureExtract.InstanceClassName.getClassName(j));

        buffer.append("\n");

        for (Map.Entry<Attribute, ArrayList<Double>> entry : allSelectedFeature.entrySet()) {
            buffer.append("\"").append(entry.getKey().name()).append("\"");
            for (int i = 0; i < 7; i++) {
                if (entry.getValue().get(i) != null)
                    buffer.append(",").append(entry.getValue().get(i));
                else
                    buffer.append(",0");
            }
            buffer.append("\n");
        }
        String fileName = "all_feature_comparison.csv";
        System.out.println("特征选择结果输出到" + fileName);
        try {
            FileUtils.writeStringToFile(new File(RunFeatureExtract.ProjectBasePath + "output" + File.separator + fileName), buffer.toString(), "utf8", false);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}


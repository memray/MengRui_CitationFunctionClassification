package edu.whu.irlab.dfki;

import java.io.*;
import java.util.*;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.bean.Instance;
import edu.whu.irlab.classify.svm_scale;
import edu.whu.irlab.dfki.data.ConverterUtils;
import edu.whu.irlab.dfki.data.InstancesFilter;
import edu.whu.irlab.dfki.data.etc.Data2HTMLProcessor;
import edu.whu.irlab.dfki.feature.FeatureConverter;
import edu.whu.irlab.dfki.feature.FeatureProcessor;
import edu.whu.irlab.dfki.feature.FeatureScaler;
import edu.whu.irlab.dfki.featureSelection.FeatureSelection;
import weka.core.Instances;


/**
 * 分类实验的主程序
 * @author Memray
 */
public class RunFeatureExtract {
    public enum FeatureSetName{
        BaseLine1, BaseLine2
        //, AbuJbara, Dong, Charles, Our, OurContext
    }
    public enum InstanceClassName{
        Idea(1), Basis(2), Comparison(3), Background(4);
        private int id;
        InstanceClassName(int id){
            this.id = id;
        }
        public static InstanceClassName getClassName(int id){
            for(InstanceClassName className : InstanceClassName.values())
                if (className.id == id)
                    return className;
            return null;
        }

    }

    public static String TargetFeatureName;


    //工程的绝对路径
    public static String ProjectBasePath = System.getProperty("user.dir") + File.separator;
    public static String OutputBasePath = ProjectBasePath + "output" + File.separator;
    // 原数据Data以及从数据库中取出的一些信息以及基本的初始化处理被缓存化到这里(提取了数据库信息，替换了标记等)
    public static String DataCachePath = ProjectBasePath + "output" + File.separator + "DFKIDataCache.dat";
    // 存放特征缓存文件的位置，包含DataCache中的数据以及额外抽取的特征数据
    public static String FeatureCachePath = ProjectBasePath + "output" + File.separator + "DFKIFeatureCache_Context.dat";

    HashSet<String> labels = new HashSet<>();
    public static ArrayList<DataPoint> dataPoints = new ArrayList<>();
    //指明是否需要进行词干提取，基本只对FeatureCache生成部分有影响。输出特征向量的一般需要额外处理
    public static Boolean Lemmatization = false;
    //指明是否需要保留每个特征的属性名，不保留的话会按照属性值进行合并，如subjectWords_we和ourSubjectWords_we会被合并为一个向量
    public static Boolean retainAttributeName = true;
    /**
     * 初始化操作，包括读取元数据，路径变量设置等
     */
    public void init() {
    }

    /**
     * Transfer to feature vectors
     */
    public void feature_generate(Boolean lemmatization) {
        /**
         * 1. 抽取特征，基本生成一次缓存后可以快速调用
         */
        FeatureProcessor.createFeatureFile(dataPoints, this.FeatureCachePath,
                this.TargetFeatureName, lemmatization);

        /**
         * 2. 根据feature data生成特征向量
         */
        ArrayList<Instance> instanceList = FeatureConverter.getOurInstanceList(dataPoints);
        /**
         * 3. 转化为Weka的instances
         *    包括对class所在的Attribute进行离散化处理
         */
        Instances instances = ConverterUtils.convertInstances(instanceList, instanceList.get(0).getAttributes());

        /*
         * 这里看会否需要对同名的attribute进行过滤，比如we这个词会在多个特征组中重复出现
         * 这样会collapse很多词表类的特征
         */
        if(!retainAttributeName) {
            ArrayList<weka.core.Attribute> retainedAttributes = InstancesFilter.getFilteredAttributes(instances);
            instances = InstancesFilter.filterInstancesByAttributes(instances, retainedAttributes);
        }
        System.out.println("共有" + instances.numAttributes() + "个特征");
        /**
         * 4. 对生成的特征文件进行scale
         */
        instances = FeatureScaler.scale(instances, "-S 1.0 -T 0.0".split("\\s"));

        //(使用svm_scale进行scale)
//        svm_scale.scale("-l 0 -u 1 H:\\NetDrive\\WorkSpace\\Master\\MengRui_CitationFunctionClassification\\output\\all\\all_vector.libsvm".split("\\s"), "H:\\NetDrive\\WorkSpace\\Master\\MengRui_CitationFunctionClassification\\output\\all\\all_svmscale_normalize_vector.libsvm");
        String featureDescription = "";

        System.out.println("全部数据的libsvm输出到：\t" + OutputBasePath + "all" + featureDescription + ".libsvm");
        FeatureConverter.exportLibsvmFeatureVector(instances, OutputBasePath + "all_vector.libsvm");
        System.out.println("全部数据的特征可视化输出到：\t" + OutputBasePath + "visualization\\all_data_report_FilteredNgram_CombinedContext.html");
//        new Data2HTMLProcessor().generateHTMLReport(RunFeatureExtract.dataPoints, null, OutputBasePath + "visualization\\all_data_report_StemmedNgram_SubCitLinked_CombinedContext.html");
        /**
         * 5. 导出每一个特征集的特征列表(通过过滤器对不同特征集进行过滤)，执行对应的特征选择，将特征向量输出为libsvm格式
         */
        for (RunFeatureExtract.FeatureSetName featureSetName : RunFeatureExtract.FeatureSetName.values()) {
//            featureSetName = FeatureSetName.OurContext;
            System.err.println("导出" + featureSetName + "特征方案");
            /**
             * 5.1 通过过滤器获取到该特征名下的子Instances
             */
            Instances subInstances = InstancesFilter.filterInstancesByFeatureSetName(instances, featureSetName);
            System.err.println(featureSetName + " 有 " + subInstances.numAttributes() + "个特征");

            /**
             * 5.2 开始导出特征向量，包括特征列表和libsvm向量文件
             */
//            FeatureConverter.exportAttributeList(subInstances, featureSetName, featureSetName + "_all_attribute"+featureDescription+".txt");
            System.out.println(featureSetName+"全部特征列表输出到：\t"+RunFeatureExtract.OutputBasePath + featureSetName + featureDescription + File.separator + featureSetName + "_all_attribute" + featureDescription + ".txt");
            FeatureConverter.exportAttributeList(subInstances, RunFeatureExtract.OutputBasePath + featureSetName + featureDescription + File.separator + featureSetName + "_all_attribute" + featureDescription + ".txt");
            System.out.println(featureSetName + "全部特征向量的libsvm输出到：\t" + RunFeatureExtract.OutputBasePath + featureSetName + featureDescription + File.separator + featureSetName + "_all_vector" + featureDescription + ".libsvm");
            FeatureConverter.exportLibsvmFeatureVector(subInstances, RunFeatureExtract.OutputBasePath+featureSetName + featureDescription + File.separator+ featureSetName + "_all_vector"+featureDescription+".libsvm");
            System.out.println("特征属性列表和向量文件输出完毕");
            /**
             * 5.3 执行特征选择，返回一个按照得分从大到小排序的特征列表
             * TODO 尽管我们的特征选择结果能够正常计算并导出，但是最终分类的结果总是与WEKA GUI的特征结果有较大差距，故暂时放弃
             */
            System.out.println("正在执行InfoGain特征选择");
            FeatureSelection.runFeatureSelectionOnGivenInstances(subInstances, featureSetName + featureDescription);

//            break;
        }
        /**
         * 6. 通过特征选择找出两两类别之间的特征差异
         */
        // FeatureSelection.runSelectionBetweenClasses(instances);
    }


    public static void main(String[] args) {
        RunFeatureExtract classifier = new RunFeatureExtract();
        /**
         * 是否考虑进行词干提取Lemmatization
         */
        classifier.init();//读取源数据
        classifier.feature_generate(Lemmatization);//生成特征信息


    }

}

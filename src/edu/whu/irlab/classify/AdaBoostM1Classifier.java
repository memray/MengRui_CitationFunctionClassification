package edu.whu.irlab.classify;

import java.io.File;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.converters.LibSVMLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;

public class AdaBoostM1Classifier {
	public static void main(String[] args) {
		LibSVMLoader libSVMLoader = new LibSVMLoader();
		try {
			//读取vector数据
			libSVMLoader.setSource(new File("output\\our_feature.scale"));
			Instances originalData = libSVMLoader.getDataSet();
			//将数据的类别数据由numeric转化为nominal
	        NumericToNominal convert= new NumericToNominal();
	        String[] options= new String[2];
	        options[0]="-R";
	        options[1]= "1-47,"+(originalData.classIndex()+1)+"-"+(originalData.classIndex()+1);  //range of variables to make numeric

	        convert.setOptions(options);
	        convert.setInputFormat(originalData);

	        Instances dataSet = Filter.useFilter(originalData, convert);

	        //-P <num> Percentage of weight mass to base training on.  (default 100, reduce to around 90 speed up)
	        //-Q Use resampling for boosting.
	        //-S <num> Random number seed.  (default 1)
            //-I <num> Number of iterations.  (default 10)
	        //-D If set, classifier is runFeatureSelectionOnGivenInstances in debug mode and may output additional info to the console
	       
	        //-W Full name of base classifier.  (default: weka.classifiers.trees.DecisionStump)
	       
	        int numFolds = 10;
			Evaluation evaluation = new Evaluation(dataSet);
			String params = "-I 100 -Q";
			evaluation.crossValidateModel("AdaBoostM1", dataSet, numFolds, params.split(" "), new Random(1));
			System.out.println(evaluation.toSummaryString("\nResults\n======\n", true));
			System.out.println("Hold on");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}

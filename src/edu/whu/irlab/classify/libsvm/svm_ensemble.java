package edu.whu.irlab.classify.libsvm;

import java.io.*;
import java.util.*;


public strictfp class svm_ensemble {
	//
	// construct and solve various formulations
	//
	public static final int LIBSVM_VERSION = 318;
	public static final Random rand = new Random();

	private static svm_print_interface svm_print_stdout = new svm_print_interface() {
		public void print(String s) {
			System.out.print(s);
			System.out.flush();
		}
	};

	private static svm_print_interface svm_print_string = svm_print_stdout;

	static void info(String s) {
		//svm_print_string.print(s);
	}

	/**
	 * 执行C_SVC的计算
	 * @param prob
	 * @param param
	 * @param alpha
	 * @param si
	 * @param Cp
	 * @param Cn
	 */
	private static void solve_c_svc(svm_problem prob, svm_parameter param,
			double[] alpha, Solver.SolutionInfo si, double Cp, double Cn) {
		int l = prob.l;
		double[] minus_ones = new double[l];
		byte[] y = new byte[l];

		int i;

		for (i = 0; i < l; i++) {
			alpha[i] = 0;
			minus_ones[i] = -1;
			if (prob.y[i] > 0)
				y[i] = +1;
			else
				y[i] = -1;
		}

		Solver s = new Solver();
		s.Solve(l, new SVC_Q(prob, param, y), minus_ones, y, alpha, Cp, Cn,
				param.eps, si, param.shrinking);

		double sum_alpha = 0;
		for (i = 0; i < l; i++)
			sum_alpha += alpha[i];

		if (Cp == Cn)
			svm_ensemble.info("nu = " + sum_alpha / (Cp * prob.l) + "\n");

		for (i = 0; i < l; i++)
			alpha[i] *= y[i];
	}

	private static void solve_nu_svc(svm_problem prob, svm_parameter param,
			double[] alpha, Solver.SolutionInfo si) {
		int i;
		int l = prob.l;
		double nu = param.nu;

		byte[] y = new byte[l];

		for (i = 0; i < l; i++)
			if (prob.y[i] > 0)
				y[i] = +1;
			else
				y[i] = -1;

		double sum_pos = nu * l / 2;
		double sum_neg = nu * l / 2;

		for (i = 0; i < l; i++)
			if (y[i] == +1) {
				alpha[i] = Math.min(1.0, sum_pos);
				sum_pos -= alpha[i];
			} else {
				alpha[i] = Math.min(1.0, sum_neg);
				sum_neg -= alpha[i];
			}

		double[] zeros = new double[l];

		for (i = 0; i < l; i++)
			zeros[i] = 0;

		Solver_NU s = new Solver_NU();
		s.Solve(l, new SVC_Q(prob, param, y), zeros, y, alpha, 1.0, 1.0,
				param.eps, si, param.shrinking);
		double r = si.r;

		svm_ensemble.info("C = " + 1 / r + "\n");

		for (i = 0; i < l; i++)
			alpha[i] *= y[i] / r;

		si.rho /= r;
		si.obj /= (r * r);
		si.upper_bound_p = 1 / r;
		si.upper_bound_n = 1 / r;
	}

	private static void solve_one_class(svm_problem prob, svm_parameter param,
			double[] alpha, Solver.SolutionInfo si) {
		int l = prob.l;
		double[] zeros = new double[l];
		byte[] ones = new byte[l];
		int i;

		int n = (int) (param.nu * prob.l); // # of alpha's at upper bound

		for (i = 0; i < n; i++)
			alpha[i] = 1;
		if (n < prob.l)
			alpha[n] = param.nu * prob.l - n;
		for (i = n + 1; i < l; i++)
			alpha[i] = 0;

		for (i = 0; i < l; i++) {
			zeros[i] = 0;
			ones[i] = 1;
		}

		Solver s = new Solver();
		s.Solve(l, new ONE_CLASS_Q(prob, param), zeros, ones, alpha, 1.0, 1.0,
				param.eps, si, param.shrinking);
	}

	private static void solve_epsilon_svr(svm_problem prob,
			svm_parameter param, double[] alpha, Solver.SolutionInfo si) {
		int l = prob.l;
		double[] alpha2 = new double[2 * l];
		double[] linear_term = new double[2 * l];
		byte[] y = new byte[2 * l];
		int i;

		for (i = 0; i < l; i++) {
			alpha2[i] = 0;
			linear_term[i] = param.p - prob.y[i];
			y[i] = 1;

			alpha2[i + l] = 0;
			linear_term[i + l] = param.p + prob.y[i];
			y[i + l] = -1;
		}

		Solver s = new Solver();
		s.Solve(2 * l, new SVR_Q(prob, param), linear_term, y, alpha2, param.C,
				param.C, param.eps, si, param.shrinking);

		double sum_alpha = 0;
		for (i = 0; i < l; i++) {
			alpha[i] = alpha2[i] - alpha2[i + l];
			sum_alpha += Math.abs(alpha[i]);
		}
		svm_ensemble.info("nu = " + sum_alpha / (param.C * l) + "\n");
	}

	private static void solve_nu_svr(svm_problem prob, svm_parameter param,
			double[] alpha, Solver.SolutionInfo si) {
		int l = prob.l;
		double C = param.C;
		double[] alpha2 = new double[2 * l];
		double[] linear_term = new double[2 * l];
		byte[] y = new byte[2 * l];
		int i;

		double sum = C * param.nu * l / 2;
		for (i = 0; i < l; i++) {
			alpha2[i] = alpha2[i + l] = Math.min(sum, C);
			sum -= alpha2[i];

			linear_term[i] = -prob.y[i];
			y[i] = 1;

			linear_term[i + l] = prob.y[i];
			y[i + l] = -1;
		}

		Solver_NU s = new Solver_NU();
		s.Solve(2 * l, new SVR_Q(prob, param), linear_term, y, alpha2, C, C,
				param.eps, si, param.shrinking);

		svm_ensemble.info("epsilon = " + (-si.r) + "\n");

		for (i = 0; i < l; i++)
			alpha[i] = alpha2[i] - alpha2[i + l];
	}

	/**
	 * decision_function
	 * @author Memray
	 */
	static class decision_function {
		double[] alpha;
		double rho;
	};

	/**
	 * 训练单个的svm，返回决策函数
	 * @param prob
	 * @param param
	 * @param Cp
	 * @param Cn
	 * @return
	 */
	static decision_function svm_train_one(
		svm_problem prob, svm_parameter param,
		double Cp, double Cn)
	{
		double[] alpha = new double[prob.l];
		Solver.SolutionInfo si = new Solver.SolutionInfo();
		/**
		 * 根据输入设定的分类器或者回归函数进行对应的解
		 */
		switch (param.svm_type) {
		case svm_parameter.C_SVC:
			solve_c_svc(prob, param, alpha, si, Cp, Cn);
			break;
		case svm_parameter.NU_SVC:
			solve_nu_svc(prob, param, alpha, si);
			break;
		case svm_parameter.ONE_CLASS:
			solve_one_class(prob, param, alpha, si);
			break;
		case svm_parameter.EPSILON_SVR:
			solve_epsilon_svr(prob, param, alpha, si);
			break;
		case svm_parameter.NU_SVR:
			solve_nu_svr(prob, param, alpha, si);
			break;
		}

		/*
		 * 输出obj最佳目标值，rho决策函数的偏执项
		 */
		svm_ensemble.info("obj = " + si.obj + ", rho = " + si.rho + "\n");

		// output SVs

		int nSV = 0;
		int nBSV = 0;
		for (int i = 0; i < prob.l; i++) {
			if (Math.abs(alpha[i]) > 0) {
				++nSV;
				if (prob.y[i] > 0) {
					if (Math.abs(alpha[i]) >= si.upper_bound_p)
						++nBSV;
				} else {
					if (Math.abs(alpha[i]) >= si.upper_bound_n)
						++nBSV;
				}
			}
		}
		/*
		 * 输出nSV 支持向量个数   以及 nBSV  有界支持向量的个数
		 */
		svm_ensemble.info("nSV = " + nSV + ", nBSV = " + nBSV + "\n");

		decision_function f = new decision_function();
		f.alpha = alpha;
		f.rho = si.rho;
		return f;
	}

	// Platt's binary SVM Probablistic Output: an improvement from Lin et al.
	private static void sigmoid_train(int l, double[] dec_values,
			double[] labels, double[] probAB) {
		double A, B;
		double prior1 = 0, prior0 = 0;
		int i;

		for (i = 0; i < l; i++)
			if (labels[i] > 0)
				prior1 += 1;
			else
				prior0 += 1;

		int max_iter = 100; // Maximal number of iterations
		double min_step = 1e-10; // Minimal step taken in line search
		double sigma = 1e-12; // For numerically strict PD of Hessian
		double eps = 1e-5;
		double hiTarget = (prior1 + 1.0) / (prior1 + 2.0);
		double loTarget = 1 / (prior0 + 2.0);
		double[] t = new double[l];
		double fApB, p, q, h11, h22, h21, g1, g2, det, dA, dB, gd, stepsize;
		double newA, newB, newf, d1, d2;
		int iter;

		// Initial Point and Initial Fun Value
		A = 0.0;
		B = Math.log((prior0 + 1.0) / (prior1 + 1.0));
		double fval = 0.0;

		for (i = 0; i < l; i++) {
			if (labels[i] > 0)
				t[i] = hiTarget;
			else
				t[i] = loTarget;
			fApB = dec_values[i] * A + B;
			if (fApB >= 0)
				fval += t[i] * fApB + Math.log(1 + Math.exp(-fApB));
			else
				fval += (t[i] - 1) * fApB + Math.log(1 + Math.exp(fApB));
		}
		for (iter = 0; iter < max_iter; iter++) {
			// Update Gradient and Hessian (use H' = H + sigma I)
			h11 = sigma; // numerically ensures strict PD
			h22 = sigma;
			h21 = 0.0;
			g1 = 0.0;
			g2 = 0.0;
			for (i = 0; i < l; i++) {
				fApB = dec_values[i] * A + B;
				if (fApB >= 0) {
					p = Math.exp(-fApB) / (1.0 + Math.exp(-fApB));
					q = 1.0 / (1.0 + Math.exp(-fApB));
				} else {
					p = 1.0 / (1.0 + Math.exp(fApB));
					q = Math.exp(fApB) / (1.0 + Math.exp(fApB));
				}
				d2 = p * q;
				h11 += dec_values[i] * dec_values[i] * d2;
				h22 += d2;
				h21 += dec_values[i] * d2;
				d1 = t[i] - p;
				g1 += dec_values[i] * d1;
				g2 += d1;
			}

			// Stopping Criteria
			if (Math.abs(g1) < eps && Math.abs(g2) < eps)
				break;

			// Finding Newton direction: -inv(H') * g
			det = h11 * h22 - h21 * h21;
			dA = -(h22 * g1 - h21 * g2) / det;
			dB = -(-h21 * g1 + h11 * g2) / det;
			gd = g1 * dA + g2 * dB;

			stepsize = 1; // Line Search
			while (stepsize >= min_step) {
				newA = A + stepsize * dA;
				newB = B + stepsize * dB;

				// New function value
				newf = 0.0;
				for (i = 0; i < l; i++) {
					fApB = dec_values[i] * newA + newB;
					if (fApB >= 0)
						newf += t[i] * fApB + Math.log(1 + Math.exp(-fApB));
					else
						newf += (t[i] - 1) * fApB
								+ Math.log(1 + Math.exp(fApB));
				}
				// Check sufficient decrease
				if (newf < fval + 0.0001 * stepsize * gd) {
					A = newA;
					B = newB;
					fval = newf;
					break;
				} else
					stepsize = stepsize / 2.0;
			}

			if (stepsize < min_step) {
				svm_ensemble.info("Line search fails in two-class probability estimates\n");
				break;
			}
		}

		if (iter >= max_iter)
			svm_ensemble.info("Reaching maximal iterations in two-class probability estimates\n");
		probAB[0] = A;
		probAB[1] = B;
	}

	private static double sigmoid_predict(double decision_value, double A,
			double B) {
		double fApB = decision_value * A + B;
		if (fApB >= 0)
			return Math.exp(-fApB) / (1.0 + Math.exp(-fApB));
		else
			return 1.0 / (1 + Math.exp(fApB));
	}

	// Method 2 from the multiclass_prob paper by Wu, Lin, and Weng
	private static void multiclass_probability(int k, double[][] r, double[] p) {
		int t, j;
		int iter = 0, max_iter = Math.max(100, k);
		double[][] Q = new double[k][k];
		double[] Qp = new double[k];
		double pQp, eps = 0.005 / k;

		for (t = 0; t < k; t++) {
			p[t] = 1.0 / k; // Valid if k = 1
			Q[t][t] = 0;
			for (j = 0; j < t; j++) {
				Q[t][t] += r[j][t] * r[j][t];
				Q[t][j] = Q[j][t];
			}
			for (j = t + 1; j < k; j++) {
				Q[t][t] += r[j][t] * r[j][t];
				Q[t][j] = -r[j][t] * r[t][j];
			}
		}
		for (iter = 0; iter < max_iter; iter++) {
			// stopping condition, recalculate QP,pQP for numerical accuracy
			pQp = 0;
			for (t = 0; t < k; t++) {
				Qp[t] = 0;
				for (j = 0; j < k; j++)
					Qp[t] += Q[t][j] * p[j];
				pQp += p[t] * Qp[t];
			}
			double max_error = 0;
			for (t = 0; t < k; t++) {
				double error = Math.abs(Qp[t] - pQp);
				if (error > max_error)
					max_error = error;
			}
			if (max_error < eps)
				break;

			for (t = 0; t < k; t++) {
				double diff = (-Qp[t] + pQp) / Q[t][t];
				p[t] += diff;
				pQp = (pQp + diff * (diff * Q[t][t] + 2 * Qp[t])) / (1 + diff)
						/ (1 + diff);
				for (j = 0; j < k; j++) {
					Qp[j] = (Qp[j] + diff * Q[t][j]) / (1 + diff);
					p[j] /= (1 + diff);
				}
			}
		}
		if (iter >= max_iter)
			svm_ensemble.info("Exceeds max_iter in multiclass_prob\n");
	}

	// Cross-validation decision values for probability estimates
	private static void svm_binary_svc_probability(svm_problem prob,
			svm_parameter param, double Cp, double Cn, double[] probAB) {
		int i;
		int nr_fold = 5;
		int[] perm = new int[prob.l];
		double[] dec_values = new double[prob.l];

		// random shuffle
		for (i = 0; i < prob.l; i++)
			perm[i] = i;
		for (i = 0; i < prob.l; i++) {
			int j = i + rand.nextInt(prob.l - i);
			do {
				int _ = perm[i];
				perm[i] = perm[j];
				perm[j] = _;
			} while (false);
		}
		for (i = 0; i < nr_fold; i++) {
			int begin = i * prob.l / nr_fold;
			int end = (i + 1) * prob.l / nr_fold;
			int j, k;
			svm_problem subprob = new svm_problem();

			subprob.l = prob.l - (end - begin);
			subprob.x = new svm_node[subprob.l][];
			subprob.y = new double[subprob.l];

			k = 0;
			for (j = 0; j < begin; j++) {
				subprob.x[k] = prob.x[perm[j]];
				subprob.y[k] = prob.y[perm[j]];
				++k;
			}
			for (j = end; j < prob.l; j++) {
				subprob.x[k] = prob.x[perm[j]];
				subprob.y[k] = prob.y[perm[j]];
				++k;
			}
			int p_count = 0, n_count = 0;
			for (j = 0; j < k; j++)
				if (subprob.y[j] > 0)
					p_count++;
				else
					n_count++;

			if (p_count == 0 && n_count == 0)
				for (j = begin; j < end; j++)
					dec_values[perm[j]] = 0;
			else if (p_count > 0 && n_count == 0)
				for (j = begin; j < end; j++)
					dec_values[perm[j]] = 1;
			else if (p_count == 0 && n_count > 0)
				for (j = begin; j < end; j++)
					dec_values[perm[j]] = -1;
			else {
				svm_parameter subparam = (svm_parameter) param.clone();
				subparam.probability = 0;
				subparam.C = 1.0;
				subparam.nr_weight = 2;
				subparam.weight_label = new int[2];
				subparam.weight = new double[2];
				subparam.weight_label[0] = +1;
				subparam.weight_label[1] = -1;
				subparam.weight[0] = Cp;
				subparam.weight[1] = Cn;
				svm_model submodel = svm_train(subprob, subparam);
				for (j = begin; j < end; j++) {
					double[] dec_value = new double[1];
					svm_predict_values(submodel, prob.x[perm[j]], dec_value);
					dec_values[perm[j]] = dec_value[0];
					// ensure +1 -1 order; reason not using CV subroutine
					dec_values[perm[j]] *= submodel.label[0];
				}
			}
		}
		sigmoid_train(prob.l, dec_values, prob.y, probAB);
	}

	// Return parameter of a Laplace distribution
	private static double svm_svr_probability(svm_problem prob,
			svm_parameter param) {
		int i;
		int nr_fold = 5;
		double[] ymv = new double[prob.l];
		double mae = 0;

		svm_parameter newparam = (svm_parameter) param.clone();
		newparam.probability = 0;
		svm_cross_validation(prob, newparam, nr_fold, ymv);
		for (i = 0; i < prob.l; i++) {
			ymv[i] = prob.y[i] - ymv[i];
			mae += Math.abs(ymv[i]);
		}
		mae /= prob.l;
		double std = Math.sqrt(2 * mae * mae);
		int count = 0;
		mae = 0;
		for (i = 0; i < prob.l; i++)
			if (Math.abs(ymv[i]) > 5 * std)
				count = count + 1;
			else
				mae += Math.abs(ymv[i]);
		mae /= (prob.l - count);
		svm_ensemble.info("Prob. model for test data: target value = predicted value + z,\nz: Laplace distribution e^(-|z|/sigma)/(2sigma),sigma="
				+ mae + "\n");
		return mae;
	}

	// label: label name, start: begin of each class, count: #data of classes,
	// perm: indices to the original data
	// perm, length l, must be allocated before calling this subroutine
	/**
	 * 统计数据中的类别数目，并按照类别号进行重新排序，排序信息保存在perm[]中，perm[i]=j 表示排序后的第i个数据在原数据中排在第j个位置
	 * 同时将类别名class_label转化为一个label_id以方便后来操作
	 * 
	 * Group training data of the same class This function transform the data
	 * from label_name to a form of label_order Labels are ordered by their
	 * first occurrence in the training set.
	 * 
	 * @param prob
	 *            svm_problem
	 * @param nr_class_ret
	 *            nr_class_ret[0] = nr_class;
	 *            存储总的类别的个数  Store the name of class label
	 * @param label_ret
	 *            label_ret[0] = label;
	 *            label[i]存储第i类的类别名字 label[i]
	 * @param start_ret
	 * 			  start_ret[0] = start;
	 *			      存储每个类别数据的起始位置
	 * @param count_ret
	 * 			  count_ret[0] = count;
	 *			      存储每个类别的数据个数
	 * @param perm
	 * 			  perm存储的是按照类别排序后的信息
	 *            perm[i]=j 表示排序后的第i个数据在原数据中排在第j个位置
	 */
	private static void svm_group_classes(svm_problem prob, int[] nr_class_ret,
			int[][] label_ret, int[][] start_ret, int[][] count_ret, int[] perm) {
		int l = prob.l;
		int max_nr_class = 16;
		int nr_class = 0;// 存储总共有多少个类别 store the number of class
		int[] label = new int[max_nr_class]; // label[i]存储第i类的类别名字 label[i]
												// stores the label names of the
												// i th class
		int[] count = new int[max_nr_class]; // count[i]存储第i个类别的数据个数 count[i]
												// stores the count of the i th
												// class
		int[] data_label = new int[l]; // data_label[i]存储第i个数据的类别序号id
										// data_label[i] stores the order number
										// of class label
		int i;

		/*
		 * 遍历数据，并统计每一个类别的数据个数 traverse every data, and count the number of data
		 * for each class
		 */
		for (i = 0; i < l; i++) {
			int this_label = (int) (prob.y[i]);// 这行数据的类别名称 label_name
			int j;
			/*
			 * 与label[]中的名称逐一比较 如果相同则表示这个类标曾经出现过，则直接添加这个类的统计个数 可以用hash简化代码逻辑
			 * if this_label appeared before, add the count directly
			 */
			for (j = 0; j < nr_class; j++) {
				if (this_label == label[j]) {
					++count[j];
					break;
				}
			}
			/*
			 * 否则这个类标第一次出现，给当前数据的data_label[i]设为j
			 */
			data_label[i] = j;
			
			/*
			 *  如果j == nr_class 说明该类标第一次出现
			 *  max_nr_class、label[]和count[]按照需要增大
			 *	if this_label first appears
			 */
			if (j == nr_class) {
				if (nr_class == max_nr_class) {
					max_nr_class *= 2;
					int[] new_data = new int[max_nr_class];
					System.arraycopy(label, 0, new_data, 0, label.length);
					label = new_data;
					new_data = new int[max_nr_class];
					System.arraycopy(count, 0, new_data, 0, count.length);
					count = new_data;
				}
				//给新的类别设上 label_name[]和 label_count[],总类别数++
				label[nr_class] = this_label;
				count[nr_class] = 1;
				++nr_class;
			}
		}

		//
		// Labels are ordered by their first occurrence in the training set.
		// However, for two-class sets with -1/+1 labels and -1 appears first,
		// we swap labels to ensure that internally the binary SVM has positive
		// data corresponding to the +1 instances.
		//
		/**
		 * 对于2类问题（-1/+1）进行点额外处理
		 * a special processing for two-class sets with -1/+1 labels
		 */
		if (nr_class == 2 && label[0] == -1 && label[1] == +1) {
			do {
				int _ = label[0];
				label[0] = label[1];
				label[1] = _;
			} while (false);
			do {
				int _ = count[0];
				count[0] = count[1];
				count[1] = _;
			} while (false);
			for (i = 0; i < l; i++) {
				if (data_label[i] == 0)
					data_label[i] = 1;
				else
					data_label[i] = 0;
			}
		}

		int[] start = new int[nr_class];
		start[0] = 0;

		/*
		 * 计算每个类别的数据的起始位置,用于处理perm数组
		 *  for each class, next_class_start_index = last_class_start_index + last_class_length
		 */
		for (i = 1; i < nr_class; i++)
			start[i] = start[i - 1] + count[i - 1];

		/*
		 *  将所有的数据按照类别的序号进行排序
		 *  perm[i]=j 表示排序后的第i个数据在原数据中排在第j个位置
		 *  sort the data according their class_order
 		 *  perm[i]=j means j is the data's original index in training data
		 */
		for (i = 0; i < l; i++) {
			perm[start[data_label[i]]] = i;
			++start[data_label[i]];
		}
		/*
		 *  重新计算start[]，作为返回
		 *  refresh the start array, for each class, next_class_start_index = last_class_start_index + last_class_length
		 */
		start[0] = 0;
		for (i = 1; i < nr_class; i++)
			start[i] = start[i - 1] + count[i - 1];

		/*
		 * nr_class_ret[0]存储总的类别个数
		 */
		nr_class_ret[0] = nr_class; // number of class
		/*
		 * label_ret[0] = label;存储在原数据上（未排序的）每个数据的label_id
		 */
		label_ret[0] = label; // label[i] stores the label names of the i th class
		start_ret[0] = start;//存储每个类别数据的起始位置
		count_ret[0] = count;//存储每个类别的数据个数
	}


	/**
	 * ensemble训练，对数据重新抽样并生成num_classifier个小分类器
	 * 1. 对训练数据进行重新抽样，生成num_classifier组新的数据，保证从每个类别均匀抽样并且抽样后每个类别的训练数据基本相等，保证每个类别中的数据大于一定数目
	 * 2. 针对每组抽样训练出新的分类器
	 * TODO 这里用了通用的param，但是每个子分类器可以有独立的参数
	 */
	public static svm_model[] svm_train_ensemble(svm_problem prob, svm_parameter param) {

		/**
		 * 抽样生成每个子问题
		 */
		svm_problem[] sub_problems = ensembleSampling(prob, param.minimalDataSize, param.classifierNumber);

		/**
		 * sample新的均匀的子数据集之后，训练出对应的模型
		 */
		svm_model[] models = new svm_model[param.classifierNumber];
		for(int i = 0; i < param.classifierNumber; i++){
			models[i] = new svm_model();
			models[i] = svm_train_one_ensemble(sub_problems[i], param);
		}

		return models;
	}

	private static svm_problem[] ensembleSampling(svm_problem prob, int minimalDataSize, int num_classifier) {
		svm_problem[] sub_problems = new svm_problem[num_classifier];
		/**
		 * 统计全部数据的类别分布
		 */
		HashMap<Double, ArrayList<svm_node[]>> classDataMap = new HashMap<Double, ArrayList<svm_node[]>>();
		for(int i = 0; i < prob.l; i++)
			if(!classDataMap.containsKey(prob.y[i])){
				classDataMap.put(prob.y[i], new ArrayList<svm_node[]>());
				classDataMap.get(prob.y[i]).add(prob.x[i]);
			}else
				classDataMap.get(prob.y[i]).add(prob.x[i]);
		/**
		 * 抽样生成num_classifier个子问题
		 */
		int temp_size = 0;
		int temp_start = 0;
		svm_node temp_svm_node[][];
		for(int i = 0; i < num_classifier; i++){
			sub_problems[i] = new svm_problem();
			for(Map.Entry<Double, ArrayList<svm_node[]>>entry : classDataMap.entrySet()){
				//如果该类别下的实例个数少于要求的最小数目，则将该类别中所有数据加入
				temp_size = entry.getValue().size()<minimalDataSize?entry.getValue().size():minimalDataSize;
				sub_problems[i].l += temp_size;
			}
			sub_problems[i].x = new svm_node[sub_problems[i].l][];
			sub_problems[i].y = new double[sub_problems[i].l];
			temp_start = 0;
			for(Map.Entry<Double, ArrayList<svm_node[]>>entry : classDataMap.entrySet()){
				//如果该类别下的实例个数少于要求的最小数目，则将该类别中所有数据加入
				temp_size = entry.getValue().size()<minimalDataSize?entry.getValue().size():minimalDataSize;
				//抽样对应个数的数据
				temp_svm_node = sampleSvm_Node(entry.getValue(), temp_size);
				//将抽样出的数据加入子问题中
				for(int j = 0; j < temp_size; j++){
					sub_problems[i].x[temp_start+j] = temp_svm_node[j];
					sub_problems[i].y[temp_start+j] = entry.getKey();
				}
				temp_start += temp_size;
			}
		}
		return sub_problems;
	}

	private static svm_node[][] sampleSvm_Node(ArrayList<svm_node[]> svm_nodes, int temp_size) {
		Collections.shuffle(svm_nodes);
		svm_node[][] returnArray = new svm_node[temp_size][];
		for(int i = 0; i < temp_size; i++)
			returnArray[i] = svm_nodes.get(i);
		return returnArray;
	}

	/**
	 * 在新的均匀的子训练数据上进行训练，只改写了分类部分
	 * @param prob 子训练数据
	 * @param param 分类的参数
	 * @return 对应的svm模型
	 */
	public static svm_model svm_train_one_ensemble(svm_problem prob, svm_parameter param){
		svm_model model = new svm_model();
		model.param = param;
		/**
		 * 回归问题
		 */
		if (param.svm_type == svm_parameter.ONE_CLASS
				|| param.svm_type == svm_parameter.EPSILON_SVR
				|| param.svm_type == svm_parameter.NU_SVR) {
			// regression or one-class-classify
			model.nr_class = 2;
			model.label = null;
			model.nSV = null;
			model.probA = null;
			model.probB = null;
			model.sv_coef = new double[1][];

			if (param.probability == 1
					&& (param.svm_type == svm_parameter.EPSILON_SVR || param.svm_type == svm_parameter.NU_SVR)) {
				model.probA = new double[1];
				model.probA[0] = svm_svr_probability(prob, param);
			}

			decision_function f = svm_train_one(prob, param, 0, 0);
			model.rho = new double[1];
			model.rho[0] = f.rho;

			int nSV = 0;
			int i;
			for (i = 0; i < prob.l; i++)
				if (Math.abs(f.alpha[i]) > 0)
					++nSV;
			model.l = nSV;
			model.SV = new svm_node[nSV][];
			model.sv_coef[0] = new double[nSV];
			model.sv_indices = new int[nSV];
			int j = 0;
			for (i = 0; i < prob.l; i++)
				if (Math.abs(f.alpha[i]) > 0) {
					model.SV[j] = prob.x[i];
					model.sv_coef[0][j] = f.alpha[i];
					model.sv_indices[j] = i + 1;
					++j;
				}
		}
		/**
		 * 分类问题
		 */
		else {
			int l = prob.l;//l为分类数据的个数
			// in each of arrays below, array[0] store the number of items
			int[] tmp_nr_class = new int[1];// store the label name of class
			int[][] tmp_label = new int[1][];
			int[][] tmp_start = new int[1][];
			int[][] tmp_count = new int[1][];

			/*
			 * perm存储的是按照类别排序后的信息
			 * perm[i]=j 表示排序后的第i个数据在原数据中排在第j个位置
			 */
			int[] perm = new int[l];

			/**
			 * 处理数据，将训练数据按照类别分组，并重新排序，将[排序后]to[排序前]的映射关系存储在perm[]中
			 *  group training data of the same class
			 */
			svm_group_classes(prob, tmp_nr_class, tmp_label, tmp_start,
					tmp_count, perm);

			int nr_class = tmp_nr_class[0]; // 存储总的类别的个数 number of class
			int[] label = tmp_label[0]; // label[i] 存储第i个类别的label_name
			int[] start = tmp_start[0];//存储每个类别数据的起始位置
			int[] count = tmp_count[0];//存储每个类别的数据个数

			if (nr_class == 1)
				svm_ensemble.info("WARNING: training data in only one class. See README for details.\n");

			svm_node[][] x = new svm_node[l][];

			/**
			 * 按照类别重新排序，将特征存储到新的数组x[]中
			 */
			int i;
			for (i = 0; i < l; i++)
				x[i] = prob.x[perm[i]];

			/**
			 * 计算每个类别的权重C
			 * calculate weighted C
			 */
			double[] weighted_C = new double[nr_class];
			//默认都设为一样的权重 C
			for (i = 0; i < nr_class; i++)
				weighted_C[i] = param.C;
			//如果有指定某类别的权重C，则根据设置重新给其设C
			for (i = 0; i < param.nr_weight; i++) {
				int j;
				for (j = 0; j < nr_class; j++)
					if (param.weight_label[i] == label[j])
						break;
				if (j == nr_class)
					System.err.print("WARNING: class label "
							+ param.weight_label[i]
							+ " specified in weight is not found\n");
				else
					weighted_C[j] *= param.weight[i];
			}

			/**
			 * 两两进行二值分类，训练出k*(k-1)/2个决策函数
			 *  train k*(k-1)/2 models
			 */
			//记录下这个向量是否alpha为0，不为0的即支持向量
			boolean[] nonzero = new boolean[l];
			for (i = 0; i < l; i++)
				nonzero[i] = false;
			//实例化 k*(k-1)/2 个决策函数
			decision_function[] f = new decision_function[nr_class
					* (nr_class - 1) / 2];

			//实例化 k*(k-1)/2 个 问题A、B,对于概率预测有用
			double[] probA = null, probB = null;
			if (param.probability == 1) {
				probA = new double[nr_class * (nr_class - 1) / 2];
				probB = new double[nr_class * (nr_class - 1) / 2];
			}
			//生成的分类器的计数器，最后p=k*(k-1)/2
			int p = 0;
			//循环一个类别作为 正例类别
			for (i = 0; i < nr_class; i++)
				//循环另一个类别作为 负例类别
				for (int j = i + 1; j < nr_class; j++) {
					/**
					 * 新建一个problem，将两个类别的数据合在一起
					 */
					svm_problem sub_prob = new svm_problem();
					//找到两个类别子集的起始和结束位置
					int si = start[i], sj = start[j];
					int ci = count[i], cj = count[j];
					//将两个数据集合并
					sub_prob.l = ci + cj;
					sub_prob.x = new svm_node[sub_prob.l][];
					sub_prob.y = new double[sub_prob.l];
					int k;
					for (k = 0; k < ci; k++) {
						sub_prob.x[k] = x[si + k];
						sub_prob.y[k] = +1;
					}
					for (k = 0; k < cj; k++) {
						sub_prob.x[ci + k] = x[sj + k];
						sub_prob.y[ci + k] = -1;
					}

					//如果是预测概率（默认为0）,则进行二元的概率svc训练，并记录下probAB
					if (param.probability == 1) {
						double[] probAB = new double[2];
						svm_binary_svc_probability(sub_prob, param,
								weighted_C[i], weighted_C[j], probAB);
						probA[p] = probAB[0];
						probB[p] = probAB[1];
					}

					/**
					 * 进行二类分类svm_train_one(),返回一个决策函数
					 */
					f[p] = svm_train_one(sub_prob, param, weighted_C[i],
							weighted_C[j]);
					/*
					 * 循环找每一个数据看f[p].alpha[k]是否是非零
					 * 大部分的alpha是为零的，w只是少数数据的线性组合
					 * 具有非零alpha的向量称为支持向量
					 */
					for (k = 0; k < ci; k++)
						if (!nonzero[si + k] && Math.abs(f[p].alpha[k]) > 0)
							nonzero[si + k] = true;
					for (k = 0; k < cj; k++)
						if (!nonzero[sj + k] && Math.abs(f[p].alpha[ci + k]) > 0)
							nonzero[sj + k] = true;
					++p;
				}

			/**
			 * 构建输出，保存下model的各个信息
			 * build output
 			 */
			model.nr_class = nr_class;

			model.label = new int[nr_class];
			for (i = 0; i < nr_class; i++)
				model.label[i] = label[i];

			model.rho = new double[nr_class * (nr_class - 1) / 2];
			for (i = 0; i < nr_class * (nr_class - 1) / 2; i++)
				model.rho[i] = f[i].rho;

			//如果要求输出概率，则记录下来probA和probB
			if (param.probability == 1) {
				model.probA = new double[nr_class * (nr_class - 1) / 2];
				model.probB = new double[nr_class * (nr_class - 1) / 2];
				for (i = 0; i < nr_class * (nr_class - 1) / 2; i++) {
					model.probA[i] = probA[i];
					model.probB[i] = probB[i];
				}
			} else {
				model.probA = null;
				model.probB = null;
			}
			//Total nSV
			int nnz = 0;
			//每个类别下支持向量的个数nSV
			int[] nz_count = new int[nr_class];
			model.nSV = new int[nr_class];
			for (i = 0; i < nr_class; i++) {
				int nSV = 0;
				for (int j = 0; j < count[i]; j++)
					if (nonzero[start[i] + j]) {
						++nSV;
						++nnz;
					}
				model.nSV[i] = nSV;
				nz_count[i] = nSV;
			}

			svm_ensemble.info("Total nSV = " + nnz + "\n");

			model.l = nnz;
			//保存下支持向量的信息
			model.SV = new svm_node[nnz][];
			model.sv_indices = new int[nnz];
			p = 0;
			for (i = 0; i < l; i++)
				if (nonzero[i]) {
					model.SV[p] = x[i];
					model.sv_indices[p++] = perm[i] + 1;
				}

			int[] nz_start = new int[nr_class];
			nz_start[0] = 0;
			for (i = 1; i < nr_class; i++)
				nz_start[i] = nz_start[i - 1] + nz_count[i - 1];

			/*
			 * 计算出支持向量在决策函数中的系数
			 */
			model.sv_coef = new double[nr_class - 1][];
			for (i = 0; i < nr_class - 1; i++)
				model.sv_coef[i] = new double[nnz];

			p = 0;
			for (i = 0; i < nr_class; i++)
				for (int j = i + 1; j < nr_class; j++) {
					// classifier (i,j): coefficients with
					// i are in sv_coef[j-1][nz_start[i]...],
					// j are in sv_coef[i][nz_start[j]...]

					int si = start[i];
					int sj = start[j];
					int ci = count[i];
					int cj = count[j];

					int q = nz_start[i];
					int k;
					for (k = 0; k < ci; k++)
						if (nonzero[si + k])
							model.sv_coef[j - 1][q++] = f[p].alpha[k];
					q = nz_start[j];
					for (k = 0; k < cj; k++)
						if (nonzero[sj + k])
							model.sv_coef[i][q++] = f[p].alpha[ci + k];
					++p;
				}
		}
		return model;
	}

	/**
	 * Interface functions
	 */
	public static svm_model svm_train(svm_problem prob, svm_parameter param) {
		svm_model model = new svm_model();
		model.param = param;

		/**
		 * 回归问题
		 */
		if (param.svm_type == svm_parameter.ONE_CLASS
				|| param.svm_type == svm_parameter.EPSILON_SVR
				|| param.svm_type == svm_parameter.NU_SVR) {
			// regression or one-class-classify
			model.nr_class = 2;
			model.label = null;
			model.nSV = null;
			model.probA = null;
			model.probB = null;
			model.sv_coef = new double[1][];

			if (param.probability == 1
					&& (param.svm_type == svm_parameter.EPSILON_SVR || param.svm_type == svm_parameter.NU_SVR)) {
				model.probA = new double[1];
				model.probA[0] = svm_svr_probability(prob, param);
			}

			decision_function f = svm_train_one(prob, param, 0, 0);
			model.rho = new double[1];
			model.rho[0] = f.rho;

			int nSV = 0;
			int i;
			for (i = 0; i < prob.l; i++)
				if (Math.abs(f.alpha[i]) > 0)
					++nSV;
			model.l = nSV;
			model.SV = new svm_node[nSV][];
			model.sv_coef[0] = new double[nSV];
			model.sv_indices = new int[nSV];
			int j = 0;
			for (i = 0; i < prob.l; i++)
				if (Math.abs(f.alpha[i]) > 0) {
					model.SV[j] = prob.x[i];
					model.sv_coef[0][j] = f.alpha[i];
					model.sv_indices[j] = i + 1;
					++j;
				}
		}
		/**
		 * 分类问题
		 */
		else {
			int l = prob.l;//l为分类数据的个数
			// in each of arrays below, array[0] store the number of items
			int[] tmp_nr_class = new int[1];// store the label name of class
			int[][] tmp_label = new int[1][];
			int[][] tmp_start = new int[1][];
			int[][] tmp_count = new int[1][];
			
			/*
			 * perm存储的是按照类别排序后的信息
			 * perm[i]=j 表示排序后的第i个数据在原数据中排在第j个位置
			 */
			int[] perm = new int[l];

			/**
			 * 处理数据，将训练数据按照类别分组，并重新排序，将[排序后]to[排序前]的映射关系存储在perm[]中
			 *  group training data of the same class
			 */
			svm_group_classes(prob, tmp_nr_class, tmp_label, tmp_start,
					tmp_count, perm);
			
			int nr_class = tmp_nr_class[0]; // 存储总的类别的个数 number of class
			int[] label = tmp_label[0]; // label[i] 存储第i个类别的label_name
			int[] start = tmp_start[0];//存储每个类别数据的起始位置
			int[] count = tmp_count[0];//存储每个类别的数据个数

			if (nr_class == 1)
				svm_ensemble.info("WARNING: training data in only one class. See README for details.\n");

			svm_node[][] x = new svm_node[l][];
			
			/**
			 * 按照类别重新排序，将特征存储到新的数组x[]中
			 */
			int i;
			for (i = 0; i < l; i++)
				x[i] = prob.x[perm[i]];

			/**
			 * 计算每个类别的权重C
			 * calculate weighted C
			 */
			double[] weighted_C = new double[nr_class];
			//默认都设为一样的权重 C
			for (i = 0; i < nr_class; i++)
				weighted_C[i] = param.C;
			//如果有指定某类别的权重C，则根据设置重新给其设C
			for (i = 0; i < param.nr_weight; i++) {
				int j;
				for (j = 0; j < nr_class; j++)
					if (param.weight_label[i] == label[j])
						break;
				if (j == nr_class)
					System.err.print("WARNING: class label "
							+ param.weight_label[i]
							+ " specified in weight is not found\n");
				else
					weighted_C[j] *= param.weight[i];
			}

			/**
			 * 两两进行二值分类，训练出k*(k-1)/2个模型
			 *  train k*(k-1)/2 models
			 */
			boolean[] nonzero = new boolean[l];
			for (i = 0; i < l; i++)
				nonzero[i] = false;
			//实例化 k*(k-1)/2 个决策函数
			decision_function[] f = new decision_function[nr_class
					* (nr_class - 1) / 2];

			//实例化 k*(k-1)/2 个 问题A、B,对于概率预测有用
			double[] probA = null, probB = null;
			if (param.probability == 1) {
				probA = new double[nr_class * (nr_class - 1) / 2];
				probB = new double[nr_class * (nr_class - 1) / 2];
			}

			int p = 0;
			
			//循环一个类别作为 正例类别
			for (i = 0; i < nr_class; i++)
				//循环另一个类别作为 负例类别
				for (int j = i + 1; j < nr_class; j++) {
					svm_problem sub_prob = new svm_problem();
					//找到两个类别子集的起始和结束位置
					int si = start[i], sj = start[j];
					int ci = count[i], cj = count[j];
					//将两个数据集合并并进行训练
					sub_prob.l = ci + cj;
					sub_prob.x = new svm_node[sub_prob.l][];
					sub_prob.y = new double[sub_prob.l];
					int k;
					for (k = 0; k < ci; k++) {
						sub_prob.x[k] = x[si + k];
						sub_prob.y[k] = +1;
					}
					for (k = 0; k < cj; k++) {
						sub_prob.x[ci + k] = x[sj + k];
						sub_prob.y[ci + k] = -1;
					}

					//如果是预测概率（默认为0）,则进行二元的概率svc训练
					if (param.probability == 1) {
						double[] probAB = new double[2];
						svm_binary_svc_probability(sub_prob, param,
								weighted_C[i], weighted_C[j], probAB);
						probA[p] = probAB[0];
						probB[p] = probAB[1];
					}

					//进行二类分类svm_train_one(),返回一个决策函数
					f[p] = svm_train_one(sub_prob, param, weighted_C[i],
							weighted_C[j]);
					/*
					 * 循环找每一个数据看f[p].alpha[k]是否是非零
					 * 大部分的lapha是为零的，w只是少数数据的线性组合
					 * 具有非零alpha的向量称为支持向量
					 */
					for (k = 0; k < ci; k++)
						if (!nonzero[si + k] && Math.abs(f[p].alpha[k]) > 0)
							nonzero[si + k] = true;
					for (k = 0; k < cj; k++)
						if (!nonzero[sj + k] && Math.abs(f[p].alpha[ci + k]) > 0)
							nonzero[sj + k] = true;
					++p;
				}

			// build output

			model.nr_class = nr_class;

			model.label = new int[nr_class];
			for (i = 0; i < nr_class; i++)
				model.label[i] = label[i];

			model.rho = new double[nr_class * (nr_class - 1) / 2];
			for (i = 0; i < nr_class * (nr_class - 1) / 2; i++)
				model.rho[i] = f[i].rho;

			if (param.probability == 1) {
				model.probA = new double[nr_class * (nr_class - 1) / 2];
				model.probB = new double[nr_class * (nr_class - 1) / 2];
				for (i = 0; i < nr_class * (nr_class - 1) / 2; i++) {
					model.probA[i] = probA[i];
					model.probB[i] = probB[i];
				}
			} else {
				model.probA = null;
				model.probB = null;
			}

			int nnz = 0;
			int[] nz_count = new int[nr_class];
			model.nSV = new int[nr_class];
			for (i = 0; i < nr_class; i++) {
				int nSV = 0;
				for (int j = 0; j < count[i]; j++)
					if (nonzero[start[i] + j]) {
						++nSV;
						++nnz;
					}
				model.nSV[i] = nSV;
				nz_count[i] = nSV;
			}

			svm_ensemble.info("Total nSV = " + nnz + "\n");

			model.l = nnz;
			model.SV = new svm_node[nnz][];
			model.sv_indices = new int[nnz];
			p = 0;
			for (i = 0; i < l; i++)
				if (nonzero[i]) {
					model.SV[p] = x[i];
					model.sv_indices[p++] = perm[i] + 1;
				}

			int[] nz_start = new int[nr_class];
			nz_start[0] = 0;
			for (i = 1; i < nr_class; i++)
				nz_start[i] = nz_start[i - 1] + nz_count[i - 1];

			model.sv_coef = new double[nr_class - 1][];
			for (i = 0; i < nr_class - 1; i++)
				model.sv_coef[i] = new double[nnz];

			p = 0;
			for (i = 0; i < nr_class; i++)
				for (int j = i + 1; j < nr_class; j++) {
					// classifier (i,j): coefficients with
					// i are in sv_coef[j-1][nz_start[i]...],
					// j are in sv_coef[i][nz_start[j]...]

					int si = start[i];
					int sj = start[j];
					int ci = count[i];
					int cj = count[j];

					int q = nz_start[i];
					int k;
					for (k = 0; k < ci; k++)
						if (nonzero[si + k])
							model.sv_coef[j - 1][q++] = f[p].alpha[k];
					q = nz_start[j];
					for (k = 0; k < cj; k++)
						if (nonzero[sj + k])
							model.sv_coef[i][q++] = f[p].alpha[ci + k];
					++p;
				}
		}
		return model;
	}

	/**
	 * 执行分层交叉检验 Stratified cross validation
	 *
	 * @param prob
	 * @param param
	 * @param nr_fold
	 * @param target
	 */
	public static void svm_cross_validation(svm_problem prob,
			svm_parameter param, int nr_fold, double[] target) {
		int i;
		// 记录每一折数据的开始位置
		int[] fold_start = new int[nr_fold + 1];
		int l = prob.l;
		/*
		 * perm存储的是按照类别排序后的信息
		 * perm[i]=j 表示排序后的第i个数据在原数据中排在第j个位置
		 * 再后来perm表示一个 [按fold排序]到[按类别排序]的映射
		 */
		int[] perm = new int[l];

		// stratified cv may not give leave-one-out rate
		// Each class to l folds -> some folds may have zero elements
		/**
		 * 1. 数据准备：处理分组数据(按类别重排序并等分)，将数据分成nr_fold份并对应组合
		 */
		// 1.1 如果要执行的是分类问题
		if ((param.svm_type == svm_parameter.C_SVC || param.svm_type == svm_parameter.NU_SVC)
				&& nr_fold < l)// fold number should less than number of data
		{
			int[] tmp_nr_class = new int[1];
			int[][] tmp_label = new int[1][];
			int[][] tmp_start = new int[1][];
			int[][] tmp_count = new int[1][];

			/**
			 * 统计数据中的class信息并将class_label映射为一个class_id，并进行排序
			 * 排序信息保存在perm[]中，perm存储的是perm[i]=j 表示排序后的第i个数据在原数据中排在第j个位置
			 */
			svm_group_classes(prob, tmp_nr_class, tmp_label, tmp_start,
					tmp_count, perm);

			int nr_class = tmp_nr_class[0];//存储总的类别的个数
			int[] start = tmp_start[0];//存储每个类别数据的起始位置
			int[] count = tmp_count[0];//存储每个类别的数据个数

			/**
			 *  对每一个类别内的数据进行重新排序
			 *  random shuffle
			 */
			int[] fold_count = new int[nr_fold];
			int c;
			int[] index = new int[l];
			for (i = 0; i < l; i++)
				index[i] = perm[i];
			/*
			 * 遍历每一个类别，并将类别内的元素进行重排序
			 */
			for (c = 0; c < nr_class; c++)
				//遍历类别内的每一个元素
				for (i = 0; i < count[c]; i++) {
					//随机找到该类别下标范围内的另一个元素，与当前的元素交换
					int j = i + rand.nextInt(count[c] - i);
					do {
						int _ = index[start[c] + j];
						index[start[c] + j] = index[start[c] + i];
						index[start[c] + i] = _;
					} while (false);
				}
			//计算每一折内的元素个数
			for (i = 0; i < nr_fold; i++) {
				fold_count[i] = 0;
				//第i折的数据个数等于 sum(每一类数据数目/折数)
				for (c = 0; c < nr_class; c++)
					fold_count[i] += (i + 1) * count[c] / nr_fold - i
							* count[c] / nr_fold;
			}
			//计算每一折的起始位置，第i折的起始位置=第i-1折的位置 + 第i-1折的个数
			fold_start[0] = 0;
			for (i = 1; i <= nr_fold; i++)
				fold_start[i] = fold_start[i - 1] + fold_count[i - 1];
			
			/**
			 *  使用perm[]，对数据按照fold进行分组
			 *  即每一个类别的数据均分到每一个fold中，perm[]的下标按照 group 1,group 2,..,group nr的顺序组织，perm[i]指向的就是均匀分配到的某一个类别的数据
			 *  data grouped by fold using the array perm
			 */
			//遍历每一类
			for (c = 0; c < nr_class; c++)
				//遍历每一折
				for (i = 0; i < nr_fold; i++) {
					//确定第i折数据在第c类中的起始位置和结束位置
					int begin = start[c] + i * count[c] / nr_fold;
					int end = start[c] + (i + 1) * count[c] / nr_fold;
					// perm表示一个 [按fold排序]到[按类别排序]的映射
					for (int j = begin; j < end; j++) {
						perm[fold_start[i]] = index[j];
						fold_start[i]++;
					}
				}
			//重新刷新fold_start[]，指明每一个fold内数据所属的范围
			fold_start[0] = 0;
			for (i = 1; i <= nr_fold; i++)
				fold_start[i] = fold_start[i - 1] + fold_count[i - 1];
		}
		// 1.2 如果要执行的是回归问题等
		else {
			for (i = 0; i < l; i++)
				perm[i] = i;
			for (i = 0; i < l; i++) {
				int j = i + rand.nextInt(l - i);
				do {
					int _ = perm[i];
					perm[i] = perm[j];
					perm[j] = _;
				} while (false);
			}
			for (i = 0; i <= nr_fold; i++)
				fold_start[i] = i * l / nr_fold;
		}

		/**
		 * 2. 执行nr_fold次循环，使用n-1折数据作为训练数据并训练模型，对剩下的一折数据进行预测
		 */
		//执行nr_fold次循环
		for (i = 0; i < nr_fold; i++) {
			/**
			 * 2.1 数据准备，生成对应的子问题
			 */
			// 确定第i折数据在总数据中的开始位置和结束位置的下标
			int begin = fold_start[i];
			int end = fold_start[i + 1];
			int j, k;

			// 建立新的svm_problem，l为数据个数，y为类标，x为数据特征向量
			svm_problem subprob = new svm_problem();
			subprob.l = l - (end - begin);
			subprob.x = new svm_node[subprob.l][];
			subprob.y = new double[subprob.l];

			k = 0;
			// 把第i折之外的数据作为train_data输入到subprob中
			for (j = 0; j < begin; j++) {
				subprob.x[k] = prob.x[perm[j]];
				subprob.y[k] = prob.y[perm[j]];
				++k;
			}
			for (j = end; j < l; j++) {
				subprob.x[k] = prob.x[perm[j]];
				subprob.y[k] = prob.y[perm[j]];
				++k;
			}
			/**
			 * 2.2 在生成的子问题数据上进行训练。
			 * 这里修改为针对每一个分类器训练出若干个小分类器
			 */
			svm_model[] ensemble_models = svm_train_ensemble(subprob, param);

			/**
			 * 2.3 使用训练出的分类器对测试数据进行预测（我们走下边的分支）
 			 */
			// 是分类问题且param.probability==1（默认为0），则进行概率预测
			if (param.probability == 1
					&& (param.svm_type == svm_parameter.C_SVC || param.svm_type == svm_parameter.NU_SVC)) {
				double[] prob_estimates = new double[svm_get_nr_class(ensemble_models[0])];
				for (j = begin; j < end; j++)
					target[perm[j]] = svm_predict_probability(ensemble_models[0],
							prob.x[perm[j]], prob_estimates);
			}
			// 使用其它n-1折的数据训练出的模型，对待测试的数据test_data进行类别的预测
			else {
				for (j = begin; j < end; j++) {
					target[perm[j]] = svm_predict_ensemble(ensemble_models, prob.x[perm[j]]);
					//System.out.println(target[perm[j]]);
				}
			}


			/**
			 * 原来的生成训练器代码，为每一折数据的sub_prob生成一个svm_model
			 */
/*			
			// 在生成的n折子问题数据上进行训练
			svm_model submodel = svm_train(subprob, param);

			// （走下边的分支）是分类问题且param.probability==1（默认为0），则进行概率预测
			if (param.probability == 1
					&& (param.svm_type == svm_parameter.C_SVC || param.svm_type == svm_parameter.NU_SVC)) {
				double[] prob_estimates = new double[svm_get_nr_class(submodel)];
				for (j = begin; j < end; j++)
					target[perm[j]] = svm_predict_probability(submodel,
							prob.x[perm[j]], prob_estimates);
			}
			// 使用其它n-1折的数据训练出的模型，对待测试的数据test_data进行类别的预测
			else {
				for (j = begin; j < end; j++) {
					target[perm[j]] = svm_predict(submodel, prob.x[perm[j]]);
				}
			}*/
		}
	}

	public static int svm_get_svm_type(svm_model model) {
		return model.param.svm_type;
	}

	public static int svm_get_nr_class(svm_model model) {
		return model.nr_class;
	}

	public static void svm_get_labels(svm_model model, int[] label) {
		if (model.label != null)
			for (int i = 0; i < model.nr_class; i++)
				label[i] = model.label[i];
	}

	public static void svm_get_sv_indices(svm_model model, int[] indices) {
		if (model.sv_indices != null)
			for (int i = 0; i < model.l; i++)
				indices[i] = model.sv_indices[i];
	}

	public static int svm_get_nr_sv(svm_model model) {
		return model.l;
	}

	public static double svm_get_svr_probability(svm_model model) {
		if ((model.param.svm_type == svm_parameter.EPSILON_SVR || model.param.svm_type == svm_parameter.NU_SVR)
				&& model.probA != null)
			return model.probA[0];
		else {
			System.err
					.print("Model doesn't contain information for SVR probability inference\n");
			return 0;
		}
	}

	public static double svm_predict_values(svm_model model, svm_node[] x,
			double[] dec_values) {
		int i;
		if (model.param.svm_type == svm_parameter.ONE_CLASS
				|| model.param.svm_type == svm_parameter.EPSILON_SVR
				|| model.param.svm_type == svm_parameter.NU_SVR) {
			double[] sv_coef = model.sv_coef[0];
			double sum = 0;
			for (i = 0; i < model.l; i++)
				sum += sv_coef[i]
						* Kernel.k_function(x, model.SV[i], model.param);
			sum -= model.rho[0];
			dec_values[0] = sum;

			if (model.param.svm_type == svm_parameter.ONE_CLASS)
				return (sum > 0) ? 1 : -1;
			else
				return sum;
		} else {
			int nr_class = model.nr_class;
			int l = model.l;

			double[] kvalue = new double[l];
			for (i = 0; i < l; i++)
				kvalue[i] = Kernel.k_function(x, model.SV[i], model.param);

			int[] start = new int[nr_class];
			start[0] = 0;
			for (i = 1; i < nr_class; i++)
				start[i] = start[i - 1] + model.nSV[i - 1];

			int[] vote = new int[nr_class];
			for (i = 0; i < nr_class; i++)
				vote[i] = 0;

			int p = 0;
			for (i = 0; i < nr_class; i++)
				for (int j = i + 1; j < nr_class; j++) {
					double sum = 0;
					int si = start[i];
					int sj = start[j];
					int ci = model.nSV[i];
					int cj = model.nSV[j];

					int k;
					double[] coef1 = model.sv_coef[j - 1];
					double[] coef2 = model.sv_coef[i];
					for (k = 0; k < ci; k++)
						sum += coef1[si + k] * kvalue[si + k];
					for (k = 0; k < cj; k++)
						sum += coef2[sj + k] * kvalue[sj + k];
					sum -= model.rho[p];
					dec_values[p] = sum;

					if (dec_values[p] > 0)
						++vote[i];
					else
						++vote[j];
					p++;
				}

			int vote_max_idx = 0;
			for (i = 1; i < nr_class; i++)
				if (vote[i] > vote[vote_max_idx])
					vote_max_idx = i;

			return model.label[vote_max_idx];
		}
	}

	/**
	 * 根据ensemble分类器对数据进行预测
	 * @param models
	 * @param x
	 * @return
	 */
	public static double svm_predict_ensemble(svm_model[] models, svm_node[] x) {
		HashMap<Double, Integer> resultMap = new HashMap<Double, Integer>();
		double pred_result;
		for(svm_model model : models){
			pred_result = svm_predict(model, x);
			if(!resultMap.containsKey(pred_result))
				resultMap.put(pred_result,1);
			else
				resultMap.put(pred_result,resultMap.get(pred_result)+1);
		}
		double max_label = 0.0;
		int max_number = 0;
		for(Map.Entry<Double, Integer> entry : resultMap.entrySet())
			if(entry.getValue() > max_number){
				max_number = entry.getValue();
				max_label = entry.getKey();
			}
		return max_label;
	}


	public static double svm_predict(svm_model model, svm_node[] x) {
		int nr_class = model.nr_class;
		double[] dec_values;
		if (model.param.svm_type == svm_parameter.ONE_CLASS
				|| model.param.svm_type == svm_parameter.EPSILON_SVR
				|| model.param.svm_type == svm_parameter.NU_SVR)
			dec_values = new double[1];
		else
			dec_values = new double[nr_class * (nr_class - 1) / 2];
		double pred_result = svm_predict_values(model, x, dec_values);
		return pred_result;
	}

	public static double svm_predict_probability(svm_model model, svm_node[] x,
			double[] prob_estimates) {
		if ((model.param.svm_type == svm_parameter.C_SVC || model.param.svm_type == svm_parameter.NU_SVC)
				&& model.probA != null && model.probB != null) {
			int i;
			int nr_class = model.nr_class;
			double[] dec_values = new double[nr_class * (nr_class - 1) / 2];
			svm_predict_values(model, x, dec_values);

			double min_prob = 1e-7;
			double[][] pairwise_prob = new double[nr_class][nr_class];

			int k = 0;
			for (i = 0; i < nr_class; i++)
				for (int j = i + 1; j < nr_class; j++) {
					pairwise_prob[i][j] = Math.min(Math.max(
							sigmoid_predict(dec_values[k], model.probA[k],
									model.probB[k]), min_prob), 1 - min_prob);
					pairwise_prob[j][i] = 1 - pairwise_prob[i][j];
					k++;
				}
			multiclass_probability(nr_class, pairwise_prob, prob_estimates);

			int prob_max_idx = 0;
			for (i = 1; i < nr_class; i++)
				if (prob_estimates[i] > prob_estimates[prob_max_idx])
					prob_max_idx = i;
			return model.label[prob_max_idx];
		} else
			return svm_predict(model, x);
	}

	static final String svm_type_table[] = { "c_svc", "nu_svc", "one_class",
			"epsilon_svr", "nu_svr", };

	static final String kernel_type_table[] = { "linear", "polynomial", "rbf",
			"sigmoid", "precomputed" };

	public static void svm_save_model(String model_file_name, svm_model model)
			throws IOException {
		DataOutputStream fp = new DataOutputStream(new BufferedOutputStream(
				new FileOutputStream(model_file_name)));

		svm_parameter param = model.param;

		fp.writeBytes("svm_type " + svm_type_table[param.svm_type] + "\n");
		fp.writeBytes("kernel_type " + kernel_type_table[param.kernel_type]
				+ "\n");

		if (param.kernel_type == svm_parameter.POLY)
			fp.writeBytes("degree " + param.degree + "\n");

		if (param.kernel_type == svm_parameter.POLY
				|| param.kernel_type == svm_parameter.RBF
				|| param.kernel_type == svm_parameter.SIGMOID)
			fp.writeBytes("gamma " + param.gamma + "\n");

		if (param.kernel_type == svm_parameter.POLY
				|| param.kernel_type == svm_parameter.SIGMOID)
			fp.writeBytes("coef0 " + param.coef0 + "\n");

		int nr_class = model.nr_class;
		int l = model.l;
		fp.writeBytes("nr_class " + nr_class + "\n");
		fp.writeBytes("total_sv " + l + "\n");

		{
			fp.writeBytes("rho");
			for (int i = 0; i < nr_class * (nr_class - 1) / 2; i++)
				fp.writeBytes(" " + model.rho[i]);
			fp.writeBytes("\n");
		}

		if (model.label != null) {
			fp.writeBytes("label");
			for (int i = 0; i < nr_class; i++)
				fp.writeBytes(" " + model.label[i]);
			fp.writeBytes("\n");
		}

		if (model.probA != null) // regression has probA only
		{
			fp.writeBytes("probA");
			for (int i = 0; i < nr_class * (nr_class - 1) / 2; i++)
				fp.writeBytes(" " + model.probA[i]);
			fp.writeBytes("\n");
		}
		if (model.probB != null) {
			fp.writeBytes("probB");
			for (int i = 0; i < nr_class * (nr_class - 1) / 2; i++)
				fp.writeBytes(" " + model.probB[i]);
			fp.writeBytes("\n");
		}

		if (model.nSV != null) {
			fp.writeBytes("nr_sv");
			for (int i = 0; i < nr_class; i++)
				fp.writeBytes(" " + model.nSV[i]);
			fp.writeBytes("\n");
		}

		fp.writeBytes("SV\n");
		double[][] sv_coef = model.sv_coef;
		svm_node[][] SV = model.SV;

		for (int i = 0; i < l; i++) {
			for (int j = 0; j < nr_class - 1; j++)
				fp.writeBytes(sv_coef[j][i] + " ");

			svm_node[] p = SV[i];
			if (param.kernel_type == svm_parameter.PRECOMPUTED)
				fp.writeBytes("0:" + (int) (p[0].value));
			else
				for (int j = 0; j < p.length; j++)
					fp.writeBytes(p[j].index + ":" + p[j].value + " ");
			fp.writeBytes("\n");
		}

		fp.close();
	}

	private static double atof(String s) {
		return Double.valueOf(s).doubleValue();
	}

	private static int atoi(String s) {
		return Integer.parseInt(s);
	}

	private static boolean read_model_header(BufferedReader fp, svm_model model) {
		svm_parameter param = new svm_parameter();
		model.param = param;
		try {
			while (true) {
				String cmd = fp.readLine();
				String arg = cmd.substring(cmd.indexOf(' ') + 1);

				if (cmd.startsWith("svm_type")) {
					int i;
					for (i = 0; i < svm_type_table.length; i++) {
						if (arg.indexOf(svm_type_table[i]) != -1) {
							param.svm_type = i;
							break;
						}
					}
					if (i == svm_type_table.length) {
						System.err.print("unknown classify type.\n");
						return false;
					}
				} else if (cmd.startsWith("kernel_type")) {
					int i;
					for (i = 0; i < kernel_type_table.length; i++) {
						if (arg.indexOf(kernel_type_table[i]) != -1) {
							param.kernel_type = i;
							break;
						}
					}
					if (i == kernel_type_table.length) {
						System.err.print("unknown kernel function.\n");
						return false;
					}
				} else if (cmd.startsWith("degree"))
					param.degree = atoi(arg);
				else if (cmd.startsWith("gamma"))
					param.gamma = atof(arg);
				else if (cmd.startsWith("coef0"))
					param.coef0 = atof(arg);
				else if (cmd.startsWith("nr_class"))
					model.nr_class = atoi(arg);
				else if (cmd.startsWith("total_sv"))
					model.l = atoi(arg);
				else if (cmd.startsWith("rho")) {
					int n = model.nr_class * (model.nr_class - 1) / 2;
					model.rho = new double[n];
					StringTokenizer st = new StringTokenizer(arg);
					for (int i = 0; i < n; i++)
						model.rho[i] = atof(st.nextToken());
				} else if (cmd.startsWith("label")) {
					int n = model.nr_class;
					model.label = new int[n];
					StringTokenizer st = new StringTokenizer(arg);
					for (int i = 0; i < n; i++)
						model.label[i] = atoi(st.nextToken());
				} else if (cmd.startsWith("probA")) {
					int n = model.nr_class * (model.nr_class - 1) / 2;
					model.probA = new double[n];
					StringTokenizer st = new StringTokenizer(arg);
					for (int i = 0; i < n; i++)
						model.probA[i] = atof(st.nextToken());
				} else if (cmd.startsWith("probB")) {
					int n = model.nr_class * (model.nr_class - 1) / 2;
					model.probB = new double[n];
					StringTokenizer st = new StringTokenizer(arg);
					for (int i = 0; i < n; i++)
						model.probB[i] = atof(st.nextToken());
				} else if (cmd.startsWith("nr_sv")) {
					int n = model.nr_class;
					model.nSV = new int[n];
					StringTokenizer st = new StringTokenizer(arg);
					for (int i = 0; i < n; i++)
						model.nSV[i] = atoi(st.nextToken());
				} else if (cmd.startsWith("SV")) {
					break;
				} else {
					System.err.print("unknown text in model file: [" + cmd
							+ "]\n");
					return false;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static svm_model svm_load_model(String model_file_name)
			throws IOException {
		return svm_load_model(new BufferedReader(
				new FileReader(model_file_name)));
	}

	public static svm_model svm_load_model(BufferedReader fp)
			throws IOException {
		// read parameters

		svm_model model = new svm_model();
		model.rho = null;
		model.probA = null;
		model.probB = null;
		model.label = null;
		model.nSV = null;

		if (read_model_header(fp, model) == false) {
			System.err.print("ERROR: failed to read model\n");
			return null;
		}

		// read sv_coef and SV

		int m = model.nr_class - 1;
		int l = model.l;
		model.sv_coef = new double[m][l];
		model.SV = new svm_node[l][];

		for (int i = 0; i < l; i++) {
			String line = fp.readLine();
			StringTokenizer st = new StringTokenizer(line, " \t\n\r\f:");

			for (int k = 0; k < m; k++)
				model.sv_coef[k][i] = atof(st.nextToken());
			int n = st.countTokens() / 2;
			model.SV[i] = new svm_node[n];
			for (int j = 0; j < n; j++) {
				model.SV[i][j] = new svm_node();
				model.SV[i][j].index = atoi(st.nextToken());
				model.SV[i][j].value = atof(st.nextToken());
			}
		}

		fp.close();
		return model;
	}

	public static String svm_check_parameter(svm_problem prob,
			svm_parameter param) {
		// svm_type

		//判断是否有a和f参数，没有的话没法执行ensemble方法
		if(param.classifierNumber <= 0 || param.minimalDataSize <= 0)
			return "wrong classifierNumber or minimalDataSize";

		int svm_type = param.svm_type;
		if (svm_type != svm_parameter.C_SVC && svm_type != svm_parameter.NU_SVC
				&& svm_type != svm_parameter.ONE_CLASS
				&& svm_type != svm_parameter.EPSILON_SVR
				&& svm_type != svm_parameter.NU_SVR)
			return "unknown classify type";

		// kernel_type, degree

		int kernel_type = param.kernel_type;
		if (kernel_type != svm_parameter.LINEAR
				&& kernel_type != svm_parameter.POLY
				&& kernel_type != svm_parameter.RBF
				&& kernel_type != svm_parameter.SIGMOID
				&& kernel_type != svm_parameter.PRECOMPUTED)
			return "unknown kernel type";

		if (param.gamma < 0)
			return "gamma < 0";

		if (param.degree < 0)
			return "degree of polynomial kernel < 0";

		// cache_size,eps,C,nu,p,shrinking

		if (param.cache_size <= 0)
			return "cache_size <= 0";

		if (param.eps <= 0)
			return "eps <= 0";

		if (svm_type == svm_parameter.C_SVC
				|| svm_type == svm_parameter.EPSILON_SVR
				|| svm_type == svm_parameter.NU_SVR)
			if (param.C <= 0)
				return "C <= 0";

		if (svm_type == svm_parameter.NU_SVC
				|| svm_type == svm_parameter.ONE_CLASS
				|| svm_type == svm_parameter.NU_SVR)
			if (param.nu <= 0 || param.nu > 1)
				return "nu <= 0 or nu > 1";

		if (svm_type == svm_parameter.EPSILON_SVR)
			if (param.p < 0)
				return "p < 0";

		if (param.shrinking != 0 && param.shrinking != 1)
			return "shrinking != 0 and shrinking != 1";

		if (param.probability != 0 && param.probability != 1)
			return "probability != 0 and probability != 1";

		if (param.probability == 1 && svm_type == svm_parameter.ONE_CLASS)
			return "one-class SVM probability output not supported yet";

		// check whether nu-svc is feasible

		if (svm_type == svm_parameter.NU_SVC) {
			int l = prob.l;
			int max_nr_class = 16;
			int nr_class = 0;
			int[] label = new int[max_nr_class];
			int[] count = new int[max_nr_class];

			int i;
			for (i = 0; i < l; i++) {
				int this_label = (int) prob.y[i];
				int j;
				for (j = 0; j < nr_class; j++)
					if (this_label == label[j]) {
						++count[j];
						break;
					}

				if (j == nr_class) {
					if (nr_class == max_nr_class) {
						max_nr_class *= 2;
						int[] new_data = new int[max_nr_class];
						System.arraycopy(label, 0, new_data, 0, label.length);
						label = new_data;

						new_data = new int[max_nr_class];
						System.arraycopy(count, 0, new_data, 0, count.length);
						count = new_data;
					}
					label[nr_class] = this_label;
					count[nr_class] = 1;
					++nr_class;
				}
			}

			for (i = 0; i < nr_class; i++) {
				int n1 = count[i];
				for (int j = i + 1; j < nr_class; j++) {
					int n2 = count[j];
					if (param.nu * (n1 + n2) / 2 > Math.min(n1, n2))
						return "specified nu is infeasible";
				}
			}
		}

		return null;
	}

	public static int svm_check_probability_model(svm_model model) {
		if (((model.param.svm_type == svm_parameter.C_SVC || model.param.svm_type == svm_parameter.NU_SVC)
				&& model.probA != null && model.probB != null)
				|| ((model.param.svm_type == svm_parameter.EPSILON_SVR || model.param.svm_type == svm_parameter.NU_SVR) && model.probA != null))
			return 1;
		else
			return 0;
	}

	public static void svm_set_print_string_function(
			svm_print_interface print_func) {
		if (print_func == null)
			svm_print_string = svm_print_stdout;
		else
			svm_print_string = print_func;
	}
}

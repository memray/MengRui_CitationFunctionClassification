package edu.whu.irlab.classify.libsvm;

/**
 * 保存分类的基本信息，包括训练数据的个数，类标名称以及所有的特征向量
 */
public class svm_problem implements java.io.Serializable
{
	//save the number of training data
	public int l;
	//save names of class label
	public double[] y;
	//save feature vectors
	public svm_node[][] x;
}

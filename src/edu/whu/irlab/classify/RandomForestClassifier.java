package edu.whu.irlab.classify;

import java.io.File;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.converters.LibSVMLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;

public class RandomForestClassifier {
	public static void main(String[] args) {
		LibSVMLoader libSVMLoader = new LibSVMLoader();
		try {
			//读取vector数据
			libSVMLoader.setSource(new File("output\\our_feature.scale"));
			Instances originalData = libSVMLoader.getDataSet();
			//将数据的类别数据由numeric转化为nominal
	        NumericToNominal convert= new NumericToNominal();
	        String[] options= new String[2];
	        options[0]="-R";
	        options[1]= "1-47,"+(originalData.classIndex()+1)+"-"+(originalData.classIndex()+1);  //range of variables to make numeric

	        convert.setOptions(options);
	        convert.setInputFormat(originalData);

	        Instances dataSet = Filter.useFilter(originalData, convert);

			//-I, number of trees
			int I = 1000;
			//-K, Number of features to consider (<1=int(logM+1)).
			int K = 2 * (int) Math.sqrt(dataSet.numAttributes());
//			int K = (int) Math.log(dataSet.numAttributes()+1);
			//-S,  Seed for random number generator.  (default 1)
			int S = 0;
			//number of folds 
			int numFolds = 10;
			
			//-depth <num>  The maximum depth of the trees, 0 for unlimited. (default 0)
			//-O Don't calculate the out of bag error.
			//-print Print the individual trees in the output
			//-num-slots <num>	  Number of execution slots.  (default 1 - i.e. no parallelism)
			//-output-debug-info  If set, classifier is runFeatureSelectionOnGivenInstances in debug mode and  may output additional info to the console
			//-do-not-check-capabilities  If set, classifier capabilities are not checked before classifier is built (use with caution).
			Evaluation evaluation = new Evaluation(dataSet);
			String params = "-I "+I+" -K "+K+" -S "+S;
			evaluation.crossValidateModel("RandomForest", dataSet, numFolds, params.split(" "), new Random(1));
			System.out.println(evaluation.toSummaryString("\nResults\n======\n", true));
			System.out.println("Hold on");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}

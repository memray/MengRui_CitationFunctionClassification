package edu.whu.irlab.classify;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.io.FileUtils;

import edu.whu.irlab.classify.libsvm.svm;
import edu.whu.irlab.classify.libsvm.svm_ensemble;
import edu.whu.irlab.classify.libsvm.svm_model;
import edu.whu.irlab.classify.libsvm.svm_node;
import edu.whu.irlab.classify.libsvm.svm_parameter;
import edu.whu.irlab.classify.libsvm.svm_print_interface;
import edu.whu.irlab.classify.libsvm.svm_problem;
import edu.whu.irlab.dfki.feature.FeatureExtractUtils;

public strictfp class svm_train {
    private svm_parameter param; // set by parse_command_line()
    private svm_problem prob; // set by read_problem()
    private svm_model model;
    private String input_file_name; // set by parse_command_line()
    private String model_file_name; // set by parse_command_line()
    private String error_msg;
    private int cross_validation;// a boolean tells whether to do a cross_validation , set by parse_command_line()
    private int nr_fold;// the number of fold, set by parse_command_line()
    private String predictOutputFile;

    private static svm_print_interface svm_print_null = new svm_print_interface() {
        public void print(String s) {
        }
    };


    /**
     * 参数出错时输出帮助信息
     */
    private static void exit_with_help() {
        System.out
                .print("Usage: svm_train [options] training_set_file [model_file]\n"
                        + "options:\n"
                        + "-s svm_type : set type of SVM (default 0)\n"
                        + "	0 -- C-SVC		(multi-class classification)\n"
                        + "	1 -- nu-SVC		(multi-class classification)\n"
                        + "	2 -- one-class SVM\n"
                        + "	3 -- epsilon-SVR	(regression)\n"
                        + "	4 -- nu-SVR		(regression)\n"
                        + "-t kernel_type : set type of kernel function (default 2)\n"
                        + "	0 -- linear: u'*v\n"
                        + "	1 -- polynomial: (gamma*u'*v + coef0)^degree\n"
                        + "	2 -- radial basis function: exp(-gamma*|u-v|^2)\n"
                        + "	3 -- sigmoid: tanh(gamma*u'*v + coef0)\n"
                        + "	4 -- precomputed kernel (kernel values in training_set_file)\n"
                        + "-d degree : set degree in kernel function (default 3)\n"
                        + "-g gamma : set gamma in kernel function (default 1/num_features)\n"
                        + "-r coef0 : set coef0 in kernel function (default 0)\n"
                        + "-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)\n"
                        + "-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)\n"
                        + "-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)\n"
                        + "-m cachesize : set cache memory size in MB (default 100)\n"
                        + "-e epsilon : set tolerance of termination criterion (default 0.001)\n"
                        + "-h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)\n"
                        + "-b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)\n"
                        + "-wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)\n"
                        + "-v n : n-fold cross validation mode\n"
                        + "-q : quiet mode (no outputs)\n"
                        + "-a n : 设置每个抽样数据集中数据的个数\n"
                        + "-f n: 设置需要训练多少个ensemble分类器\n");
        System.exit(1);
    }

    /**
     * 执行n折交叉检验，及分成n组分别训练后再进行test
     */
    private void do_cross_validation() {
        double[] target = new double[prob.l];

        /**
         * 1. 训练并执行交叉检验
         */
        svm.svm_cross_validation(prob, param, nr_fold, target);

        /**
         * 2. 输出交叉检验的具体评价
         */
        do_evaluation(target);
    }

    private void do_cross_validation_ensemble() {
        double[] target = new double[prob.l];

        /**
         * 1. 训练并执行交叉检验
         */
        svm_ensemble.svm_cross_validation(prob, param, nr_fold, target);

        /**
         * 2. 输出交叉检验的具体评价
         */
        do_evaluation(target);
    }


    private void do_evaluation(double[] target) {
        int i;
        int total_correct = 0;
        double total_error = 0;
        double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
        // evaluate for regression
        if (param.svm_type == svm_parameter.EPSILON_SVR
                || param.svm_type == svm_parameter.NU_SVR) {
            for (i = 0; i < prob.l; i++) {
                double y = prob.y[i];
                double v = target[i];
                total_error += (v - y) * (v - y);
                sumv += v;
                sumy += y;
                sumvv += v * v;
                sumyy += y * y;
                sumvy += v * y;
            }
            System.out.print("Cross Validation Mean squared error = "
                    + total_error / prob.l + "\n");
            System.out
                    .print("Cross Validation Squared correlation coefficient = "
                            + ((prob.l * sumvy - sumv * sumy) * (prob.l * sumvy - sumv
                            * sumy))
                            / ((prob.l * sumvv - sumv * sumv) * (prob.l * sumyy - sumy
                            * sumy)) + "\n");
        }
        // evaluate for classification
        else {
            /*
             * 下面为我们增加的部分，增加了Macro-F的评价
			 */
            // 输出完整的评测结果，包括各个部分的正确情况以及Macro-F
            // 记录下每个类别总个数
            HashMap<Double, Integer> categoryS = new HashMap<Double, Integer>();

            // 记录下每个类别分正确的个数
            HashMap<Double, Integer> categoryR = new HashMap<Double, Integer>();

            // 记录下每个类别预测的个数
            HashMap<Double, Integer> categoryT = new HashMap<Double, Integer>();

            for (i = 0; i < prob.l; i++) {
                if (categoryS.containsKey(prob.y[i])) {
                    categoryS.put(prob.y[i], categoryS.get(prob.y[i]) + 1);
                } else {
                    categoryS.put(prob.y[i], 1);
                }

                if (categoryT.containsKey(target[i])) {
                    categoryT.put(target[i], categoryT.get(target[i]) + 1);
                } else {
                    categoryT.put(target[i], 1);
                }

                if (target[i] == prob.y[i]) {
                    ++total_correct;
                    if (categoryR.containsKey(prob.y[i])) {
                        categoryR.put(prob.y[i], categoryR.get(prob.y[i]) + 1);
                    } else {
                        categoryR.put(prob.y[i], 1);
                    }
                }
            }


            int k = 0;
            double Macro_F = 0.0;
            for (Double d : categoryS.keySet()) {
                double f = 0.0;
                if (categoryS.get(d)!=null && categoryR.get(d)!=null && categoryT.get(d)!=null)
                    f = FeatureExtractUtils.fScoreCompute(1.0 * categoryR.get(d)
                            / categoryT.get(d), 1.0 * categoryR.get(d)
                            / categoryS.get(d), 1.0);
                System.out.println("Class " + d + "\tF-measure :  " + f);
                if(categoryS.get(d)!=null && categoryR.get(d)!=null && categoryT.get(d)!=null) {
                    System.out.println("\tTotal: " + categoryS.get(d) + "\tRight: " + categoryR.get(d) + "\tAccuracy: " + (float) categoryR.get(d) / (float) categoryS.get(d));
                    System.out
                            .println("\tP: " + 1.0 * categoryR.get(d)
                                    / categoryT.get(d) + "\t" + "R: " + 1.0
                                    * categoryR.get(d) / categoryS.get(d) + "\t"
                                    + "F: " + f);
                }
                Macro_F += f;
                k++;
            }

            System.out.print("Cross Validation Accuracy = " + 100.0
                    * total_correct / prob.l + "%\n");
            System.out.println("Macro-F = " + Macro_F / k);

            /**
             * 将交叉检验的结果输出到指定文件中predictOutputFile
             */
            if (predictOutputFile != null && predictOutputFile.trim() != "") {
                File f = new File(predictOutputFile);
                try {
                    StringBuilder builder = new StringBuilder();
                    for (i = 0; i < prob.l; i++)
                        builder.append((int) target[i]).append("\n");
                    FileUtils.write(f, builder.toString(), false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 执行svm_train的具体逻辑操作
     */
    private void run(String argv[])
            throws IOException {
        //处理输入参数
        parse_command_line(argv);
        //读取数据并转化为一个svm_problem
        read_problem();
        error_msg = svm.svm_check_parameter(prob, param);

        if (error_msg != null) {
            System.err.print("ERROR: " + error_msg + "\n");
            System.exit(1);
        }
        //根据是否设置交叉检验来执行不同的操作
        // 在指定数据集上执行n折交叉检验 if cross_validation then do it, but with no model output
        if (cross_validation != 0) {
            do_cross_validation();
        }
        // 训练并保存模型 train and save the model
        else {
            model = svm.svm_train(prob, param);
            svm.svm_save_model(model_file_name, model);
        }
    }


    private void run_ensemble(String argv[])
            throws IOException {
        //处理输入参数
        parse_command_line(argv);
        //读取数据并转化为一个svm_problem
        read_problem();
        error_msg = svm.svm_check_parameter(prob, param);

        if (error_msg != null) {
            System.err.print("ERROR: " + error_msg + "\n");
            System.exit(1);
        }
        //根据是否设置交叉检验来执行不同的操作
        // 在指定数据集上执行n折交叉检验 if cross_validation then do it, but with no model output
        if (cross_validation != 0) {
            do_cross_validation_ensemble();
        }
        // 训练并保存模型 train and save the model
        else {
            model = svm.svm_train(prob, param);
            svm.svm_save_model(model_file_name, model);
        }
    }

    public static void main(String argv[]) throws IOException {
        svm_train t = new svm_train();
        t.run(argv);
//        t.run_ensemble(argv);
    }

    /**
     * 执行训练svm_train的入口
     *
     * @param argv
     * @throws IOException
     */
    public void train(String argv[])
            throws IOException {
        svm.rand.setSeed(0);
        run(argv);
    }


    /**
     * 执行ensemble训练svm_train的入口
     *
     * @param argv
     * @throws IOException
     */
    public void train_ensemble(String argv[])
            throws IOException {
        svm.rand.setSeed(0);
        run_ensemble(argv);
    }

    private static double atof(String s) {
        double d = Double.valueOf(s).doubleValue();
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            System.err.print("NaN or Infinity in input\n");
            System.exit(1);
        }
        return (d);
    }

    private static int atoi(String s) {
        return Integer.parseInt(s);
    }

    private void parse_command_line(String argv[]) {
        int i;
        svm_print_interface print_func = null; // default printing to stdout

        param = new svm_parameter();
        // default values
        param.svm_type = svm_parameter.C_SVC;
        param.kernel_type = svm_parameter.RBF;
        param.degree = 3;
        param.gamma = 0; // 1/num_features
        param.coef0 = 0;
        param.nu = 0.5;
        param.cache_size = 100;
        param.C = 1;
        param.eps = 1e-3;
        param.p = 0.1;
        param.shrinking = 1;
        param.probability = 0;
        param.nr_weight = 0;
        param.weight_label = new int[0];
        param.weight = new double[0];
        cross_validation = 0;

        // parse options
        for (i = 0; i < argv.length; i++) {
            if (argv[i].charAt(0) != '-')
                break;
            if (++i >= argv.length)
                exit_with_help();
            switch (argv[i - 1].charAt(1)) {
                case 's':
                    param.svm_type = atoi(argv[i]);
                    break;
                case 't':
                    param.kernel_type = atoi(argv[i]);
                    break;
                case 'd':
                    param.degree = atoi(argv[i]);
                    break;
                case 'g':
                    param.gamma = atof(argv[i]);
                    break;
                case 'r':
                    param.coef0 = atof(argv[i]);
                    break;
                case 'n':
                    param.nu = atof(argv[i]);
                    break;
                case 'm':
                    param.cache_size = atof(argv[i]);
                    break;
                case 'c':
                    param.C = atof(argv[i]);
                    break;
                case 'e':
                    param.eps = atof(argv[i]);
                    break;
                case 'p':
                    param.p = atof(argv[i]);
                    break;
                case 'h':
                    param.shrinking = atoi(argv[i]);
                    break;
                case 'b':
                    param.probability = atoi(argv[i]);
                    break;
                //新添加参数a，抽样小数据集的最小数据个数
                case 'a':
                    param.minimalDataSize = atoi(argv[i]);
                    break;
                //新添加参数f，抽样小数据集及分类器的个数
                case 'f':
                    param.classifierNumber = atoi(argv[i]);
                    break;
                case 'q':
                    print_func = svm_print_null;
                    i--;//单一参数，需要指针回退
                    break;
            /**
             * 新加的，保存交叉检验预测的分类结果
			 */
                case 'z':
                    this.predictOutputFile = argv[i];
                    break;
                case 'v':
                    cross_validation = 1;
                    nr_fold = atoi(argv[i]);
                    if (nr_fold < 2) {
                        System.err.print("n-fold cross validation: n must >= 2\n");
                        exit_with_help();
                    }
                    break;
                case 'w':
                    ++param.nr_weight;
                {
                    int[] old = param.weight_label;
                    param.weight_label = new int[param.nr_weight];
                    System.arraycopy(old, 0, param.weight_label, 0,
                            param.nr_weight - 1);
                }

                {
                    double[] old = param.weight;
                    param.weight = new double[param.nr_weight];
                    System.arraycopy(old, 0, param.weight, 0,
                            param.nr_weight - 1);
                }

                param.weight_label[param.nr_weight - 1] = atoi(argv[i - 1]
                        .substring(2));
                param.weight[param.nr_weight - 1] = atof(argv[i]);
                break;
                default:
                    System.err.print("Unknown option: " + argv[i - 1] + "\n");
                    exit_with_help();
            }
        }

        svm.svm_set_print_string_function(print_func);

        // determine filenames

        if (i >= argv.length)
            exit_with_help();

        input_file_name = argv[i];

        if (i < argv.length - 1)
            model_file_name = argv[i + 1];
        else {
            int p = argv[i].lastIndexOf('/');
            ++p; // whew...
            model_file_name = argv[i].substring(p) + ".model";
        }
    }

    // read in a problem (in svmlight format)

    private void read_problem() throws IOException {
        BufferedReader fp = new BufferedReader(new FileReader(input_file_name));
        Vector<Double> vy = new Vector<Double>();
        Vector<svm_node[]> vx = new Vector<svm_node[]>();
        int max_index = 0;

        /**
         * read a line from training file and parse
         */
        while (true) {
            String line = fp.readLine();
            if (line == null)
                break;
            //按照" \t\n\r\f:"将一行数据切分为tokens
            StringTokenizer st = new StringTokenizer(line, " \t\n\r\f:");
            // get the class label
            vy.addElement(atof(st.nextToken()));
            // get the length(number of dimensions) of vector
            int m = st.countTokens() / 2;
            svm_node[] x = new svm_node[m];
            // save the vector from file_line into the Vector<svm_node[]>vx
            // 保存特征数据到svm_node[] x
            for (int j = 0; j < m; j++) {
                x[j] = new svm_node();
                x[j].index = atoi(st.nextToken());
                x[j].value = atof(st.nextToken());
            }
            if (m > 0)
                max_index = Math.max(max_index, x[m - 1].index);
            vx.addElement(x);
        }
        /**
         * instantiate a svm_problem and enrich it
         */
        prob = new svm_problem();
        prob.l = vy.size();
        prob.x = new svm_node[prob.l][];
        for (int i = 0; i < prob.l; i++)
            prob.x[i] = vx.elementAt(i);
        prob.y = new double[prob.l];
        for (int i = 0; i < prob.l; i++)
            prob.y[i] = vy.elementAt(i);
        // normalize the gamma
        if (param.gamma == 0 && max_index > 0)
            param.gamma = 1.0 / max_index;
        // read the user-designed kernel
        if (param.kernel_type == svm_parameter.PRECOMPUTED)
            for (int i = 0; i < prob.l; i++) {
                if (prob.x[i][0].index != 0) {
                    System.err
                            .print("Wrong kernel matrix: first column must be 0:sample_serial_number\n");
                    System.exit(1);
                }
                if ((int) prob.x[i][0].value <= 0
                        || (int) prob.x[i][0].value > max_index) {
                    System.err
                            .print("Wrong input format: sample_serial_number out of range\n");
                    System.exit(1);
                }
            }

        fp.close();
    }
}

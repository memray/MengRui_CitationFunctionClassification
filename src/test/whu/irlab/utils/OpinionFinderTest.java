package test.whu.irlab.utils;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import edu.whu.irlab.dfki.feature.FeatureExtractFunctions;
import edu.whu.irlab.dfki.feature.utils.GrammarUtils;
import edu.whu.irlab.dfki.feature.utils.OpinionFinder;
import org.junit.Test;

/**
 * Created by Memray on 2015/3/16.
 */
public class OpinionFinderTest {

    @Test
    public void extractPositiveWords(){
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for(DataPoint dp: runFeatureExtract.dataPoints){
            System.out.println(++count);
            if(count < 11)
                continue;
            FeatureExtractFunctions.initDataPoint(dp);
            //替换引文标记
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            //再进行句法分析
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(dp.getSentence());
            System.out.println(OpinionFinder.extractPositiveWords(dp));
            System.out.println(OpinionFinder.extractNegativeWords(dp));
            System.out.println();
        }
    }
}

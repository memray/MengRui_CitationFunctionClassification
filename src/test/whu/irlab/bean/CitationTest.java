package test.whu.irlab.bean;

import edu.whu.irlab.bean.Citation;
import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.feature.utils.GrammarUtils;
import org.junit.Test;
import test.whu.irlab.TestGlobalUtils;

import java.util.ArrayList;

/**
 * Created by Memray on 2015/4/9.
 */
public class CitationTest {

    @Test
    public void extractCitations() {
        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0;
        for (DataPoint dataPoint : dataPoints) {
            count++;
//            GrammarUtils.getTypedDependencies(dataPoint);
            System.out.println("[" + dataPoint.getLabel2() + "]"+dataPoint.getSentence());
            dataPoint.getFeature().setPreviousSentenceCitationList(Citation.extractCitations(dataPoint.getFormerNeighborSentence()));
            dataPoint.getFeature().setCitationList(Citation.extractCitations(dataPoint.getAclSentence().getContent()));
            dataPoint.getFeature().setFollowingSentenceCitationList(Citation.extractCitations(dataPoint.getNextNeighborSentence()));
            System.out.println("\t\t\t"+dataPoint.getFormerNeighborSentence());
            System.out.println("\t\t" + dataPoint.getAclSentence().getContent());
            System.out.println("\t\t\t" + dataPoint.getNextNeighborSentence());
            System.out.println();
        }
    }
}

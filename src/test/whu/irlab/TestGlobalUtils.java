package test.whu.irlab;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;

import java.util.ArrayList;

/**
 * Created by Memray on 2015/3/30.
 */
public class TestGlobalUtils {
    public static ArrayList<DataPoint> getData(){
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataCache(runFeatureExtract.dataPoints);
        return runFeatureExtract.dataPoints;
    }
}

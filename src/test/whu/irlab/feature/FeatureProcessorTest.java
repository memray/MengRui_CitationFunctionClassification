package test.whu.irlab.feature;

import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureProcessor;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import org.junit.Test;

/**
 * Created by Memray on 2015/3/17.
 */
public class FeatureProcessorTest {
    @Test
    public void createFeatureVectorFile(){
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();

        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        FeatureProcessor.createFeatureFile(runFeatureExtract.dataPoints, runFeatureExtract.FeatureCachePath,
                runFeatureExtract.TargetFeatureName, true);

    }

}

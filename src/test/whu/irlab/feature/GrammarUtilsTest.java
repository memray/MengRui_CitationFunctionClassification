package test.whu.irlab.feature;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeGraph;
import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import edu.whu.irlab.dfki.feature.FeatureExtractFunctions;
import edu.whu.irlab.dfki.feature.utils.GrammarUtils;
import org.junit.Test;
import test.whu.irlab.TestGlobalUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Memary on 2015/3/16.
 */
public class GrammarUtilsTest {

    @Test
    public void extractTense(){
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for(DataPoint dp: runFeatureExtract.dataPoints){
            System.out.println(++count);
            if(count < 6)
                continue;
            FeatureExtractFunctions.initDataPoint(dp);
            //替换引文标记
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            //再进行句法分析
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(dp.getSentence());
            int tense = GrammarUtils.extractTense(dp);
            switch (tense) {
                case 0 : {
                    System.out.println("过去时");
                    break;
                }
                case 1 : {
                    System.out.println("现在时");
                    break;
                }
                case 2 : {
                    System.out.println("未来时");
                    break;
                }
                case 3 : {
                    System.out.println("完成时");
                    break;
                }
                case 4 : {
                    System.out.println("进行时");
                    break;
                }
                case 5 : {
                    System.out.println("识别失败");
                    break;
                }
            }
            System.out.println();
        }
    }


    @Test
    public void findEntitiesNpNodes(){
        LexicalizedParser lexicalizedParser;

        lexicalizedParser = LexicalizedParser
                    .loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");


        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        for(DataPoint dataPoint : dataPoints) {
            List<HasWord> rawWords = Sentence.toWordList(dataPoint.getSentence().trim().split("\\s+"));
            Tree parse = lexicalizedParser.apply(rawWords);

            // 进行句法分析生成句法树,使用TreeGraphNode以方便寻找父节点
            dataPoint.tree = new TreeGraph(parse);
            System.out.println(dataPoint.getSentence());
            ArrayList<String> subjects = GrammarUtils.getLinkedKeyWordsWithEntity(dataPoint.tree,
                    GrammarUtils.EntityType.Author, new String[]{"VB", "JJ", "RB"}, 1, true);
            ArrayList<String> citations = GrammarUtils.getLinkedKeyWordsWithEntity(dataPoint.tree,
                    GrammarUtils.EntityType.Citation, new String[]{"VB", "JJ", "RB"}, 1, true);

            System.out.println(subjects);
            System.out.println(citations);

            System.out.println();
        }
    }

    @Test
    public void getLinkedDependenciesOfCertainEntity() {
        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0;
        for (DataPoint dataPoint : dataPoints) {
            count++;
            GrammarUtils.getTypedDependencies(dataPoint);
            /*if(!GrammarUtils.containsNegation(dataPoint.getSentence())) {
                continue;
            }*/
            System.out.println(count+" : "+dataPoint.getSentence());
            String[] posTags = {"VB", "JJ", "RB"};
            System.out.println(GrammarUtils.getLinkedDependenciesOfCertainEntity(dataPoint, GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization));
            System.out.println(GrammarUtils.getLinkedDependenciesOfCertainEntity(dataPoint, GrammarUtils.EntityType.Citation, posTags, 2, RunFeatureExtract.Lemmatization));

        }
    }

    @Test
    public void findSubject() {
        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0;
        for (DataPoint dataPoint : dataPoints) {
            count++;
            GrammarUtils.getTypedDependencies(dataPoint);
            if(!GrammarUtils.containsNegation(dataPoint.getSentence())) {
                continue;
            }
            System.out.println("[" + dataPoint.getLabel2() + "]"+dataPoint.getSentence());
            System.out.println("\t\t\t\t"+dataPoint.getFormerNeighborSentence());
//            Citation.extractCitations(dataPoint.getFormerNeighborSentence());
            System.out.println("\t\t\t\t" + dataPoint.getAclSentence().getContent());
//            Citation.extractCitations(dataPoint.getAclSentence().getContent());
            System.out.println("\t\t\t\t"+dataPoint.getNextNeighborSentence());
//            Citation.extractCitations(dataPoint.getNextNeighborSentence());

            System.out.println(GrammarUtils.findSubjectPhrase(dataPoint));
            System.out.println();
        }
    }

}

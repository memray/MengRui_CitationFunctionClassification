package test.whu.irlab.feature;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeGraph;
import edu.whu.irlab.bean.Citation;
import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import edu.whu.irlab.dfki.feature.FeatureExtractFunctions;
import edu.whu.irlab.dfki.feature.utils.GrammarUtils;
import org.junit.Test;
import test.whu.irlab.TestGlobalUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Memray on 3/3/2015.
 */
public class FeatureExtractFunctionsTest {
    @Test
    public void extractOurFeatures() {
        LexicalizedParser lexicalizedParser;

        lexicalizedParser = LexicalizedParser
                .loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");


        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0;
        for (DataPoint dataPoint : dataPoints) {
            System.out.println(++count + "  " + dataPoint.getSentence());
            if (count < 3)
                continue;
//            dataPoint.setSentence("Assuming comparison we have access to an \"oracle\" which determines the predominant sense, or most frequent sense (MFS), of each noun in our WSJ test data perfectly, and we assign this most frequent sense to each noun in the test data, we will have achieved an accuracy of 61.1% as shown in the column MFS accuracy of Ta\u00ADble 1. Finally, we note that we have an average of 310 BC training examples and 406 WSJ adaptation examples per noun.");
//            dataPoint.setOriginSentence("Assuming comparison we have access to an \"oracle\" which determines the predominant sense, or most frequent sense (MFS), of each noun in our WSJ test data perfectly, and we assign this most frequent sense to each noun in the test data, we will have achieved an accuracy of 61.1% as shown in the column MFS accuracy of Ta\u00ADble 1. Finally, we note that we have an average of 310 BC training examples and 406 WSJ adaptation examples per noun.");
            List<HasWord> rawWords = Sentence.toWordList(dataPoint.getSentence().trim().split("\\s+"));
            Tree parse = lexicalizedParser.apply(rawWords);
            dataPoint.tree = new TreeGraph(parse);

            GrammarUtils.getTypedDependencies(dataPoint);
            FeatureExtractFunctions.extractAbuJbaraFeatures(dataPoint);
//            FeatureExtractFunctions.extractOurFeatures(dataPoint);
            System.out.println(dataPoint.getFeature().getCitationLinkedKeyAbuWords());
            System.out.println();

        }
    }

    @Test
    public void isSelfCiting() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        int count = 0;
        for (DataPoint dataPoint : runFeatureExtract.dataPoints) {
            System.out.println(count++);
            FeatureExtractFunctions.isSelfCiting(dataPoint);
        }
    }

    @Test
    public void citationTagReplacement() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            System.out.println(++count);
            if (count < 71)
                continue;
            //dp.setSentence("we obtain substantially better results than <citation>");
            System.out.println(dp.getSentence());
            FeatureExtractFunctions.initDataPoint(dp);
            //替换引文标记
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            System.out.println(dp.getSentence());
            System.out.println();
        }
    }

    @Test
    public void containsWord() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();

        int count = 0;

        for (DataPoint dataPoint : runFeatureExtract.dataPoints) {
            //System.out.println(count++);
            if (FeatureExtractFunctions.containsSpecificWord(dataPoint.getSentence(), "\\bbut\\b")) {
                System.out.println(++count + "\t" + dataPoint.getSentence());
                System.out.println();
            }
        }
    }

    @Test
    public void findRootWord() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            FeatureExtractFunctions.initDataPoint(dp);
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(++count + "\t" + GrammarUtils.findRootWord(dp, RunFeatureExtract.Lemmatization));
        }
    }

    @Test
    public void getDependencyRelationsCount() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            FeatureExtractFunctions.initDataPoint(dp);
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            System.out.println(dp.getSentence());
            System.out.println();
            /*GrammarUtils.getTypedDependencies(dp);
            GrammarUtils.getDependencyRelations(dp, true);*/
        }
    }

    @Test
    public void findMainVerb() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            System.out.println(++count);
            FeatureExtractFunctions.initDataPoint(dp);
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(GrammarUtils.findMainVerb(dp, RunFeatureExtract.Lemmatization));
        }
    }


    @Test
    public void hasOtherContrast() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            System.out.println(++count);
            if (count < 284)
                continue;
            FeatureExtractFunctions.initDataPoint(dp);
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(GrammarUtils.hasOtherContrast(dp));
        }
    }

    @Test
    public void hasSelfComp() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            System.out.println(++count);
            if (count < 64)
                continue;
//            dp.setSentence("we obtain substantially better results than <citation>");
//            dp.setSentence("In this paper we report on a project to extract better biomolecular data from biomedical text.");
            FeatureExtractFunctions.initDataPoint(dp);
            //替换引文标记
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            //再进行句法分析
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(dp.getSentence());
            System.out.println(GrammarUtils.hasSelfComp(dp));
            System.out.println();
        }
    }

    @Test
    public void hasOtherComp() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            System.out.println(++count);
//
            if (count < 279)
                continue;
            FeatureExtractFunctions.initDataPoint(dp);
            //替换引文标记
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            //再进行句法分析
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(dp.getSentence());
            System.out.println(GrammarUtils.hasOtherComp(dp));
            System.out.println();
        }
    }

    @Test
    public void hasSelfGood() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        int count = 0;

        for (DataPoint dp : runFeatureExtract.dataPoints) {
            System.out.println(++count);
//            dp.setSentence("This study show that combining good features derived from the source and target dependency trees , distortion surface order-based features (like the distortion used in Pharaoh < >) and language model-like features results in a model which significantly outperforms models using only some of the information sources");
            FeatureExtractFunctions.initDataPoint(dp);
            //替换引文标记
            dp.setSentence(FeatureExtractFunctions.citationTagReplacement(dp.getSentence()));
            //再进行句法分析
            GrammarUtils.getTypedDependencies(dp);
            System.out.println(dp.getSentence());
            System.out.println(GrammarUtils.hasSelfGood(dp));
            System.out.println();
        }
    }

    @Test
    public void isContextRelated() {
        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0, relatedCount = 0;
        for (DataPoint dataPoint : dataPoints) {
            count++;
            System.out.println("[" + count + "][" + dataPoint.getLabel2() + "]["+dataPoint.getAclSentence().getAclid()+ "]["+dataPoint.getAclSentence().getId()+"]" + dataPoint.getSentence());

            GrammarUtils.getTypedDependencies(dataPoint);
            dataPoint.getFeature().setPreviousSentenceCitationList(Citation.extractCitations(dataPoint.getFormerNeighborSentence()));
            dataPoint.getFeature().setCitationList(Citation.extractCitations(dataPoint.getAclSentence().getContent()));
            dataPoint.getFeature().setFollowingSentenceCitationList(Citation.extractCitations(dataPoint.getNextNeighborSentence()));

            /*System.out.println(dataPoint.getFeature().getPreviousSentenceCitationList());
            System.out.println(dataPoint.getFeature().getCitationList());
            System.out.println(dataPoint.getFeature().getFollowingSentenceCitationList());*/


//            System.out.println(GrammarUtils.findSubjectPhrase(dataPoint));

            if(FeatureExtractFunctions.isCurrentContextRelated(dataPoint.getSentence(), dataPoint.getFeature().getPreviousSentenceCitationList())){
//            if(FeatureExtractFunctions.isContextRelated(dataPoint.getFormerNeighborSentence(), dataPoint.getFeature().getPreviousSentenceCitationList())){
                ArrayList<String> subjects = FeatureExtractFunctions.wordExtract(dataPoint.getFormerNeighborSentence(), FeatureExtractFunctions.ourSubjectFilter);
                if (subjects.size() > 0)
                    relatedCount++;
                System.out.println("\t\t\t\t"+dataPoint.getFormerNeighborSentence());
                System.out.println("\t\t\t\t" + dataPoint.getAclSentence().getContent());
                System.out.println("\t\t\t\t"+dataPoint.getNextNeighborSentence());
                String[] posTags = {"VB", "JJ", "RB"};
                System.out.println((GrammarUtils.getLinkedKeyWordsOfCertainEntity(dataPoint.getTaggedWords(), dataPoint.getTypedDependencies(), GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization)));
                System.out.println((GrammarUtils.getLinkedKeyWordsOfCertainEntity(dataPoint.getTaggedWords(), dataPoint.getTypedDependencies(), GrammarUtils.EntityType.Citation, posTags, 2, RunFeatureExtract.Lemmatization)));

                System.out.println("Match!");
                System.out.println();
            }else{
                System.out.println("\t\t\t\t"+dataPoint.getFormerNeighborSentence());
                System.out.println("\t\t\t\t" + dataPoint.getAclSentence().getContent());
                System.out.println("\t\t\t\t"+dataPoint.getNextNeighborSentence());
                String[] posTags = {"VB", "JJ", "RB"};
                System.out.println((GrammarUtils.getLinkedKeyWordsOfCertainEntity(dataPoint.getTaggedWords(), dataPoint.getTypedDependencies(), GrammarUtils.EntityType.Author, posTags, 2, RunFeatureExtract.Lemmatization)));
                System.out.println((GrammarUtils.getLinkedKeyWordsOfCertainEntity(dataPoint.getTaggedWords(), dataPoint.getTypedDependencies(), GrammarUtils.EntityType.Citation, posTags, 2, RunFeatureExtract.Lemmatization)));
            }
            System.out.println();
        }
        System.out.println(relatedCount);
    }

    @Test
    public void checkDBContent() {
        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0;

        String head = "";

        for (DataPoint dataPoint : dataPoints) {
            count++;
            if(count<890)
                continue;
            System.out.println("[" + count + "][" + dataPoint.getLabel2() + "][" + dataPoint.getAclSentence().getAclid() + "][" + dataPoint.getAclSentence().getId() + "]" + dataPoint.getSentence());
            System.out.println("\t\t\t\t" + dataPoint.getFormerNeighborSentence());
            System.out.println("\t\t\t\t" + dataPoint.getAclSentence().getContent());
            System.out.println("\t\t\t\t" + dataPoint.getNextNeighborSentence());
            head = dataPoint.getSentence().substring(0, 20);
            if (!dataPoint.getAclSentence().getContent().startsWith(head))
                System.out.println();
        }

    }

    @Test
    public void extractExtraOurFeatures() {
        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0;

        for (DataPoint dataPoint : dataPoints) {
            count++;
            if(count<30)
                continue;

            System.out.println("[" + count + "][" + dataPoint.getLabel2() + "][" + dataPoint.getAclSentence().getAclid() + "][" + dataPoint.getAclSentence().getId() + "]" + dataPoint.getSentence());
            System.out.println("\t\t\t\t" + dataPoint.getFormerNeighborSentence());
            System.out.println("\t\t\t\t" + dataPoint.getAclSentence().getContent());
            System.out.println("\t\t\t\t" + dataPoint.getNextNeighborSentence());

            GrammarUtils.getTypedDependencies(dataPoint);
            FeatureExtractFunctions.extractExtraOurFeatures(dataPoint);

        }
    }

    @Test
    public void extractAbuJbaraFeatures() {
        ArrayList<DataPoint> dataPoints = TestGlobalUtils.getData();

        int count = 0;

        for (DataPoint dataPoint : dataPoints) {
            count++;
            if(count < 4)
                continue;

            System.out.println("[" + count + "][" + dataPoint.getLabel2() + "][" + dataPoint.getAclSentence().getAclid() + "][" + dataPoint.getAclSentence().getId() + "]" + dataPoint.getSentence());
            System.out.println("\t\t\t\t" + dataPoint.getFormerNeighborSentence());
            System.out.println("\t\t\t\t" + dataPoint.getAclSentence().getContent());
            System.out.println("\t\t\t\t" + dataPoint.getNextNeighborSentence());

            GrammarUtils.getTypedDependencies(dataPoint);
            FeatureExtractFunctions.extractAbuJbaraFeatures(dataPoint);
            if(dataPoint.getFeature().getNegationCueBoolean()){
                continue;
            }

        }
    }
}

package test.whu.irlab.feature;

import edu.whu.irlab.bean.DataPoint;
import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import edu.whu.irlab.dfki.feature.FeatureExtractFunctions;
import edu.whu.irlab.dfki.feature.FeatureExtractor;
import org.junit.Test;
import test.whu.irlab.TestGlobalUtils;

import java.util.ArrayList;

/**
 * Created by Memray on 3/4/2015.
 */
public class FeatureExtractorTest {

    @Test
    public void physicalFeatureExtract() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();
        int count = 0;
        for(DataPoint dataPoint: runFeatureExtract.dataPoints){
            //System.out.println(count++);
            FeatureExtractor.physicalFeatureExtract(dataPoint);
        }
    }

    @Test
    public void featureExtract(){
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();

        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataCache(runFeatureExtract.dataPoints);
        FeatureCacheUtils.extractFeatures(runFeatureExtract.dataPoints, runFeatureExtract.FeatureCachePath, 1641, true);
    }

}

package test.whu.irlab.feature;

import edu.whu.irlab.dfki.RunFeatureExtract;
import edu.whu.irlab.dfki.feature.FeatureCacheUtils;
import org.junit.Test;

/**
 * Created by Memray on 2015/3/17.
 */
public class FeatureCacheUtilsTest {

    @Test
    public void generateFeatureCacheData() {
        RunFeatureExtract runFeatureExtract = new RunFeatureExtract();
        runFeatureExtract.init();

        runFeatureExtract.dataPoints = FeatureCacheUtils.readDataPoints();
        FeatureCacheUtils.extractFeatures(runFeatureExtract.dataPoints, runFeatureExtract.FeatureCachePath, 1642, true);


    }
}
